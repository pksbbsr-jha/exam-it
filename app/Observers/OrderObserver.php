<?php

namespace App\Observers;

use App\Models\CourseChapter;
use App\Models\CourseModule;
use App\Models\CourseSubmodule;

class OrderObserver
{
    public function creating(CourseChapter|CourseModule|CourseSubmodule $model)
    {
        if (is_a($model, CourseChapter::class)) {
            $model->order = $model->course->chapters()->count();
        } else if (is_a($model, CourseModule::class)) {
            $model->order = $model->chapter->modules()->count();
        } else if (is_a($model, CourseSubmodule::class)) {
            $model->order = $model->module->submodules()->count();
        }
    }
}
