<?php

namespace App\Http\Resources;

use App\Models\Discussion;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Discussion
 * */
class DiscussionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,

            'user' => $this->whenLoaded('user'),
            'submodule' => $this->whenLoaded('submodule'),
            'group' => $this->whenLoaded('group'),
            'messages_count' => $this->when(isset($this->messages_count), $this->messages_count),
        ];
    }
}
