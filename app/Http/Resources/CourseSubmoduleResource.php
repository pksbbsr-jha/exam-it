<?php

namespace App\Http\Resources;

use App\Models\CourseSubmodule;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin CourseSubmodule
 * */
class CourseSubmoduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'module' => CourseModuleResource::make($this->whenLoaded('module')),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
