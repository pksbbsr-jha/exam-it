<?php

namespace App\Http\Resources;

use App\Models\CourseModule;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin CourseModule
 * */
class CourseModuleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'chapter' => CourseChapterResource::make($this->whenLoaded('chapter')),
            'submodules' => CourseSubmoduleResource::make($this->whenLoaded('submodules')),
            'is_free' => $this->is_free,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
