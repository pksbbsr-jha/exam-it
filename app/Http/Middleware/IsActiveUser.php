<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class IsActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
//        abort(403, 'There is evidence to suggest that you have infringed on terms and conditions that you have agreed to.');

        /** @var User|null $user */
        $user = $request->user();

        if(!$user) {
            abort(403);
        }

        if(! is_null($user->blocked_at)) {
            abort(403, 'There is evidence to suggest that you have infringed on terms and conditions that you have agreed to.');
        }


        return $next($request);
    }
}
