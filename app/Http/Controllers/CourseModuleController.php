<?php /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */

namespace App\Http\Controllers;

use App\Dtos\ToastDto;
use App\Dtos\ToastLevelEnum;
use App\Models\Course;
use App\Models\CourseChapter;
use App\Models\CourseModule;
use Illuminate\Http\Request;

class CourseModuleController extends Controller
{
    public function redirectToCourses()
    {
        return redirect(route('admin.courses'));
    }

    public function redirectToChapter(CourseModule $module)
    {
        return redirect(route('admin.chapters.show', $module->chapter_id));
    }

    public function store(Request $request)
    {
        $moduleArr = $this->moduleFormToCourseModuleData($request);

        $chapter = CourseChapter::find($request->input('parent_id'));

        $chapter->modules()->save(CourseModule::make($moduleArr));

        return redirect()->route('admin.courses.show', $chapter->course)
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Module was successfully created!'
                )
            ]);
    }

    public function update(Request $request, CourseModule $module)
    {
        $module->fill($this->moduleFormToCourseModuleData($request));

        $module->save();

        return redirect()->route('admin.courses.show', $module->chapter->course)
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Chapter was successfully updated.'
                )
            ]);
    }

    public function reorder(Request $request, CourseModule $module)
    {
        $request->validate([
            'direction' => 'string|in:up,down'
        ]);

        $oldOrder = $module->order;
        $newOrder = $request->input('direction') === 'up' ? $oldOrder - 1 : $oldOrder + 1;

        /** @var CourseModule $substituteModule */
        $substituteModule = $module->chapter->modules()->where('order', $newOrder)->first();

        if (!$substituteModule || $newOrder < 0) {
            return redirect()->back()
                ->with([
                    'toast' => new ToastDto(
                        level: ToastLevelEnum::error,
                        title: 'Could not change order.'
                    ),

                ]);
        }

        $module->order = $newOrder;
        $module->save();

        $substituteModule->order = $oldOrder;
        $substituteModule->save();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Modules ordering updated successfully.'
                ),
            ]);
    }

    public function togglePaidOrFree(Request $request, CourseModule $module)
    {
//        dd('here');
        $request->validate([
            'is_free' => ['required', 'boolean']
        ]);

//        $module->is_free = $request->boolean('is_free');
        $module->is_free = !$module->is_free;

        $module->save();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Module was marked as ' . ($request->boolean('is_free') ? 'FREE' : 'PAID')
                ),
            ]);
    }

    public function destroy(CourseModule $module)
    {
        $module->delete();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'module was successfully deleted.'
                ),
            ]);
    }

    public function moduleFormToCourseModuleData(Request $request): array
    {
        $request->validate([
            'name' => 'required',
            'parent_id' => 'required|exists:courses_chapters,id'
        ]);

        return [
            'name' => $request->input('name'),
        ];
    }
}
