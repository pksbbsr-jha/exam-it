<?php /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */

namespace App\Http\Controllers;

use App\Dtos\ToastDto;
use App\Dtos\ToastLevelEnum;
use App\Models\DiscussionMessage;
use App\Models\DiscussionMessageRating;
use Illuminate\Http\Request;

class DiscussionMessageController extends Controller
{
    public function rate(Request $request, DiscussionMessage $discussionMessage)
    {
        $request->validate([
            'positive' => 'boolean|nullable',
        ]);

        $user = $request->user();

        $currentRating = $discussionMessage->ratings()
            ->where('user_id', $user->id)
            ->first();

        /** @var bool|null $isPositive */
        $isPositive = $request->boolean('positive');

        if($currentRating && $request->input('positive') === null) {
            $currentRating->delete();

            return redirect()->back()
                ->with([
                    'toast' => new ToastDto(
                        level: ToastLevelEnum::success,
                        title: 'Rating was removed!'
                    )
                ]);
        }

        if (!$currentRating) {
            $currentRating = new DiscussionMessageRating();
            $currentRating->user()->associate($user);
            $currentRating->discussionMessage()->associate($discussionMessage);
        }

        $currentRating->positive = $isPositive;
        $currentRating->save();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Message was successfully rated!'
                )
            ]);
    }

    public function delete(DiscussionMessage $discussionMessage)
    {
        $discussionMessage->delete();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Message was successfully deleted!'
                )
            ]);

    }
}
