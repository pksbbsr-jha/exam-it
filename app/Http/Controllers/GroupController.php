<?php /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */

namespace App\Http\Controllers;

use App\Dtos\ToastDto;
use App\Dtos\ToastLevelEnum;
use App\Http\Resources\GroupResource;
use App\Models\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function index()
    {
        return inertia('Groups/Index', [
            'groups' => GroupResource::collection(
                Group::with(['owner'])->withCount('members')
                    ->orderByDesc('id')->paginate(10, '*', 'page')
            )
        ]);
    }



//    public function store(Request $request)
//    {
//        $course = Course::create($this->courseFormToCourseData($request));
//
//        return redirect()->route('admin.courses.show', $course)
//            ->with([
//                'toast' => new ToastDto(
//                    level: ToastLevelEnum::success,
//                    title: 'Course was successfully created!'
//                )
//            ]);
//    }

    public function show(Group $group)
    {
        return inertia('Groups/Show', [
            'group' => $group->load(['owner']),
            'members' => $group->members()
                ->orderBy('created_at')
                ->get(),
            'invitations' => $group->invitations()
                ->orderBy('created_at')
                ->get()
        ]);
    }

//    public function edit(User $user)
//    {
//        return inertia('Courses/Edit', [
//            'course' => $course
//        ]);
//    }

//    public function update(Request $request, User $user)
//    {
//
//        $course->fill($this->courseFormToCourseData($request));
//
//        $course->save();
//
//        return redirect()->route('admin.courses.show', $course)
//            ->with([
//                'toast' => new ToastDto(
//                    level: ToastLevelEnum::success,
//                    title: 'Course was successfully updated.'
//                )
//            ]);
//    }

//    public function destroy(User $user)
//    {
//        $course->delete();
//    }
//
//    public function courseFormToCourseData(Request $request): array
//    {
//        $request->validate([
//            'name' => 'required',
////            'description' => 'required',
//            'price' => 'required|numeric',
//            'meta' => 'required|array',
//            'meta.includes' => 'required|array',
//            'meta.lessons' => 'required|array',
//            'meta.requirements' => 'required|array',
//        ]);
//
//        return [
//            'name' => $request->input('name'),
//            'description' => $request->input('description'),
//            'price' => $request->input('price'),
//            'meta' => [
//                'includes' => $request->input('meta.includes'),
//                'lessons' => $request->input('meta.lessons'),
//                'requirements' => $request->input('meta.requirements'),
//            ]
//        ];
//    }

    public function destroy(Request $request, Group $group)
    {
        $group->delete();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Group was successfully deleted!'
                )
            ]);
    }
}
