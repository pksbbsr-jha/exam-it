<?php /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */

namespace App\Http\Controllers;

use App\Dtos\ToastDto;
use App\Dtos\ToastLevelEnum;
use App\Http\Resources\CourseResource;
use App\Models\Course;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index()
    {
        return inertia('Courses/Index', [
            'courses' => CourseResource::collection(Course::orderByDesc('created_at')->paginate(10, '*', 'page'))
        ]);
    }

    public function create()
    {
        return inertia('Courses/Create');
    }

    public function store(Request $request)
    {
        $course = Course::create($this->courseFormToCourseData($request));

        return redirect()->route('admin.courses.show', $course)
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Course was successfully created!'
                )
            ]);
    }

    public function show(Course $course)
    {
        return inertia('Courses/Show', [
            'course' => $course,
            'chapters' => $course->chapters()
                ->orderBy('order')
                ->with([
                    'modules' => function (HasMany $query) {
                        $query->orderBy('order')
                            ->with([
                                'submodules' => function (HasMany $submodulesQuery) {
                                    $submodulesQuery->orderBy('order')
                                        ->withCount('questions');
                                }]);
                    },
                ])
                ->get()
        ]);
    }

    public function edit(Course $course)
    {
        return inertia('Courses/Edit', [
            'course' => $course
        ]);
    }

    public function update(Request $request, Course $course)
    {

        $course->fill($this->courseFormToCourseData($request));

        $course->save();

        return redirect()->route('admin.courses.show', $course)
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Course was successfully updated.'
                )
            ]);
    }

    public function destroy(Course $course)
    {
        $course->delete();
    }

    public function courseFormToCourseData(Request $request): array
    {
        $request->validate([
            'name' => 'required',
//            'description' => 'required',
            'price' => 'required|numeric',
            'meta' => 'required|array',
            'meta.includes' => 'required|array',
            'meta.lessons' => 'required|array',
            'meta.requirements' => 'required|array',
        ]);

        return [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
            'meta' => [
                'includes' => $request->input('meta.includes'),
                'lessons' => $request->input('meta.lessons'),
                'requirements' => $request->input('meta.requirements'),
            ]
        ];
    }


}
