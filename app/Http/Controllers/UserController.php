<?php /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */

namespace App\Http\Controllers;

use App\Dtos\ToastDto;
use App\Dtos\ToastLevelEnum;
use App\Http\Resources\UserResource;
use App\Mail\UserInvitation;
use App\Models\Enums\UserRole;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\Enum;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $query = User::orderByDesc('id');

        if($request->input('name')) {
            $query->where('name', 'ilike', '%' . $request->input('name') . '%');
        }

        if($request->input('username')) {
            $query->where('username', 'ilike', '%' . $request->input('username') . '%');
        }

        if($request->input('email')) {
            $query->where('email', 'ilike', '%' . $request->input('email') . '%');
        }

        return inertia('Users/Index', [
            'users' => UserResource::collection($query->paginate(10, '*', 'page'))
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'username' => ['required', 'unique:users,username'],
            'password' => ['required', 'min:3'],
            'password_confirmation' => ['required', 'same:password'],
            'email' => ['required', 'email', 'unique:users,email'],
            'role' => ['required', new Enum(UserRole::class)],
        ]);

        $user = new User($request->only(['name', 'username', 'email', 'role']));
        $user->password = bcrypt($request->input('password'));
        $user->save();

        /**
         * Will use general for now
         */
//        \Mail::to($user)
//            ->queue(new UserInvitation($request->user(), $user));

        event(new Registered($user));

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'User was successfully added! User will receive email invitation.'
                ),
            ]);
    }

    public function blockAdd(Request $request, User $user)
    {
        $user->blocked_at = now();
        $user->save();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'User was successfully blocked!'
                )
            ]);
    }

    public function blockRemove(Request $request, User $user)
    {
        $user->blocked_at = null;
        $user->save();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'User was successfully unblocked!'
                )
            ]);
    }

    public function adminAdd(Request $request, User $user)
    {
        $user->role = UserRole::ADMIN;
        $user->blocked_at = null;
        $user->save();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'User was successfully added as an admin!'
                )
            ]);
    }

    public function adminRemove(Request $request, User $user)
    {
        $user->role = UserRole::STUDENT;
        $user->save();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'User was successfully removed as an admin!'
                )
            ]);
    }


//
//    public function store(Request $request)
//    {
//        $course = Course::create($this->courseFormToCourseData($request));
//
//        return redirect()->route('admin.courses.show', $course)
//            ->with([
//                'toast' => new ToastDto(
//                    level: ToastLevelEnum::success,
//                    title: 'Course was successfully created!'
//                )
//            ]);
//    }

//    public function show(User $user)
//    {
//        return inertia('Courses/Show', [
//            'course' => $course,
//            'chapters' => $course->chapters()
//                ->orderBy('order')
//                ->with([
//                    'modules' => function (HasMany $query) {
//                        $query->orderBy('order')
//                            ->with([
//                                'submodules' => function (HasMany $submodulesQuery) {
//                                    $submodulesQuery->orderBy('order');
//                                }]);
//                    },
//                ])
//                ->get()
//        ]);
//    }

//    public function edit(User $user)
//    {
//        return inertia('Courses/Edit', [
//            'course' => $course
//        ]);
//    }

//    public function update(Request $request, User $user)
//    {
//
//        $course->fill($this->courseFormToCourseData($request));
//
//        $course->save();
//
//        return redirect()->route('admin.courses.show', $course)
//            ->with([
//                'toast' => new ToastDto(
//                    level: ToastLevelEnum::success,
//                    title: 'Course was successfully updated.'
//                )
//            ]);
//    }

//    public function destroy(User $user)
//    {
//        $course->delete();
//    }
//
//    public function courseFormToCourseData(Request $request): array
//    {
//        $request->validate([
//            'name' => 'required',
////            'description' => 'required',
//            'price' => 'required|numeric',
//            'meta' => 'required|array',
//            'meta.includes' => 'required|array',
//            'meta.lessons' => 'required|array',
//            'meta.requirements' => 'required|array',
//        ]);
//
//        return [
//            'name' => $request->input('name'),
//            'description' => $request->input('description'),
//            'price' => $request->input('price'),
//            'meta' => [
//                'includes' => $request->input('meta.includes'),
//                'lessons' => $request->input('meta.lessons'),
//                'requirements' => $request->input('meta.requirements'),
//            ]
//        ];
//    }
}
