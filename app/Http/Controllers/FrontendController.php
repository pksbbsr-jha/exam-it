<?php /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\View;

class FrontendController extends Controller
{
    protected $layout;

    public function index()
    {
        return inertia('Homepage/Index', [
            'courses' => [],
            'discussions' => [],
            'chats' => [],
            'forum' => [],
            'groups' => []
        ]);
    }

    public function dashboard()
    {
        return inertia('Dashboard/Index', [
            'courses' => [],
            'discussions' => [],
            'chats' => [],
            'forum' => [],
            'groups' => []
        ]);
    }

    public function courses()
    {
        return inertia('Courses/Index', [
            'courses' => [],
            'discussions' => [],
            'chats' => [],
            'forum' => [],
            'groups' => []
        ]);
    }

    public function courseDetail($id)
    {  
        return inertia('Courses/CourseDetail', [
            'courseDetails' => [
            ],
        ]);
    }

    public function signup()
    {  
        $this->layout = View::make("layouts.registration");
        
        //return View::render('Signup/Index');
        //return view('Signup/Index');
        //return inertia('Signup/Index');

        $this->layout = View::make('layouts.registration');
        $this->layout->content = View::make('layouts.registration');
        return inertia('Signup/Index');

    }
    public function discussion()
    {
        return inertia('MessageBoard/Index', [
            'discussions' => [],
        ]);
    }
    public function forum()
    {
        return inertia('MessageBoard/Forum', [
            'forum' => [],
        ]);
    }

    public function chat()
    {
        return inertia('MessageBoard/Chat', [
            'chat' => [],
        ]);
    }
    public function courseList(){
        return inertia('Courses/CourseList',[
            'courseList' => [],
        ]);
    }
    public function publicProfile(){
        return inertia('PublicProfile/Index', [
            'index'=> [],
        ]);
    }
    public function accountSettings(){
        return inertia('AccountSettings/Index',[
            'index'=> [],
        ]);
    }
    public function discussionList(){
        return inertia('Discussion/Index',[
            'index' =>[],
        ]);
    }
    public function explanation(){
        return inertia('Discussion/Explanation',[
            'explanation' => [],
        ]);
    }
    public function coursePage(){
        return inertia('Courses/CoursePage',[
            'coursePage' => [],
        ]);
    }
    public function discussionGroup(){
        return inertia('Discussion/DiscussionGroup',[
            'discussionGroup' => [],
        ]);
    }
    public function quiz(){
        return inertia('Discussion/DiscussionQuiz',[
            'discussionQuiz' => [],
        ]);
    }
    public function discussionDetails(){
        return inertia('Discussion/DiscussionDetails',[
            'discussionDetails' => [],
        ]);
    }
    public function coursePurchased(){
        return inertia('Courses/CoursePurchased',[
            'coursePurchased' => [],
        ]);
    }
}
