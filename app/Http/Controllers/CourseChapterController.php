<?php /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */

namespace App\Http\Controllers;

use App\Dtos\ToastDto;
use App\Dtos\ToastLevelEnum;
use App\Models\Course;
use App\Models\CourseChapter;
use Illuminate\Http\Request;

class CourseChapterController extends Controller
{

    public function redirectToCourses()
    {
        return redirect(route('admin.courses'));
    }

    public function redirectToCourse(CourseChapter $chapter)
    {
        return redirect(route('admin.courses.show', $chapter->course_id));
    }

    public function store(Request $request)
    {
        $course = Course::find($request->input('parent_id'));

        $course->chapters()->save(CourseChapter::make($this->chapterFormToCourseChapterData($request)));

        return redirect()->route('admin.courses.show', $course)
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Chapter was successfully created!'
                )
            ]);
    }

    public function update(Request $request, CourseChapter $chapter)
    {
        $chapter->fill($this->chapterFormToCourseChapterData($request));

        $chapter->save();

        return redirect()->route('admin.courses.show', $chapter->course)
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Chapter was successfully updated.'
                )
            ]);
    }

    public function reorder(Request $request, CourseChapter $chapter)
    {
        $request->validate([
            'direction' => 'string|in:up,down'
        ]);

        $oldOrder = $chapter->order;
        $newOrder = $request->input('direction') === 'up' ? $oldOrder - 1 : $oldOrder + 1;

        $substituteChapter = $chapter->course->chapters()->where('order', $newOrder)->first();

        if (!$substituteChapter || $newOrder < 0) {
            return redirect()->back()
                ->with([
                    'toast' => new ToastDto(
                        level: ToastLevelEnum::error,
                        title: 'Could not change order.'
                    ),

                ]);
        }

        $chapter->order = $newOrder;
        $chapter->save();

        $substituteChapter->order = $oldOrder;
        $substituteChapter->save();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Chapters ordering updated successfully.'
                ),
            ]);
    }

    public function destroy(CourseChapter $chapter)
    {
        $chapter->delete();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Chapter was successfully deleted.'
                ),
            ]);
    }

    public function chapterFormToCourseChapterData(Request $request): array
    {
        $request->validate([
            'name' => 'required',
            'parent_id' => 'required|exists:courses,id'
        ]);

        return [
            'name' => $request->input('name'),
        ];
    }
}
