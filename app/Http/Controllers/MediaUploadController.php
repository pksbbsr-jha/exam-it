<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Validator;
use Spatie\ImageOptimizer\Image;

class MediaUploadController extends Controller
{
    /**
     * TODO: Improve readability of this code (use class for file info)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function upload(Request $request)
    {
        $types = ['jpg', 'jpeg', 'png', 'svg', 'gif'];

        $validator = Validator::make($request->all(), [
            'file' => ['required', 'image', 'mimes:' . implode(',', $types), 'max:1024'] // todo: increase
        ], [
            'mimes' => 'Provided file type is wrong. Supported types are: ' . implode(', ', $types),
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        $file = $request->file('file');

        $destinationPath = $this->getDestinationPath($file);

        $img = \Intervention\Image\Facades\Image::make($file->path());

        if($img->getWidth() > 1200) {
            $img->resize(1200, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        // TODO: Handle duplicates
        $img->save($destinationPath);

        // file validation on resizing

        return response()->json([
            'location' => url('storage/' . str_replace(storage_path('app/public/'), '', $destinationPath)),
        ]);
    }

    protected function getDestinationPath(UploadedFile $file)
    {
        $filenameWithExtension = $file->getClientOriginalName();

        $filename = pathinfo($filenameWithExtension, PATHINFO_FILENAME);
        $extension = pathinfo($filenameWithExtension, PATHINFO_EXTENSION);

        $destinationPath = storage_path('app/public/images/' . $filename . '.' . $extension);

        if(!file_exists($destinationPath)) {
            return $destinationPath;
        }

        return storage_path('app/public/images/' . $filename . time() . '.' . $extension);
    }
}
