<?php /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */

namespace App\Http\Controllers;

use App\Dtos\ToastDto;
use App\Dtos\ToastLevelEnum;
use App\Models\Course;
use App\Models\CourseModule;
use App\Models\CourseSubmodule;
use Illuminate\Http\Request;

class CourseSubmoduleController extends Controller
{
    public function redirectToCourses()
    {
        return redirect(route('admin.courses'));
    }

    public function redirectToModule(CourseSubmodule $submodule)
    {
        return redirect(route('admin.modules.show', $submodule->module_id));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'parent_id' => 'required|exists:courses_modules,id'
        ]);

        $module = CourseModule::find($request->input('parent_id'));

        $module->submodules()->save(CourseSubmodule::make([...$request->only(['name']), 'content' => '<p>Submodule content</p>']));

        return redirect()->route('admin.courses.show', $module->chapter->course)
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Submodule was successfully created!'
                )
            ]);
    }

    public function show(CourseSubmodule $submodule)
    {
        return inertia('Submodules/Show', [
            'submodule' => $submodule,
            'questions' => $submodule->questions()
                ->orderByDesc('created_at')
                ->get()
        ]);
    }

    public function edit(CourseSubmodule $submodule)
    {
        return inertia('Submodules/Edit', [
            'submodule' => $submodule
        ]);
    }

    public function updateContent(Request $request, CourseSubmodule $submodule)
    {
        $request->validate([
            'name' => 'required',
            'content' => 'required',
        ]);

        $submodule->fill($request->only(['name', 'content']));

        $submodule->save();

        return redirect()->route('admin.submodules.show', $submodule->id)
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Submodule was successfully updated.'
                )
            ]);
    }

    public function update(Request $request, CourseSubmodule $submodule)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $submodule->fill($request->only(['name']));

        $submodule->save();

        return redirect()->route('admin.courses.show', $submodule->module->chapter->course)
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Submodule was successfully updated.'
                )
            ]);

    }

    public function reorder(Request $request, CourseSubmodule $submodule)
    {
        $request->validate([
            'direction' => 'string|in:up,down'
        ]);

        $oldOrder = $submodule->order;
        $newOrder = $request->input('direction') === 'up' ? $oldOrder - 1 : $oldOrder + 1;

        /** @var CourseSubmodule $substituteSubmodule */
        $substituteSubmodule = $submodule->module->submodules()->where('order', $newOrder)->first();

        if (!$substituteSubmodule || $newOrder < 0) {
            return redirect()->back()
                ->with([
                    'toast' => new ToastDto(
                        level: ToastLevelEnum::error,
                        title: 'Could not change order.'
                    ),

                ]);
        }

        $submodule->order = $newOrder;
        $submodule->save();

        $substituteSubmodule->order = $oldOrder;
        $substituteSubmodule->save();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Submodules ordering updated successfully.'
                ),
            ]);
    }

    public function destroy(CourseSubmodule $submodule)
    {
        $submodule->delete();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'module was successfully deleted.'
                ),
            ]);
    }
}
