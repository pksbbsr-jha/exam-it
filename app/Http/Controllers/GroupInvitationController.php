<?php /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */

namespace App\Http\Controllers;

use App\Dtos\ToastDto;
use App\Dtos\ToastLevelEnum;
use App\Models\Group;
use App\Models\GroupInvitation;
use App\Models\User;
use Illuminate\Http\Request;

class GroupInvitationController extends Controller
{
    public function add(Request $request, Group $group)
    {
        $request->validate([
            'email_or_username' => 'required',
        ]);

        $emailOrUsername = $request->input('email_or_username');

        /** @var false|string $email */
        $email = filter_var($emailOrUsername, FILTER_VALIDATE_EMAIL);

        /** @var null|User $user */
        $user = User::where($email ? 'email' : 'username', $emailOrUsername)->first();

        if (!$user) {
            if (!$email) {
                return redirect()->back()
                    ->with([
                        'toast' => new ToastDto(
                            level: ToastLevelEnum::error,
                            title: 'We could not find a student with provided username.'
                        ),
                    ]);
            }


            // Invite external
            $groupInvitation = GroupInvitation::where('email', $email)
                ->where('group_id', $group->id)
                ->first();

            if ($groupInvitation) {
                return redirect()->back()
                    ->with([
                        'toast' => new ToastDto(
                            level: ToastLevelEnum::warning,
                            title: 'Student with this email has already been invited to the group.'
                        ),
                    ]);
            }

            $groupInvitation = new GroupInvitation();
            $groupInvitation->group()->associate($group);
            $groupInvitation->email = $email;
            $groupInvitation->save();

            // TODO: Send invitation email

            return redirect()->back()
                ->with([
                    'toast' => new ToastDto(
                        level: ToastLevelEnum::warning,
                        title: 'Email invitation has been sent.'
                    ),
                ]);

        }

        $group->members()->attach($user);

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Student has been successfully added to this group.'
                ),
            ]);
    }

    public function remove(Request $request, Group $group, GroupInvitation $groupInvitation)
    {
        if($groupInvitation->group_id !== $group->id) {
            abort(403, 'Invitation does not belong to request group!');
        }

        $groupInvitation->delete();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Group invitations has been successfully deleted.',
                ),
            ]);
    }

    public function resendInvite(Request $request, Group $group, GroupInvitation $groupInvitation)
    {
        if($groupInvitation->group_id !== $group->id) {
            abort(403, 'Invitation does not belong to request group!');
        }

        // TODO: Send invite

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Group invitations has been successfully re-sent.',
                ),
            ]);
    }
}
