<?php /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */

namespace App\Http\Controllers;

use App\Http\Resources\DiscussionResource;
use App\Models\Discussion;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DiscussionController extends Controller
{
    public function index()
    {
        return inertia('Discussions/Index', [
            'discussions' => DiscussionResource::collection(
                Discussion::with(['user', 'group', 'submodule'])
                    ->withCount('messages')
                    ->orderByDesc('id')->paginate(10, '*', 'page')
            )
        ]);
    }

//
//    public function store(Request $request)
//    {
//        $course = Course::create($this->courseFormToCourseData($request));
//
//        return redirect()->route('admin.courses.show', $course)
//            ->with([
//                'toast' => new ToastDto(
//                    level: ToastLevelEnum::success,
//                    title: 'Course was successfully created!'
//                )
//            ]);
//    }

    public function show(Discussion $discussion)
    {
        return inertia('Discussions/Show', [
            'discussion' => $discussion->load([
                'user',
                'group',
                'submodule'
            ]),
            'messages' => $discussion->messages()
                ->orderBy('created_at')
                ->whereNull('parent_id')
                ->withCount('childrenMessages')
                ->with([
                    'user',
                    'childrenMessagesRecursive' => function (HasMany $query) {
                        $query->with(['user', 'ratings']);
                    },
                    'ratings' => function (HasMany $query) {
                        /**
                         * TODO: only aggregated data
                         */
//                        return
//                            $query->groupBy(['positive'])
//                            ->select(\DB::raw('COUNT(id)'), 'positive');
                    },
                ])
                ->paginate(10, '*', 'page_messages')
        ]);
    }

//    public function edit(User $user)
//    {
//        return inertia('Courses/Edit', [
//            'course' => $course
//        ]);
//    }

//    public function update(Request $request, User $user)
//    {
//
//        $course->fill($this->courseFormToCourseData($request));
//
//        $course->save();
//
//        return redirect()->route('admin.courses.show', $course)
//            ->with([
//                'toast' => new ToastDto(
//                    level: ToastLevelEnum::success,
//                    title: 'Course was successfully updated.'
//                )
//            ]);
//    }

//    public function destroy(User $user)
//    {
//        $course->delete();
//    }
//
//    public function courseFormToCourseData(Request $request): array
//    {
//        $request->validate([
//            'name' => 'required',
////            'description' => 'required',
//            'price' => 'required|numeric',
//            'meta' => 'required|array',
//            'meta.includes' => 'required|array',
//            'meta.lessons' => 'required|array',
//            'meta.requirements' => 'required|array',
//        ]);
//
//        return [
//            'name' => $request->input('name'),
//            'description' => $request->input('description'),
//            'price' => $request->input('price'),
//            'meta' => [
//                'includes' => $request->input('meta.includes'),
//                'lessons' => $request->input('meta.lessons'),
//                'requirements' => $request->input('meta.requirements'),
//            ]
//        ];
//    }
}
