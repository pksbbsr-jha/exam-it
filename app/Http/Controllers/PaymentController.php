<?php /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */

namespace App\Http\Controllers;

use App\Dtos\ToastDto;
use App\Dtos\ToastLevelEnum;
use App\Http\Resources\PaymentResource;
use App\Models\Payment;

class PaymentController extends Controller
{
    public function index()
    {
        return inertia('Payments/Index', [
            'payments' => PaymentResource::collection(Payment::with('user')->orderByDesc('id')->paginate(10, '*', 'page'))
        ]);
    }


    public function refund(Payment $payment)
    {
        $payment->refunded_at = now();
        $payment->save();

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Payment was successfully refunded!'
                )
            ]);

    }
//
//    public function store(Request $request)
//    {
//        $course = Course::create($this->courseFormToCourseData($request));
//
//        return redirect()->route('admin.courses.show', $course)
//            ->with([
//                'toast' => new ToastDto(
//                    level: ToastLevelEnum::success,
//                    title: 'Course was successfully created!'
//                )
//            ]);
//    }

//    public function show(User $user)
//    {
//        return inertia('Courses/Show', [
//            'course' => $course,
//            'chapters' => $course->chapters()
//                ->orderBy('order')
//                ->with([
//                    'modules' => function (HasMany $query) {
//                        $query->orderBy('order')
//                            ->with([
//                                'submodules' => function (HasMany $submodulesQuery) {
//                                    $submodulesQuery->orderBy('order');
//                                }]);
//                    },
//                ])
//                ->get()
//        ]);
//    }

//    public function edit(User $user)
//    {
//        return inertia('Courses/Edit', [
//            'course' => $course
//        ]);
//    }

//    public function update(Request $request, User $user)
//    {
//
//        $course->fill($this->courseFormToCourseData($request));
//
//        $course->save();
//
//        return redirect()->route('admin.courses.show', $course)
//            ->with([
//                'toast' => new ToastDto(
//                    level: ToastLevelEnum::success,
//                    title: 'Course was successfully updated.'
//                )
//            ]);
//    }

//    public function destroy(User $user)
//    {
//        $course->delete();
//    }
//
//    public function courseFormToCourseData(Request $request): array
//    {
//        $request->validate([
//            'name' => 'required',
////            'description' => 'required',
//            'price' => 'required|numeric',
//            'meta' => 'required|array',
//            'meta.includes' => 'required|array',
//            'meta.lessons' => 'required|array',
//            'meta.requirements' => 'required|array',
//        ]);
//
//        return [
//            'name' => $request->input('name'),
//            'description' => $request->input('description'),
//            'price' => $request->input('price'),
//            'meta' => [
//                'includes' => $request->input('meta.includes'),
//                'lessons' => $request->input('meta.lessons'),
//                'requirements' => $request->input('meta.requirements'),
//            ]
//        ];
//    }
}
