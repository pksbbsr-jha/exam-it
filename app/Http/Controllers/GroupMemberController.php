<?php /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */

namespace App\Http\Controllers;

use App\Dtos\ToastDto;
use App\Dtos\ToastLevelEnum;
use App\Models\Group;
use App\Models\User;
use Illuminate\Http\Request;

class GroupMemberController extends Controller
{
    public function remove(Request $request, Group $group, User $user)
    {
        $result = $group->members()->detach($user->id);

        if ($result === 0) {
            return redirect()->back()
                ->with([
                    'toast' => new ToastDto(
                        level: ToastLevelEnum::error,
                        title: 'Student does not belongs to this group',
                    ),
                ]);
        }

        return redirect()->back()
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Student has been successfully removed from the group',
                ),
            ]);
    }
}
