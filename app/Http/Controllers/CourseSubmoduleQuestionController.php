<?php /** @noinspection PhpArrayShapeAttributeCanBeAddedInspection */

namespace App\Http\Controllers;

use App\Dtos\ToastDto;
use App\Dtos\ToastLevelEnum;
use App\Models\Course;
use App\Models\CourseModule;
use App\Models\CourseQuestion;
use App\Models\CourseSubmodule;
use Illuminate\Http\Request;

class CourseSubmoduleQuestionController extends Controller
{
    public function create(Request $request, CourseSubmodule $submodule)
    {
        return inertia('Submodules/Questions/Create', [
            'submodule_id' => $submodule->id
        ]);
    }

    public function store(Request $request, CourseSubmodule $submodule)
    {
        $submodule->questions()->create($this->questionFormToData($request));
        return redirect()->route('admin.submodules.show', $submodule)
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Question was successfully created!'
                )
            ]);
    }

    public function edit(Request $request, CourseSubmodule $submodule, CourseQuestion $question)
    {
        return inertia('Submodules/Questions/Edit', [
            'submodule_id' => $submodule->id,
            'question' => $question
        ]);
    }

    public function update(Request $request, CourseSubmodule $submodule, CourseQuestion $question)
    {
        $question->fill($this->questionFormToData($request));
        $question->save();

        return redirect()->route('admin.submodules.show', $submodule)
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Question was successfully created!'
                )
            ]);
    }

    public function delete(Request $request, CourseSubmodule $submodule, CourseQuestion $question)
    {
        $question->delete();

        return redirect()->route('admin.submodules.show', $submodule)
            ->with([
                'toast' => new ToastDto(
                    level: ToastLevelEnum::success,
                    title: 'Question was successfully deleted!'
                )
            ]);

    }


    protected function questionFormToData(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'question' => 'required',
            'answers' => 'required|array',
            'answers.*' => 'required|string',
            'explanation' => 'required|string',
//            'duration' => 'required|numeric',
        ]);

        return [
            'name' => $request->input('name'),
            'question' => $request->input('question'),
            'answers' => $request->input('answers'),
            'explanation' => $request->input('explanation'),
//            'duration' => $request->input('duration'),
        ];
    }

}
