<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserInvitation extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(public User $inviter, public User $invitee)
    {

    }

    public function build()
    {
        return $this->markdown('emails.user-invitation')
            ->subject($this->inviter->name . " invited you to join Examit Portal");
    }
}
