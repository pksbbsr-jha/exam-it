<?php

namespace App\Dtos;

enum ToastLevelEnum: string {
    case success = 'success';
    case error = 'error';
    case info = 'info';
    case warning = 'warning';
}
