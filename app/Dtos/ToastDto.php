<?php

namespace App\Dtos;

use Spatie\LaravelData\Data;

class ToastDto extends Data
{
    public function __construct(
        public ToastLevelEnum $level,
        public string|null $title = null,
        public string|null $message = null,
    ) {
    }
}
