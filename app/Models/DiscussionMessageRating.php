<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\DiscussionMessageRating
 *
 * @property int $id
 * @property int $discussions_message_id
 * @property int $user_id
 * @property bool $positive
 * @property int $created_at
 * @property int|null $deleted_at
 * @property-read \App\Models\DiscussionMessage $discussionMessage
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @property-read \App\Models\User $user
 * @method static \Database\Factories\DiscussionMessageRatingFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessageRating newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessageRating newQuery()
 * @method static \Illuminate\Database\Query\Builder|DiscussionMessageRating onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessageRating query()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessageRating whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessageRating whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessageRating whereDiscussionsMessageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessageRating whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessageRating wherePositive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessageRating whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|DiscussionMessageRating withTrashed()
 * @method static \Illuminate\Database\Query\Builder|DiscussionMessageRating withoutTrashed()
 * @mixin \Eloquent
 * @noinspection PhpFullyQualifiedNameUsageInspection
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */
class DiscussionMessageRating extends Model
{
    use HasFactory;
    use SoftDeletes;
    use RevisionableTrait;

    protected $table = 'discussions_messages_ratings';

    const UPDATED_AT = null;

    protected $casts = [
        'id' => 'int',
        'discussions_message_id' => 'int',
        'user_id' => 'int',
        'positive' => 'boolean',
        'created_at' => 'timestamp',
        'deleted_at' => 'timestamp',
    ];

    protected $fillable = [
        'positive',
    ];

    public function discussionMessage(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(DiscussionMessage::class, 'discussions_message_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
