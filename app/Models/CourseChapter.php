<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\CourseChapter
 *
 * @property int $id
 * @property int $course_id
 * @property int $order
 * @property string $name
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $deleted_at
 * @property-read \App\Models\Course $course
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CourseModule[] $modules
 * @property-read int|null $modules_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Database\Factories\CourseChapterFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseChapter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseChapter newQuery()
 * @method static \Illuminate\Database\Query\Builder|CourseChapter onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseChapter query()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseChapter whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseChapter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseChapter whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseChapter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseChapter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseChapter whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseChapter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|CourseChapter withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CourseChapter withoutTrashed()
 * @mixin \Eloquent
 * @noinspection PhpFullyQualifiedNameUsageInspection
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */
class CourseChapter extends Model
{
    use HasFactory;
    use SoftDeletes;
    use RevisionableTrait;

    protected $table = 'courses_chapters';

    protected $casts = [
        'id' => 'int',
        'name' => 'string',
        'position' => 'int',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'deleted_at' => 'timestamp',
    ];

    protected $fillable = [
        'name',
        'order'
    ];

    public function course(): BelongsTo
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    public function modules(): HasMany
    {
        return $this->hasMany(CourseModule::class, 'chapter_id', 'id');
    }



}
