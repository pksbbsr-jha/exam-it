<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\DiscussionMessage
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int $discussion_id
 * @property int $user_id
 * @property string $content
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|DiscussionMessage[] $childrenMessages
 * @property-read int|null $children_messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|DiscussionMessage[] $childrenMessagesRecursive
 * @property-read int|null $children_messages_recursive_count
 * @property-read \App\Models\Discussion $discussion
 * @property-read DiscussionMessage|null $parentMessage
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DiscussionMessageRating[] $ratings
 * @property-read int|null $ratings_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @property-read \App\Models\User $user
 * @method static \Database\Factories\DiscussionMessageFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessage newQuery()
 * @method static \Illuminate\Database\Query\Builder|DiscussionMessage onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessage whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessage whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessage whereDiscussionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessage whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiscussionMessage whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|DiscussionMessage withTrashed()
 * @method static \Illuminate\Database\Query\Builder|DiscussionMessage withoutTrashed()
 * @mixin \Eloquent
 * @noinspection PhpFullyQualifiedNameUsageInspection
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */
class DiscussionMessage extends Model
{
    use HasFactory;
    use SoftDeletes;
    use RevisionableTrait;

    protected $table = 'discussions_messages';

    protected $casts = [
        'id' => 'int',
        'parent_id' => 'int',
        'discussion_id' => 'int',
        'user_id' => 'int',
        'submodule_id' => 'int',
        'content' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'deleted_at' => 'timestamp',
    ];

    protected $fillable = [
        'subject',
    ];

    public function discussion(): BelongsTo
    {
        return $this->belongsTo(Discussion::class, 'discussion_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function parentMessage(): BelongsTo
    {
        return $this->belongsTo(DiscussionMessage::class, 'parent_id');
    }

    public function childrenMessages(): HasMany
    {
        return $this->hasMany(DiscussionMessage::class, 'parent_id');
    }

    public function childrenMessagesRecursive(): HasMany
    {
        return $this->childrenMessages()->with('childrenMessagesRecursive');
    }

    public function ratings()
    {
        return $this->hasMany(DiscussionMessageRating::class, 'discussions_message_id');
    }
}
