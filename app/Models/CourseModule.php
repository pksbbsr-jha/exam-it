<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\CourseModule
 *
 * @property int $id
 * @property int $chapter_id
 * @property int $order
 * @property string $name
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $deleted_at
 * @property bool $is_free
 * @property-read \App\Models\CourseChapter $chapter
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CourseSubmodule[] $submodules
 * @property-read int|null $submodules_count
 * @method static \Database\Factories\CourseModuleFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseModule newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseModule newQuery()
 * @method static \Illuminate\Database\Query\Builder|CourseModule onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseModule query()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseModule whereChapterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseModule whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseModule whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseModule whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseModule whereIsFree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseModule whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseModule whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseModule whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|CourseModule withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CourseModule withoutTrashed()
 * @mixin \Eloquent
 * @noinspection PhpFullyQualifiedNameUsageInspection
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */
class CourseModule extends Model
{
    use HasFactory;
    use SoftDeletes;
    use RevisionableTrait;

    protected $table = 'courses_modules';

    protected $casts = [
        'id' => 'int',
        'name' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'deleted_at' => 'timestamp',
    ];

    protected $fillable = [
        'name',
    ];

    protected static function booted()
    {
        CourseChapter::creating(function(CourseChapter $model) {
            $model->order = $model->course->chapters()->count();
        });
    }

    public function chapter(): BelongsTo
    {
        return $this->belongsTo(CourseChapter::class, 'chapter_id');
    }

    public function submodules(): HasMany
    {
        return $this->hasMany(CourseSubmodule::class, 'module_id', 'id');
    }


}
