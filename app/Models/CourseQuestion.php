<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\CourseQuestion
 *
 * @property int $id
 * @property int $submodule_id
 * @property string $name
 * @property string $question
 * @property array $answers
 * @property string $explanation
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @property-read \App\Models\CourseSubmodule $submodule
 * @method static \Illuminate\Database\Eloquent\Builder|CourseQuestion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseQuestion newQuery()
 * @method static \Illuminate\Database\Query\Builder|CourseQuestion onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseQuestion query()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseQuestion whereAnswers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseQuestion whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseQuestion whereExplanation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseQuestion whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseQuestion whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseQuestion whereSubmoduleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseQuestion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|CourseQuestion withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CourseQuestion withoutTrashed()
 * @mixin \Eloquent
 * @noinspection PhpFullyQualifiedNameUsageInspection
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */
class CourseQuestion extends Model
{
    use HasFactory;
    use SoftDeletes;
    use RevisionableTrait;

    protected $table = 'courses_questions';

    protected $casts = [
        'id' => 'int',
        'name' => 'string',
        'question' => 'string',
        'answers' => 'array',
        'explanation' => 'string',
        'duration' => 'int',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'deleted_at' => 'timestamp',
    ];

    protected $fillable = [
        'name',
        'question',
        'answers',
        'explanation',
        'duration',
    ];

    public function submodule(): BelongsTo
    {
        return $this->belongsTo(CourseSubmodule::class, 'submodule_id');
    }


}
