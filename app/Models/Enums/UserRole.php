<?php

namespace App\Models\Enums;

enum UserRole: string {
    case SUPER_ADMIN = 'super_admin';
    case ADMIN = 'admin';
    case STUDENT = 'student';

    public function isAdmin(): bool
    {
        return $this->isSuperAdmin() || $this === UserRole::ADMIN;
    }

    public function isSuperAdmin(): bool
    {
        return $this === UserRole::SUPER_ADMIN;
    }
}
