<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\GroupInvitation
 *
 * @property int $id
 * @property int $group_id
 * @property string $email
 * @property int $invitation_sent_at
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $deleted_at
 * @property-read \App\Models\Group $group
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Illuminate\Database\Eloquent\Builder|GroupInvitation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GroupInvitation newQuery()
 * @method static \Illuminate\Database\Query\Builder|GroupInvitation onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|GroupInvitation query()
 * @method static \Illuminate\Database\Eloquent\Builder|GroupInvitation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GroupInvitation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GroupInvitation whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GroupInvitation whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GroupInvitation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GroupInvitation whereInvitationSentAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GroupInvitation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|GroupInvitation withTrashed()
 * @method static \Illuminate\Database\Query\Builder|GroupInvitation withoutTrashed()
 * @mixin \Eloquent
 * @noinspection PhpFullyQualifiedNameUsageInspection
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */
class GroupInvitation extends Model
{
    use HasFactory;
    use SoftDeletes;
    use RevisionableTrait;

    protected $table = 'group_invitations';

    protected $casts = [
        'id' => 'int',
        'group_id' => 'int',
        'email' => 'string',
        'user_id' => 'int',
        'invitation_sent_at' => 'timestamp',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'deleted_at' => 'timestamp',
    ];

    protected $fillable = [
        'email',
        'invitation_sent_at',
    ];

    public function group(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Group::class, 'group_id');
    }


    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

}
