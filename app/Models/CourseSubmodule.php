<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Models\CourseSubmodule
 *
 * @property int $id
 * @property int $module_id
 * @property int $order
 * @property string $name
 * @property string $content
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $deleted_at
 * @property-read \App\Models\CourseModule $module
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CourseQuestion[] $questions
 * @property-read int|null $questions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read int|null $revision_history_count
 * @method static \Database\Factories\CourseSubmoduleFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSubmodule newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSubmodule newQuery()
 * @method static \Illuminate\Database\Query\Builder|CourseSubmodule onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSubmodule query()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSubmodule whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSubmodule whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSubmodule whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSubmodule whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSubmodule whereModuleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSubmodule whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSubmodule whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseSubmodule whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|CourseSubmodule withTrashed()
 * @method static \Illuminate\Database\Query\Builder|CourseSubmodule withoutTrashed()
 * @mixin \Eloquent
 * @noinspection PhpFullyQualifiedNameUsageInspection
 * @noinspection PhpUnnecessaryFullyQualifiedNameInspection
 */
class CourseSubmodule extends Model
{
    use HasFactory;
    use SoftDeletes;
    use RevisionableTrait;

    protected $table = 'courses_submodules';

    protected $casts = [
        'id' => 'int',
        'name' => 'string',
        'content' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'deleted_at' => 'timestamp',
    ];

    protected $fillable = [
        'name',
        'content',
    ];

    public function module(): BelongsTo
    {
        return $this->belongsTo(CourseModule::class, 'module_id');
    }

    public function questions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CourseQuestion::class, 'submodule_id');
    }

}
