"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_app_js_Pages_Dashboard_Banner_tsx"],{

/***/ "./resources/app/js/Pages/Dashboard/Banner.tsx":
/*!*****************************************************!*\
  !*** ./resources/app/js/Pages/Dashboard/Banner.tsx ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {



var __importDefault = void 0 && (void 0).__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var Banner = function Banner() {
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "sm-hide"
  }, react_1["default"].createElement("div", {
    className: "text-28 text-white"
  }, "Good morning, ", react_1["default"].createElement("span", {
    className: "text-w-600"
  }, "Mike Schneller!"))));
};

exports["default"] = Banner;

/***/ })

}]);
//# sourceMappingURL=resources_app_js_Pages_Dashboard_Banner_tsx.js.map