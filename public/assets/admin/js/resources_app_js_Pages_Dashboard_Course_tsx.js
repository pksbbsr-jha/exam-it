"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_app_js_Pages_Dashboard_Course_tsx"],{

/***/ "./resources/app/js/Pages/Dashboard/Course.tsx":
/*!*****************************************************!*\
  !*** ./resources/app/js/Pages/Dashboard/Course.tsx ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {



var __importDefault = void 0 && (void 0).__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var Course = function Course() {
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "col-lg-4 col-md-6 mt24"
  }, react_1["default"].createElement("div", {
    className: "start-learn-course start-learn-course--open z10"
  }, react_1["default"].createElement("div", {
    className: "learn-course-present-top text-white"
  }, react_1["default"].createElement("div", {
    className: "learn-course-present-desc_image"
  }), react_1["default"].createElement("div", {
    className: "start-learn-bottom"
  }, react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "AP Physics C: Mechanics"), react_1["default"].createElement("div", {
    className: "course-progress-wrap mt16"
  }, react_1["default"].createElement("div", {
    className: "row justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "33%")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A"
  }, "3/10 modules"))), react_1["default"].createElement("div", {
    className: "mt8"
  }, react_1["default"].createElement("div", {
    className: "progress-course"
  }, react_1["default"].createElement("div", {
    className: "cssProgress-success",
    style: {
      width: "33%"
    }
  })))), react_1["default"].createElement("a", {
    className: "mt32 btn btn--sm btn--wide btn--blue",
    href: "#"
  }, react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View")), react_1["default"].createElement("a", {
    className: "mt16 btn btn--sm btn--white btn--wide",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "Random Question Generator")))))));
};

exports["default"] = Course;

/***/ })

}]);
//# sourceMappingURL=resources_app_js_Pages_Dashboard_Course_tsx.js.map