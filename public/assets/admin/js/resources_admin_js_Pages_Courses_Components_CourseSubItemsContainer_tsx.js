"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_admin_js_Pages_Courses_Components_CourseSubItemsContainer_tsx"],{

/***/ "./resources/admin/js/Pages/Courses/Components/CourseSubItemsContainer.tsx":
/*!*********************************************************************************!*\
  !*** ./resources/admin/js/Pages/Courses/Components/CourseSubItemsContainer.tsx ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {



var __importDefault = void 0 && (void 0).__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var antd_1 = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");

var CourseSubItemsContainer = function CourseSubItemsContainer(props) {
  return react_1["default"].createElement(antd_1.Spin, {
    spinning: props.isReordering,
    delay: 500
  }, react_1["default"].createElement(antd_1.Row, {
    gutter: [0, 10]
  }, props.children));
};

exports["default"] = CourseSubItemsContainer;

/***/ })

}]);
//# sourceMappingURL=resources_admin_js_Pages_Courses_Components_CourseSubItemsContainer_tsx.js.map