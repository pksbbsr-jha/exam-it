"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_app_js_Pages_Dashboard_MyAchievents_tsx"],{

/***/ "./resources/app/js/Pages/Dashboard/MyAchievents.tsx":
/*!***********************************************************!*\
  !*** ./resources/app/js/Pages/Dashboard/MyAchievents.tsx ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {



var __importDefault = void 0 && (void 0).__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var MyAcheivents = function MyAcheivents() {
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "user-all-course mt30"
  }, react_1["default"].createElement("div", {
    className: "user-all-course__head"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center px-8"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-18 text-w-600"
  }, "My Achievements")))), react_1["default"].createElement("div", {
    className: "user-all-course__body"
  }, react_1["default"].createElement("div", {
    className: "row px-12 mt-24"
  }, react_1["default"].createElement("div", {
    className: "col-lg-4 col-md-6 mt24"
  }, react_1["default"].createElement("a", {
    className: "achievment-box",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "achievment-box__icon"
  }, react_1["default"].createElement("div", {
    className: "svg-image-bronze-ach"
  })), react_1["default"].createElement("div", {
    className: "achievment-box__cont"
  }, react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Award for Demo title"), react_1["default"].createElement("div", {
    className: "mt8 text-14 text-6E"
  }, "20 January 2021")))), react_1["default"].createElement("div", {
    className: "col-lg-4 col-md-6 mt24"
  }, react_1["default"].createElement("a", {
    className: "achievment-box",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "achievment-box__icon"
  }, react_1["default"].createElement("div", {
    className: "svg-image-silver-ach"
  })), react_1["default"].createElement("div", {
    className: "achievment-box__cont"
  }, react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Award for Demo title"), react_1["default"].createElement("div", {
    className: "mt8 text-14 text-6E"
  }, "20 January 2021")))), react_1["default"].createElement("div", {
    className: "col-lg-4 col-md-6 mt24"
  }, react_1["default"].createElement("a", {
    className: "achievment-box",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "achievment-box__icon"
  }, react_1["default"].createElement("div", {
    className: "svg-image-gold-ach"
  })), react_1["default"].createElement("div", {
    className: "achievment-box__cont"
  }, react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Award for Demo title"), react_1["default"].createElement("div", {
    className: "mt8 text-14 text-6E"
  }, "20 January 2021"))))))));
};

exports["default"] = MyAcheivents;

/***/ })

}]);
//# sourceMappingURL=resources_app_js_Pages_Dashboard_MyAchievents_tsx.js.map