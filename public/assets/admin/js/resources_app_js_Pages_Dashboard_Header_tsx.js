"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_app_js_Pages_Dashboard_Header_tsx"],{

/***/ "./resources/app/js/Pages/Dashboard/Header.tsx":
/*!*****************************************************!*\
  !*** ./resources/app/js/Pages/Dashboard/Header.tsx ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {



var __importDefault = void 0 && (void 0).__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var Header = function Header() {
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "user-all-course__head"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center px-8 justify-content-between mt-12"
  }, react_1["default"].createElement("div", {
    className: "col-md-auto mt12"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center px-8 mt-12 justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto mt12"
  }, react_1["default"].createElement("div", {
    className: "text-18 text-w-600"
  }, "My Courses")), react_1["default"].createElement("div", {
    className: "col-auto mt12 sm-show"
  }, react_1["default"].createElement("a", {
    className: "btn-view",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View More Courses"))), react_1["default"].createElement("div", {
    className: "col-md-auto mt12"
  }, react_1["default"].createElement("ul", {
    className: "grey-menu text-14 text-w-600"
  }, react_1["default"].createElement("li", {
    className: "current-menu-item"
  }, react_1["default"].createElement("a", {
    href: "#"
  }, "All")), react_1["default"].createElement("li", null, react_1["default"].createElement("a", {
    href: "#"
  }, "Current")), react_1["default"].createElement("li", null, react_1["default"].createElement("a", {
    href: "#"
  }, "Completed")))))), react_1["default"].createElement("div", {
    className: "col-md-auto mt12 sm-hide"
  }, react_1["default"].createElement("a", {
    className: "btn-view",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View More Courses"))))));
};

exports["default"] = Header;

/***/ })

}]);
//# sourceMappingURL=resources_app_js_Pages_Dashboard_Header_tsx.js.map