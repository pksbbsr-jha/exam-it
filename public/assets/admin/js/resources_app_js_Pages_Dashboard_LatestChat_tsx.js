"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_app_js_Pages_Dashboard_LatestChat_tsx"],{

/***/ "./resources/app/js/Pages/Dashboard/LatestChat.tsx":
/*!*********************************************************!*\
  !*** ./resources/app/js/Pages/Dashboard/LatestChat.tsx ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {



var __importDefault = void 0 && (void 0).__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var LatestChat = function LatestChat() {
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "col-lg-6 mt30"
  }, react_1["default"].createElement("div", {
    className: "white-box"
  }, react_1["default"].createElement("div", {
    className: "white-box-head"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-w-600"
  }, "Latest Chats")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("a", {
    className: "btn-view",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View All"))))), react_1["default"].createElement("div", {
    className: "white-box-body auto-h-530"
  }, react_1["default"].createElement("div", {
    className: "row mt-16"
  }, react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Jacob_Jones"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "chat-message-data"
  }, "10"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Dianne_Russell"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "chat-message-data"
  }, "1"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Jenny_Wilson +2"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Robert_Fox"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "chat-message-data"
  }, "2"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Jacob_Jones"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "chat-message-data"
  }, "10"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Dianne_Russell"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "chat-message-data"
  }, "1"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Jenny_Wilson +2"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Robert_Fox"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "chat-message-data"
  }, "2"))))))))));
};

exports["default"] = LatestChat;

/***/ })

}]);
//# sourceMappingURL=resources_app_js_Pages_Dashboard_LatestChat_tsx.js.map