"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_app_js_Pages_Dashboard_Forum_tsx"],{

/***/ "./resources/app/js/Pages/Dashboard/Forum.tsx":
/*!****************************************************!*\
  !*** ./resources/app/js/Pages/Dashboard/Forum.tsx ***!
  \****************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {



var __importDefault = void 0 && (void 0).__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var Forum = function Forum() {
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "mt30 forum-module"
  }, react_1["default"].createElement("div", {
    className: "forum-module-head"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-w-600"
  }, "Forum")), react_1["default"].createElement("div", {
    className: "col-auto md-hide"
  }, react_1["default"].createElement("div", {
    className: "filter-search-field"
  }, react_1["default"].createElement("input", {
    className: "wpcf7-form-control",
    type: "search",
    placeholder: "Search..."
  }))))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("a", {
    className: "btn-view",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View All"))))), react_1["default"].createElement("div", {
    className: "white-box-body sm-show"
  }, react_1["default"].createElement("div", {
    className: "row mt-16"
  }, react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col-12"
  }, react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "3 replies")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col-12"
  }, react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "3 replies")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col-12"
  }, react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "3 replies")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col-12"
  }, react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "3 replies")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col-12"
  }, react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "3 replies")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))))), react_1["default"].createElement("div", {
    className: "forum-module-body sm-hide"
  }, react_1["default"].createElement("table", {
    className: "forum-module-table"
  }, react_1["default"].createElement("thead", null, react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Title")), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Replies")), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Author")), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Last message")))), react_1["default"].createElement("tbody", null, react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"), react_1["default"].createElement("td", null, "14"), react_1["default"].createElement("td", null, " ", react_1["default"].createElement("a", {
    href: "#"
  }, "Robert_Fox")), react_1["default"].createElement("td", null, "02/02/2022 \u2022 1:30 PM")), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "Lorem ipsum dolor "), react_1["default"].createElement("td", null, "7"), react_1["default"].createElement("td", null, " ", react_1["default"].createElement("a", {
    href: "#"
  }, "Dianne_Russell")), react_1["default"].createElement("td", null, "02/02/2022 \u2022 1:30 PM")), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"), react_1["default"].createElement("td", null, "7"), react_1["default"].createElement("td", null, " ", react_1["default"].createElement("a", {
    href: "#"
  }, "Dianne_Russell")), react_1["default"].createElement("td", null, "02/02/2022 \u2022 1:30 PM")), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"), react_1["default"].createElement("td", null, "7"), react_1["default"].createElement("td", null, " ", react_1["default"].createElement("a", {
    href: "#"
  }, "Dianne_Russell")), react_1["default"].createElement("td", null, "02/02/2022 \u2022 1:30 PM")), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"), react_1["default"].createElement("td", null, "7"), react_1["default"].createElement("td", null, " ", react_1["default"].createElement("a", {
    href: "#"
  }, "Dianne_Russell")), react_1["default"].createElement("td", null, "02/02/2022 \u2022 1:30 PM")))))));
};

exports["default"] = Forum;

/***/ })

}]);
//# sourceMappingURL=resources_app_js_Pages_Dashboard_Forum_tsx.js.map