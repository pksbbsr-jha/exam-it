"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_app_js_Pages_Dashboard_LatestDiscussion_tsx"],{

/***/ "./resources/app/js/Pages/Dashboard/LatestDiscussion.tsx":
/*!***************************************************************!*\
  !*** ./resources/app/js/Pages/Dashboard/LatestDiscussion.tsx ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {



var __importDefault = void 0 && (void 0).__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var LatestDiscussion = function LatestDiscussion() {
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "col-lg-6 mt30"
  }, react_1["default"].createElement("div", {
    className: "white-box"
  }, react_1["default"].createElement("div", {
    className: "white-box-head"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-w-600"
  }, "Latest Discussions")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("a", {
    className: "btn-view",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View All"))))), react_1["default"].createElement("div", {
    className: "white-box-body auto-h-530"
  }, react_1["default"].createElement("div", {
    className: "row mt-16"
  }, react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "w384"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A sm-hide"
  }, "Module 3.1: Vectors and Scalars"), react_1["default"].createElement("div", {
    className: "mt8 sm-hide"
  }), react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("button", {
    className: "toggleDiscussionInWishlist",
    type: "button"
  }))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-tag discussion-tag-public"
  }, "Public ")))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "w384"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A sm-hide"
  }, "Module 3.1: Vectors and Scalars"), react_1["default"].createElement("div", {
    className: "mt8 sm-hide"
  }), react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("button", {
    className: "toggleDiscussionInWishlist",
    type: "button"
  }))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-tag discussion-tag-private"
  }, "Private")))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "w384"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A sm-hide"
  }, "Module 3.1: Vectors and Scalars"), react_1["default"].createElement("div", {
    className: "mt8 sm-hide"
  }), react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("button", {
    className: "toggleDiscussionInWishlist",
    type: "button"
  }))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-tag discussion-tag-group"
  }, "Group ")))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "w384"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A sm-hide"
  }, "Module 3.1: Vectors and Scalars"), react_1["default"].createElement("div", {
    className: "mt8 sm-hide"
  }), react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("button", {
    className: "toggleDiscussionInWishlist",
    type: "button"
  }))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-tag discussion-tag-public"
  }, "Public ")))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "w384"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A sm-hide"
  }, "Module 3.1: Vectors and Scalars"), react_1["default"].createElement("div", {
    className: "mt8 sm-hide"
  }), react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("button", {
    className: "toggleDiscussionInWishlist",
    type: "button"
  }))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-tag discussion-tag-private"
  }, "Private")))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "w384"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A sm-hide"
  }, "Module 3.1: Vectors and Scalars"), react_1["default"].createElement("div", {
    className: "mt8 sm-hide"
  }), react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("button", {
    className: "toggleDiscussionInWishlist",
    type: "button"
  }))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-tag discussion-tag-group"
  }, "Group ")))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))))))));
};

exports["default"] = LatestDiscussion;

/***/ })

}]);
//# sourceMappingURL=resources_app_js_Pages_Dashboard_LatestDiscussion_tsx.js.map