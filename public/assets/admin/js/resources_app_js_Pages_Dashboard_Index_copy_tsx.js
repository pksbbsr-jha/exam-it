"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_app_js_Pages_Dashboard_Index_copy_tsx"],{

/***/ "./resources/app/js/Pages/Dashboard/Index copy.tsx":
/*!*********************************************************!*\
  !*** ./resources/app/js/Pages/Dashboard/Index copy.tsx ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {



var __importDefault = void 0 && (void 0).__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var Index = function Index() {
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("main", {
    className: "main"
  }, react_1["default"].createElement("div", {
    className: "small-decor-head"
  }, react_1["default"].createElement("picture", null, react_1["default"].createElement("img", {
    className: "responsive-img cover-img",
    src: "/assets/app/baseimages/sm-head-decor@1x.png",
    srcSet: "/assets/app/base/images/sm-head-decor@1x.png, /assets/app/base/sm-head-decor@2x.png 2x",
    alt: ""
  }))), react_1["default"].createElement("div", {
    className: "container mt-96 z10 mb-mt-126"
  }, react_1["default"].createElement("div", {
    className: "sm-hide"
  }, react_1["default"].createElement("div", {
    className: "text-28 text-white"
  }, "Good morning, ", react_1["default"].createElement("span", {
    className: "text-w-600"
  }, "Mike Schneller! "))), react_1["default"].createElement("div", {
    className: "user-all-course mt30"
  }, react_1["default"].createElement("div", {
    className: "user-all-course__head"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center px-8 justify-content-between mt-12"
  }, react_1["default"].createElement("div", {
    className: "col-md-auto mt12"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center px-8 mt-12 justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto mt12"
  }, react_1["default"].createElement("div", {
    className: "text-18 text-w-600"
  }, "My Courses")), react_1["default"].createElement("div", {
    className: "col-auto mt12 sm-show"
  }, react_1["default"].createElement("a", {
    className: "btn-view",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View More Courses"))), react_1["default"].createElement("div", {
    className: "col-md-auto mt12"
  }, react_1["default"].createElement("ul", {
    className: "grey-menu text-14 text-w-600"
  }, react_1["default"].createElement("li", {
    className: "current-menu-item"
  }, react_1["default"].createElement("a", {
    href: "#"
  }, "All")), react_1["default"].createElement("li", null, react_1["default"].createElement("a", {
    href: "#"
  }, "Current")), react_1["default"].createElement("li", null, react_1["default"].createElement("a", {
    href: "#"
  }, "Completed")))))), react_1["default"].createElement("div", {
    className: "col-md-auto mt12 sm-hide"
  }, react_1["default"].createElement("a", {
    className: "btn-view",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View More Courses"))))), react_1["default"].createElement("div", {
    className: "user-all-course__body"
  }, react_1["default"].createElement("div", {
    className: "row px-12 mt-24"
  }, react_1["default"].createElement("div", {
    className: "col-lg-4 col-md-6 mt24"
  }, react_1["default"].createElement("div", {
    className: "start-learn-course start-learn-course--open z10"
  }, react_1["default"].createElement("div", {
    className: "learn-course-present-top text-white"
  }, react_1["default"].createElement("div", {
    className: "learn-course-present-desc_image"
  }, react_1["default"].createElement("picture", null, react_1["default"].createElement("source", {
    srcSet: "images/course-demo@1x.webp, images/course-demo@2x.webp 2x",
    type: "image/webp"
  }, react_1["default"].createElement("img", {
    className: "responsive-img cover-img",
    src: "/assets/app/base/images/course-demo@1x.jpg",
    srcSet: "images/course-demo@1x.jpg, images/course-demo@2x.jpg 2x",
    alt: ""
  })))), react_1["default"].createElement("div", {
    className: "start-learn-bottom"
  }, react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "AP Physics C: Mechanics"), react_1["default"].createElement("div", {
    className: "course-progress-wrap mt16"
  }, react_1["default"].createElement("div", {
    className: "row justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "33%")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A"
  }, "3/10 modules"))), react_1["default"].createElement("div", {
    className: "mt8"
  }, react_1["default"].createElement("div", {
    className: "progress-course"
  }, react_1["default"].createElement("div", {
    className: "cssProgress-success",
    style: {
      width: "33%"
    }
  })))), react_1["default"].createElement("a", {
    className: "mt32 btn btn--sm btn--wide btn--blue",
    href: "#"
  }, react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View")), react_1["default"].createElement("a", {
    className: "mt16 btn btn--sm btn--white btn--wide",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "Random Question Generator"))))), react_1["default"].createElement("div", {
    className: "col-lg-4 col-md-6 mt24"
  }, react_1["default"].createElement("div", {
    className: "start-learn-course start-learn-course--open z10"
  }, react_1["default"].createElement("div", {
    className: "learn-course-present-top text-white"
  }, react_1["default"].createElement("div", {
    className: "learn-course-present-desc_image"
  }, react_1["default"].createElement("picture", null, react_1["default"].createElement("source", {
    srcSet: "images/cd2@1x.webp, images/cd2@2x.webp 2x",
    type: "image/webp"
  }, react_1["default"].createElement("img", {
    className: "responsive-img cover-img",
    src: "/assets/app/base/images/cd2@1x.jpg",
    srcSet: "/assets/app/base/images/cd2@1x.jpg, /assets/app/base/images/cd2@2x.jpg 2x",
    alt: ""
  })))), react_1["default"].createElement("div", {
    className: "start-learn-bottom"
  }, react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "AP Physics C: Mechanics"), react_1["default"].createElement("div", {
    className: "course-progress-wrap mt16"
  }, react_1["default"].createElement("div", {
    className: "row justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "33%")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A"
  }, "3/10 modules"))), react_1["default"].createElement("div", {
    className: "mt8"
  }, react_1["default"].createElement("div", {
    className: "progress-course"
  }, react_1["default"].createElement("div", {
    className: "cssProgress-success",
    style: {
      width: "33%"
    }
  })))), react_1["default"].createElement("a", {
    className: "mt32 btn btn--sm btn--wide btn--blue",
    href: "#"
  }, react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View")), react_1["default"].createElement("a", {
    className: "mt16 btn btn--sm btn--white btn--wide",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "Random Question Generator"))))), react_1["default"].createElement("div", {
    className: "col-lg-4 col-md-6 mt24"
  }, react_1["default"].createElement("div", {
    className: "start-learn-course start-learn-course--open z10"
  }, react_1["default"].createElement("div", {
    className: "learn-course-present-top text-white"
  }, react_1["default"].createElement("div", {
    className: "learn-course-present-desc_image"
  }, react_1["default"].createElement("picture", null, react_1["default"].createElement("source", {
    srcSet: "/assets/app/base/images/cd3@1x.webp, /assets/app/base/images/cd3@2x.webp 2x",
    type: "image/webp"
  }, react_1["default"].createElement("img", {
    className: "responsive-img cover-img",
    src: "/assets/app/base/images/cd3@1x.jpg",
    srcSet: "/assets/app/base/images/cd3@1x.jpg, /assets/app/base/images/cd3@2x.jpg 2x",
    alt: ""
  })))), react_1["default"].createElement("div", {
    className: "start-learn-bottom"
  }, react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "AP Physics C: Mechanics"), react_1["default"].createElement("div", {
    className: "course-progress-wrap mt16"
  }, react_1["default"].createElement("div", {
    className: "row justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "33%")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A"
  }, "3/10 modules"))), react_1["default"].createElement("div", {
    className: "mt8"
  }, react_1["default"].createElement("div", {
    className: "progress-course"
  }, react_1["default"].createElement("div", {
    className: "cssProgress-success",
    style: {
      width: "33%"
    }
  })))), react_1["default"].createElement("a", {
    className: "mt32 btn btn--sm btn--wide btn--blue",
    href: "#"
  }, react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View")), react_1["default"].createElement("a", {
    className: "mt16 btn btn--sm btn--white btn--wide",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "Random Question Generator")))))))), react_1["default"].createElement("div", {
    className: "row"
  }, react_1["default"].createElement("div", {
    className: "col-lg-6 mt30"
  }, react_1["default"].createElement("div", {
    className: "white-box"
  }, react_1["default"].createElement("div", {
    className: "white-box-head"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-w-600"
  }, "Latest Discussions")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("a", {
    className: "btn-view",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View All"))))), react_1["default"].createElement("div", {
    className: "white-box-body auto-h-530"
  }, react_1["default"].createElement("div", {
    className: "row mt-16"
  }, react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "w384"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A sm-hide"
  }, "Module 3.1: Vectors and Scalars"), react_1["default"].createElement("div", {
    className: "mt8 sm-hide"
  }), react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("button", {
    className: "toggleDiscussionInWishlist",
    type: "button"
  }))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-tag discussion-tag-public"
  }, "Public ")))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "w384"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A sm-hide"
  }, "Module 3.1: Vectors and Scalars"), react_1["default"].createElement("div", {
    className: "mt8 sm-hide"
  }), react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("button", {
    className: "toggleDiscussionInWishlist",
    type: "button"
  }))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-tag discussion-tag-private"
  }, "Private")))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "w384"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A sm-hide"
  }, "Module 3.1: Vectors and Scalars"), react_1["default"].createElement("div", {
    className: "mt8 sm-hide"
  }), react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("button", {
    className: "toggleDiscussionInWishlist",
    type: "button"
  }))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-tag discussion-tag-group"
  }, "Group ")))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "w384"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A sm-hide"
  }, "Module 3.1: Vectors and Scalars"), react_1["default"].createElement("div", {
    className: "mt8 sm-hide"
  }), react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("button", {
    className: "toggleDiscussionInWishlist",
    type: "button"
  }))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-tag discussion-tag-public"
  }, "Public ")))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "w384"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A sm-hide"
  }, "Module 3.1: Vectors and Scalars"), react_1["default"].createElement("div", {
    className: "mt8 sm-hide"
  }), react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("button", {
    className: "toggleDiscussionInWishlist",
    type: "button"
  }))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-tag discussion-tag-private"
  }, "Private")))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "w384"
  }, react_1["default"].createElement("div", {
    className: "text-14 text-9A sm-hide"
  }, "Module 3.1: Vectors and Scalars"), react_1["default"].createElement("div", {
    className: "mt8 sm-hide"
  }), react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("button", {
    className: "toggleDiscussionInWishlist",
    type: "button"
  }))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-tag discussion-tag-group"
  }, "Group ")))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))))))), react_1["default"].createElement("div", {
    className: "col-lg-6 mt30"
  }, react_1["default"].createElement("div", {
    className: "white-box"
  }, react_1["default"].createElement("div", {
    className: "white-box-head"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-w-600"
  }, "Latest Chats")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("a", {
    className: "btn-view",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View All"))))), react_1["default"].createElement("div", {
    className: "white-box-body auto-h-530"
  }, react_1["default"].createElement("div", {
    className: "row mt-16"
  }, react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Jacob_Jones"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "chat-message-data"
  }, "10"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Dianne_Russell"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "chat-message-data"
  }, "1"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Jenny_Wilson +2"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Robert_Fox"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "chat-message-data"
  }, "2"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Jacob_Jones"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "chat-message-data"
  }, "10"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Dianne_Russell"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "chat-message-data"
  }, "1"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Jenny_Wilson +2"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?"))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("a", {
    className: "chat-item",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row align-items-end"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Robert_Fox"), react_1["default"].createElement("div", {
    className: "mt8"
  }, " "), react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Hi, how are you?")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "chat-message-data"
  }, "2")))))))))), react_1["default"].createElement("div", {
    className: "mt30 forum-module"
  }, react_1["default"].createElement("div", {
    className: "forum-module-head"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-w-600"
  }, "Forum")), react_1["default"].createElement("div", {
    className: "col-auto md-hide"
  }, react_1["default"].createElement("div", {
    className: "filter-search-field"
  }, react_1["default"].createElement("input", {
    className: "wpcf7-form-control",
    type: "search",
    placeholder: "Search..."
  }))))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("a", {
    className: "btn-view",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__text"
  }, "View All"))))), react_1["default"].createElement("div", {
    className: "white-box-body sm-show"
  }, react_1["default"].createElement("div", {
    className: "row mt-16"
  }, react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col-12"
  }, react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "3 replies")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col-12"
  }, react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "3 replies")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col-12"
  }, react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "3 replies")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col-12"
  }, react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "3 replies")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))), react_1["default"].createElement("div", {
    className: "col-12 mt16"
  }, react_1["default"].createElement("div", {
    className: "discussion-item"
  }, react_1["default"].createElement("div", {
    className: "row px-6"
  }, react_1["default"].createElement("div", {
    className: "col-12"
  }, react_1["default"].createElement("a", {
    className: "text-16 text-w-600",
    href: "#"
  }, " Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"))), react_1["default"].createElement("div", {
    className: "mt12 row px-6 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "3 replies")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "discussion-user-ask"
  }, "asked by ", react_1["default"].createElement("a", {
    href: "#"
  }, "username")))))))), react_1["default"].createElement("div", {
    className: "forum-module-body sm-hide"
  }, react_1["default"].createElement("table", {
    className: "forum-module-table"
  }, react_1["default"].createElement("thead", null, react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Title")), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Replies")), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Author")), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Last message")))), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"), react_1["default"].createElement("td", null, "14"), react_1["default"].createElement("td", null, " ", react_1["default"].createElement("a", {
    href: "#"
  }, "Robert_Fox")), react_1["default"].createElement("td", null, "02/02/2022 \u2022 1:30 PM")), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "Lorem ipsum dolor "), react_1["default"].createElement("td", null, "7"), react_1["default"].createElement("td", null, " ", react_1["default"].createElement("a", {
    href: "#"
  }, "Dianne_Russell")), react_1["default"].createElement("td", null, "02/02/2022 \u2022 1:30 PM")), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"), react_1["default"].createElement("td", null, "7"), react_1["default"].createElement("td", null, " ", react_1["default"].createElement("a", {
    href: "#"
  }, "Dianne_Russell")), react_1["default"].createElement("td", null, "02/02/2022 \u2022 1:30 PM")), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"), react_1["default"].createElement("td", null, "7"), react_1["default"].createElement("td", null, " ", react_1["default"].createElement("a", {
    href: "#"
  }, "Dianne_Russell")), react_1["default"].createElement("td", null, "02/02/2022 \u2022 1:30 PM")), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun"), react_1["default"].createElement("td", null, "7"), react_1["default"].createElement("td", null, " ", react_1["default"].createElement("a", {
    href: "#"
  }, "Dianne_Russell")), react_1["default"].createElement("td", null, "02/02/2022 \u2022 1:30 PM"))))), react_1["default"].createElement("div", {
    className: "mt30 groups-module"
  }, react_1["default"].createElement("div", {
    className: "groups-module-head"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-w-600"
  }, "My Groups")))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("a", {
    className: "btn btn--sm-40 btn--white btn-create-group",
    href: "#"
  }, " ", react_1["default"].createElement("span", {
    className: "btn__icon"
  }, react_1["default"].createElement("svg", {
    width: "12",
    height: "12",
    viewBox: "0 0 12 12",
    fill: "none"
  }, react_1["default"].createElement("path", {
    d: "M12 5.57143V6.42857C12 6.66527 11.8081 6.85714 11.5714 6.85714H6.85714V11.5714C6.85714 11.8081 6.66527 12 6.42857 12H5.57143C5.33473 12 5.14286 11.8081 5.14286 11.5714V6.85714H0.428571C0.191878 6.85714 0 6.66527 0 6.42857V5.57143C0 5.33473 0.191878 5.14286 0.428571 5.14286H5.14286V0.428571C5.14286 0.191878 5.33473 0 5.57143 0H6.42857C6.66527 0 6.85714 0.191878 6.85714 0.428571V5.14286H11.5714C11.8081 5.14286 12 5.33473 12 5.57143Z",
    fill: "#2153CC"
  }))), react_1["default"].createElement("span", {
    className: "btn__text"
  }, "Create group"))))), react_1["default"].createElement("div", {
    className: "groups-module-body"
  }, react_1["default"].createElement("table", {
    className: "groups-module-table"
  }, react_1["default"].createElement("thead", null, react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Group name")), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Permissions")), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Members")), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Actions")))), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "Demo group name"), react_1["default"].createElement("td", null, "Admin"), react_1["default"].createElement("td", null, "14"), react_1["default"].createElement("td", null, " ", react_1["default"].createElement("a", {
    className: "action-view",
    href: "#",
    "data-fancybox": true,
    "data-src": "#view-admin-group",
    "data-modal": "true"
  }, "View"), react_1["default"].createElement("div", {
    className: "popup-inline-exa nopad",
    id: "view-admin-group"
  }, react_1["default"].createElement("form", {
    action: "#"
  }, react_1["default"].createElement("div", {
    className: "svg-image-close",
    "data-fancybox-close": true
  }), react_1["default"].createElement("div", {
    className: "popup-inline-exa-head"
  }, react_1["default"].createElement("div", {
    className: "form__styler"
  }, react_1["default"].createElement("div", {
    className: "row"
  }, react_1["default"].createElement("div", {
    className: "col-lg-6"
  }, react_1["default"].createElement("div", {
    className: "field-label"
  }, "Group name"), react_1["default"].createElement("input", {
    type: "text",
    placeholder: "Demo group name"
  }))))), react_1["default"].createElement("table", {
    className: "groups-module-table"
  }, react_1["default"].createElement("thead", null, react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Email / Username")), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Status")), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Actions")))), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "mike_schneller ", react_1["default"].createElement("span", {
    className: "text-6E"
  }, "(admin)")), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "status-usename"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center d-inline-flex"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "circ circ-green"
  })), react_1["default"].createElement("div", {
    className: "col"
  }, "Active")))), react_1["default"].createElement("td", null, "-")), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "leslie_alexander"), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "status-usename"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center d-inline-flex"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "circ circ-green"
  })), react_1["default"].createElement("div", {
    className: "col"
  }, "Active")))), react_1["default"].createElement("td", null, react_1["default"].createElement("button", {
    className: "btn-chat-options button-tippy d-inline-flex btn-transparent",
    type: "button",
    "data-template": "btn-group-actions-tippy"
  }, react_1["default"].createElement("svg", {
    width: "16",
    height: "4",
    viewBox: "0 0 16 4",
    fill: "none"
  }, react_1["default"].createElement("path", {
    "fill-rule": "evenodd",
    "clip-rule": "evenodd",
    d: "M2 0C3.10457 0 4 0.89543 4 2C4 3.10457 3.10457 4 2 4C0.89543 4 0 3.10457 0 2C0 0.89543 0.89543 0 2 0ZM10 2C10 0.89543 9.10457 0 8 0C6.89543 0 6 0.89543 6 2C6 3.10457 6.89543 4 8 4C9.10457 4 10 3.10457 10 2ZM14 0C15.1046 0 16 0.89543 16 2C16 3.10457 15.1046 4 14 4C12.8954 4 12 3.10457 12 2C12 0.89543 12.8954 0 14 0Z",
    fill: "#4D546E"
  }))))), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "rdebbie_baker"), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "status-usename"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center d-inline-flex"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "circ circ-green"
  })), react_1["default"].createElement("div", {
    className: "col"
  }, "Active")))), react_1["default"].createElement("td", null, react_1["default"].createElement("button", {
    className: "btn-chat-options button-tippy d-inline-flex btn-transparent",
    type: "button",
    "data-template": "btn-group-actions-tippy"
  }, react_1["default"].createElement("svg", {
    width: "16",
    height: "4",
    viewBox: "0 0 16 4",
    fill: "none"
  }, react_1["default"].createElement("path", {
    "fill-rule": "evenodd",
    "clip-rule": "evenodd",
    d: "M2 0C3.10457 0 4 0.89543 4 2C4 3.10457 3.10457 4 2 4C0.89543 4 0 3.10457 0 2C0 0.89543 0.89543 0 2 0ZM10 2C10 0.89543 9.10457 0 8 0C6.89543 0 6 0.89543 6 2C6 3.10457 6.89543 4 8 4C9.10457 4 10 3.10457 10 2ZM14 0C15.1046 0 16 0.89543 16 2C16 3.10457 15.1046 4 14 4C12.8954 4 12 3.10457 12 2C12 0.89543 12.8954 0 14 0Z",
    fill: "#4D546E"
  }))))), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "jessica.hanson@example.com"), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "status-usename"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center d-inline-flex"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "circ circ-yellow"
  })), react_1["default"].createElement("div", {
    className: "col"
  }, "Invite sent")))), react_1["default"].createElement("td", null, react_1["default"].createElement("button", {
    className: "btn-chat-options button-tippy d-inline-flex btn-transparent",
    type: "button",
    "data-template": "btn-group-actions-tippy"
  }, react_1["default"].createElement("svg", {
    width: "16",
    height: "4",
    viewBox: "0 0 16 4",
    fill: "none"
  }, react_1["default"].createElement("path", {
    "fill-rule": "evenodd",
    "clip-rule": "evenodd",
    d: "M2 0C3.10457 0 4 0.89543 4 2C4 3.10457 3.10457 4 2 4C0.89543 4 0 3.10457 0 2C0 0.89543 0.89543 0 2 0ZM10 2C10 0.89543 9.10457 0 8 0C6.89543 0 6 0.89543 6 2C6 3.10457 6.89543 4 8 4C9.10457 4 10 3.10457 10 2ZM14 0C15.1046 0 16 0.89543 16 2C16 3.10457 15.1046 4 14 4C12.8954 4 12 3.10457 12 2C12 0.89543 12.8954 0 14 0Z",
    fill: "#4D546E"
  }))))), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "debbie.baker@example.com"), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "status-usename"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center d-inline-flex"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "circ circ-yellow"
  })), react_1["default"].createElement("div", {
    className: "col"
  }, "Invite sent")))), react_1["default"].createElement("td", null, react_1["default"].createElement("button", {
    className: "btn-chat-options button-tippy d-inline-flex btn-transparent",
    type: "button",
    "data-template": "btn-group-actions-tippy"
  }, react_1["default"].createElement("svg", {
    width: "16",
    height: "4",
    viewBox: "0 0 16 4",
    fill: "none"
  }, react_1["default"].createElement("path", {
    "fill-rule": "evenodd",
    "clip-rule": "evenodd",
    d: "M2 0C3.10457 0 4 0.89543 4 2C4 3.10457 3.10457 4 2 4C0.89543 4 0 3.10457 0 2C0 0.89543 0.89543 0 2 0ZM10 2C10 0.89543 9.10457 0 8 0C6.89543 0 6 0.89543 6 2C6 3.10457 6.89543 4 8 4C9.10457 4 10 3.10457 10 2ZM14 0C15.1046 0 16 0.89543 16 2C16 3.10457 15.1046 4 14 4C12.8954 4 12 3.10457 12 2C12 0.89543 12.8954 0 14 0Z",
    fill: "#4D546E"
  }))))), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "jackson.graham@example.com"), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "status-usename"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center d-inline-flex"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "circ circ-yellow"
  })), react_1["default"].createElement("div", {
    className: "col"
  }, "Invite sent")))), react_1["default"].createElement("td", null, react_1["default"].createElement("button", {
    className: "btn-chat-options button-tippy d-inline-flex btn-transparent",
    type: "button",
    "data-template": "btn-group-actions-tippy"
  }, react_1["default"].createElement("svg", {
    width: "16",
    height: "4",
    viewBox: "0 0 16 4",
    fill: "none"
  }, react_1["default"].createElement("path", {
    "fill-rule": "evenodd",
    "clip-rule": "evenodd",
    d: "M2 0C3.10457 0 4 0.89543 4 2C4 3.10457 3.10457 4 2 4C0.89543 4 0 3.10457 0 2C0 0.89543 0.89543 0 2 0ZM10 2C10 0.89543 9.10457 0 8 0C6.89543 0 6 0.89543 6 2C6 3.10457 6.89543 4 8 4C9.10457 4 10 3.10457 10 2ZM14 0C15.1046 0 16 0.89543 16 2C16 3.10457 15.1046 4 14 4C12.8954 4 12 3.10457 12 2C12 0.89543 12.8954 0 14 0Z",
    fill: "#4D546E"
  }))))), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-6E"
  }, "10 members")), react_1["default"].createElement("td", null), react_1["default"].createElement("td", null))), react_1["default"].createElement("div", {
    className: "mt-1"
  }), react_1["default"].createElement("div", {
    className: "btn-tippy-area",
    id: "btn-group-actions-tippy"
  }, react_1["default"].createElement("ul", {
    className: "list"
  }, react_1["default"].createElement("li", null, " ", react_1["default"].createElement("a", {
    className: "btn-t",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center flex-nowrap"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "svg-image-sms_outlined-blue"
  })), react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Send private message"))))), react_1["default"].createElement("li", null, " ", react_1["default"].createElement("a", {
    className: "btn-t",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center flex-nowrap"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "svg-image-restore"
  })), react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Re-send invite"))))), react_1["default"].createElement("li", null, " ", react_1["default"].createElement("a", {
    className: "btn-t",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "row px-6 align-items-center flex-nowrap"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "svg-image-delete"
  })), react_1["default"].createElement("div", {
    className: "col"
  }, react_1["default"].createElement("div", {
    className: "text-14"
  }, "Delete"))))))), react_1["default"].createElement("div", {
    className: "popup-inline-exa-footer"
  }, react_1["default"].createElement("div", {
    className: "row justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("a", {
    className: "btn btn--white btn--sm--14",
    href: "#"
  }, react_1["default"].createElement("span", {
    className: "btn__icon"
  }, react_1["default"].createElement("svg", {
    width: "16",
    height: "16",
    viewBox: "0 0 16 16",
    fill: "none"
  }, react_1["default"].createElement("path", {
    "fill-rule": "evenodd",
    "clip-rule": "evenodd",
    d: "M13.3346 3.66671V3.00004C13.3346 2.81595 13.1854 2.66671 13.0013 2.66671H10.0013V2.00004C10.0013 1.63185 9.70283 1.33337 9.33463 1.33337H6.66797C6.29978 1.33337 6.0013 1.63185 6.0013 2.00004V2.66671H3.0013C2.81721 2.66671 2.66797 2.81595 2.66797 3.00004V3.66671C2.66797 3.8508 2.81721 4.00004 3.0013 4.00004H13.0013C13.1854 4.00004 13.3346 3.8508 13.3346 3.66671ZM5.24797 14.6667C4.54655 14.6684 3.96373 14.1264 3.91464 13.4267L3.33464 5.33337H12.668L12.1013 13.4267C12.0522 14.1264 11.4694 14.6684 10.768 14.6667H5.24797Z",
    fill: "#2153CC"
  }))), react_1["default"].createElement("span", {
    className: "btn__text"
  }, "Delete group"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("a", {
    className: "btn btn--blue btn--sm--14",
    href: "#"
  }, react_1["default"].createElement("span", {
    className: "btn__icon"
  }, react_1["default"].createElement("svg", {
    width: "12",
    height: "12",
    viewBox: "0 0 12 12",
    fill: "none"
  }, react_1["default"].createElement("path", {
    d: "M12 5.57143V6.42857C12 6.66527 11.8081 6.85714 11.5714 6.85714H6.85714V11.5714C6.85714 11.8081 6.66527 12 6.42857 12H5.57143C5.33473 12 5.14286 11.8081 5.14286 11.5714V6.85714H0.428571C0.191878 6.85714 0 6.66527 0 6.42857V5.57143C0 5.33473 0.191878 5.14286 0.428571 5.14286H5.14286V0.428571C5.14286 0.191878 5.33473 0 5.57143 0H6.42857C6.66527 0 6.85714 0.191878 6.85714 0.428571V5.14286H11.5714C11.8081 5.14286 12 5.33473 12 5.57143Z",
    fill: "#2153CC"
  }))), react_1["default"].createElement("span", {
    className: "btn__text"
  }, "Add members"))))))))), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "Demo group name 2"), react_1["default"].createElement("td", null, "User"), react_1["default"].createElement("td", null, "14"), react_1["default"].createElement("td", null, " ", react_1["default"].createElement("a", {
    className: "action-view",
    href: "#",
    "data-fancybox": true,
    "data-src": "#view-user-group",
    "data-modal": "true"
  }, "View"), react_1["default"].createElement("div", {
    className: "popup-inline-exa nopad",
    id: "view-user-group"
  }, react_1["default"].createElement("form", {
    action: "#"
  }, react_1["default"].createElement("div", {
    className: "svg-image-close",
    "data-fancybox-close": true
  }), react_1["default"].createElement("div", {
    className: "popup-inline-exa-head"
  }, react_1["default"].createElement("div", {
    className: "text-18 text-w-700"
  }, "Demo group name")), react_1["default"].createElement("table", {
    className: "groups-module-table"
  }, react_1["default"].createElement("thead", null, react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Username")), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-w-600 text-6E"
  }, "Actions")))), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "leslie_alexander ", react_1["default"].createElement("span", {
    className: "text-6E"
  }, "(you)")), react_1["default"].createElement("td", null)), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "mike_schneller ", react_1["default"].createElement("span", {
    className: "text-6E"
  }, "(admin)")), react_1["default"].createElement("td", null, react_1["default"].createElement("a", {
    className: "text-blue",
    href: "#"
  }, "Send private message"))), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, "rdebbie_baker"), react_1["default"].createElement("td", null, react_1["default"].createElement("a", {
    className: "text-blue",
    href: "#"
  }, "Send private message"))), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "text-14 text-6E"
  }, "3 members")), react_1["default"].createElement("td", null))), react_1["default"].createElement("div", {
    className: "mt-1"
  }), react_1["default"].createElement("div", {
    className: "popup-inline-exa-footer"
  }, react_1["default"].createElement("div", {
    className: "row justify-content-between"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("a", {
    className: "btn--def",
    href: "#"
  }, react_1["default"].createElement("span", {
    className: "btn__text"
  }, "Leave group"))), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("a", {
    className: "btn btn--blue",
    href: "#",
    "data-fancybox-close": true
  }, react_1["default"].createElement("span", {
    className: "btn__text"
  }, "Close"))))))))), react_1["default"].createElement("tr", null, react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "row px-4 align-items-center"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, "Demo group name 3"), react_1["default"].createElement("div", {
    className: "col-auto sm-hide"
  }, react_1["default"].createElement("div", {
    className: "tag-new"
  }, "New")))), react_1["default"].createElement("td", null, "User"), react_1["default"].createElement("td", null, "14"), react_1["default"].createElement("td", null, react_1["default"].createElement("div", {
    className: "row px-8 align-items-center justify-content-end"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("a", {
    className: "action-decline",
    href: "#"
  }, "Decline")), react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("a", {
    className: "action-accept",
    href: "#"
  }, "Accept")))))))), react_1["default"].createElement("div", {
    className: "user-all-course mt30"
  }, react_1["default"].createElement("div", {
    className: "user-all-course__head"
  }, react_1["default"].createElement("div", {
    className: "row align-items-center px-8"
  }, react_1["default"].createElement("div", {
    className: "col-auto"
  }, react_1["default"].createElement("div", {
    className: "text-18 text-w-600"
  }, "My Achievements")))), react_1["default"].createElement("div", {
    className: "user-all-course__body"
  }, react_1["default"].createElement("div", {
    className: "row px-12 mt-24"
  }, react_1["default"].createElement("div", {
    className: "col-lg-4 col-md-6 mt24"
  }, react_1["default"].createElement("a", {
    className: "achievment-box",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "achievment-box__icon"
  }, react_1["default"].createElement("div", {
    className: "svg-image-bronze-ach"
  })), react_1["default"].createElement("div", {
    className: "achievment-box__cont"
  }, react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Award for Demo title"), react_1["default"].createElement("div", {
    className: "mt8 text-14 text-6E"
  }, "20 January 2021")))), react_1["default"].createElement("div", {
    className: "col-lg-4 col-md-6 mt24"
  }, react_1["default"].createElement("a", {
    className: "achievment-box",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "achievment-box__icon"
  }, react_1["default"].createElement("div", {
    className: "svg-image-silver-ach"
  })), react_1["default"].createElement("div", {
    className: "achievment-box__cont"
  }, react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Award for Demo title"), react_1["default"].createElement("div", {
    className: "mt8 text-14 text-6E"
  }, "20 January 2021")))), react_1["default"].createElement("div", {
    className: "col-lg-4 col-md-6 mt24"
  }, react_1["default"].createElement("a", {
    className: "achievment-box",
    href: "#"
  }, react_1["default"].createElement("div", {
    className: "achievment-box__icon"
  }, react_1["default"].createElement("div", {
    className: "svg-image-gold-ach"
  })), react_1["default"].createElement("div", {
    className: "achievment-box__cont"
  }, react_1["default"].createElement("div", {
    className: "text-16 text-w-600"
  }, "Award for Demo title"), react_1["default"].createElement("div", {
    className: "mt8 text-14 text-6E"
  }, "20 January 2021")))))))), react_1["default"].createElement("div", {
    className: "mt70"
  }))))));
};

exports["default"] = Index;

/***/ })

}]);
//# sourceMappingURL=resources_app_js_Pages_Dashboard_Index_copy_tsx.js.map