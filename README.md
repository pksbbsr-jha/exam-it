## Requirements
* PHP 8.1
* Postgresql 13

## Local setup

### Env
* copy `.env.example` to `.env`

### DB
* Create new DB in postgres
* update `.env` file

### Host files
* `sudo nano /etc/hosts`
* add `127.0.0.1 examit.test`**
* save

### Migrations
* run `php artisan migrate --seed`

### NPM
* `npm install`

### Run

Run 
* `php artisan serve` and app will be available on [http://examit.test:8000/](http://examit.test:8000/)
* `npm run hot` to work with react admin

### TODO: 
Package needs at least one of the following images optimizers

sudo apt-get install jpegoptim
sudo apt-get install optipng
sudo apt-get install pngquant
sudo npm install -g svgo@1.3.2
sudo apt-get install gifsicle
sudo apt-get install webp
