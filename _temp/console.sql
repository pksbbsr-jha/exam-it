DROP DATABASE test;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO examit_staging;

GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO examit;


SELECT  *
FROM pg_stat_activity
WHERE datname = 'examit_staging';

SELECT
    pg_terminate_backend (pid)
FROM
    pg_stat_activity
WHERE
        datname = 'examit';

ALTER DATABASE examit_old RENAME TO examit;
ALTER DATABASE examit_staging_old RENAME TO examit_staging;


SELECT * FROM pg_tables WHERE tableowner = 'examit_staging';
SELECT * FROM pg_tables WHERE tableowner = 'examit';

DO $$
    DECLARE row RECORD;
    BEGIN
        FOR row IN SELECT * FROM pg_tables
                   WHERE schemaname = 'public' AND tableowner = 'forge' LOOP
                EXECUTE FORMAT('ALTER TABLE %I.%I OWNER TO examit',row.schemaname,row.tablename);
            END LOOP;
    END;
$$;
