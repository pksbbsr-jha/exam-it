<x-auth-layout>
   <div class="b-wrap">
      <main class="main">
         <div class="grid-3-col grid-3-col-mb">
            <div class="form-top-panel sm-hide">
               <div class="text-14 tar">Already have an account? <a href="/login">Sign In</a></div>
            </div>
            <div class="mt24 sm-show"></div>
            <div class="container">
               <div class="w460">
                  <div class="logo tac"><img src="/assets/app/base/images/logo.svg" alt=""></div>
                  <div class="mt32">
                    <form method="POST" action="{{ route('register') }}" class="form__styler form-white-box password-strength-wrap">
                        @csrf
                        <div class="text-24 tac text-w-600">Create your ExamiT account</div>
                        <div class="row px-8 mt16">
                           <div class="col-md-6 mt16">
                              <a class="btn-social" href="#">
                                 <span class="btn__icon">
                                    <div class="svg-image-google"></div>
                                 </span>
                                 <span class="btn__text text-16 text-w-600">Google</span>
                              </a>
                           </div>
                           <div class="col-md-6 mt16">
                              <a class="btn-social" href="#">
                                 <span class="btn__icon">
                                    <div class="svg-image-facebook"></div>
                                 </span>
                                 <span class="btn__text text-16 text-w-600">Facebook</span>
                              </a>
                           </div>
                        </div>
                        <div class="mt32">
                           <div class="or-email"><span>or with email</span></div>
                        </div>
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />
                        <div class="mt24">
                            <x-label class="field-label" for="name" :value="__('Full name')" />
                            <div class="wpcf7-form-control-wrap">
                                <x-input id="name" class="wpcf7-form-control" placeholder="Full name" type="text" name="name" :value="old('name')" required autofocus />
                            </div>
                        </div>
                        <div class="mt24">
                            <x-label class="field-label" for="name" :value="__('Username')" />
                            <div class="wpcf7-form-control-wrap">
                                <x-input id="name" class="wpcf7-form-control" placeholder="Username" type="text" name="username" :value="old('username')" required />
                            </div>
                        </div>
                        <div class="mt24">
                            <x-label class="field-label" for="name" :value="__('Email')" />
                            <div class="wpcf7-form-control-wrap">
                                <x-input id="email" class="wpcf7-form-control" placeholder="example@email.com" type="email" name="email" :value="old('email')" required />
                            </div>
                        </div>
                        <div class="mt24">
                            <x-label class="field-label" for="password" :value="__('Password')" />
                            <div class="wpcf7-form-control-wrap">
                                <x-input id="password" class="wpcf7-form-control" placeholder="Enter a strong password" type="password" name="password" required autocomplete="new-password" />
                            </div>
                           <div class="mt12 row px-4">
                              <div class="col-3">
                                 <div class="password-strength password-strength-1"></div>
                              </div>
                              <div class="col-3">
                                 <div class="password-strength password-strength-2"></div>
                              </div>
                              <div class="col-3">
                                 <div class="password-strength password-strength-3"></div>
                              </div>
                              <div class="col-3">
                                 <div class="password-strength password-strength-4"></div>
                              </div>
                           </div>
                        </div>
                        <div class="mt24">
                            <x-label class="field-label" for="password_confirmation" :value="__('Confirm Password')" />
                            <div class="wpcf7-form-control-wrap">
                                <x-input id="password_confirmation" class="wpcf7-form-control" placeholder="Confirm your password" type="password" name="password_confirmation" required />
                            </div>
                        </div>
                        <div class="mt24 checkbox-styled"><span class="wpcf7-form-control-wrap"><span class="wpcf7-form-control wpcf7-checkbox">
                           <label>
                           <input type="checkbox"><span class="wpcf7-list-item-label">I accept the <a href="#">Terms of use </a>and the <a href="#">Privacy policy</a></span>
                           </label></span></span>
                        </div>
                        <div class="mt40"> 
                            <x-button type="submit"> {{ __('Register') }} </x-button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="form-top-panel sm-show">
               <div class="text-14 tar">Already have an account? <a href="/login">Sign In</a></div>
            </div>
            <div class="mt100 sm-hide"></div>
         </div>
      </main>
   </div>
</x-auth-layout>
<?php /*<x-auth-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div>
                <x-label for="name" :value="__('Name')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
            </div>

            <!-- Username -->
            <div class="mt-4">
                <x-label for="name" :value="__('Username')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="username" :value="old('username')" required autofocus />
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />
                <x-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Register') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-auth-layout>*/?>
