<x-auth-layout>
    <div class="b-wrap">
      <main class="main">
        <div class="grid-3-col">
          <div class="form-top-panel"></div>
          <div class="container">
            <div class="w460">
              <div class="logo tac"><img src="/assets/app/base/images/logo.svg" alt=""></div>
              <div class="mt32">
                <form class="form__styler form-white-box password-strength-wrap" method="POST" action="{{ route('password.email') }}">
                @csrf
                  <div class="text-24 text-w-600">Reset your password</div>
                  <div class="mt12 reset-desc">Enter the email address associated with your account and we'll send you a link to reset your password</div>
                  <x-auth-validation-errors class="mb-4" :errors="$errors" />
                  <div class="mt32">
                    <x-label class="field-label" for="email" :value="__('Email')" />
                    <div class="wpcf7-form-control-wrap">
                      <x-input id="email" class="wpcf7-form-control" type="email" name="email" :value="old('email')" placeholder="example@email.com" required autofocus />
                    </div>
                  </div>
                  <div class="mt40"> 
                    <x-button type="submit">{{ __('Email Password Reset Link') }}</x-button>
                  </div>
                  <div class="mt24 tac"><a href="/login">Return to sign in</a></div>
                </form>
              </div>
            </div>
          </div>
          <div class="mt100"></div>
        </div>
      </main>
    </div>
</x-auth-layout>
