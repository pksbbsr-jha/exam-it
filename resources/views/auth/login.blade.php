<x-auth-layout>
<main class="main">
        <div class="grid-3-col grid-3-col-mb">
          <div class="form-top-panel sm-hide">
            <div class="text-14 tar">Don’t have an account? <a href="/register">Sign up</a></div>
          </div>
          <div class="mt24 sm-show"></div>
          <div class="container">
            <div class="w460">
              <div class="logo tac"><img src="/assets/app/base/images/logo.svg" alt=""></div>
              <div class="mt32">
                <form method="POST" action="{{ route('login') }}" class="form__styler form-white-box password-strength-wrap">
                @csrf
                  <div class="text-24 tac text-w-600">Sign in to your account</div>
                  <div class="row px-8 mt16">
                    <div class="col-md-6 mt16"><a class="btn-social" href="#"> <span class="btn__icon">
                          <div class="svg-image-google"></div></span><span class="btn__text text-16 text-w-600">Google</span></a></div>
                    <div class="col-md-6 mt16"><a class="btn-social" href="#"> <span class="btn__icon">
                          <div class="svg-image-facebook"></div></span><span class="btn__text text-16 text-w-600">Facebook</span></a></div>
                  </div>
                  <div class="mt32">
                    <div class="or-email"><span>or with email</span></div>
                  </div>
                  <x-auth-validation-errors class="mb-4" :errors="$errors" />
                  <div class="mt24"> 
                    <x-label class="field-label" for="name" :value="__('Email')" />
                    <div class="wpcf7-form-control-wrap">
                        <x-input id="email" class="wpcf7-form-control" placeholder="example@email.com" type="email" name="email" :value="old('email')" required />
                    </div>
                  </div>
                  <div class="mt24"> 
                    <div class="row align-items-center justify-content-between">
                      <div class="col-auto">
                        <div class="field-label">Password</div>
                      </div>
                      <div class="col-auto"> <a href="/forgot-password">Forgot your password?</a></div>
                      <div class="col-12 mt8"></div>
                    </div>
                    <div class="wpcf7-form-control-wrap">
                      <x-input id="password" class="wpcf7-form-control wpcf7-password" placeholder="••••••••" type="password" name="password" required />
                    </div>
                  </div>
                  <div class="mt40"> 
                    <x-button type="submit">{{ __('Sign In') }}</x-button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="form-top-panel sm-show">
            <div class="text-14 tar">Already have an account? <a href="/register">Sign up</a></div>
          </div>
          <div class="mt100 sm-hide">                 </div>
        </div>
      </main>
    <?php /*<x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="current-password" />
            </div>

            <!-- Remember Me -->
            <div class="block mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-button class="ml-3">
                    {{ __('Log in') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>*/?>
</x-auth-layout>
