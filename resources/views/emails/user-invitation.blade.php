@php
    /* @var App\Models\User $inviter */
    /* @var App\Models\User $invitee */
@endphp

@component('mail::message')

Hello {{ $invitee->name }}.

{{ $inviter->name }} invited you to join {{ config('app.name') }} portal.

Click the link below to confirm your account.

@component('mail::button', ['url' => config('app.url')])
    Confirm account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
