<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="apple-touch-icon" href="{{ asset('/assets/app/base/images/favicons/apple-touch-icon.png')}}'">
    <link rel="apple-touch-icon" sizes="180x180"
          href="{{ asset('/assets/app/base/images/favicons/apple-touch-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"
          href="{{ asset('/assets/app/base/images/favicons/android-chrome-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="96x96"
          href="{{ asset('/assets/app/base/images/favicons/android-chrome-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="32x32"
          href="{{ asset('/assets/app/base/images/favicons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16"
          href="{{ asset('/assets/app/base/images/favicons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{ asset('/assets/app/base/images/favicons/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('/assets/app/base/images/favicons/mstile-144x144.png') }}">
    <meta name="msapplication-config" content="{{ asset('/assets/app/base/images/favicons/browserconfig.xml') }}">
    <meta name="theme-color" content="#fff">
    <link href="{{ asset('/assets/app/base/css/main.min.css') }}" rel="stylesheet"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
    <link href="https://fonts.googleapis.com/css2?family=Inter&amp;family=Mulish&amp;display=swap" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
    <link href="https://fonts.googleapis.com/css2?family=Inter&amp;family=Mulish&amp;display=swap" rel="stylesheet">

    {{--    <link href="{{ mix('/assets/app/css/app.css') }}" rel="stylesheet"/>--}}

    @routes

    <script crossorigin src="{{ mix('/assets/app/js/registartion.js') }}" defer></script>
    <script src="{{ asset('/assets/admin/tinymce/tinymce.min.js') }}" defer></script>
    <script src="https://www.wiris.net/demo/plugins/app/WIRISplugins.js?viewer=image&async=true"></script>

    @inertiaHead

</head>
<body>
<div class="b-wrap"> 
    @inertia
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="{{ asset('/assets/app/base/js/main.min.js') }}" defer></script>
</body>
</html>
