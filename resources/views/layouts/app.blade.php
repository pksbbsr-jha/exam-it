<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="apple-touch-icon" href="{{ asset('/assets/app/base/images/favicons/apple-touch-icon.png')}}'">
    <link rel="apple-touch-icon" sizes="180x180"
          href="{{ asset('/assets/app/base/images/favicons/apple-touch-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"
          href="{{ asset('/assets/app/base/images/favicons/android-chrome-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="96x96"
          href="{{ asset('/assets/app/base/images/favicons/android-chrome-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="32x32"
          href="{{ asset('/assets/app/base/images/favicons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16"
          href="{{ asset('/assets/app/base/images/favicons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{ asset('/assets/app/base/images/favicons/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('/assets/app/base/images/favicons/mstile-144x144.png') }}">
    <meta name="msapplication-config" content="{{ asset('/assets/app/base/images/favicons/browserconfig.xml') }}">
    <meta name="theme-color" content="#fff">
    <link href="{{ asset('/assets/app/base/css/main.min.css') }}" rel="stylesheet"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
    <link href="https://fonts.googleapis.com/css2?family=Inter&amp;family=Mulish&amp;display=swap" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
    <link href="https://fonts.googleapis.com/css2?family=Inter&amp;family=Mulish&amp;display=swap" rel="stylesheet">

    {{--    <link href="{{ mix('/assets/app/css/app.css') }}" rel="stylesheet"/>--}}

    @routes

    <script crossorigin src="{{ mix('/assets/app/js/app.js') }}" defer></script>
    <script src="{{ asset('/assets/admin/tinymce/tinymce.min.js') }}" defer></script>
    <script src="https://www.wiris.net/demo/plugins/app/WIRISplugins.js?viewer=image&async=true"></script>

    @inertiaHead

</head>
<body>
<div class="b-wrap">
    <header class="header">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-auto">
                    <div class="row align-items-center px-24">
                        <div class="col-auto">
                            <div class="logo"><a href="/"><img src="/assets/app/base/images/logo.svg" alt=""></a></div>
                        </div>
                        <?php if(Request::route()->getName() === 'app.index'){?>
                        <div class="col-auto md-hide">
                            <nav class="nav">
                                <ul class="nav-list"> 
                                    <li><a class="scrollto" href="#about">About</a></li>
                                    <li><a class="scrollto" href="#what-you-will-learn">What you will learn</a></li>
                                    <li><a class="scrollto" href="#testimonials">Testimonials</a></li>
                                    <li><a class="scrollto" href="#faq">FAQ</a></li>
                                </ul>
                            </nav>
                        </div>
                        <?php }?>
                    </div>
                </div>
                <div class="col-auto md-hide">
                    <div class="row px-16">
                        <div class="col-auto">
                            <a class="sh-menu-item" href="/dashboard">
                                <div class="row px-7 align-items-center">
                                    <div class="col-auto">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                  d="M11.41 3H12.59C12.8523 3.0011 13.1037 3.10526 13.29 3.29L20.12 10.12C20.6828 10.6821 20.9993 11.4446 21 12.24V20C21 20.5523 20.5523 21 20 21H15C14.4477 21 14 20.5523 14 20V14.25C14 14.1119 13.8881 14 13.75 14H10.25C10.1119 14 10 14.1119 10 14.25V20C10 20.5523 9.55228 21 9 21H4C3.44772 21 3 20.5523 3 20V12.24C3.0007 11.4446 3.31723 10.6821 3.88 10.12L10.71 3.29C10.8963 3.10526 11.1477 3.0011 11.41 3ZM18.71 11.54L12.17 5H11.83L5.29 11.54C5.10526 11.7263 5.0011 11.9777 5 12.24V19H8V13C8 12.4477 8.44772 12 9 12H15C15.5523 12 16 12.4477 16 13V19H19V12.24C18.9989 11.9777 18.8947 11.7263 18.71 11.54Z"
                                                  fill="#121110"></path>
                                        </svg>
                                    </div>
                                    <div class="col">
                                        <div class="text-16 text-w-600">Dashboard</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-auto">
                            <a class="sh-menu-item" href="/courses">
                                <div class="row px-7 align-items-center">
                                    <div class="col-auto">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                  d="M21 4H15C13.8495 4.00398 12.7564 4.50315 12 5.37C11.2436 4.50315 10.1505 4.00398 9 4H3C2.44772 4 2 4.44772 2 5V18C2 18.5523 2.44772 19 3 19H8.59C8.85234 19.0011 9.10374 19.1053 9.29 19.29L9.71 19.71C9.89626 19.8947 10.1477 19.9989 10.41 20H13.59C13.8523 19.9989 14.1037 19.8947 14.29 19.71L14.71 19.29C14.8963 19.1053 15.1477 19.0011 15.41 19H21C21.5523 19 22 18.5523 22 18V5C22 4.44772 21.5523 4 21 4ZM11 16.54C10.3923 16.1875 9.70252 16.0012 9 16H4V6H9C10.1046 6 11 6.89543 11 8V16.54ZM15 16H20V6H15C13.8954 6 13 6.89543 13 8V16.54C13.6077 16.1875 14.2975 16.0012 15 16Z"
                                                  fill="#121110"></path>
                                        </svg>
                                    </div>
                                    <div class="col">
                                        <div class="text-16 text-w-600">Courses</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-auto">
                            <a class="sh-menu-item" href="/dashboard-discussion">
                                <div class="row px-7 align-items-center">
                                    <div class="col-auto">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M19.0164 11.9857C19.0164 14.1917 17.2239 15.98 15.0127 15.98H6.00455C5.20238 15.9648 4.42744 16.2704 3.85259 16.8288L1.85077 18.826C1.75902 18.921 1.63269 18.975 1.50045 18.9757C1.22406 18.9757 1 18.7522 1 18.4765V5.99429C1 3.78831 2.79249 2 5.00364 2H15.0127C17.2239 2 19.0164 3.78831 19.0164 5.99429V11.9857ZM3.00182 5.99429V14.9815C3.86809 14.3333 4.92171 13.9829 6.00455 13.9829H15.0127C16.1183 13.9829 17.0146 13.0887 17.0146 11.9857V5.99429C17.0146 4.8913 16.1183 3.99715 15.0127 3.99715H5.00364C3.89807 3.99715 3.00182 4.8913 3.00182 5.99429ZM13.5114 9.98859H14.5123C14.7887 9.98859 15.0127 9.76505 15.0127 9.4893V8.49073C15.0127 8.21498 14.7887 7.99144 14.5123 7.99144H13.5114C13.235 7.99144 13.0109 8.21498 13.0109 8.49073V9.4893C13.0109 9.76505 13.235 9.98859 13.5114 9.98859ZM10.5086 9.98859H9.50773C9.23134 9.98859 9.00728 9.76505 9.00728 9.4893V8.49073C9.00728 8.21498 9.23134 7.99144 9.50773 7.99144H10.5086C10.785 7.99144 11.0091 8.21498 11.0091 8.49073V9.4893C11.0091 9.76505 10.785 9.98859 10.5086 9.98859ZM7.00546 9.4893V8.49073C7.00546 8.21498 6.7814 7.99144 6.505 7.99144H5.50409C5.2277 7.99144 5.00364 8.21498 5.00364 8.49073V9.4893C5.00364 9.76505 5.2277 9.98859 5.50409 9.98859H6.505C6.7814 9.98859 7.00546 9.76505 7.00546 9.4893ZM17.0146 17.9772C19.2257 17.9772 21.0182 16.1889 21.0182 13.9829V5.99429C22.1159 6.00525 23.0001 6.8962 23 7.99144V22.5007C23 22.7765 22.7759 23 22.4995 23C22.3673 22.9992 22.241 22.9452 22.1492 22.8502L20.1474 20.8531C19.5824 20.2886 18.815 19.9724 18.0155 19.9743H7.00546C5.89989 19.9743 5.00364 19.0802 5.00364 17.9772H17.0146Z" fill="#121110"></path>
                                        </svg>
                                    </div>
                                    <div class="col">
                                        <div class="text-16 text-w-600">Message Board</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php if(Auth::check()){?>
                <div class="col-auto">
                    <div class="md-show">
                        <div class="svg-image-menu hamburger"></div>
                    </div>
                    <div class="row px-12 md-hide">
                        <div class="col-auto">
                            <a href="#">
                                <div class="svg-image-notifications_outlined"></div>
                            </a>
                        </div>
                        <div class="col-auto">
                            <a class="user-menu" href="#">
                                <div class="row no-gutters">
                                    <div class="col-auto">
                                        <div class="svg-image-account_circle"></div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="svg-image-caret_down"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php  } else {?>
                    <div class="col-auto">
                        <div class="md-show"> 
                            <div class="svg-image-menu hamburger"></div>
                        </div>
                        <div class="row px-12 md-hide">
                            <div class="col-auto"> <a class="btn btn--white" href="/login"> <span class="btn__text">Log In</span></a></div>
                            <div class="col-auto"> <a class="btn btn--blue" href="/register"> <span class="btn__text">Sign Up</span></a></div>
                        </div>
                    </div>
                <?php }?>
            </div>
        </div>
    </header>
    <div class="mobile-menu-area">
        <div class="mobile-menu-area-inner">
            <div class="grid-row-2">
                <div class="grid-row-el">
                    <ul class="list mobile-menu-list">
                        <li><a class="mb-menu-item" href="#">
                                <div class="row px-6 align-items-center">
                                    <div class="col-auto">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                  d="M11.41 3H12.59C12.8523 3.0011 13.1037 3.10526 13.29 3.29L20.12 10.12C20.6828 10.6821 20.9993 11.4446 21 12.24V20C21 20.5523 20.5523 21 20 21H15C14.4477 21 14 20.5523 14 20V14.25C14 14.1119 13.8881 14 13.75 14H10.25C10.1119 14 10 14.1119 10 14.25V20C10 20.5523 9.55228 21 9 21H4C3.44772 21 3 20.5523 3 20V12.24C3.0007 11.4446 3.31723 10.6821 3.88 10.12L10.71 3.29C10.8963 3.10526 11.1477 3.0011 11.41 3ZM18.71 11.54L12.17 5H11.83L5.29 11.54C5.10526 11.7263 5.0011 11.9777 5 12.24V19H8V13C8 12.4477 8.44772 12 9 12H15C15.5523 12 16 12.4477 16 13V19H19V12.24C18.9989 11.9777 18.8947 11.7263 18.71 11.54Z"
                                                  fill="#121110"></path>
                                        </svg>
                                    </div>
                                    <div class="col-auto">Dashboard</div>
                                </div>
                            </a></li>
                        <li class="current-menu-item"><a class="mb-menu-item" href="#">
                                <div class="row px-6 align-items-center">
                                    <div class="col-auto">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                  d="M21 4H15C13.8495 4.00398 12.7564 4.50315 12 5.37C11.2436 4.50315 10.1505 4.00398 9 4H3C2.44772 4 2 4.44772 2 5V18C2 18.5523 2.44772 19 3 19H8.59C8.85234 19.0011 9.10374 19.1053 9.29 19.29L9.71 19.71C9.89626 19.8947 10.1477 19.9989 10.41 20H13.59C13.8523 19.9989 14.1037 19.8947 14.29 19.71L14.71 19.29C14.8963 19.1053 15.1477 19.0011 15.41 19H21C21.5523 19 22 18.5523 22 18V5C22 4.44772 21.5523 4 21 4ZM11 16.54C10.3923 16.1875 9.70252 16.0012 9 16H4V6H9C10.1046 6 11 6.89543 11 8V16.54ZM15 16H20V6H15C13.8954 6 13 6.89543 13 8V16.54C13.6077 16.1875 14.2975 16.0012 15 16Z"
                                                  fill="#121110"></path>
                                        </svg>
                                    </div>
                                    <div class="col-auto">Courses</div>
                                </div>
                            </a></li>
                        <li><a class="mb-menu-item" href="#">
                                <div class="row px-6 align-items-center">
                                    <div class="col-auto">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                  d="M12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 9.34784 20.9464 6.8043 19.0711 4.92893C17.1957 3.05357 14.6522 2 12 2ZM12 5C13.6569 5 15 6.34315 15 8C15 9.65685 13.6569 11 12 11C10.3431 11 9 9.65685 9 8C9 6.34315 10.3431 5 12 5ZM12 18.9868C14.2151 18.9868 16.2994 17.9384 17.62 16.16C17.8246 15.8609 17.8514 15.4745 17.69 15.15L17.46 14.68C16.9568 13.6531 15.9136 13.0015 14.77 13H9.23C8.07012 13.0012 7.01488 13.671 6.52 14.72L6.31 15.16C6.15251 15.4818 6.17921 15.8633 6.38 16.16C7.70056 17.9384 9.78493 18.9868 12 18.9868Z"
                                                  fill="#121110"></path>
                                        </svg>
                                    </div>
                                    <div class="col-auto">My Account</div>
                                </div>
                            </a></li>
                    </ul>
                </div>
                <div class="grid-row-el">
                    <ul class="list">
                        <li><a class="mb-menu-item" href="#">
                                <div class="row px-6 align-items-center">
                                    <div class="col-auto">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                  d="M5 19H10.5C10.7761 19 11 19.2239 11 19.5V20.5C11 20.7761 10.7761 21 10.5 21H5C3.89543 21 3 20.1046 3 19V5C3 3.89543 3.89543 3 5 3H10.5C10.7761 3 11 3.22386 11 3.5V4.5C11 4.77614 10.7761 5 10.5 5H5V19ZM15.64 6.15L20.78 11.28C20.9189 11.4217 20.9977 11.6116 21 11.81V12.19C20.9998 12.3888 20.9207 12.5795 20.78 12.72L15.64 17.85C15.5461 17.9447 15.4183 17.9979 15.285 17.9979C15.1517 17.9979 15.0239 17.9447 14.93 17.85L14.22 17.15C14.1253 17.0561 14.0721 16.9283 14.0721 16.795C14.0721 16.6617 14.1253 16.5339 14.22 16.44L17.67 13H7.5C7.22386 13 7 12.7761 7 12.5V11.5C7 11.2239 7.22386 11 7.5 11H17.67L14.22 7.56C14.1259 7.46784 14.0729 7.34169 14.0729 7.21C14.0729 7.07831 14.1259 6.95216 14.22 6.86L14.93 6.15C15.0239 6.05534 15.1517 6.0021 15.285 6.0021C15.4183 6.0021 15.5461 6.05534 15.64 6.15Z"
                                                  fill="#121110"></path>
                                        </svg>
                                    </div>
                                    <div class="col-auto">Log Out</div>
                                </div>
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @inertia
    <footer class="footer text-white">
        <div class="container">
            <div class="row px-48 justify-content-between mt-30">
                <div class="col-xl-3 col-md-6 mt30">
                    <div class="footer-logo"><img src="/assets/app/base/images/footer-logo.svg" alt=""></div>
                    <div class="mt15 text-14">Company address will be here</div>
                    <div class="mt24">
                        <div class="mt16 text-16">
                            <div class="row align-items-center px-6">
                                <div class="col-auto">
                                    <div class="svg-image-icon-email"></div>
                                </div>
                                <div class="col"><a href="mailto:example@mail.com">example@mail.com</a></div>
                            </div>
                        </div>
                        <div class="mt16 text-16">
                            <div class="row align-items-center px-6">
                                <div class="col-auto">
                                    <div class="svg-image-icon-tel"></div>
                                </div>
                                <div class="col"><a href="tel:+10000000000">+1 000 000 0000</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-auto col-md-6 mt30">
                    <div class="wf-lg-360">
                        <h4 class="text-21">Subscribe and stay tuned!</h4>
                        <form class="mt23 form-subscribe" action="#">
                            <div class="wpcf7-form-control-wrap email">
                                <input class="wpcf7-form-control wpcf7-email" type="email" placeholder="email">
                            </div>
                            <input class="wpcf7-form-control wpcf7-submit" type="submit" value="Send">
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 mt30">
                    <div class="row mt-21 px-48 social-row">
                        <div class="col-6 mt21"><a class="d-inline-block" href="#">
                                <div class="row align-items-center px-5 flex-nowrap">
                                    <div class="col-auto">
                                        <div class="wf20">
                                            <div class="svg-image-fb"></div>
                                        </div>
                                    </div>
                                    <div class="col text-mulish text-light-blue">Facebook</div>
                                </div>
                            </a></div>
                        <div class="col-6 mt21"><a class="d-inline-block" href="#">
                                <div class="row align-items-center px-5 flex-nowrap">
                                    <div class="col-auto">
                                        <div class="wf20">
                                            <div class="svg-image-inst"></div>
                                        </div>
                                    </div>
                                    <div class="col text-mulish text-light-blue">Instagram</div>
                                </div>
                            </a></div>
                        <div class="col-6 mt21"><a class="d-inline-block" href="#">
                                <div class="row align-items-center px-5 flex-nowrap">
                                    <div class="col-auto">
                                        <div class="wf20">
                                            <div class="svg-image-tw"></div>
                                        </div>
                                    </div>
                                    <div class="col text-mulish text-light-blue">Twitter</div>
                                </div>
                            </a></div>
                        <div class="col-6 mt21"><a class="d-inline-block" href="#">
                                <div class="row align-items-center px-5 flex-nowrap">
                                    <div class="col-auto">
                                        <div class="wf20">
                                            <div class="svg-image-in"></div>
                                        </div>
                                    </div>
                                    <div class="col text-mulish text-light-blue">Linkedin</div>
                                </div>
                            </a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
            <div class="container">
                <div class="row justify-content-between mt-16">
                    <div class="col-md-auto mt16"> © 2022. ExamiT. All Rights Reserved.</div>
                    <div class="col-md-auto mt16">
                        <div class="row px-8">
                            <div class="col-auto"><a href="#">Terms & Conditions</a></div>
                            <div class="col-auto"><a href="#">Privacy Policy</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>


<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="{{ asset('/assets/app/base/js/main.min.js') }}" defer></script>
</body>
</html>
