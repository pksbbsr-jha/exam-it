<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
{{--    <link href="{{ asset('/assets/admin/tinymce/skins/ui/oxide/content.min.css') }}" rel="stylesheet"/>--}}
    <link href="{{ mix('/assets/admin/css/app.css') }}" rel="stylesheet"/>
    @routes
    <script crossorigin src="{{ mix('/assets/admin/js/app.js') }}" defer></script>
    <script src="{{ asset('/assets/admin/tinymce/tinymce.min.js') }}" defer></script>
    <script src="https://www.wiris.net/demo/plugins/app/WIRISplugins.js?viewer=image&async=true"></script>
    @inertiaHead
</head>
<body>
@inertia
</body>
</html>
