

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="format-detection" content="telephone=no">
        <link rel="apple-touch-icon" href="{{ asset('/assets/app/base/images/favicons/apple-touch-icon.png')}}'">
        <link rel="apple-touch-icon" sizes="180x180"  href="{{ asset('/assets/app/base/images/favicons/apple-touch-icon-180x180.png')}}">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('/assets/app/base/images/favicons/android-chrome-192x192.png')}}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/assets/app/base/images/favicons/android-chrome-96x96.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/assets/app/base/images/favicons/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/assets/app/base/images/favicons/favicon-16x16.png')}}">
        <link rel="manifest" href="{{ asset('/assets/app/base/images/favicons/manifest.json')}}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('/assets/app/base/images/favicons/mstile-144x144.png') }}">
        <meta name="msapplication-config" content="{{ asset('/assets/app/base/images/favicons/browserconfig.xml') }}">
        <meta name="theme-color" content="#fff">
        <link href="{{ asset('/assets/app/base/css/main.min.css') }}" rel="stylesheet"/>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
        <link href="https://fonts.googleapis.com/css2?family=Inter&amp;family=Mulish&amp;display=swap" rel="stylesheet">

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">
        <link href="https://fonts.googleapis.com/css2?family=Inter&amp;family=Mulish&amp;display=swap" rel="stylesheet">
        
    </head>
    <body>
        <div class="page-404">
            <div class="b-wrap">
                <main class="main">
                    <div class="error-box">
                        <div class="container">
                            <div class="logo tac"><img src="/assets/app/base/images/logo.svg" alt=""></div>
                            <div class="oops mt40">
                            <div class="tac"> <img src="/assets/app/base/images/oops.svg" alt=""></div>
                            <div class="mt32 tac">
                                <h3 class="text-21 text-nova text-w-600">Something went wrong...</h3>
                                <div class="tac text-16 mt12 text-6E">
                                    <p>Please try again or report an issue to support</p>
                                    <p class="text-center text-bold">@yield('code') | @yield('message')</p>
                                </div>
                                <div class="mt32">
                                <div class="row px-12 justify-content-center mt-20">
                                    <div class="col-md-auto mt20"><a class="btn btn--sm btn--white mb-wide" href="#"> <span class="btn__text">Try Again</span></a></div>
                                    <div class="col-md-auto mt20"><a class="btn btn--sm btn--blue mb-wide" href="/"> <span class="btn__text">Back to Homepage</span></a></div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </body>
</html>
