document.addEventListener("DOMContentLoaded", function(event) {
    imageOffsetRight();
    imageOffsetLeft();



    $('.hamburger').on('click', function(){
        $('.hamburger, html, body').toggleClass('is-active');
        $('.mobile-menu-area').fadeToggle();
    });

    if($('.header').length) {
        getHeightHeader();
    }

    $(".vc").on('click', function(){
        $(".vc").fadeOut();
        $('html, body').removeClass('is-active');
    })
    if($(".vc").is(':visible')){
        $('html, body').addClass('is-active');
    }
    $(".just-close-popup").on('click', function(){
        $(".quize-popup").fadeOut();
    })

    if($('.start-learn-course').length) {
        $('.b-wrap').css({
            'overflow': 'visible'
        })
    }

    

    if(window.matchMedia('(max-width: 767px)').matches){
        $(".return-to-chat-list").on('click', function(){
            $(".dashboard-chat-area-right").fadeOut();
        });
        $(".chat-item").on('click', function(e){
            e.preventDefault();
            $(".dashboard-chat-area-right").fadeIn();
        });

        $(".filter-dropdown__title").removeClass('is-active');
        $(".filter-dropdown__body").hide();
    }

    $('.scrollto').on('click', function(){
        let hHeader = $('.header').outerHeight();
        let offset = hHeader - 2;
        let target = $(this).attr('href');
        let $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - offset
        }, 800, 'swing');

        $('.hamburger, html, body').removeClass('is-active');
        $('.mobile-menu-area').fadeOut();

    });

    const header = document.querySelector(".header");

    const headroom = new Headroom(header, {
    tolerance : 10,
    // onNotTop : function() {
    //     setTimeout(function(){
    //         getHeightHeader();
    //     }, 400)
    // },
    onTop : function() {
        setTimeout(function(){
            getHeightHeader();
        }, 400)
    },
    });
    headroom.init();
    
});

//header height
function getHeightHeader() {
    let hHeader = $('.header').outerHeight();
    $(":root").css("--headerHeight", hHeader + 'px');
};


//header height

// RESIZE OPTIMIZATION
(function() {
    let throttle = function(type, name, obj) {
        obj = obj || window;
        let running = false;
        let func = function() {
            if (running) { return; }
            running = true;
             requestAnimationFrame(function() {
                obj.dispatchEvent(new CustomEvent(name));
                running = false;
            });
        };
        obj.addEventListener(type, func);
    };
    throttle("resize", "optimizedResize");
})();
window.addEventListener("optimizedResize", function() {
    imageOffsetRight();
    imageOffsetLeft();
    getHeightHeader();
    appHeight();
});



//FUNCTIONS

function imageOffsetRight(){
    let $imageRightWide = $('.image-right');
    if($imageRightWide.length) {

        $imageRightWide.each(function(){
            let $this = $(this);
            let rt = ($(window).width() - ($this.offset().left + $this.outerWidth()));

            $this.css({
                'width': $this.outerWidth() + rt + 'px'
            })
        })
        
    }
}
function imageOffsetLeft(){
    let $imageLeftWide = $('.image-left');
    if($imageLeftWide.length) {

        $imageLeftWide.each(function(){
            let $this = $(this);
            let lt = $this.offset().left;

            $this.css({
                'width': $this.outerWidth() + lt + 'px'
            })
        })

        
    }
}



const swiperBq = new Swiper('.swiper-bq-slider', {
    effect: 'fade',
    autoHeight: true,
    fadeEffect: {
        crossFade: true
    },
    loop: true,
    pagination: {
      el: '.swiper-bq-paginations',
      clickable: true
    },
    navigation: {
      nextEl: '.svg-image-arrow_prev',
      prevEl: '.svg-image-arrow_next',
    },
});


$(".accordion-title").on("click", function(e) {

    e.preventDefault();
    let $this = $(this);

    if (!$this.hasClass("is-active")) {
        $(".accordion-body").slideUp(400);
        $(".accordion-title").removeClass("is-active");
    }

    $this.toggleClass("is-active");
    $this.next().slideToggle();
});
$(".course-list-title").on("click", function(e) {

    e.preventDefault();
    let $this = $(this);

    if (!$this.hasClass("is-active")) {
        $(".course-list-content").slideUp(400);
        $(".course-list-title").removeClass("is-active");
    }

    $this.toggleClass("is-active");
    $this.next().slideToggle();
});


$(".filter-dropdown__title").on("click", function(e) {

    e.preventDefault();
    let $this = $(this);

    if (!$this.hasClass("is-active")) {
        $(".filter-dropdown__body").slideUp(400);
        $(".filter-dropdown__title").removeClass("is-active");
    }

    $this.toggleClass("is-active");
    $this.next().slideToggle();
});

$('[data-fancybox]').fancybox({
    hideScrollbar: false,
    baseClass: "exa-popup",
    animationEffect: "zoom-in-out",
    animationDuration: 500,
    transitionDuration: 300,
});

$('select').selectric({
    nativeOnMobile: false
});
$('.select-group').selectric({
    nativeOnMobile: false,
    labelBuilder: '{value}',
    optionsItemBuilder: function(itemData) {
                            return '<span>' + itemData.text + '</span>' + '<span>' + itemData.alt + '</span>';
    },
    onInit: function() {
        $(".group-select-create").insertBefore( ".discuss-theme-item .selectric-scroll" );
    }
});

// $('.select-group').on('selectric-open', function(event, element, selectric) {
//     console.log(selectric)
// });


$('.view-exp').on('click', function(e){
    e.preventDefault();
    $(this).next('.explanation-box').fadeToggle();

    $(this).find('.btn__text').text(($(this).find('.btn__text').text() == "View explanation") ? "Hide explanation" : "View explanation").fadeIn();
})
$('.svg-image-close.demo').trigger('click');
$('.close-quize').on('click', function(){
    $('.quize-popup').fadeOut();
    $.fancybox.close();
    $(":root").css("--offsetRight", 0  + 'px');
    $('.w945').removeClass('is-change');
    $('.vc').fadeOut();
    $('html, body').removeClass('is-active');
});

const offsetRight = () => {
    let popupWidth = $('.quize-popup').outerWidth();
    let contWidth = $('.cont-ch-wrap').innerWidth();
    let windowWidth = $(window).width();
    
    let offsetRightQ = popupWidth - ((windowWidth - contWidth) / 2) + 30
    $(":root").css("--offsetRight", offsetRightQ  + 'px');
}
offsetRight();

if($('.quize-popup').is(":visible")){
    $('.w945').addClass('is-change');

    window.addEventListener("optimizedResize", function() {
        offsetRight();
    });
    
}


$('.toggleDiscussionInWishlist').on('click', function(){
    $(this).toggleClass('is-active')
})

tippy('.discuss-user-tippy', {
	content(reference) {
		const id = reference.getAttribute('data-template');
		const template = document.getElementById(id);
		return template.innerHTML;
	},
	allowHTML: true,
	interactive: true,
	placement: 'top',
	animation: false,
	theme: 'white-theme',
	trigger: 'click'
});
tippy('.button-tippy', {
	content(reference) {
		const id = reference.getAttribute('data-template');
		const template = document.getElementById(id);
		return template.innerHTML;
	},
	allowHTML: true,
	interactive: true,
	placement: 'bottom-end',
	animation: false,
	theme: 'btn-theme',
	trigger: 'click'
});


// $(function() {

//     function passwordCheck(password) {
//       if (password.length >= 8)
//         strength += 1;
  
//       if (password.match(/(?=.*[0-9])/))
//         strength += 1;
  
//     //   if (password.match(/(?=.*[!,%,&,@,#,$,^,*,?,_,~,<,>,])/))
//     //     strength += 1;
  
//       if (password.match(/(?=.*[a-z])/))
//         strength += 1;
  
//       if (password.match(/(?=.*[A-Z])/))
//         strength += 1;
  
//       displayBar(strength);
//     }
  
//     function displayBar(strength) {
//       switch (strength) {
//         case 1:
//             $("#password-strength span").css({
//             "width": "20%",
//             "background": "#de1616"
//             });
//             break;

//         case 2:
//             $("#password-strength span").css({
//             "width": "40%",
//             "background": "#de1616"
//             });
//             break;

//         case 3:
//             $("#password-strength span").css({
//             "width": "60%",
//             "background": "#de1616"
//             });
//             break;

//         case 4:
//             $("#password-strength span").css({
//             "width": "80%",
//             "background": "#FFA200"
//             });
//             break;

//         default:
//             $("#password-strength span").css({
//             "width": "0",
//             "background": "#de1616"
//             });
//       }
//     }
  
  
   
  
//     $(".password-strength-wrap .wpcf7-password").keyup(function() {
//       strength = 0;
//       let password = $(this).val();
//       passwordCheck(password);
//     });
  
//   });


$('.discuss-theme-create-select select').on('change', function(){
    
	if($(this).children('[value="Group"]').is(':selected')) {	
        $('.discuss-theme-item').hide();
        $('.group-select').show();
	} else if($(this).children('[value="Private"]').is(':selected')){
        $('.discuss-theme-item').hide();
        $('.private-select').show();
    } else {
        $('.discuss-theme-item').hide();
    }
})
if($('.discuss-theme-create-select select').children('[value="Group"]').is(':selected')) {
    $('.group-select').show();
} else if($('.discuss-theme-create-select select').children('[value="Private"]').is(':selected')){
    $('.private-select').show();
} else {
    $('.discuss-theme-item').hide();
}



$(".btn-emulate-file-select, .user-photo").on('click', function(){
    $('.user-select-photo input').trigger('click');
})

const appHeight = () => {
    const doc = document.documentElement
    doc.style.setProperty(`--appheight`, `${window.innerHeight}px`)
   }
appHeight();


$('.back-to-module').on('click', function(e){
    e.preventDefault();
    let $this = $(this);
    let $thisId = $this.attr('href');
    let hHeader = $('.header').outerHeight();
    $([document.documentElement, document.body]).animate({
        scrollTop: $($thisId).offset().top - hHeader
    }, 1000);
});
$('.next-question').on('click', function(e){
    
    e.preventDefault();
    $('.vc').fadeIn();
    
    let $this = $(this);
    let $thisId = $this.attr('href');
    let hHeader = $('.header').outerHeight();
    $([document.documentElement, document.body]).animate({
        scrollTop: $($thisId).offset().top - hHeader
    }, 1000);
    $('html, body').addClass('is-active');
});

if($('.sub-module-content').length) {
    enterView({
        selector: '.sub-module-content',
        once: false,
        offset: .5,
        progress: function(el) {
            let $this = $(el);
            let $thisContent = $(el).find('.sub-module-content__text');
            let $id = $this.attr('id');
            let $idNext = $this.next('.sub-module-content').attr('id');
            let insertToContent = $(".popup-module-content-onScroll")
            
            let cloneCurrentContent = $thisContent.clone()
            insertToContent.html(cloneCurrentContent)
            // setTimeout(function(){
                // $('.popup-module-content-onScroll').addClass('is-active');
            // }, 500)
            
    
            $('.back-to-module').attr('href', '#'+$id);
            if($idNext !== undefined) {
                $('.next-question').attr('href', '#'+$idNext);
            }
            
        }
    });
}
if($('.back-to-module').length) {
    let contEl = $('.sub-module-content').eq(1)
    new Waypoint({
        element: contEl,
        handler: function(direction) {
            if(direction === 'down') {
                $('.back-to-module').fadeIn();
            } else {
                $('.back-to-module').fadeOut();
            }
            
        },
        offset: '50%' 
    })
}


// $('.btn__text:contains(" for "), .btn__text:contains(" a "), .btn__text:contains(" an "), .btn__text:contains(" the ")').contents().each(function () {
//     if (this.nodeType == 3) {
//         $(this).parent().html(function (_, oldValue) {
//             return oldValue.replace(/ for /g, "<span class='text-small'>$&</span>").replace(/ a /g, "<span class='text-small'>$&</span>")
//             .replace(/ a /g, "<span class='text-small'>$&</span>")
//             .replace(/ an /g, "<span class='text-small'>$&</span>")
//             .replace(/ the /g, "<span class='text-small'>$&</span>")
//         })
//     }
// });
// $('.btn__text:contains("a")').contents().each(function () {
//     if (this.nodeType == 3) {
//         $(this).parent().html(function (_, oldValue) {
//             return oldValue.replace(/a/g, "<span class='text-small'>$&</span>")
//         })
//     }
// });
// $('.btn__text:contains("the")').contents().each(function () {
//     if (this.nodeType == 3) {
//         $(this).parent().html(function (_, oldValue) {
//             return oldValue.replace(/the/g, "<span class='text-small'>$&</span>")
//         })
//     }
// });
