import {InertiaApp} from '@inertiajs/inertia-react'
import React from 'react'
import {render} from 'react-dom'

import {InertiaProgress} from '@inertiajs/progress'
import moment from 'moment';
import * as Sentry from '@sentry/react';
import {BrowserTracing} from '@sentry/tracing';

const app = document.getElementById('app')

moment.locale('en_US');


Sentry.init({
    dsn: "https://6db8534671cb4ab78c982f666df98647@o1164505.ingest.sentry.io/6255109",
    integrations: [new BrowserTracing()],

    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    tracesSampleRate: 1.0,
});


// noinspection JSCheckFunctionSignatures,RequiredAttributes
render(
    <InertiaApp
        initialPage={JSON.parse(app.dataset.page)}
        resolveComponent={name => import(`./Pages/${name}`).then(module => module.default)}
    />,
    app
)

InertiaProgress.init();
