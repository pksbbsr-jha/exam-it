import React from 'react';

type LatestDiscussion = {
    discussion: {
        topic: string,
        description: string,
        type: string,
        replies: string,
        asked_by: string
    }
}
const LatestDiscussion = (props:LatestDiscussion) => {

    return (
        <>
            <div className="col-12 mt16">
                <div className="discussion-item">
                    <div className="row px-6">
                        <div className="col">
                            <div className="w384">
                                <div className="text-14 text-9A sm-hide">{props.discussion.topic}</div>
                                <div className="mt8 sm-hide"></div><a className="text-16 text-w-600" href="#">{props.discussion.description}</a>
                            </div>
                        </div>
                        <div className="col-auto">
                            <button className="toggleDiscussionInWishlist" type="button"></button>
                        </div>
                    </div>
                    <div className="mt12 row px-6 align-items-center">
                        <div className="col">
                            <div className="row px-6 align-items-center">
                                <div className="col-auto">
                                    <div className="discussion-tag discussion-tag-private">{props.discussion.type}</div>
                                </div>
                                <div className="col-auto">
                                    <div className="text-14 text-9A">{props.discussion.replies}</div>
                                </div>
                            </div>
                        </div>
                        <div className="col-auto">
                            <div className="discussion-user-ask">asked by <a href="#">{props.discussion.asked_by}</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default LatestDiscussion;
