import React from 'react';
import { Link } from '@inertiajs/inertia-react'

type CourseProps = {
    course: {
        id: number,
        name: string,
        img:{
            src: string, 
            sourceSet:{}[], 
            srcSet: {}[]
        }
    }
};

const Course = (props: CourseProps) => {
    const course = props.course;
    return (
        <>
            <div className="col-lg-4 col-md-6 mt24">
                <div className="start-learn-course start-learn-course--open z10">
                    <div className="learn-course-present-top text-white">
                        <div className="learn-course-present-desc_image">
                            <picture>
                                <source srcSet={`/assets/app/base/images/${props.course.img.sourceSet[0]}, /assets/app/base/images/${props.course.img.sourceSet[1]} 2x`} type="image/webp" /><img className="responsive-img cover-img" src={`/assets/app/base/images/${props.course.img.src}`}  srcSet={`/assets/app/base/images/${props.course.img.srcSet[0]}, /assets/app/base/images/${props.course.img.srcSet[1]}`} alt="" />
                            </picture>
                        </div>
                    </div>
                    <div className="start-learn-bottom">
                        <div className="text-16 text-w-600">{props.course.name}</div>
                        <div className="course-progress-wrap mt16">
                            <div className="row justify-content-between">
                                <div className="col-auto">
                                    <div className="text-14">33%</div>
                                </div>
                                <div className="col-auto">
                                    <div className="text-14 text-9A">3/10 modules</div>
                                </div>
                            </div>
                            <div className="mt8">
                                <div className="progress-course">
                                    <div className="cssProgress-success" style={{width:`33%`}}></div>
                                </div>
                            </div>
                        </div><Link className="mt32 btn btn--sm btn--wide btn--blue" href={`/course/detail/${props.course.id}`}><span className="btn__text">View</span></Link><Link className="mt16 btn btn--sm btn--white btn--wide" href="#"> <span className="btn__text">Random Question Generator</span></Link>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Course;
