import React from 'react';

type ChatMessage = {
    message: {
        id: number,
        name: string,
        message: string,
        val: number,
        img:{
            src: string, 
            sourceSet:{}[], 
            srcSet: {}[]
        }
    }
};

const ChatMessage = (props: ChatMessage) => {
    const message = props.message;
    return (
        <>
            <div className="dashboard-chat-companion">
                <a className="chat-item" href="#">
                    <div className="row px-6">
                        <div className="col-auto">
                            <div className="message-user-photo">
                                <picture>
                                    <source srcSet={`/assets/app/base/images/${props.message.img.sourceSet[0]}, /assets/app/base/images/${props.message.img.sourceSet[1]} 2x`} type="image/webp" /><img className="responsive-img cover-img" src={`/assets/app/base/images/${props.message.img.src}`}  srcSet={`/assets/app/base/images/${props.message.img.srcSet[0]}, /assets/app/base/images/${props.message.img.srcSet[1]}`} alt="" />
                                </picture>
                            </div>
                        </div>
                        <div className="col">
                            <div className="row align-items-end">
                                <div className="col">
                                    <div className="text-14">{props.message.name}</div>
                                    <div className="mt8"> </div>
                                    <div className="text-16 text-w-600">{props.message.message}</div>
                                </div>
                                <div className="col-auto">
                                    <div className="chat-message-data">{props.message.val}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
                    
        </>
    );
};
                                              
export default ChatMessage;