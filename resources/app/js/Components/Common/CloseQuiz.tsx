import React from "react";

const CloseQuiz =() => {
    return(
        <>
            <div className="popup-inline-exa" id="close-quize">
                <div className="svg-image-close" data-fancybox-close></div>
                <div className="w475">
                    <div className="tac"><img src="/assets/app/base/images/question.svg" alt="" /></div>
                    <div className="mt32 tac">
                        <h3 className="text-24 text-nova">Are you sure you want to close the quiz?</h3>
                        <div className="tac text-16 mt12 text-6E">
                            <p>Don't worry, we'll save your progress and you can continue your quiz at any time</p>
                        </div>
                        <div className="mt32">
                            <div className="row px-12 justify-content-center mt-20">
                                <div className="col-md-auto mt20"><a className="btn btn--sm btn--white mb-wide" href="javascript:;" data-fancybox-close> <span className="btn__text">Cancel</span></a></div>
                                <div className="col-md-auto mt20">
                                    <a className="btn btn--sm btn--blue close-quize mb-wide" href="javascript:;"> <span className="btn__text">Close Quiz</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};
export default CloseQuiz;