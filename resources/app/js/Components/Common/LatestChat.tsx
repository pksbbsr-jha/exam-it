import React from 'react';

type LatestChatProps = {
    message: {
       name: string, 
       message:string
    }
}

const LatestChat = (props:LatestChatProps) => {

    return (
        <>
            <div className="col-12 mt16">
                <a className="chat-item" href="#">
                    <div className="row align-items-end">
                        <div className="col">
                            <div className="text-14">{props.message.name}</div>
                            <div className="mt8"> </div>
                            <div className="text-16 text-w-600">{props.message.message}</div>
                        </div>
                        <div className="col-auto">
                            <div className="chat-message-data">10</div>
                        </div>
                    </div>
                </a>
            </div>
        </>
    );
};

export default LatestChat;
