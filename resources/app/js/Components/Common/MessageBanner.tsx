import React from 'react';


const MessageBanner = () => {

    return (
        <>
            <div className="sm-hide">
                <div className="text-28 text-white">Message board </div>
            </div>
        </>
    );
};

export default MessageBanner;
