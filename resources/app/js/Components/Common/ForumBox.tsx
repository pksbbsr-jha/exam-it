import React from 'react';

type ForumBox = {
    forumMessages: {
        title: string, 
        totalReplies: number,
        author: string, 
        lastMessageTime: string
    }[]
}

const ForumBox = (props:ForumBox) => {

    return (
        <>
            <div className="white-box-body sm-show">
                <div className="row mt-16">
                    {props.forumMessages.length > 0 ? props.forumMessages.map((message, index)=><div className="col-12 mt16" key={index}>
                        <div className="discussion-item">
                            <div className="row px-6">
                                <div className="col-12"><a className="text-16 text-w-600" href="#">{message.title}</a></div>
                            </div>
                            <div className="mt12 row px-6 align-items-center">
                                <div className="col">
                                    <div className="discussion-user-ask">{message.totalReplies} {message.totalReplies > 1 ? 'replies' : 'reply'}</div>
                                </div>
                                <div className="col-auto">
                                    <div className="discussion-user-ask">asked by <a href="#">{message.author}</a></div>
                                </div>
                            </div>
                        </div>
                    </div>):<p className='text-right'>Forum not available.</p>}
                </div>
            </div>
            <div className="forum-module-body sm-hide">
                <table className="forum-module-table">
                    <thead>
                        <tr>
                            <td>
                                <div className="text-14 text-w-600 text-6E">Title</div>
                            </td>
                            <td>
                                <div className="text-14 text-w-600 text-6E">Replies</div>
                            </td>
                            <td>
                                <div className="text-14 text-w-600 text-6E">Author</div>
                            </td>
                            <td>
                                <div className="text-14 text-w-600 text-6E">Last message</div>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        {props.forumMessages.length > 0 ? props.forumMessages.map((message, index)=><tr key={index}>
                            <td>{message.title}</td>
                            <td>{message.totalReplies}</td>
                            <td> <a href="#">{message.author}</a></td>
                            <td>{message.lastMessageTime}</td>
                        </tr>):<tr><td colSpan={4} className="text-center">Messages not available.</td></tr>}
                    </tbody>
                </table>
            </div>
        </>
    );
};

export default ForumBox;
