import React from 'react';

// type ChatInboxProps = {
//     messages: {
//         name: string, 
//         message: number,
//         type: string, 
//     }[]
// }

//const ChatInbox = (props:ChatInboxProps) => {
const ChatInbox = () => {
    return (
        <>
            <div className="dashboard-chat-area-right">
                <div className="dashboard-chat-companion-head">
                    <div className="row align-items-center justify-content-between">
                        <div className="col-auto">
                            <div className="row align-items-center px-6">
                                <div className="col-auto sm-show">
                                    <div className="return-to-chat-list">
                                        <div className="row align-items-center px-7 d-inline-flex">
                                            <div className="col-auto">
                                                <div className="svg-image-back"></div>
                                            </div>
                                            <div className="col-auto text-9A text-14">Back</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-auto">
                                    <div className="message-user-photo">
                                        <picture>
                                            <source srcSet="assets/app/base/images/Avatar2@1x.webp, assets/app/base/images/Avatar2@2x.webp 2x" type="image/webp" /><img src="assets/app/base/images/Avatar2@1x.jpg" srcSet="assets/app/base/images/Avatar2@1x.jpg, assets/app/base/images/Avatar2@2x.jpg 2x" alt="" />
                                        </picture>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="text-16 text-w-600">Dianne_Russell</div>
                                </div>
                            </div>
                        </div>
                        <div className="col-auto">
                            <button className="btn-chat-options button-tippy" type="button" data-template="btn-chat-options-tippy">
                                <svg width="16" height="4" viewBox="0 0 16 4" fill="none">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M2 0C3.10457 0 4 0.89543 4 2C4 3.10457 3.10457 4 2 4C0.89543 4 0 3.10457 0 2C0 0.89543 0.89543 0 2 0ZM10 2C10 0.89543 9.10457 0 8 0C6.89543 0 6 0.89543 6 2C6 3.10457 6.89543 4 8 4C9.10457 4 10 3.10457 10 2ZM14 0C15.1046 0 16 0.89543 16 2C16 3.10457 15.1046 4 14 4C12.8954 4 12 3.10457 12 2C12 0.89543 12.8954 0 14 0Z" fill="#4D546E"></path>
                                </svg>
                            </button>
                            <div className="btn-tippy-area" id="btn-chat-options-tippy">
                                <ul className="list">
                                    <li> <a className="btn-t" href="#">
                                        <div className="row px-6 align-items-center flex-nowrap">
                                            <div className="col-auto">
                                                <div className="svg-image-leave-chat"></div>
                                            </div>
                                            <div className="col">
                                                <div className="text-14">Leave chat</div>
                                            </div>
                                        </div></a>
                                    </li>
                                    <li> <a className="btn-t" href="#">
                                        <div className="row px-6 align-items-center flex-nowrap">
                                            <div className="col-auto">
                                                <div className="svg-image-private-chat"></div>
                                            </div>
                                            <div className="col">
                                                <div className="text-14">Add a person to this chat</div>
                                            </div>
                                        </div></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="dashboard-chat-companion-body">
                    <div className="companion-message-wrap">
                        <div className="row align-items-center">
                            <div className="col">
                                <div className="row px-4 align-items-center">
                                    <div className="col-auto">
                                        <div className="message-user-photo w24">
                                            <picture>
                                                <source srcSet="assets/app/base/images/Avatar2@1x.webp, assets/app/base/images/Avatar2@2x.webp 2x" type="image/webp" /><img src="assets/app/base/images/Avatar2@1x.jpg" srcSet="assets/app/base/images/Avatar2@1x.jpg, assets/app/base/images/Avatar2@2x.jpg 2x" alt="" />
                                            </picture>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="text-14 text-w-600">Dianne_Russell</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-auto">
                                <div className="text-12 text-c5">02/02/2022 • 10:00 AM</div>
                            </div>
                        </div>
                        <div className="companion-message-item">Hi, how are you?</div>
                    </div>
                    <div className="chat-day"> <span>TODAY</span></div>
                    <div className="companion-message-wrap">
                        <div className="row align-items-center">
                            <div className="col">
                                <div className="row px-4 align-items-center">
                                    <div className="col-auto">
                                        <div className="message-user-photo w24">
                                            <picture>
                                                <source srcSet="assets/app/base/images/Avatar2@1x.webp, assets/app/base/images/Avatar2@2x.webp 2x" type="image/webp" /><img src="assets/app/base/images/Avatar2@1x.jpg" srcSet="assets/app/base/images/Avatar2@1x.jpg, assets/app/base/images/Avatar2@2x.jpg 2x" alt="" />
                                            </picture>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="text-14 text-w-600">Dianne_Russell</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-auto">
                                <div className="text-12 text-c5">9:13 AM</div>
                            </div>
                        </div>
                        <div className="companion-message-item">Hi, how are you?</div>
                    </div>
                    <div className="you-message-wrap">
                        <div className="row align-items-center">
                            <div className="col">
                                <div className="text-12 text-c5">9:13 AM</div>
                            </div>
                            <div className="col-auto">
                                <div className="row px-4 align-items-center">
                                    <div className="col">
                                        <div className="text-14 text-w-600">You</div>
                                    </div>
                                    <div className="col-auto">
                                        <div className="message-user-photo w24">
                                            <picture>
                                                <source srcSet="assets/app/base/images/Avatar2@1x.webp, assets/app/base/images/Avatar2@2x.webp 2x" type="image/webp" /><img src="assets/app/base/images/Avatar2@1x.jpg" srcSet="assets/app/base/images/Avatar2@1x.jpg, assets/app/base/images/Avatar2@2x.jpg 2x" alt="" />
                                            </picture>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="you-message-item">I’m good, thanks!</div>
                    </div>
                </div>
                <div className="dashboard-chat-companion-footer">
                    <div className="dashboard-chat-message-send-wrap">
                        <div className="row align-items-center">
                            <div className="col">
                                <input type="text" placeholder="Write a message..." />
                            </div>
                            <div className="col-auto">
                                <div className="sm-hide">
                                    <input type="submit" value="Send" disabled />
                                </div>
                                <div className="sm-show">
                                    <button className="send-message--mb" type="submit">
                                        <div className="svg-image-send"></div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default ChatInbox;
