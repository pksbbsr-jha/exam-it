import React from "react";
type ContentProps = {
    contentbody: {
        id: number,
        name: string,
        description_short: string,
        description_list :string,
        description_lists :string,
        description_listb : string,
        description_left: string,
        description: string,
        description_shrt: string,      
        description_aftr_img: string,
        meta:{
            requirements:{}[],
            contents:{}[]
        },
        img:{
            src: string, 
            sourceSet:{}[], 
            srcSet: {}[]
        }
    }
};
 const ContentBody =(props: ContentProps)=>{
    return(
        <>
           <div className="sub-module-content">
                <div className="sub-module-content__text">
                    <h3>{props.contentbody.name}</h3>
                    <p>{props.contentbody.description_short}</p>
                    <p>{props.contentbody.description_list}</p>
                    <p>{props.contentbody.description_lists}<br/>{props.contentbody.description_listb}</p>
                    <p>{props.contentbody.description_left}</p>
                    <p>{props.contentbody.description_shrt}</p>
                    <picture>
                        <source srcSet={`/assets/app/base/images/${props.contentbody.img.sourceSet[0]}, /assets/app/base/images/${props.contentbody.img.sourceSet[1]} 2x`} type="image/webp" /><img src={`/assets/app/base/images/${props.contentbody.img.src}`} srcSet={`/assets/app/base/images/${props.contentbody.img.srcSet[0]}, /assets/app/base/images/${props.contentbody.img.srcSet[1]}`} alt="" />
                    </picture>

                    <p>{props.contentbody.description}</p>

                    <ol>
                        {props.contentbody.meta.contents.map((content, index)=><li key={index}>{content}</li>)}
                    </ol>

                    <p>{props.contentbody.description_aftr_img}</p>

                    <ol>
                        {props.contentbody.meta.requirements.map((requirment, index)=><li key={index}>{requirment}</li>)}
                    </ol>
                </div>
                <br/>
                <a className="btn--module" href="#"><span className="btn__text">View Discussion for This Sub-Module </span></a>
            </div>

        </>
    );
 };
 export default ContentBody;