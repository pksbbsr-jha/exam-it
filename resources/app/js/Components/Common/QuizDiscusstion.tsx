import React from "react";

const QuizDiscusstion = () => {
    return (
        <>
            <div className="quize-popup no-gutters">
                <div className="col-auto">
                    <div className="popup-module-head">
                        <div className="row px-12 justify-content-between align-items-center">
                            <div className="col">
                                <div className="sm-show">
                                    <label className="toggle-checkbox">
                                        <input type="checkbox" />
                                        <div className="toggle-checkbox-styled"></div>
                                        <div className="toggle-checkbox-text text-14">View content related to question</div>
                                    </label>
                                </div>
                                <ul className="quize-menu list sm-hide">
                                    <li><a className="is-active" href="#">Quiz</a></li>
                                    <li><a href="#">Discussions</a></li>
                                </ul>
                            </div>
                            <div className="col-auto">
                                <div className="svg-image-close" data-fancybox data-src="#close-quize" data-modal="true"></div>
                            </div>
                        </div>
                    </div>
                    <div className="popup-module-head sm-show">
                        <ul className="quize-menu list">
                            <li><a className="is-active" href="#">Quiz</a></li>
                            <li><a href="#">Discussions</a></li>
                        </ul>
                    </div>
                    <div className="popup-module-head-title"> <b>Quiz </b>(Module 3.1: Vectors and Scalars)</div>
                </div>
                <div className="col">
                    <div className="popup-module-content">
                        <picture>
                            <source srcSet="/assets/app/base/images/qm1@1x.webp, /assets/app/base/images/qm1@2x.webp 2x" type="image/webp" /><img src="/assets/app/base/images/qm1@1x.jpg" srcSet="/assets/app/base/images/qm1@1x.jpg, images/qm1@2x.jpg 2x" alt="" />
                        </picture>
                        <div className="quize-question text-w-600 mt24">Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem?</div>
                        <div className="row px-12">
                            <div className="col-6 mt24">
                                <div className="question-small-images">
                                    <div className="question-small-images__el">
                                        <picture>
                                            <source srcSet="/assets/app/base/images/v1@1x.webp, /assets/app/base/images/v1@2x.webp 2x" type="image/webp" /><img className="responsive-img" src="/assets/app/base/images/v1@1x.jpg" srcSet="/assets/app/base/images/v1@1x.jpg, images/v1@2x.jpg 2x" alt="" />
                                        </picture>
                                    </div>
                                    <div className="question-small-v mt15">A.</div>
                                </div>
                            </div>
                            <div className="col-6 mt24">
                                <div className="question-small-images">
                                    <div className="question-small-images__el">
                                        <picture>
                                            <source srcSet="/assets/app/base/images/v1@1x.webp, images/v1@2x.webp 2x" type="image/webp" /><img className="responsive-img" src="/assets/app/base/images/v1@1x.jpg" srcSet="/assets/app/base/images/v1@1x.jpg, /assets/app/base/images/v1@2x.jpg 2x" alt="" />
                                        </picture>
                                    </div>
                                    <div className="question-small-v mt15">B.</div>
                                </div>
                            </div>
                            <div className="col-6 mt24">
                                <div className="question-small-images">
                                    <div className="question-small-images__el">
                                        <picture>
                                            <source srcSet="/assets/app/base/images/v1@1x.webp, /assets/app/base/images/v1@2x.webp 2x" type="image/webp" /><img className="responsive-img" src="/assets/app/base/images/v1@1x.jpg" srcSet="/assets/app/base/images/v1@1x.jpg, /assets/app/base/images/v1@2x.jpg 2x" alt="" />
                                        </picture>
                                    </div>
                                    <div className="question-small-v mt15">C.</div>
                                </div>
                            </div>
                            <div className="col-6 mt24">
                                <div className="question-small-images">
                                    <div className="question-small-images__el">
                                        <picture>
                                            <source srcSet="/assets/app/base/images/v1@1x.webp, /assets/app/base/images/v1@2x.webp 2x" type="image/webp" /><img className="responsive-img" src="/assets/app/base/images/v1@1x.jpg" srcSet="/assets/app/base/images/v1@1x.jpg, /assets/app/base/images/v1@2x.jpg 2x" alt="" />
                                        </picture>
                                    </div>
                                    <div className="question-small-v mt15">D.</div>
                                </div>
                            </div>
                        </div>
                        <div className="quize-answers mt24">
                            <div className="row mt-12">
                                <div className="col-12 mt12">
                                    <label className="quize-answer-item-wrap incorrect-answer">
                                        <input type="checkbox" />
                                        <span className="quize-answer-item">
                                            <span className="quize-answer-data">A.</span>
                                            <span className="quize-answer-text">Lorem ipsum dolor sit amet</span>
                                        </span>
                                    </label>
                                    <div className="mt4 incorrect-answer-text">Your answer is incorrect</div>
                                </div>
                                <div className="col-12 mt12">
                                    <label className="quize-answer-item-wrap">
                                        <input type="checkbox" /><span className="quize-answer-item"><span className="quize-answer-data">B.</span><span className="quize-answer-text">Lorem ipsum dolor sit amet</span></span>
                                    </label>
                                </div>
                                <div className="col-12 mt12">
                                    <label className="quize-answer-item-wrap">
                                        <input type="checkbox" /><span className="quize-answer-item"><span className="quize-answer-data">C.</span><span className="quize-answer-text">Lorem ipsum dolor sit amet</span></span>
                                    </label>
                                </div>
                                <div className="col-12 mt12">
                                    <label className="quize-answer-item-wrap correct-answer">
                                        <input type="checkbox" /><span className="quize-answer-item"><span className="quize-answer-data">D.</span><span className="quize-answer-text">Lorem ipsum dolor sit amet</span></span>
                                    </label>
                                    <div className="mt4 correct-answer-text">Your answer is correct!</div>
                                </div>
                            </div>
                        </div><a className="mt16 btn btn--sm btn--sm-40 btn--white view-exp mb-wide" href="#"><span className="btn__text">Hide explanation</span></a>
                        <div className="explanation-box mt24" style={{ display: `block` }}>
                            <div className="text-nova text-w-600">Explanation</div>
                            <div className="mt12">
                                <p>There are two types of variables in physics; Vectors and Scalars. We write vectors by either an arrow above the vector, A, or boldface, A.  For example, a vector “A” can be written two ways:</p>
                            </div>
                            <div className="mt16"> <b>A   or   A</b></div>
                        </div>
                        <div className="sm-show mt16"><a className="btn btn--sm btn--sm-40 btn--white mb-wide" href="#"><span className="btn__text">View text</span></a></div>
                    </div>
                </div>
                <div className="col-auto">
                    <div className="popup-module-footer">
                        <div className="row align-items-end mt-24">
                            <div className="col-md mt24">
                                <div className="course-progress-wrap wf-lg-240">
                                    <div className="row justify-content-between text-9A">
                                        <div className="col-auto">
                                            <div className="text-14">3/10 questions</div>
                                        </div>
                                        <div className="col-auto">
                                            <div className="text-14">2 correct answers</div>
                                        </div>
                                    </div>
                                    <div className="mt8">
                                        <div className="progress-course">
                                            <div className="cssProgress-success" style={{ width: `33%` }}></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-auto mt24"><a className="btn btn--blue btn--sm btn--sm-40 mb-wide" href="#"><span className="btn__text">Next Question</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};
export default QuizDiscusstion;