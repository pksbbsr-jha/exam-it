import React from 'react';


const MessageBody = () => {

    return (
        <>
            <div className="row align-items-center px-8 justify-content-between mt-30">
                <div className="col-md-auto mt30 mb-mt16">
                    <ul className="grey-menu text-14 text-w-600 grey-menu-md">
                        <li className="current-menu-item"><a href="#">All</a></li>
                        <li><a href="#">Group</a></li>
                        <li><a href="#">Public</a></li>
                        <li><a href="#">Private</a></li>
                        <li><a href="#">Favorites</a></li>
                    </ul>
                </div>
                <div className="col-lg-auto mt30 mb-mt16">
                    <div className="row align-items-center justify-content-between mt-30 px-8 mbmt-16">
                        <div className="col-lg-auto col-md-6 mt30 order-md-1 order-2 mb-mt16">
                            <div className="search-module-el search-module-el-md">
                                <div className="filter-search-field">
                                    <input className="wpcf7-form-control" type="search" placeholder="Search..." />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-auto col-md-6 mt30 order-md-2 order-1 mb-mt16">
                            <a className="btn-sort-by" href="#">
                                <div className="btn-sort-by__icon">
                                    <div className="svg-image-filter_list"></div>
                                </div>
                                <div className="btn-sort-by__title">Sort by:  </div>
                                <div className="btn-sort-by__name">  Most recent</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default MessageBody;