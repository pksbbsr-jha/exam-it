import React from "react";
import ContentBody from "./ContentBody";

const CourseContent = () =>{
    const courseContent = [
        {
            id: 1,
            name: '1. The Coordinate System',
            description_short: `The coordinate system is extremely important in physics! Every problem and situation will be described in relation to the coordinate system. The coordinate system is our x-y axis, and each axis is called a dimension.`,
            description_list :'',
            description_lists :'',
            description_listb : '',
            description_left: '',
            description: `For example, latitude and longitude (and altitude) is the coordinate system we use on Earth, and all locations are described in relation to this.`,
            description_shrt:'',
            description_aftr_img: '',
            meta:{
                requirements:[],
                contents:[]
            },
            
            img: {
                src: 'yx@1x.jpg',
                sourceSet: ['yx@1x.webp', 'yx@2x.webp 2x'],
                srcSet: ['yx@1x.jpg', 'yx@2x.jpg 2x']
            }
        },
        {
            id: 2,
            name: '2. Vectors vs. Scalars',
            description_short: `There are two types of variables in physics; Vectors and Scalars. We write vectors by either an arrow above the vector, A, or boldface, A.  For example, a vector “A” can be written two ways:`,
            description_list :`A   or   A`,
            description_lists :`Vectors have two parts: Magnitude and Direction`,
            description_listb : `Scalars only have one part: Magnitude`,
            description_left: `Note: Scientists don’t always write vectors in boldface, or with an arrow, because it is implicitly understood which variables are vectors. In fact, later on in this course you will encounter depictions of variables that may not be boldface or have an arrow, but are vectors. It is required that you know which variables are vectors.`,
            description_shrt:`Here are some examples of vectors:`,
            description: `Direction can be described several ways:`,
            description_aftr_img: `Examples of Scalars (you can see how a direction doesn’t make sense)`,
            meta:{
                requirements:['time','mass','energy'],
                contents:['right, left, up, down','north, south, east, west', 'angle in relation to an axis (usually in degrees)','x-y axis (for example, +x direction)']
            },
            
            img: {
                src: 'm-table@1x.jpg',
                sourceSet: ['m-table@1x.webp', 'm-table@2x.webp 2x'],
                srcSet: ['m-table@1x.jpg', 'm-table@2x.jpg 2x']
            }
        }
    ]
    return (
        <>
            
            <div className="course-content-box-header">
                <div className="row justify-content-between mt-20">
                    <div className="col-lg mt20">
                        <h3 className="text-24 text-nova">Module 3.1: Vectors and Scalars</h3>
                        <div className="mt4 text-16 text-9A">Demo course name</div>
                    </div>
                    <div className="col-lg-auto mt20">
                        <div className="course-progress-wrap wf-lg-240">
                            <div className="row justify-content-between">
                                <div className="col-auto">
                                    <div className="text-14">33%</div>
                                </div>
                                <div className="col-auto">
                                    <div className="text-14 text-9A">3/10 modules</div>
                                </div>
                            </div>
                            <div className="mt8">
                                <div className="progress-course">
                                    <div className="cssProgress-success" style={{ width: `33%`}}></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            {courseContent.length > 0 ? courseContent.map((contentbody, index)=><ContentBody key={index} contentbody={contentbody}/>):<p className='text-right'>Courses not available.</p>}
         
            <div className="course-content-box-footer tac"> <a className="btn--module" href="#">
                <span className="btn__text">Complete Module & Take a Quiz</span></a>
            </div>
        </>
    );
};
export default CourseContent;