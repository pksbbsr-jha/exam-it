import React from 'react';


const CourseHeader = () => {

    return (
        <>
            <div className="user-all-course__head">
                <div className="row align-items-center px-8 justify-content-between mt-12">
                    <div className="col-md-auto mt12">
                        <div className="row align-items-center px-8 mt-12 justify-content-between">
                            <div className="col-auto mt12">
                                <div className="text-18 text-w-600">My Courses</div>
                            </div>
                            <div className="col-auto mt12 sm-show"><a className="btn-view" href="#"> <span className="btn__text">View More Courses</span></a></div>
                            <div className="col-md-auto mt12">
                                <ul className="grey-menu text-14 text-w-600">
                                    <li className="current-menu-item"><a href="#">All</a></li>
                                    <li><a href="#">Current</a></li>
                                    <li><a href="#">Completed</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-auto mt12 sm-hide"><a className="btn-view" href="#"> <span className="btn__text">View More Courses</span></a></div>
                </div>
            </div>
        </>
    );
};

export default CourseHeader;
