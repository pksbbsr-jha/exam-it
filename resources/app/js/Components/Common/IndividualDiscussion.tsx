import React from "react";

const IndividualDisscusion = () =>{
    return (
        <>
            <div className="col-auto">
                <div className="popup-module-head">
                    <div className="row px-12 justify-content-between align-items-center">
                        <div className="col"><a href="#">
                            <div className="row align-items-center px-7 d-inline-flex">
                                <div className="col-auto">
                                    <div className="svg-image-back"></div>
                                </div>
                                <div className="col-auto text-9A text-14">Back to Duscussion list</div>
                            </div></a></div>
                        <div className="col-auto">
                            <div className="svg-image-close close-quize"></div>
                        </div>
                    </div>
                    <div className="mt15">
                        <div className="discuss-title text-ov">Lorem ipsum dolor sit amet, consectetur adipisci elit...</div>(1. The Coordinate System)
                    </div>
                    <div className="mt15 md-hide">
                        <div className="row px-8 align-items-center">
                            <div className="col">
                                <div className="row px-8 align-items-center">
                                    <div className="col-auto">
                                        <div className="discussion-tag discussion-tag-private">Private</div>
                                    </div>
                                    <div className="col-auto">
                                        <button className="toggleDiscussionInWishlist" type="button"></button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-auto"><a className="btn--module" href="#"><span className="btn__text">Edit List of People Who Have Access</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col">
                <div className="popup-module-content">
                    <div className="message">
                        <div className="message-header">
                            <div className="row px-8 align-items-center">
                                <div className="col-auto">
                                    <div className="discuss-user discuss-user-tippy" data-template="user-info-01">
                                        <div className="row px-5 align-items-center">
                                            <div className="col-auto">
                                                <div className="message-user-photo">
                                                    <picture>
                                                        <source srcSet="/assets/app/base/images/Avatar@1x.webp, /assets/app/base/images/Avatar@2x.webp 2x" type="image/webp" /><img src="/assets/app/base/images/Avatar@1x.jpg" srcSet="/assets/app/base/images/Avatar@1x.jpg, /assets/app/base/images/Avatar@2x.jpg 2x" alt="" />
                                                    </picture>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <div className="text-w-600 text-16">maude_hall</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="discuss-user-tippy-area" id="user-info-01">
                                        <div className="message-user-photo">
                                            <picture>
                                                <source srcSet="/assets/app/base/images/Avatar@1x.webp, /assets/app/base/images/Avatar@2x.webp 2x" type="image/webp" /><img src="/assets/app/base/images/Avatar@1x.jpg" srcSet="/assets/app/base/images/Avatar@1x.jpg, images/Avatar@2x.jpg 2x" alt="" />
                                            </picture>
                                        </div>
                                        <div className="mt8"><a className="text-16 text-blue text-w-600" href="#">maude_hall</a></div>
                                        <div className="mt12"> <a className="btn btn--blue btn--32" href="#"> <span className="btn__icon">
                                            <div className="svg-image-sms_outlined"></div></span><span className="btn__text">Send message</span></a></div>
                                    </div>
                                </div>
                                <div className="col-auto">
                                    <div className="text-16 text-c5">14 min</div>
                                </div>
                            </div>
                        </div>
                        <div className="message-content text-16 text-w-600 mt8">Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun?</div>
                        <div className="message-footer mt8 text-16">
                            <div className="row align-items-center justify-content-between">
                                <div className="col-auto">
                                    <div className="row align-items-center px-6">
                                        <div className="col-auto"><a href="#">
                                            <div className="row px-2 align-items-center">
                                                <div className="col-auto">
                                                    <div className="svg-image-reply"></div>
                                                </div>
                                                <div className="col text-9A">Reply</div>
                                            </div></a></div>
                                    </div>
                                </div>
                                <div className="col-auto"></div>
                            </div>
                        </div>
                    </div>
                    <div className="reply-message mt24">
                        <div className="message">
                            <div className="message-header">
                                <div className="row px-8 align-items-center">
                                    <div className="col-auto">
                                        <div className="discuss-user discuss-user-tippy" data-template="user-info-02">
                                            <div className="row px-5 align-items-center">
                                                <div className="col-auto">
                                                    <div className="message-user-photo">
                                                        <picture>
                                                            <source srcSet="/assets/app/base/images/Avatar2@1x.webp, /assets/app/base/images/Avatar2@2x.webp 2x" type="image/webp" /><img src="/assets/app/base/images/Avatar2@1x.jpg" srcSet="/assets/app/base/images/Avatar2@1x.jpg, /assets/app/base/images/Avatar2@2x.jpg 2x" alt="" />
                                                        </picture>
                                                    </div>
                                                </div>
                                                <div className="col">
                                                    <div className="text-w-600 text-16">Dianne_Russell</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="discuss-user-tippy-area" id="user-info-02">
                                            <div className="message-user-photo">
                                                <picture>
                                                    <source srcSet="/assets/app/base/images/Avatar2@1x.webp, /assets/app/base/images/Avatar2@2x.webp 2x" type="image/webp" /><img src="/assets/app/base/images/Avatar2@1x.jpg" srcSet="/assets/app/base/images/Avatar2@1x.jpg, images/Avatar2@2x.jpg 2x" alt="" />
                                                </picture>
                                            </div>
                                            <div className="mt8"><a className="text-16 text-blue text-w-600" href="#">Dianne_Russell</a></div>
                                            <div className="mt12"> <a className="btn btn--blue btn--32" href="#"> <span className="btn__icon">
                                                <div className="svg-image-sms_outlined"></div></span><span className="btn__text">Send message</span></a></div>
                                        </div>
                                    </div>
                                    <div className="col-auto">
                                        <div className="text-16 text-c5">24 min</div>
                                    </div>
                                </div>
                            </div>
                            <div className="message-content text-16 mt8">But don't you think the timing is off because many other apps have done this even earlier, causing people to switch apps?</div>
                            <div className="message-footer mt8 text-16">
                                <div className="row align-items-center justify-content-between">
                                    <div className="col-auto">
                                        <div className="row align-items-center px-6">
                                            <div className="col-auto"><a href="#">
                                                <div className="row px-2 align-items-center">
                                                    <div className="col-auto">
                                                        <div className="svg-image-reply"></div>
                                                    </div>
                                                    <div className="col text-9A">Reply</div>
                                                </div></a>
                                            </div>
                                            <div className="col-auto">
                                                <div className="text-9A">1 Like</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-auto">
                                        <div className="row px-6">
                                            <div className="col-auto">
                                                <button className="btn-def" type="button">
                                                    <div className="svg-image-thumbs-up"></div>
                                                </button>
                                            </div>
                                            <div className="col-auto">
                                                <button className="btn-def" type="button">
                                                    <div className="svg-image-thumbs-down"></div>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="message mt24">
                            <div className="message-header">
                                <div className="row px-8 align-items-center">
                                    <div className="col-auto">
                                        <div className="discuss-user discuss-user-tippy" data-template="user-info-02">
                                            <div className="row px-5 align-items-center">
                                                <div className="col-auto">
                                                    <div className="message-user-photo">
                                                        <picture>
                                                            <source srcSet="/assets/app/base/images/Avatar3@1x.webp, /assets/app/base/images/Avatar3@2x.webp 2x" type="image/webp" /><img src="/assets/app/base/images/Avatar3@1x.jpg" srcSet="/assets/app/base/images/Avatar3@1x.jpg, /assets/app/base/images/Avatar3@2x.jpg 2x" alt="" />
                                                        </picture>
                                                    </div>
                                                </div>
                                                <div className="col">
                                                    <div className="text-w-600 text-16">Dia_mussell</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="discuss-user-tippy-area" id="user-info-02">
                                            <div className="message-user-photo">
                                                <picture>
                                                    <source srcSet="/assets/app/base/images/Avatar3@1x.webp, /assets/app/base/images/Avatar3@2x.webp 2x" type="image/webp" /><img src="/assets/app/base/images/Avatar3@1x.jpg" srcSet="/assets/app/base/images/Avatar3@1x.jpg, /assets/app/base/images/Avatar3@2x.jpg 2x" alt="" />
                                                </picture>
                                            </div>
                                            <div className="mt8"><a className="text-16 text-blue text-w-600" href="#">Dia_mussell</a></div>
                                            <div className="mt12"> <a className="btn btn--blue btn--32" href="#"> <span className="btn__icon">
                                                <div className="svg-image-sms_outlined"></div></span><span className="btn__text">Send message</span></a></div>
                                        </div>
                                    </div>
                                    <div className="col-auto">
                                        <div className="text-16 text-c5">24 min</div>
                                    </div>
                                </div>
                            </div>
                            <div className="message-content text-16 mt8">But don't you think the timing is off because many other apps have done this even earlier, causing people to switch apps?</div>
                            <picture>
                                <source srcSet="/assets/app/base/images/v1@1x.webp, /assets/app/base/images/v1@2x.webp 2x" type="image/webp" /><img src="/assets/app/base/images/v1@1x.jpg" srcSet="/assets/app/base/images/v1@1x.jpg, /assets/app/base/images/v1@2x.jpg 2x" alt="" />
                            </picture>
                            <div className="message-footer mt8 text-16">
                                <div className="row align-items-center justify-content-between">
                                    <div className="col-auto">
                                        <div className="row align-items-center px-6">
                                            <div className="col-auto"><a href="#">
                                                <div className="row px-2 align-items-center">
                                                    <div className="col-auto">
                                                        <div className="svg-image-reply"></div>
                                                    </div>
                                                    <div className="col text-9A">Reply</div>
                                                </div></a></div>
                                            <div className="col-auto">
                                                <div className="text-9A">1 Like</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-auto">
                                        <div className="row px-6">
                                            <div className="col-auto">
                                                <button className="btn-def" type="button">
                                                    <div className="svg-image-thumbs-up"></div>
                                                </button>
                                            </div>
                                            <div className="col-auto">
                                                <button className="btn-def" type="button">
                                                    <div className="svg-image-thumbs-down"></div>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-auto">
                <div className="popup-module-footer bd0">
                    <div className="wysiwyg-discuss">
                        <textarea placeholder="Type your comment here..."></textarea>
                        <div className="wysiwyg-discuss-panel">
                            <div className="row justify-content-between align-items-center px-11">
                                <div className="col-auto">
                                    <div className="row align-items-center px-11">
                                        <div className="col-auto">
                                            <button className="btn-def" type="button">
                                                <div className="svg-image-B"></div>
                                            </button>
                                        </div>
                                        <div className="col-auto">
                                            <button className="btn-def" type="button">
                                                <div className="svg-image-I"></div>
                                            </button>
                                        </div>
                                        <div className="col-auto">
                                            <button className="btn-def" type="button">
                                                <div className="svg-image-s"></div>
                                            </button>
                                        </div>
                                        <div className="col-auto">
                                            <button className="btn-def" type="button">
                                                <div className="svg-image-paperclip"></div>
                                            </button>
                                        </div>
                                        <div className="col-auto md-show">
                                            <button className="btn-def" type="button">
                                                <div className="svg-image-link"></div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-auto">
                                    <div className="row align-items-center px-7">
                                        <div className="col-auto md-hide">
                                            <button className="btn-def" type="button">
                                                <div className="svg-image-link"></div>
                                            </button>
                                        </div>
                                        <div className="col-auto md-hide">
                                            <button className="btn-def" type="button">
                                                <div className="svg-image-smile"></div>
                                            </button>
                                        </div>
                                        <div className="col-auto">
                                            <input type="submit" value="Send" disabled />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};
export default IndividualDisscusion;