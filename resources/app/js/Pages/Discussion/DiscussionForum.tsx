import React from "react";
const DiscussionForum = () =>{
    return (
        <>
            <div className="quize-popup no-gutters">
                <div className="col-auto">
                    <div className="popup-module-head">
                        <div className="row px-12 justify-content-between align-items-center">
                            <div className="col"><b>Create new discussion</b></div>
                            <div className="col-auto">
                                <div className="svg-image-close close-quize"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="popup-module-content">
                        <form className="form__styler" action="#">
                            <div className="row mt-24">
                                <div className="col-12 mt24">
                                    <div className="field-label">Discussion title</div>
                                    <input type="text" placeholder="Discussion title" />
                                </div>
                                <div className="col-12 mt24">
                                    <div className="field-label">Your question</div>
                                    <div className="wysiwyg-discuss wysiwyg-discuss-create">
                                        <textarea placeholder="Type your question here..."></textarea>
                                        <div className="wysiwyg-discuss-panel">
                                            <div className="row justify-content-between align-items-center px-11">
                                                <div className="col-auto">
                                                    <div className="row align-items-center px-11">
                                                        <div className="col-auto">
                                                            <button className="btn-def" type="button">
                                                                <div className="svg-image-B"></div>
                                                            </button>
                                                        </div>
                                                        <div className="col-auto">
                                                            <button className="btn-def" type="button">
                                                                <div className="svg-image-I"></div>
                                                            </button>
                                                        </div>
                                                        <div className="col-auto">
                                                            <button className="btn-def" type="button">
                                                                <div className="svg-image-s"></div>
                                                            </button>
                                                        </div>
                                                        <div className="col-auto">
                                                            <button className="btn-def" type="button">
                                                                <div className="svg-image-paperclip"></div>
                                                            </button>
                                                        </div>
                                                        <div className="col-auto sm-show">
                                                            <button className="btn-def" type="button">
                                                                <div className="svg-image-link"></div>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-auto sm-hide">
                                                    <div className="row align-items-center px-7">
                                                        <div className="col-auto">
                                                            <button className="btn-def" type="button">
                                                                <div className="svg-image-link"></div>
                                                            </button>
                                                        </div>
                                                        <div className="col-auto">
                                                            <button className="btn-def" type="button">
                                                                <div className="svg-image-smile"></div>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 mt24"> 
                                    <div className="field-label">Type</div>
                                    <div className="discuss-theme-create-select">
                                        <div className="selectric-wrapper">
                                            <div className="selectric-hide-select">
                                                <select>
                                                    <option value="Group">Group</option>
                                                    <option value="Private">Private</option>
                                                    <option value="Public">Public</option>
                                                </select>
                                            </div>
                                            <div className="selectric">
                                                <span className="label">Group</span>
                                                <b className="button">▾</b>
                                            </div>
                                            <div className="selectric-items" >
                                                <div className="selectric-scroll">
                                                    <ul>
                                                        <li data-index="0" className="selected">Group</li>
                                                        <li data-index="1" className="">Private</li>
                                                        <li data-index="2" className="last">Public</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <input className="selectric-input"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 mt24"> 
                                    <div className="group-select discuss-theme-item" style={{display: `block`}}>
                                        <div className="field-label">Select group</div>
                                        <div className="selectric-wrapper selectric-select-group">
                                            <div className="selectric-hide-select">
                                                <select className="select-group" data-tabindex="-1">
                                                    <option value="Demo group" data-alt="14 people">Demo group</option>
                                                    <option value="Demo group 2" data-alt="7 people">Demo group 2</option>
                                                </select>
                                            </div>
                                            <div className="selectric">
                                                <span className="label">Demo group</span>
                                                <b className="button">▾</b>
                                            </div>
                                            <div className="selectric-items" data-tabindex="-1">
                                                <div className="group-select-create"> 
                                                    <div className="group-select-create-btn" data-fancybox="" data-src="#group-chat" data-modal="true">
                                                        <div className="row px-4 align-items-center">
                                                            <div className="col-auto">
                                                                <span className="btn__icon">
                                                                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none">
                                                                        <path d="M12 5.57143V6.42857C12 6.66527 11.8081 6.85714 11.5714 6.85714H6.85714V11.5714C6.85714 11.8081 6.66527 12 6.42857 12H5.57143C5.33473 12 5.14286 11.8081 5.14286 11.5714V6.85714H0.428571C0.191878 6.85714 0 6.66527 0 6.42857V5.57143C0 5.33473 0.191878 5.14286 0.428571 5.14286H5.14286V0.428571C5.14286 0.191878 5.33473 0 5.57143 0H6.42857C6.66527 0 6.85714 0.191878 6.85714 0.428571V5.14286H11.5714C11.8081 5.14286 12 5.33473 12 5.57143Z" fill="#2153CC"></path>
                                                                    </svg>
                                                                </span>
                                                            </div>
                                                            <div className="col">
                                                                <span className="text-14 text-w-600 text-blue">Create new group</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="selectric-scroll">
                                                    <ul>
                                                        <li data-index="0" className="selected"><span>Demo group</span><span>14 people</span></li>
                                                        <li data-index="1" className="last"><span>Demo group 2</span><span>7 people</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <input className="selectric-input" data-tabindex="0" />
                                        </div>
                                    </div>
                                    <div className="private-select discuss-theme-item">
                                        <div className="field-label">Invite by username or via email:</div>
                                        <input type="text" placeholder="email or username" />
                                    </div>
                                    <div className="popup-inline-exa nopad" id="group-chat">
                                        
                                        <div className="svg-image-close" data-fancybox-close=""></div>
                                        <div className="popup-inline-exa-head">
                                            <div className="text-18 text-w-700">Invite people to group chat</div>
                                        </div>
                                        <div className="popup-inline-exa-body form__styler">
                                            <div className="row mt-16"> 
                                            <div className="col-12 mt16"> 
                                                <div className="field-label">Group name</div>
                                                <input type="email" placeholder="Group name" />
                                            </div>
                                            <div className="col-12 mt16"> 
                                                <div className="field-label">Invite via Email or Username:</div>
                                                <input type="email" placeholder="Email or Username" />
                                                <div className="mt8"></div>
                                                <input type="email" placeholder="Email or Username" />
                                                <div className="mt12"></div><a className="text-14 text-blue" href="#">+ add line</a>
                                            </div>
                                            </div>
                                        </div>
                                        <div className="popup-inline-exa-footer"> 
                                            <div className="row justify-content-between"> 
                                            <div className="col-auto"><a className="btn--def" href="#" data-fancybox-close=""><span className="btn__text">Cancel</span></a></div>
                                            <div className="col-auto"><a className="btn btn--blue" href="#"><span className="btn__text">Save &amp; Send invites</span></a></div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div className="mt100"></div>
                    </div>
                </div>
                <div className="col-auto">
                    <div className="popup-module-footer">
                        <div className="row align-items-end mt-10">
                            <div className="col mt10"><a className="btn btn--white btn--sm btn--sm-40" href="#"><span className="btn__text">Cancel</span></a></div>
                            <div className="col-auto mt10"><a className="btn btn--blue btn--sm btn--sm-40" href="#"><span className="btn__text">Save & Publish</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};
export default DiscussionForum;