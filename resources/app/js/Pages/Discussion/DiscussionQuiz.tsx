import CourseContent from "../../Components/Common/CourseContent";
import React from "react";
import QuizDiscusstion from "../../Components/Common/QuizDiscusstion";
import CloseQuiz from "../../Components/Common/CloseQuiz";

const DiscussionQuiz =() =>{
    return (
        <>
            <main className="main">
                <div className="container">
                    <div className="cont-ch-wrap">
                        <div className="w945 is-change">
                            <div className="page-back">
                                <a href="#">
                                    <div className="row align-items-center px-7 d-inline-flex">
                                        <div className="col-auto">
                                            <div className="svg-image-back"></div>
                                        </div>
                                        <div className="col-auto text-9A text-14">Back to Courses</div>
                                    </div>
                                </a>
                            </div>
                            <div className="course-content-box" style={{ width: `799.5px`}}>
                                 <CourseContent />
                            </div>
                        </div>
                    </div>
                    <div className="mt70"></div>
                    <QuizDiscusstion />
                    <div className="view-content-v2 vc">
                        <button className="btn-view-content" type="button">View Content</button>
                    </div>
                    <CloseQuiz />
                </div>
            </main>
        </>
    );
};
export default DiscussionQuiz;