import CourseContent from "../../Components/Common/CourseContent";
import React from "react";
import Quiz from "./Quiz";

const Index = () => {
    const courses = [
        {
            id: 1,
            name: 'The Coordinate System',
            img: {
                src: 'yx@1x.jpg',
                sourceSet: ['yx@1x.webp', 'yx@2x.webp 2x'],
                srcSet: ['yx@1x.jpg', 'yx@2x.jpg 2x']
            },
            descriptionfst:'The coordinate system is extremely important in physics! Every problem and situation will be described in relation to the coordinate system. The coordinate system is our x-y axis, and each axis is called a dimension.',
            description: 'For example, latitude and longitude (and altitude) is the coordinate system we use on Earth, and all locations are described in relation to this.',
        },
        {
            id: 2,
            name: 'Vectors vs. Scalars',
            img: {
                src: 'm-table@1x.jpg',
                sourceSet: ['m-table@1x.webp', 'm-table@2x.webp 2x'],
                srcSet: ['m-table@1x.jpg', 'm-table@2x.jpg 2x']
            },
            descriptionfst:'The coordinate system is extremely important in physics! Every problem and situation will be described in relation to the coordinate system. The coordinate system is our x-y axis, and each axis is called a dimension.',
            description: 'For example, latitude and longitude (and altitude) is the coordinate system we use on Earth, and all locations are described in relation to this.',
        }
    ];
    return (
        <>
            <main className="main">
                <div className="container">
                    <div className="cont-ch-wrap">
                        <div className="w945 is-change">
                            <div className="page-back">
                                <a href="#">
                                    <div className="row align-items-center px-7 d-inline-flex">
                                        <div className="col-auto">
                                            <div className="svg-image-back"></div>
                                        </div>
                                        <div className="col-auto text-9A text-14">Back to Courses</div>
                                    </div>
                                </a>
                            </div>
                            <div className="course-content-box" style={{ width: `799.5px`}}>
                                 <CourseContent />
                            </div>
                        </div>
                    </div>
                    <div className="mt70"></div>
                    <div className="quize-popup no-gutters">
                        <Quiz />
                        <div className="col-auto"></div>
                    </div>
                </div>
            </main>
        </>
    );
};
export default Index;