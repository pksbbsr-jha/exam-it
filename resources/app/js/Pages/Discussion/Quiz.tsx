import React from 'react';
import LatestDiscussion from '../../Components/Common/LatestDiscussion';

const Quiz = () => {
    const discussions = [
        {
            topic: '',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Public',
            replies: '3 replies',
            asked_by: 'Username'
        },
        {
            topic:'',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Private',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic:'',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Group',
            replies: '3 replies',
            asked_by: 'Username'
        }
    ];

    return (
        <>
            <div className="col-auto">
                <div className="popup-module-head">
                    <div className="row px-12 justify-content-between align-items-center">
                        <div className="col"><b>Discussions </b>(1. The Coordinate System)</div>
                        <div className="col-auto">
                            <div className="svg-image-close close-quize"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col">
                <div className="popup-module-content">
                    <div className="row px-16 mt-20 align-items-center">
                        <div className="col-md mt20">
                            <ul className="grey-menu text-14 text-w-600">
                                <li className="current-menu-item"><a href="#">All</a></li>
                                <li><a href="#">Group</a></li>
                                <li><a href="#">Public</a></li>
                                <li><a href="#">Private</a></li>
                            </ul>
                        </div>
                        <div className="col-md-auto mt20 md-hide">
                            <a className="btn--module" href="#"><span className="btn__text">Create New Discussion</span></a>
                        </div>
                    </div>
                    <div className="mt30">
                        <div className="row mt-16">
                            {discussions.length > 0 ? discussions.map((discussion, index)=><LatestDiscussion discussion={discussion} key={`discussion-key-${index}`} />) : <p className='text-right'>Discussion not available.</p>}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Quiz;
