import React from "react";
import CourseContent from "../../Components/Common/CourseContent";
import DiscussionForum from "./DiscussionForum";

const DiscussionGroup = () =>{
    return (
        <>
            <main className="main">
                <div className="container">
                    <div className="cont-ch-wrap">
                        <div className="w945 is-change">
                            <div className="page-back">
                                <a href="#">
                                    <div className="row align-items-center px-7 d-inline-flex">
                                        <div className="col-auto">
                                            <div className="svg-image-back"></div>
                                        </div>
                                        <div className="col-auto text-9A text-14">Back to Courses</div>
                                    </div>
                                </a>
                            </div>
                            <div className="course-content-box" style={{ width: `799.5px`}}>
                                 <CourseContent />
                            </div>
                        </div>
                    </div>
                    <div className="mt70"></div>
                    <DiscussionForum />
                </div>
            </main>

        </>
    );
};
export default DiscussionGroup;