import React from 'react';
import MessageBanner from '../../Components/Common/MessageBanner';
import Header from './Header';
import Discussion from './Discussion';


const Index = () => {
    return (
        <>
            <main className="main">
                <div className="small-decor-head">
                    <picture><img className="responsive-img cover-img" src="/assets/app/base/images/sm-head-decor@1x.png" srcSet="/assets/app/base/images/sm-head-decor@1x.png, /assets/app/base/images/sm-head-decor@2x.png 2x" alt="" /></picture>
                </div>
                <div className="container mt-96 z10 mb-mt-126">
                    <MessageBanner />
                    <div className="user-all-course mt30">
                        <div className="user-all-course__head">
                            <Header />
                        </div>
                        <div className="user-all-course__body">
                            <div className="row align-items-center px-8 justify-content-between mt-30">
                                <div className="col-md-auto mt30 mb-mt16">
                                    <ul className="grey-menu text-14 text-w-600 grey-menu-md">
                                        <li className="current-menu-item"><a href="#">All</a></li>
                                        <li><a href="#">Group</a></li>
                                        <li><a href="#">Public</a></li>
                                        <li><a href="#">Private</a></li>
                                        <li><a href="#">Favorites</a></li>
                                    </ul>
                                </div>
                                <div className="col-lg-auto mt30 mb-mt16">
                                    <div className="row align-items-center justify-content-between mt-30 px-8 mbmt-16">
                                        <div className="col-lg-auto col-md-6 mt30 order-md-1 order-2 mb-mt16">
                                            <div className="search-module-el search-module-el-md">
                                                <div className="filter-search-field">
                                                    <input className="wpcf7-form-control" type="search" placeholder="Search..." />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-auto col-md-6 mt30 order-md-2 order-1 mb-mt16">
                                            <a className="btn-sort-by" href="#">
                                                <div className="btn-sort-by__icon">
                                                    <div className="svg-image-filter_list"></div>
                                                </div>
                                                <div className="btn-sort-by__title">Sort by:  </div>
                                                <div className="btn-sort-by__name">  Most recent</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='mt24 discuss-board-area'>
                                <Discussion />
                            </div>
                        </div>
                    </div>    
                    
                </div>
                <div className="mt70"></div>
            </main>
        </>
    );
};

export default Index;
