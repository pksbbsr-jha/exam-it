import React from 'react';
import MessageBanner from '../../Components/Common/MessageBanner';
import Header from './Header';
import ForumBox from '../../Components/Common/ForumBox';


const Forum = () => {

    const forumMessages = [
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 14,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 10,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 12,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 10,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 7,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 5,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 8,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },

    ];
    return (
        <>
            <main className="main">
                <div className="small-decor-head">
                    <picture><img className="responsive-img cover-img" src="/assets/app/base/images/sm-head-decor@1x.png" srcSet="/assets/app/base/images/sm-head-decor@1x.png, /assets/app/base/images/sm-head-decor@2x.png 2x" alt="" /></picture>
                </div>
                <div className="container mt-96 z10 mb-mt-126">
                    <MessageBanner />
                    <div className="user-all-course mt30">
                        <div className="user-all-course__head">
                            <Header />
                        </div>
                        <div className="forum-module">
                            <div className="forum-module-head">
                                <div className="row align-items-center justify-content-between">
                                    <div className="col-auto">
                                        <div className="row align-items-center">
                                            <div className="col-auto md-hide">
                                                <div className="filter-search-field">
                                                    <input className="wpcf7-form-control" type="search" placeholder="Search..." />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-auto"><a className="btn btn--sm-40 btn--white btn-create-group" href="#"> <span className="btn__icon">
                                        <svg width="12" height="12" viewBox="0 0 12 12" fill="none">
                                        <path d="M12 5.57143V6.42857C12 6.66527 11.8081 6.85714 11.5714 6.85714H6.85714V11.5714C6.85714 11.8081 6.66527 12 6.42857 12H5.57143C5.33473 12 5.14286 11.8081 5.14286 11.5714V6.85714H0.428571C0.191878 6.85714 0 6.66527 0 6.42857V5.57143C0 5.33473 0.191878 5.14286 0.428571 5.14286H5.14286V0.428571C5.14286 0.191878 5.33473 0 5.57143 0H6.42857C6.66527 0 6.85714 0.191878 6.85714 0.428571V5.14286H11.5714C11.8081 5.14286 12 5.33473 12 5.57143Z" fill="#2153CC"></path>
                                        </svg></span><span className="btn__text">New Message</span></a></div>
                                </div>
                            </div>
                            <ForumBox forumMessages={forumMessages}/>
                        </div>
                    </div>
                </div>
                <div className="mt70"></div>
            </main>
        </>
    );
};

export default Forum;
