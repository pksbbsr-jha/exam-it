import React from 'react';
import MessageBanner from '../../Components/Common/MessageBanner';
import LatestDiscussion from '../../Components/Common/LatestDiscussion';
import Header from './Header';


const Discussion = () => {
    const discussions = [
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Public',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Private',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Group',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Private',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Public',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Private',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Private',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Private',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Private',
            replies: '',
            asked_by: 'Username'
        },
    ];

    return (
        <>
            <div className='mt24 discuss-board-area'>
                <div className="row mt-16">
                    {discussions.length > 0 ? discussions.map((discussion, index)=><LatestDiscussion discussion={discussion} key={`discussion-key-${index}`} />) : <p className='text-right'>Discussion not available.</p>}
                </div>
            </div>
        </>
    );
};

export default Discussion;
