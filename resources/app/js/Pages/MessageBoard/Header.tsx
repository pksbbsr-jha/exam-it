import React from 'react';
import { Link } from '@inertiajs/inertia-react';

const Header = () => {

    return (
        <>
           <div className="row align-items-center px-20 mbpx-12 dashboard-menu">
                <div className="col-auto">
                    <Link className="text-w-600 text-6E is-active" href={`/dashboard-discussion`}>Content discussions</Link>
                </div>
                <div className="col-auto">
                    <Link className="text-w-600 text-6E" href={`/dashboard-chat`}>Chats</Link>
                </div>
                <div className="col-auto">
                    <Link className="text-w-600 text-6E" href={`/dashboard-forum`}>Forum</Link>
                </div>
            </div>
        </>
    );
};

export default Header;
