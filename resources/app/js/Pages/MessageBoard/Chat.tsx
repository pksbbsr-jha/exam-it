import React from 'react';
import MessageBanner from '../../Components/Common/MessageBanner';
import ChatMessage from '../../Components/Common/ChatMessage';
import Header from './Header';
import ChatInbox from '../../Components/Common/ChatInbox';


const Chat = () => {

    const forumMessages = [
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 14,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 10,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 12,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 10,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 7,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 5,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 8,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },

    ];
    const users = [
        {
            id: 1,
            name: 'Dianne_Russell', 
            message: 'Hi, how are you?',
            val: 1,
            img: {
                src: 'Avatar2@1x.jpg', 
                sourceSet:['Avatar2@1x.webp', 'Avatar2@2x.webp 2x'],
                srcSet:['Avatar2@1x.jpg', 'Avatar2@2x.jpg 2x']
            }
        },
        {
            id: 1,
            name: 'Dianne_Russell', 
            message: 'Hi, how are you?',
            val: 1,
            img: {
                src: 'Avatar2@1x.jpg', 
                sourceSet:['Avatar2@1x.webp', 'Avatar2@2x.webp 2x'],
                srcSet:['Avatar2@1x.jpg', 'Avatar2@2x.jpg 2x']
            }
        },
        {
            id: 1,
            name: 'Dianne_Russell', 
            message: 'Hi, how are you?',
            val: 1,
            img: {
                src: 'Avatar2@1x.jpg', 
                sourceSet:['Avatar2@1x.webp', 'Avatar2@2x.webp 2x'],
                srcSet:['Avatar2@1x.jpg', 'Avatar2@2x.jpg 2x']
            }
        },
        {
            id: 1,
            name: 'Dianne_Russell', 
            message: 'Hi, how are you?',
            val: 1,
            img: {
                src: 'Avatar2@1x.jpg', 
                sourceSet:['Avatar2@1x.webp', 'Avatar2@2x.webp 2x'],
                srcSet:['Avatar2@1x.jpg', 'Avatar2@2x.jpg 2x']
            }
        }
    ];
    const messages = [
        {
            name: 'Dianne_Russell', 
            message: 'Hi, how are you?',
            type: 'sender'
        },
        {
            name: 'Dianne_Russell', 
            message: 'Hi, how are you?',
            type: 'sender'
        },
        {
            name: 'Dianne_Russell', 
            message: 'Hi, how are you?',
            type: 'sender'
        },
        {
            name: 'you', 
            message: 'I’m good, thanks!',
            type: 'recived'
        }
    ]
    return (
        <>
            <main className="main">
                <div className="small-decor-head">
                    <picture><img className="responsive-img cover-img" src="/assets/app/base/images/sm-head-decor@1x.png" srcSet="/assets/app/base/images/sm-head-decor@1x.png, /assets/app/base/images/sm-head-decor@2x.png 2x" alt="" /></picture>
                </div>
                <div className="container mt-96 z10 mb-mt-126">
                    <MessageBanner />
                    <div className="user-all-course mt30">
                        <div className="user-all-course__head">
                            <Header />
                        </div>
                        <div className="user-all-course__body">
                            <div className="dashboard-chat-area">
                                <div className="row no-gutters">
                                    <div className="col-md-auto">
                                        <div className="dashboard-chat-area-left">
                                            <div className="dashboard-chat-menu">
                                                <ul className="grey-menu text-14 text-w-600">
                                                    <li className="current-menu-item"><a href="#">All</a></li>
                                                    <li><a href="#">Group</a></li>
                                                    <li><a href="#">Private</a></li>
                                                </ul>
                                            </div>
                                            <div className="dashboard-chat-search-area">
                                                <div className="row px-12">
                                                    <div className="col">
                                                        <div className="search-module-el">
                                                            <div className="filter-search-field">
                                                                <input className="wpcf7-form-control" type="search" placeholder="Search..." />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-auto">
                                                        <button className="btn-new-message button-tippy" type="button" data-template="btn-new-message-tippy">
                                                            <div className="svg-image-new_message"></div>
                                                        </button>
                                                        <div className="btn-tippy-area" id="btn-new-message-tippy">
                                                            <ul className="list">
                                                                <li>
                                                                    <div className="btn-t" data-fancybox data-src="#private-chat" data-modal="true">
                                                                        <div className="row px-6 align-items-center flex-nowrap">
                                                                            <div className="col-auto">
                                                                                <div className="svg-image-private-chat"></div>
                                                                            </div>
                                                                            <div className="col">
                                                                                <div className="text-14">New private chat</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="btn-t" data-fancybox data-src="#group-chat" data-modal="true">
                                                                        <div className="row px-6 align-items-center flex-nowrap">
                                                                            <div className="col-auto">
                                                                                <div className="svg-image-group-chat"></div>
                                                                            </div>
                                                                            <div className="col">
                                                                                <div className="text-14">New group chat</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div className="popup-inline-exa nopad" id="private-chat">
                                                        <form action="#">
                                                            <div className="svg-image-close" data-fancybox-close></div>
                                                            <div className="popup-inline-exa-head">
                                                                <div className="text-18 text-w-700">Invite a person to private chat</div>
                                                            </div>
                                                            <div className="popup-inline-exa-body form__styler">
                                                                <div className="row mt-16">
                                                                    <div className="col-12 mt16">
                                                                        <div className="field-label">Invite via Email or Username:</div>
                                                                        <input type="email" placeholder="Email or Username" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="popup-inline-exa-footer">
                                                                <div className="row justify-content-between">
                                                                    <div className="col-auto"><a className="btn--def" href="#" data-fancybox-close><span className="btn__text">Cancel</span></a></div>
                                                                    <div className="col-auto"><a className="btn btn--blue" href="#"><span className="btn__text">Save</span></a></div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div className="popup-inline-exa nopad" id="group-chat">
                                                        <form action="#">
                                                            <div className="svg-image-close" data-fancybox-close></div>
                                                            <div className="popup-inline-exa-head">
                                                                <div className="text-18 text-w-700">Invite people to group chat</div>
                                                            </div>
                                                            <div className="popup-inline-exa-body form__styler">
                                                                <div className="row mt-16">
                                                                    <div className="col-12 mt16">
                                                                        <div className="field-label">Group chat name</div>
                                                                        <input type="email" placeholder="Group chat name" />
                                                                    </div>
                                                                    <div className="col-12 mt16">
                                                                        <div className="field-label">Invite via Email or Username:</div>
                                                                        <input type="email" placeholder="Email or Username" />
                                                                            <div className="mt8"></div>
                                                                            <input type="email" placeholder="Email or Username" />
                                                                                <div className="mt12"></div><a className="text-14 text-blue" href="#">+ add line</a>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                                <div className="popup-inline-exa-footer">
                                                                    <div className="row justify-content-between">
                                                                        <div className="col-auto"><a className="btn--def" href="#" data-fancybox-close><span className="btn__text">Cancel</span></a></div>
                                                                        <div className="col-auto"><a className="btn btn--blue" href="#"><span className="btn__text">Save</span></a></div>
                                                                    </div>
                                                                </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="dashboard-chat-messages"> 
                                                {users.length > 0 ? users.map((message, index)=><ChatMessage message={message} key={`message-key-${index}`} />) : <p className='text-right'>Message not available.</p>}
                                                    
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md">
                                        <ChatInbox />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mt70"></div>
            </main>
        </>
    );
};

export default Chat;
