import React from 'react';

export interface IUser {
    name: string;
    age: number;
}


const Index = () => {

    return (
        <>
            <main className="main">
                <div className="container mt61 z10">
                    <div className="row px-64 align-items-lg-start align-items-end">
                        <div className="col-md-auto">
                            <div className="welcome-cont">
                                <div className="wf-lg-460 text-def">
                                    <h1>This is the Future of Learning</h1>
                                    <p>Examit’s AP course offerings provide a revolutionary model of self-paced AP exam preparation with integrated social connectivity amongst users. Our AP content is created by AP teachers!</p>
                                </div>
                                <div className="row px-8">
                                    <div className="col-auto"><a className="btn btn--md btn--blue mt40" href="#"> <span className="btn__text">Sign Up for Free</span></a></div>
                                    <div className="col-auto sm-show"><a className="btn btn--md btn--white mt40" href="#"> <span className="btn__text">Log in</span></a></div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md mw1-fix">
                            <div className="image-with-decor image-right image-welcome">
                                <div className="svg-image-dots-decor welcome-decor"></div>
                                <picture>
                                    <source srcSet={`/assets/app/base/images/welcome@1x.webp, /assets/app/base/images/welcome@2x.webp 2x`} type="image/webp" /><img className="responsive-img cover-img" src={`/assets/app/base/images/welcome@1x.jpg`}  srcSet={`/assets/app/base/images/welcome@1x.jpg, /assets/app/base/images/welcome@2x.jpg 2x`} alt="" />
                                </picture>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="bg--green pt195 text-white mt-80 pos-r ov-h" id="about">
                    <div className="svg-image-dots-decor-green"></div>
                    <div className="container">
                        <div className="w942">
                            <div className="text-def wf-750">
                                <h3>AP Physics C: Mechanics Test Prep</h3>
                                <p>This self-paced, eight-chapter course comes with 34 modules covering all content required by the AP College Board. Over 300 multiple choice practice questions mimicking the language and style of actual AP questions includes referenced links to the relevant content and answer explanations. </p>
                            </div>
                            <div className="macbook-demo mt64 sm-hide">
                                <picture>
                                    <source srcSet={`/assets/app/base/images/demo@1x.webp, /assets/app/base/images/demo@2x.webp 2x`} type="image/webp" /><img className="responsive-img cover-img" src={`/assets/app/base/images/demo@1x.jpg`}  srcSet={`/assets/app/base/images/demo@1x.jpg, /assets/app/base/images/demo@2x.jpg 2x`} alt="" />
                                </picture>
                            </div>
                            <div className="mt64 sm-show">
                                <picture>
                                    <source srcSet={`/assets/app/base/images/GalaxyS8@1x.webp, /assets/app/base/images/GalaxyS8@2x.webp 2x`} type="image/webp" /><img className="responsive-img cover-img" src={`/assets/app/base/images/GalaxyS8@1x.png`}  srcSet={`/assets/app/base/images/GalaxyS8@1x.png, /assets/app/base/images/GalaxyS8@2x.png 2x`} alt="" />
                                </picture>
                            </div>
                            <div className="mt-1"></div>
                        </div>
                    </div>
                </div>
                <div className="container ptb110" id="what-you-will-learn">
                    <div className="row mt-180 row-reverse">
                        <div className="col-12 mt180">
                            <div className="row align-items-center px-64 mt-32">
                                <div className="col-md-auto mt32">
                                    <div className="wf-lg-460 text-def text-18">
                                        <h3>What you will learn?</h3>
                                        <p>Examit’s course content is created by AP teachers who have deep knowledge and experience in content and pedagogy. The scope and sequence of these courses cover all the content required by the AP College Board and practice problems reflect the language and problem style of real AP exams. Students who want to take the AP exam, but don’t have the option of taking the class at school, can now use this site exclusively to earn a top score.</p>
                                    </div>
                                    <a className="mt40 btn btn--md btn--white btn--wide sm-hide" href="#"><span className="btn__text">Browse Course</span></a>
                                </div>
                                <div className="col-md mw1-fix mt32">
                                    <div className="image-with-decor image-right h500 pos-r">
                                        <div className="svg-image-dots-decor-green-small"></div>
                                        <picture>
                                            <source srcSet={`/assets/app/base/images/art1@1x.webp, /assets/app/base/images/art1@2x.webp 2x 2x`} type="image/webp" /><img className="responsive-img cover-img" src={`/assets/app/base/images/art1@1x.jpg`}  srcSet={`/assets/app/base/images/art1@1x.jpg, /assets/app/base/images/art1@2x.jpg 2x`} alt="" />
                                        </picture>
                                    </div>
                                    <div className="sm-show mt40"><a className="btn btn--md btn--white btn--wide" href="#"><span className="btn__text">Browse Course</span></a></div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 mt180">
                            <div className="row align-items-center px-64 mt-32">
                                <div className="col-md-auto mt32">
                                    <div className="wf-lg-460 text-def text-18">
                                        <h3>Learn Together</h3>
                                        <p>Students can draw upon the knowledge of the entire Examit student community in our revolutionary learning model. Students can create study groups, befriend other users, or become a contributor in the world community by posting and answering questions and comments referenced in the text by other users. Students can see discussion threads of relevant content embedded into the text margins of their modules to better reinforce their understanding.</p>
                                    </div>
                                    <a className="mt40 btn btn--md btn--white btn--wide sm-hide" href="#"><span className="btn__text">Try EXAMiT</span></a>
                                </div>
                                <div className="col-md mw1-fix mt32">
                                    <div className="image-with-decor image-left h500 pos-r">
                                        <div className="svg-image-dots-decor-grey"></div>
                                        <picture>
                                            <source srcSet={`/assets/app/base/images/art2@1x.webp, /assets/app/base/images/art2@2x.webp 2x`} type="image/webp" /><img className="responsive-img cover-img" src={`/assets/app/base/images/art2@1x.jpg`}  srcSet={`/assets/app/base/images/art1@1x.jpg, /assets/app/base/images/art2@2x.jpg 2x`} alt="" />
                                        </picture>
                                    </div>
                                    <div className="sm-show mt40"><a className="btn btn--md btn--white btn--wide" href="#"><span className="btn__text">Try EXAMiT</span></a></div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 mt180">
                            <div className="row align-items-center px-64 mt-32">
                                <div className="col-md-auto mt32">
                                    <div className="wf-lg-460 text-def text-18">
                                        <h3>Learn with Experts</h3>
                                        <p>All content is created by licensed AP teachers who have mastered their teaching and delivery.  These teachers are able to identify where students traditionally have difficulty and can provide several perspectives of understanding.  Content and practice problems are constantly being reviewed and updated for optimal effectiveness.</p>
                                    </div><a className="mt40 btn btn--md btn--white btn--wide sm-hide" href="#"><span className="btn__text">Sign Up for Free</span></a>
                                </div>
                                <div className="col-md mw1-fix mt32">
                                    <div className="image-with-decor image-right h500 pos-r">
                                        <div className="svg-image-dots-decor-green-small"></div>
                                        <picture>
                                            <source srcSet={`/assets/app/base/images/art3@1x.webp, /assets/app/base/images/art3@2x.webp 2x`} type="image/webp" /><img className="responsive-img cover-img" src={`/assets/app/base/images/art3@1x.jpg`}  srcSet={`/assets/app/base/images/art3@1x.jpg, /assets/app/base/images/art3@2x.jpg 2x`} alt="" />
                                        </picture>   
                                    </div>
                                    <div className="sm-show mt40"><a className="btn btn--md btn--white btn--wide" href="#"><span className="btn__text">Sign Up for Free</span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="bg--white ptb110" id="testimonials">
                    <div className="container">
                        <h3 className="text-36">What our students say:</h3>
                        <div className="mt50"></div>
                        <div className="wrapper-bq">
                            <div className="row no-gutters">
                                <div className="col-md-auto md-hide">
                                    <div className="bq-slider-decor">
                                        <div className="svg-image-bq"></div>
                                    </div>
                                </div>
                                <div className="col-lg mw1-fix">
                                    <div className="swiper-bq-slider-out">
                                        <div className="swiper swiper-bq-slider">
                                            <div className="swiper-wrapper">
                                                <div className="swiper-slide">
                                                    <div className="row align-items-center px-6 anim-1">
                                                        <div className="col-auto">
                                                            <div className="row px-4">
                                                                <div className="col-auto">
                                                                    <div className="svg-image-star"></div>
                                                                </div>
                                                                <div className="col-auto">
                                                                    <div className="svg-image-star"></div>
                                                                </div>
                                                                <div className="col-auto">
                                                                    <div className="svg-image-star"></div>
                                                                </div>
                                                                <div className="col-auto">
                                                                    <div className="svg-image-star"></div>
                                                                </div>
                                                                <div className="col-auto">
                                                                    <div className="svg-image-star"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-auto text-14 text-light-dark text-inter">by Capterra</div>
                                                    </div>
                                                    <div className="mt24">
                                                        <blockquote className="bq-slider">
                                                            <div className="bq-desc anim-2">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad mini</p>
                                                            </div>
                                                            <footer className="bq-footer mt40">
                                                                <div className="bq-author text-w-600 anim-3">Cameron Williamson</div>
                                                                <div className="mt4 text-light-dark anim-4">Student</div>
                                                            </footer>
                                                        </blockquote>
                                                    </div>
                                                </div>
                                                <div className="swiper-slide">
                                                    <div className="row align-items-center px-6 anim-1">
                                                        <div className="col-auto">
                                                            <div className="row px-4">
                                                                <div className="col-auto">
                                                                    <div className="svg-image-star"></div>
                                                                </div>
                                                                <div className="col-auto">
                                                                    <div className="svg-image-star"></div>
                                                                </div>
                                                                <div className="col-auto">
                                                                    <div className="svg-image-star"></div>
                                                                </div>
                                                                <div className="col-auto">
                                                                    <div className="svg-image-star"></div>
                                                                </div>
                                                                <div className="col-auto">
                                                                    <div className="svg-image-star"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-auto text-14 text-light-dark text-inter">by Capterra</div>
                                                    </div>
                                                    <div className="mt24">
                                                        <blockquote className="bq-slider">
                                                            <div className="bq-desc anim-2">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat.</p>
                                                            </div>
                                                            <footer className="bq-footer mt40">
                                                                <div className="bq-author text-w-600 anim-3">Cameron Williamson</div>
                                                                <div className="mt4 text-light-dark anim-4">Student</div>
                                                            </footer>
                                                        </blockquote>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="swiper-bq-arrows">
                                            <div className="row px-12">
                                                <div className="col-auto">
                                                    <div className="svg-image-arrow_prev arrow-exa"></div>
                                                </div>
                                                <div className="col-auto">
                                                    <div className="svg-image-arrow_next arrow-exa"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="swiper-bq-paginations mt40"></div>
                    </div>
                </div>
                <div className="ptb110" id="faq">
                    <div className="container">
                        <div className="w750">
                            <h3 className="text-36 tac">Frequently Asked Questions</h3>
                            <div className="mt48">
                                <ul className="list accordion-list">
                                    <li>
                                        <div className="accordion-wrap">
                                            <div className="accordion-title is-active">
                                                <div className="row justify-content-between align-items-center">
                                                    <div className="col">
                                                        <h4 className="text-w-500 text-21">What is EXAMiT?</h4>
                                                    </div>
                                                    <div className="col-auto">
                                                        <div className="svg-image-icnPlus"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="accordion-body text-def">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="accordion-wrap">
                                            <div className="accordion-title">
                                                <div className="row justify-content-between align-items-center">
                                                    <div className="col">
                                                        <h4 className="text-w-500 text-21">Lorem ipsum dolor sit amet?</h4>
                                                    </div>
                                                    <div className="col-auto">
                                                        <div className="svg-image-icnPlus"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="accordion-body text-def">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="accordion-wrap">
                                            <div className="accordion-title">
                                                <div className="row justify-content-between align-items-center">
                                                    <div className="col">
                                                        <h4 className="text-w-500 text-21">Lorem ipsum dolor sit amet?</h4>
                                                    </div>
                                                    <div className="col-auto">
                                                        <div className="svg-image-icnPlus"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="accordion-body text-def">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="accordion-wrap">
                                            <div className="accordion-title">
                                                <div className="row justify-content-between align-items-center">
                                                    <div className="col">
                                                        <h4 className="text-w-500 text-21">Lorem ipsum dolor sit amet?</h4>
                                                    </div>
                                                    <div className="col-auto">
                                                        <div className="svg-image-icnPlus"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="accordion-body text-def">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat.</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="bg--white ptb110 pt170 pos-r sm-hide">
                    <div className="line-decor-grey"></div>
                    <div className="line-decor">
                        <div className="w750 pos-r">
                            <div className="svg-image-dots-decor-white"></div>
                        </div>
                    </div>
                    <div className="container">
                        <div className="row align-items-center px-62">
                            <div className="col-md-auto">
                                <div className="wf-lg-460 text-def text-18">
                                    <h3>Not at school? No problem.  </h3>
                                    <p>Examit’s functionality is accessible while you’re traveling or just have a few extra minutes to spare.  The random question generator gives users valuable practice either on your computer or phone.  Score well to earn your quizmaster badge!</p>
                                </div><a className="mt40 btn btn--md btn--blue" href="#"><span className="btn__text">Get Started Today</span></a>
                            </div>
                            <div className="col-md">
                                <div className="GalaxyS8Right-wrap">
                                    <div className="mb-demo-image">
                                        <picture>
                                            <source srcSet={`/assets/app/base/images/mobile-exa@1x.webp, /assets/app/base/images/mobile-exa@2x.webp 2x`} type="image/webp" /><img className="responsive-img cover-img" src={`/assets/app/base/images/mobile-exa@1x.jpg`}  srcSet={`/assets/app/base/images/mobile-exa@1x.jpg, /assets/app/base/images/mobile-exa@2x.jpg 2x`} alt="" />
                                        </picture>
                                    </div>
                                    <div className="svg-image-GalaxyS8Right"></div>
                                    <div className="shadow-GalaxyS8Right"></div>
                                </div>
                                <div className="iPad-wrap">
                                    <div className="iPad-demo-image">
                                        <picture>
                                            <source srcSet={`/assets/app/base/images/tablet-exa@1x.webp, /assets/app/base/images/tablet-exa@2x.webp 2x`} type="image/webp" /><img className="responsive-img cover-img" src={`/assets/app/base/images/tablet-exa@1x.jpg`}  srcSet={`/assets/app/base/images/tablet-exa@1x.jpg, /assets/app/base/images/tablet-exa@2x.jpg 2x`} alt="" />
                                        </picture>
                                    </div>
                                    <div className="svg-image-iPad"></div>
                                    <div className="shadow-iPad"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </>
    );
};

export default Index;
