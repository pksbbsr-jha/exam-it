import React, {useState, FormEvent, ChangeEvent} from "react";
import {Inertia} from '@inertiajs/inertia';

const AccountSettings = () =>{
    const [values, setValues] = useState({})
 
      function handleChange(event:ChangeEvent<HTMLInputElement>) {
        setValues({...values, [event.target.name]: event.target.value});
      }
    
      function handleSubmit(event:FormEvent<HTMLFormElement>) {
        event.preventDefault()
        //Inertia.post('/users', values)
      }

    return(
        <>
           <div className="account-box">
                <div className="account-box-head">
                    <div className="text-w-600">Account settings</div>
                </div>
                <div className="account-box-content">
                    <form className="form__styler" onSubmit={handleSubmit}>
                        <div className="row mt-24">
                            <div className="col-12 mt24 sm-hide">
                                <div className="field-label">User picture</div>
                                <div className="row px-12 align-items-center">
                                    <div className="col-auto">
                                        <div className="user-photo">
                                            <div className="svg-image-account_photo-def"></div>
                                        </div>
                                    </div>
                                    <div className="col"><a className="btn btn--white btn--sm-40 btn-emulate-file-select" href="#"> <span className="btn__text">Upload New User Picture</span></a>
                                        <div className="user-select-photo">
                                            <input type="file" id="file" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 mt24">
                                <div className="field-label">Full name</div>
                                <input type="text" id="fullname" name="fullname"  onChange={handleChange} required={true}/>
                            </div>
                            <div className="col-12 mt24">
                                <div className="field-label">Username</div>
                                <input type="text" id="username" name="username"  onChange={handleChange} required={true}/>
                                
                            </div>
                            <div className="col-12 mt24">
                                <div className="field-label">Email</div>
                                <input type="email" id="email" name="email"  onChange={handleChange} required={true}/>
                            </div>
                            <div className="col-12 mt24">
                                <div className="field-label">New password</div>
                                <input type="password" id="password" name="password"  onChange={handleChange} required={true}/>
                               
                            </div>
                            <div className="col-12 mt24">
                                <div className="field-label">Confirm new password</div>
                                <input type="password" id="confirm_password" name="confirm_password"  onChange={handleChange} required={true}/>
                                
                            </div>
                            <div className="col-12 mt32">
                                <div className="row">
                                    <div className="col-xl-4 col-lg-5 col-md-6">
                                        <input type="submit" value="Save Changes" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> 
        </>
    );
};
export default AccountSettings;