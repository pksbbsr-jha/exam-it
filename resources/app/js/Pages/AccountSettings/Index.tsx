import React from "react";
import AccountSettings from './AccountSettings';
import PaymentInfo from "./PaymentInfo";

const Index =() =>{
    return (
        <>
            <main className="main">
                <div className="container mt30">
                    <div className="row mt-30">
                        <div className="col-lg-6 mt30">
                            <AccountSettings />
                        </div>
                        <div className="col-lg-6 mt30">
                            <PaymentInfo />
                        </div>
                    </div>
                    <div className="mt100"></div>
                </div>
            </main>
        </>
    );
};
export default Index;