import React from "react";

const PaymentInfo = () => {
    return(
        <>
           <div className="account-box">
                <div className="account-box-head">
                    <div className="text-w-600">Payment information</div>
                </div>
                <div className="payment-mobile sm-show mt-24">
                    <div className="mt24">
                        <div className="text-14 text-w-600 text-6E">Order</div>
                        <div className="mt8 text-14">Course name</div>
                    </div>
                    <div className="mt24">
                        <div className="text-14 text-w-600 text-6E">Date</div>
                        <div className="mt8 text-14">09/04/21</div>
                    </div>
                    <div className="mt24">
                        <div className="text-14 text-w-600 text-6E">Amount</div>
                        <div className="mt8 text-14">$130</div>
                    </div>
                    <div className="mt24">
                        <div className="text-14 text-w-600 text-6E">Invoice</div>
                        <div className="mt8 text-14"><a href="#" download>Download invoice</a></div>
                    </div>
                </div>
                <div className="payment-table payment-table--head sm-hide">
                    <div className="payment-col">
                        <div className="text-14 text-w-600">Order</div>
                    </div>
                    <div className="payment-col">
                        <div className="text-14 text-w-600">Date</div>
                    </div>
                    <div className="payment-col">
                        <div className="text-14 text-w-600">Amount</div>
                    </div>
                    <div className="payment-col">
                        <div className="text-14 text-w-600">Invoice</div>
                    </div>
                </div>
                <div className="payment-table sm-hide">
                    <div className="payment-col">
                        <div className="text-14">Course name</div>
                    </div>
                    <div className="payment-col">
                        <div className="text-14">09/04/21</div>
                    </div>
                    <div className="payment-col">
                        <div className="text-14">$130</div>
                    </div>
                    <div className="payment-col">
                        <div className="text-14"><a href="#" download>Download invoice</a></div>
                    </div>
                </div>
                <div className="payment-card-area">
                    <div className="field-label">Main payment card</div>
                    <div className="card-el">
                        <div className="svg-image-Visa"> </div>
                        <div className="card-hidden-number mt16">**** **** **** 0099</div><a className="btn btn--white btn--sm-40 mt16" href="#"> <span className="btn__text">Change Card</span></a>
                    </div>
                </div>
            </div>
        </>
    );
};
export default PaymentInfo;