import React from 'react';

type CourseProps = {
    course: {
        id: number,
        name: string
    }
};

const Course = (props: CourseProps) => {
    const course = props.course;
    return (
        <>
            <div className="col">
                <div className="text-16 text-w-600">{props.course.name}</div>
                <div className="course-progress-wrap mt16">
                    <div className="row justify-content-between">
                        <div className="col-auto">
                            <div className="text-14">33%</div>
                        </div>
                    </div>
                    <div className="mt8">
                        <div className="progress-course">
                            <div className="cssProgress-success" style={{width:`33%`}}></div>
                        </div>
                    </div>
                </div>
                   
            </div>
        </>
    );
};

export default Course;
