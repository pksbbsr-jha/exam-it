import React from 'react';
import Course from './Course';
import MyAchievents from './MyAchievents';

const Index =() =>{
    const courses = [
        {
            id: 1,
            name: 'Demo course name',
        }
    ];
    const achievments = [
        {
            name: 'Award for Demo title',
            class: 'svg-image-bronze-ach',
            received_on: '20 January 2021',
        },
        {
            name: 'Award for Demo title',
            class: 'svg-image-silver-ach',
            received_on: '20 January 2021',
        }
        
    ]
    return (
        <>
            <main className="main">
                <div className="small-decor-head">
                    <picture><img className="responsive-img cover-img" src="/assets/app/base/images/unsplash_J4kK8b9Fgj8@1x.png" srcSet="/assets/app/base/images/unsplash_J4kK8b9Fgj8@1x.png, /assets/app/base/images/unsplash_J4kK8b9Fgj8@2x.png 2x" alt="" /></picture>
                </div>
                <div className="container mt-96 z10">
                    <div className="row mt-30">
                        <div className="col-lg-4 mt30">
                            <aside className="aside-profile">
                                <div className="profile-photo">
                                <picture>
                                    <img src="/assets/app/base/images/wade@1x.jpg" srcSet="/assets/app/base/images/wade@1x.jpg, images/wade@2x.jpg 2x" alt="" />
                                </picture>
                                </div>
                                <div className="profile-name mt16">Wade Warren</div>
                                <div className="profile-pos mt4">Student</div>
                                <div className="mt24"> 
                                    <a className="btn btn--blue btn--sm-40 btn--wide" href="#">
                                        <span className="btn__icon">
                                            <div className="svg-image-sms_outlined"></div>
                                        </span>
                                        <span className="btn__text">Send Message</span>
                                    </a>
                                </div>
                            </aside>
                        </div>
                        <div className="col-lg-8 mt30">
                            <div className="profile-progress">
                                <div className="profile-progress-head">
                                <div className="text-w-600">Progress & Achievements</div>
                                </div>
                                <div className="profile-progress-body">
                                    <div className="course-progress-item"> 
                                        <div className="row px-11"> 
                                            <div className="col-auto">
                                                <div className="course-progress-item__image"></div>
                                            </div>
                                            <div className="col">
                                                <div className="text-16 text-w-600">
                                                    {courses.length > 0 ? courses.map((course, index) => <Course key={index} course={course} />) : <p className='text-right'>Courses not available.</p>}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <MyAchievents achievments={achievments}/>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </>
    );
};
export default Index;