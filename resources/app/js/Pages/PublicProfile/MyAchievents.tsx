import React from 'react';

type MyAchievmentsProps = {
    achievments: {
        name: string, 
        class: string,
        received_on: string
    }[]
}
const MyAchievments = (props:MyAchievmentsProps) => {

    return (
        <>
            <div className="user-all-course">
                <div className="user-all-course__body">
                    <div className="row px-12 mt-24">
                        {props.achievments.length > 0 ? props.achievments.map((achievment, index)=><div className="col-lg-6 col-md-6 mt24" key={index}>
                                <a className="achievment-box" href="#">
                                    <div className="achievment-box__icon">
                                        <div className={achievment.class}></div>
                                    </div>
                                    <div className="achievment-box__cont">
                                        <div className="text-16 text-w-600">{achievment.name}</div>
                                        <div className="mt8 text-14 text-6E">{achievment.received_on}</div>
                                    </div>
                                </a>
                            </div>) : <p>Achievment not found.</p>}
                    </div>
                </div>
            </div>
        </>
    );
};

export default MyAchievments;
