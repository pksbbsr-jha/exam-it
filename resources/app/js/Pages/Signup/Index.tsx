import React from 'react';

const Index = () => {
    
    // const [userRegistration, setuserRegistration] = useState({
    //     fullname : "",
    //     username : "",
    //     email : "",
    //     password : ""
    // });
  
    return (
        <>
            <main className="main">
                <div className="grid-3-col grid-3-col-mb">
                    <div className="form-top-panel sm-hide">
                        <div className="text-14 tar">Already have an account? <a href="#">Sign In</a></div>
                    </div>
                    <div className="mt24 sm-show"></div>
                    <div className="container">
                        <div className="w460">
                        <div className="logo tac">
                            <img className="responsive-img cover-img" src="/assets/app/base/images/logo.svg" alt="" />
                        </div>
                        <div className="mt32">
                            <form className="form__styler form-white-box password-strength-wrap" action="#">
                            <div className="text-24 tac text-w-600">Create your ExamiT account</div>
                            <div className="row px-8 mt16">
                                <div className="col-md-6 mt16"><a className="btn-social" href="#"> <span className="btn__icon">
                                    <div className="svg-image-google"></div></span><span className="btn__text text-16 text-w-600">Google</span></a></div>
                                <div className="col-md-6 mt16"><a className="btn-social" href="#"> <span className="btn__icon">
                                    <div className="svg-image-facebook"></div></span><span className="btn__text text-16 text-w-600">Facebook</span></a></div>
                            </div>
                            <div className="mt32">
                                <div className="or-email"><span>or with email</span></div>
                            </div>
                            <div className="mt24"> 
                                <div className="field-label">Full name </div>
                                <div className="wpcf7-form-control-wrap">
                                <input className="wpcf7-form-control" name="fullname" id="fullname" value="" type="text" placeholder="Full name" />
                                </div>
                            </div>
                            <div className="mt24"> 
                                <div className="field-label">Username</div>
                                <div className="wpcf7-form-control-wrap">
                                <input className="wpcf7-form-control" name="username" id="username" value=""type="text" placeholder="Username" />
                                </div>
                            </div>
                            <div className="mt24"> 
                                <div className="field-label">Email</div>
                                <div className="wpcf7-form-control-wrap">
                                <input className="wpcf7-form-control" name="email" id="email" value="" type="text" placeholder="example@email.com" />
                                </div>
                            </div>
                            <div className="mt24"> 
                                <div className="field-label">Password</div>
                                <div className="wpcf7-form-control-wrap">
                                <input className="wpcf7-form-control wpcf7-password"name="password" id="password" value="" type="password" placeholder="Enter a strong password" />
                                </div>
                                <div className="mt12 row px-4"> 
                                <div className="col-3"> 
                                    <div className="password-strength password-strength-1"></div>
                                </div>
                                <div className="col-3"> 
                                    <div className="password-strength password-strength-2"></div>
                                </div>
                                <div className="col-3"> 
                                    <div className="password-strength password-strength-3"></div>
                                </div>
                                <div className="col-3"> 
                                    <div className="password-strength password-strength-4"></div>
                                </div>
                                </div>
                            </div>
                            <div className="mt24 checkbox-styled"><span className="wpcf7-form-control-wrap"><span className="wpcf7-form-control wpcf7-checkbox">
                                    <label>
                                    <input type="checkbox" /><span className="wpcf7-list-item-label">I accept the <a href="#">Terms of use </a>and the <a href="#">Privacy policy</a></span>
                                    </label></span></span></div>
                            <div className="mt40"> 
                                <input type="submit" value="Create Account" disabled />
                            </div>
                            </form>
                        </div>
                        </div>
                    </div>
                    <div className="form-top-panel sm-show">
                        <div className="text-14 tar">Already have an account? <a href="/signup">Sign up</a></div>
                    </div>
                    <div className="mt100 sm-hide">                       </div>
                </div>
            </main>
           
        </>
    );
};

export default Index;
