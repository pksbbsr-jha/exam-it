import React from 'react';

const Index = () => {
    
    return (
        <>
            <main className="main">
                <div className="grid-3-col grid-3-col-mb">
                    <div className="form-top-panel sm-hide">
                        <div className="text-14 tar">Don’t have an account? <a href="#">Sign up</a></div>
                    </div>
                    <div className="mt24 sm-show"></div>
                    <div className="container">
                        <div className="w460">
                        <div className="logo tac"><img src="images/logo.svg" alt=""/></div>
                        <div className="mt32">
                            <form className="form__styler form-white-box password-strength-wrap" action="#">
                            <div className="text-24 tac text-w-600">Sign in to your account</div>
                            <div className="row px-8 mt16">
                                <div className="col-md-6 mt16"><a className="btn-social" href="#"> <span className="btn__icon">
                                    <div className="svg-image-google"></div></span><span className="btn__text text-16 text-w-600">Google</span></a></div>
                                <div className="col-md-6 mt16"><a className="btn-social" href="#"> <span className="btn__icon">
                                    <div className="svg-image-facebook"></div></span><span className="btn__text text-16 text-w-600">Facebook</span></a></div>
                            </div>
                            <div className="mt32">
                                <div className="or-email"><span>or with email</span></div>
                            </div>
                            <div className="mt24"> 
                                <div className="field-label">Email</div>
                                <div className="wpcf7-form-control-wrap">
                                <input className="wpcf7-form-control" type="text" placeholder="example@email.com"/>
                                </div>
                            </div>
                            <div className="mt24"> 
                                <div className="row align-items-center justify-content-between">
                                <div className="col-auto">
                                    <div className="field-label">Password</div>
                                </div>
                                <div className="col-auto"> <a href="#">Forgot your password?</a></div>
                                <div className="col-12 mt8"></div>
                                </div>
                                <div className="wpcf7-form-control-wrap">
                                <input className="wpcf7-form-control wpcf7-password" type="password" placeholder="••••••••"/>
                                </div>
                            </div>
                            <div className="mt40"> 
                                <input type="submit" value="Sign In" disabled/>
                            </div>
                            </form>
                        </div>
                        </div>
                    </div>
                    <div className="form-top-panel sm-show">
                        <div className="text-14 tar">Already have an account? <a href="#">Sign up</a></div>
                    </div>
                    <div className="mt100 sm-hide">                 </div>
                    </div>
            </main>
           
        </>
    );
};

export default Index;
