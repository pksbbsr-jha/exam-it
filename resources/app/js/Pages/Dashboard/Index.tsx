import React from 'react';
import Banner from './Banner';
import Course from '../../Components/Common/Course';
import CourseHeader from '../../Components/Common/CourseHeader';
import LatestDiscussion from '../../Components/Common/LatestDiscussion'
import LatestChat from '../../Components/Common/LatestChat';
import MyGroups from './MyGroups';
import MyAchievents from '../../Components/Common/MyAchievents';
import ForumBox from '../../Components/Common/ForumBox';

const Index = () => {
    const courses = [
        {
            id: 1,
            name: 'AP Physics A: Mechanics', 
            img: {
                src: 'sm-head-decor@1x.png', 
                sourceSet:['course-demo@1x.webp', 'course-demo@2x.webp 2x'],
                srcSet:['course-demo@1x.jpg', 'course-demo@2x.jpg 2x']
            }
        },
        {
            id: 2,
            name: 'AP Physics B: Mechanics', 
            img: {
                src: 'cd2@1x.jpg', 
                sourceSet:['cd2@1x.webp', 'cd2@2x.webp 2x'],
                srcSet:['course-demo@1x.jpg', 'course-demo@2x.jpg 2x']
            }
        },
        {
            id: 3,
            name: 'AP Physics C: Mechanics', 
            img: {
                src: 'cd3@1x.jpg', 
                sourceSet:['cd3@1x.webp', 'cd3@2x.webp 2x'],srcSet:['cd3@1x.jpg', 'cd3@2x.jpg 2x']
            }
        }
    ];
    const discussions = [
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Public',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Private',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Group',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Private',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Public',
            replies: '',
            asked_by: 'Username'
        },
        {
            topic: 'Module 3.1: Vectors and Scalars', 
            description: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun',
            type: 'Private',
            replies: '',
            asked_by: 'Username'
        },
    ];
    
    const messages = [
        {
            name: 'Jacob_Jones', 
            message: 'Hi, how are you?',
        },
        {
            name: 'Jacob_Jones', 
            message: 'Hi, how are you?',
        },
        {
            name: 'Jacob_Jones', 
            message: 'Hi, how are you?',
        },
        {
            name: 'Jacob_Jones', 
            message: 'Hi, how are you?',
        },
        {
            name: 'Jacob_Jones', 
            message: 'Hi, how are you?',
        },
        {
            name: 'Jacob_Jones', 
            message: 'Hi, how are you?',
        },
        {
            name: 'Jacob_Jones', 
            message: 'Hi, how are you?',
        },
    ];
    const forumMessages = [
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 14,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 10,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 12,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 10,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 7,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 5,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },
        {
            title: 'Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidun	', 
            totalReplies: 8,
            author: 'Jacob_Jones', 
            lastMessageTime: '02/02/2022 • 1:30 PM'
        },

    ];
    const groups = [
        {
            name: 'Demo group name', 
            permission: 'Admin',
            totalMembers: 10,
            members:[
                {
                    name: 'mike_schneller (admin)', 
                    status: 0
                },
                {
                    name: 'leslie_alexander', 
                    status: 1
                },
                {
                    name: 'rdebbie_baker', 
                    status: 0
                },
                {
                    name: 'jessica.hanson@example.com', 
                    status: 1
                },
                {
                    name: 'debbie.baker@example.com', 
                    status: 1
                }
            ]
        },
        {
            name: 'Demo group name 2', 
            permission: 'User',
            totalMembers: 15,
            members:[
                {
                    name: 'mike_schneller (admin)', 
                    status: 1
                },
                {
                    name: 'leslie_alexander', 
                    status: 1
                },
                {
                    name: 'rdebbie_baker', 
                    status: 1
                },
                {
                    name: 'jessica.hanson@example.com', 
                    status: 1
                },
                {
                    name: 'debbie.baker@example.com', 
                    status: 1
                }
            ]
        },
        {
            name: 'Demo group name 3', 
            permission: 'User',
            totalMembers: 5,
            members:[
                {
                    name: 'mike_schneller (admin)', 
                    status: 1
                },
                {
                    name: 'leslie_alexander', 
                    status: 1
                },
                {
                    name: 'rdebbie_baker', 
                    status: 1
                },
                {
                    name: 'jessica.hanson@example.com', 
                    status: 1
                },
                {
                    name: 'debbie.baker@example.com', 
                    status: 0
                }
            ]
        },
        {
            name: 'Demo group name 4', 
            permission: 'Admin',
            totalMembers: 10,
            members:[
                {
                    name: 'mike_schneller (admin)', 
                    status: 0
                },
                {
                    name: 'leslie_alexander', 
                    status: 1
                },
                {
                    name: 'rdebbie_baker', 
                    status: 0
                },
                {
                    name: 'jessica.hanson@example.com', 
                    status: 1
                },
                {
                    name: 'debbie.baker@example.com', 
                    status: 1
                }
            ]
        },
        {
            name: 'Demo group name 5', 
            permission: 'Admin',
            members:[
                {
                    name: 'mike_schneller (admin)', 
                    status: 1
                },
                {
                    name: 'leslie_alexander', 
                    status: 1
                },
                {
                    name: 'rdebbie_baker', 
                    status: 0
                },
                {
                    name: 'jessica.hanson@example.com', 
                    status: 0
                },
                {
                    name: 'debbie.baker@example.com', 
                    status: 1
                }
            ]
        }
        
    ];
    const achievments = [
        {
            name: 'Award for Demo title',
            class: 'svg-image-silver-ach',
            received_on: '20 January 2021',
        },
        {
            name: 'Award for Demo title',
            class: 'svg-image-bronze-ach',
            received_on: '20 January 2021',
        },
        {
            name: 'Award for Demo title',
            class: 'svg-image-silver-ach',
            received_on: '20 January 2021',
        }
    ]
    return (
        <>
            <main className="main">
                <div className="small-decor-head">
                    <picture><img className="responsive-img cover-img" src="/assets/app/base/images/sm-head-decor@1x.png" srcSet="/assets/app/base/images/sm-head-decor@1x.png, /assets/app/base/images/sm-head-decor@2x.png 2x" alt="" /></picture>
                </div>
                <div className="container mt-96 z10 mb-mt-126">
                    <Banner />
                    <div className="user-all-course mt30">
                        <CourseHeader />
                        <div className="user-all-course__body">
                            <div className="row px-12 mt-24">
                                {courses.length > 0 ? courses.map((course, index)=><Course key={index} course={course}/>):<p className='text-right'>Courses not available.</p>}
                            </div>
                        </div>
                    </div>    
                    <div className="row">
                        <div className="col-lg-6 mt30">
                            <div className="white-box">
                                <div className="white-box-head">
                                    <div className="row align-items-center justify-content-between">
                                        <div className="col-auto">
                                            <div className="text-w-600">Latest Discussions</div>
                                        </div>
                                        <div className="col-auto"><a className="btn-view" href="#"> <span className="btn__text">View All</span></a></div>
                                    </div>
                                </div>
                                <div className="white-box-body auto-h-530">
                                    <div className="row mt-16">
                                        {discussions.length > 0 ? discussions.map((discussion, index)=><LatestDiscussion discussion={discussion} key={`discussion-key-${index}`} />) : <p className='text-right'>Discussion not available.</p>}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 mt30">
                            <div className="white-box">
                                <div className="white-box-head">
                                    <div className="row align-items-center justify-content-between">
                                        <div className="col-auto">
                                            <div className="text-w-600">Latest Chats</div>
                                        </div>
                                        <div className="col-auto"><a className="btn-view" href="#"> <span className="btn__text">View All</span></a></div>
                                    </div>
                                </div>
                                <div className="white-box-body auto-h-530">
                                    <div className="row mt-16">
                                        {messages.length > 0 ? messages.map((message, index)=><LatestChat message={message} key={`message-key-${index}`}/>):<p className='text-right'>Chat not available.</p>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="mt30 forum-module">
                        <div className="forum-module-head">
                            <div className="row align-items-center justify-content-between">
                                <div className="col-auto">
                                    <div className="row align-items-center">
                                        <div className="col-auto">
                                            <div className="text-w-600">Forum</div>
                                        </div>
                                        <div className="col-auto md-hide">
                                            <div className="filter-search-field">
                                                <input className="wpcf7-form-control" type="search" placeholder="Search..." />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-auto"><a className="btn-view" href="#"> <span className="btn__text">View All</span></a></div>
                            </div>
                        </div>
                        <ForumBox forumMessages={forumMessages}/>
                    </div>
                    
                    <MyGroups groups={groups}/>
                    <MyAchievents achievments={achievments}/>
                </div>
                <div className="mt70"></div>
            </main>
        </>
    );
};

export default Index;
