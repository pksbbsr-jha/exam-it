import React from 'react';


const Banner = () => {

    return (
        <>
            <div className="sm-hide">
                <div className="text-28 text-white">Good morning, <span className="text-w-600">Mike Schneller!</span></div>
            </div>
        </>
    );
};

export default Banner;
