import React, { useState } from 'react';
import ThanksModal from './ThanksModal';
import UnlockModal from './UnlockModal';
import PaymentModal from './PaymentModal';

const CourseDetail = () => {
    //const [thanksModal, setThanksModal] = useState(true);

    const courseDetail = {
        id: 1,
        name: 'AP Physics A: Mechanics',
        description_short: 'This comprehensive, self-paced course includes all topics required by the AP College Board. <br>Try chapter 1: 1D Kinematics (6 modules) for Free',
        description: `<p>Examit’s AP Physics C: Mechanics course is designed and created by AP Physics teachers who have many years of experience teaching the content and covers all learning targets required by the College Board’s AP Physics C: Mechanics syllabus. Topics covered in this course include one and two dimensional kinematics, circular motion and gravity, linear and rotational dynamics, work and energy, linear and rotational momentum, and oscillations. It is recommended that students plan for a minimum of 50 hours throughout the school term to complete this course. This course is a great supplement for students enrolled in AP Physics C: Mechanics in school who want extra help, but is also designed for students who are not enrolled in an AP Physics C: Mechanics course. Students will need a strong ability in algebra and trigonometry, and be enrolled in, or have taken, a calculus class.</p>
        <p>This self-paced course is delivered in eight chapters and 34 modules, including practice quizzes at the end of modules. The content reflects the approaches and methods best suited to succeed on the AP Physics C: Mechanics exam based on the deep experience of the teachers involved in designing this course. Over 300 practice multiple choice questions mimic the language of AP exam questions and utilize problem-solving methods and perspectives best suited for success on the exam. Each practice question is linked with answer explanations and reference the relevant sub-module which explains the concepts. A random question generator will employ all practice questions from completed modules into a bank when students want extra practice. </p>
        <p>Examit employs a novel learning strategy whereby students can connect with other exam takers by directly messaging them, creating study groups, or viewing and responding to comments and questions with in-text references. Students can earn achievement levels on their badge which can be visible to other users. Examit has now opened the source of learning to the world-wide community! Try chapter one for free!</p>`,
        meta:{
            requirements:['Strong knowledge in algebra and basic trigonometric functions','Have taken, or currently enrolled in, Calculus','A minimum of 50 hours to complete all 34 modules.'],
            contents:['One and two dimensional kinematics','Work and energy', 'Circular motion and gravity','Linear and rotational momentum','Linear and rotational dynamics','Oscillations']
        },
        
        img: {
            src: 'course-demo@1x.jpg',
            sourceSet: ['course-demo@1x.webp', 'course-demo@2x.webp 2x'],
            srcSet: ['course-demo@1x.jpg', 'course-demo@2x.jpg 2x']
        },
        price: 135.54,
        contents: [
            {
                name: 'Introduction to the Cource',
                lessions: [
                    {
                        name: 'Vectors and Scalars',
                        duration: 40,
                    },
                    {
                        name: 'Quiz 1',
                        duration: 50,
                    },
                    {
                        name: 'L2 - Demo name',
                        duration: 20,
                    },
                    {
                        name: 'Quiz 2',
                        duration: 40,
                    },
                ],
                quizes:2
            },
            {
                name: 'Section 3',
                lessions: [
                    {
                        name: 'Vectors and Scalars',
                        duration: 60,
                    },
                    {
                        name: 'Quiz 1',
                        duration: 20,
                    },
                    {
                        name: 'L2 - Demo name',
                        duration: 30,
                    },
                    {
                        name: 'Quiz 2',
                        duration: 50,
                    },
                ],
                quizes:2
            },
            {
                name: 'Section 2',
                lessions: [
                    {
                        name: 'Vectors and Scalars',
                        duration: 40,
                    },
                    {
                        name: 'Quiz 1',
                        duration: 40,
                    },
                    {
                        name: 'L2 - Demo name',
                        duration: 40,
                    },
                    {
                        name: 'Quiz 2',
                        duration: 40,
                    },
                ],
                quizes:3
            },
            {
                name: 'Section 4',
                lessions: [
                    {
                        name: 'Vectors and Scalars',
                        duration: 40,
                    },
                    {
                        name: 'Quiz 1',
                        duration: 40,
                    },
                    {
                        name: 'L2 - Demo name',
                        duration: 40,
                    },
                    {
                        name: 'Quiz 2',
                        duration: 40,
                    },
                ],
                quizes:4
            },
        ]
    };
    return (
        <>
            <main className="main">
                <div className="course-header course-header-cont">
                    <div className="course-header__image">
                        <picture><img className="responsive-img cover-img" src="/assets/app/base/images/course-bg@1x.png" srcSet="/assets/app/base/images/course-bg@1x.png, /assets/app/base/images/course-bg@2x.png 2x" alt="" /></picture>
                    </div>
                    <div className="course-header__info text-white">
                        <div className="container"> 
                            <div className="row"> 
                                <div className="col-xl-8 col-lg-7">
                                <h1>{courseDetail.name}</h1>
                                <p dangerouslySetInnerHTML={{__html:courseDetail.description_short}}></p>
                                <div className="recommended-course-desc mt16">Recommended 50 hours. Eight chapters, 34 modules</div>
                                </div>
                                <div className="col-xl-4 col-lg-5"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container pt30 pb120">
                    <div className="row mt-24">
                        <div className="col-xl-8 col-lg-7 mt24 order-lg-1 order-2">
                            <div className="row mt-24">
                                <div className="col-12 mt24">
                                    <div className="exa-box">
                                        <h3 className="text-18 text-nova">What you will learn</h3>
                                        <div className="mt16">
                                            <div className="row mt-16">
                                                {courseDetail.meta.contents.map((content, index)=><div className="col-md-6 mt16" key={index}>
                                                    <div className="row px-4">
                                                        <div className="col-auto">
                                                            <div className="svg-image-check"></div>
                                                        </div>
                                                        <div className="col">
                                                            <div className="text-16">{content}</div>
                                                        </div>
                                                    </div>
                                                </div>)}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 mt24">
                                    <div className="exa-box">
                                        <h3 className="text-18 text-nova">Requirements</h3>
                                        <div className="mt16">
                                            <div className="exa-box-text">
                                                <ol>
                                                    {courseDetail.meta.requirements.map((requirment, index)=><li key={index}>{requirment}</li>)}
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 mt24">
                                    <div className="exa-box">
                                        <h3 className="text-18 text-nova">Description</h3>
                                        <div className="mt16">
                                            <div className="exa-box-text" dangerouslySetInnerHTML={{__html:courseDetail.description}}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 mt24">
                                    <div className="exa-box">
                                        <h3 className="text-18 text-nova">Course content</h3>
                                        <div className="mt16">
                                            <ul className="list course-list-accordion">
                                                {courseDetail.contents.map((content, index)=><li className="course-list-accordion-item" key={index}>
                                                    <div className="course-list-title">
                                                        <div className="row jujstify-content-between align-items-center">
                                                            <div className="col-md">
                                                                <div className="row align-items-center px-4">
                                                                    <div className="col-auto">
                                                                        <div className="svg-image-chevron_up"></div>
                                                                    </div>
                                                                    <div className="col">
                                                                        <div className="text-16 text-w-600">{content.name}</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-md-auto ls-mb-info-styles">
                                                                <div className="text-18">2 lessons • 2 quizes • 1hr 10min</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="course-list-content text-18 text-link" style={{display:index === 0 ? 'block' : 'none'}}>
                                                        <div className="row mt-16">
                                                            {content.lessions.map((lession, index)=><div className="col-12 mt16" key={index}>
                                                                <div className="row justify-content-between">
                                                                    <div className="col-auto"><a href="#">{lession.name}</a></div>
                                                                    <div className="col-auto">
                                                                        <div className="row align-items-center px-8">
                                                                            <div className="col-auto">
                                                                                <div className="svg-image-lock_outlined"></div>
                                                                            </div>
                                                                            <div className="col-auto">
                                                                                <div className="text-AF wf55 tar">{lession.duration} min</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>)}
                                                        </div>
                                                    </div>
                                                </li>)}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-5 mt24 order-lg-2 order-1">
                            <div className="start-learn-course mt-270 z10">
                                <div className="learn-course-present-top text-white">
                                    <div className="learn-course-present-desc_image">
                                        <picture>
                                            <source srcSet={`/assets/app/base/images/${courseDetail.img.sourceSet[0]}, /assets/app/base/images/${courseDetail.img.sourceSet[1]} 2x`} type="image/webp" /><img className="responsive-img cover-img" src={`/assets/app/base/images/${courseDetail.img.src}`}  srcSet={`/assets/app/base/images/${courseDetail.img.srcSet[0]}, /assets/app/base/images/${courseDetail.img.srcSet[1]}`} alt="" />
                                        </picture>
                                    </div>
                                    <div className="learn-course-present-desc_text text-18 text-w-600">Join the AP Physics C: Mechanics online community and view chapter one for free!</div>
                                </div>
                                <div className="start-learn-bottom">
                                    <div className="text-28 text-w-600">${courseDetail.price}</div>
                                    <button className="mt24 btn btn--md btn--wide btn--blue d-block" data-fancybox data-src="#unlock-course" data-modal="true"><span className="btn__text">Start Learning Now</span></button>
                                    <UnlockModal />
                                    <PaymentModal />
                                    <ThanksModal/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mt70"></div>
            </main>
        </>
    );
};

export default CourseDetail;
