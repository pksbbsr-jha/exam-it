import React from 'react';
import Category from './Category';

const Courses = () => {
    const categories = [
        {
            name: 'Physics',
        },
        {
            name: 'Demo category',
        },
        {
            name: 'Demo category',
        },
        {
            name: 'Demo category',
        },
        {
            name: 'Demo category',
        },
        {
            name: 'Demo category',
        }
    ];
    const courses = [
        {
            name: 'AP Physics C: Mechanics',
            img: {
                src: 'unsplash_XoNj0ulsn1Y@1x.jpg',
                sourceSet: ['unsplash_XoNj0ulsn1Y@1x.webp', 'unsplash_XoNj0ulsn1Y@2x.webp 2x'],
                srcSet: ['unsplash_XoNj0ulsn1Y@1x.jpg', 'unsplash_XoNj0ulsn1Y@1x.jpg 2x']
            },
            description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum.',
            price: 198.43
        },
        {
            name: 'AP Physics B: Mechanics',
            img: {
                src: 'cd2@1x.jpg',
                sourceSet: ['unsplash_OLRXnzXFBjo@1x.webp', 'unsplash_OLRXnzXFBjo@2x.webp 2x'],
                srcSet: ['unsplash_OLRXnzXFBjo@1x.jpg', 'unsplash_OLRXnzXFBjo@1x.jpg 2x']
            },
            description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum.',
            price: 175.09
        },
        {
            name: 'AP Physics C: Mechanics',
            img: {
                src: 'unsplash__W1Sg5qv1R0.jpg',
                sourceSet: ['unsplash__W1Sg5qv1R0@1x.webp', 'unsplash__W1Sg5qv1R0@2x.webp 2x'], srcSet: ['unsplash__W1Sg5qv1R0@1x.jpg', 'unsplash__W1Sg5qv1R0@2x.jpg 2x']
            },
            description: 'Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum.',
            price: 150.96
        }
    ];
    
    return (
        <>
            <form action="#">
                <div className="user-all-course__head">
                    <div className="row align-items-center justify-content-between px-12">
                        <div className="col-md-auto">
                            <div className="row px-12 align-items-center">
                                <div className="col-md-auto col">
                                    <div className="text-18 text-w-600">All Courses</div>
                                </div>
                                <div className="col-md-auto col">
                                    <div className="filter-search-field">
                                        <div className="wpcf7-form-control-wrap">
                                            <input className="wpcf7-form-control" type="search" placeholder="Search..." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-auto sm-hide">
                            <div className="select-style-v1">
                                <div className="selectric-wrapper selectric-below">
                                    <div className="selectric-hide-select">
                                        <select>
                                            <option value="val">Sort by: Most popular</option>
                                            <option value="val">Sort by: ____ </option>
                                            <option value="val">Sort by: ____ </option>
                                            <option value="val">Sort by: ____ </option>
                                            <option value="val">Sort by: ____ </option>
                                        </select>
                                    </div>
                                    <div className="selectric">
                                        <span className="label">Sort by: Most popular </span>
                                        <b className="button">▾</b>
                                    </div>
                                    <div className="selectric-items">
                                        <div className="selectric-scroll">
                                            <ul>
                                                <li data-index="0" className="selected highlighted">Sort by: Most popular </li>
                                                <li data-index="1" className="">Sort by: ____ </li>
                                                <li data-index="2" className="">Sort by: ____ </li>
                                                <li data-index="3" className="">Sort by: ____ </li>
                                                <li data-index="4" className="last">Sort by: ____ </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <input className="selectric-input" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row no-gutters">
                    <div className="col-lg-3">
                        <div className="filter-aside-left">
                            <ul className="filter-dropdown-list list">
                                <li>
                                    <div className="filter-dropdown__title is-active">
                                        <div className="text-16 text-w-600">Category</div>
                                        <div className="svg-image-chevron_up"></div>
                                    </div>
                                    <div className="filter-dropdown__body" style={{display:'block'}}>
                                        <div className="checkbox-styled">
                                            <span className="wpcf7-form-control-wrap">
                                                <span className="wpcf7-form-control wpcf7-checkbox">
                                                    {categories.length > 0 ? categories.map((category, index) => <Category key={index} category={category} />) : <p className='text-right'>Category not available.</p>}
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-9">
                        <div className="filter-result-area">
                            <div className="row mt-16 mb-mt-24">
                            {courses.length > 0 ? courses.map((course, index)=>
                                <div className="col-12 mt16 mb-mt24">
                                    <div className="filter-result-item">
                                        <div className="row mt-16 no-gutters">
                                            <div className="col-md-auto mt16">
                                                <div className="course-find-image">
                                                    <picture>
                                                        <source srcSet={`/assets/app/base/images/${course.img.sourceSet[0]}, /assets/app/base/images/${course.img.sourceSet[1]} 2x`} type="image/webp" /><img className="responsive-img cover-img" src={`/assets/app/base/images/${course.img.src}`}  srcSet={`/assets/app/base/images/${course.img.srcSet[0]}, /assets/app/base/images/${course.img.srcSet[1]}`} alt="" />
                                                    </picture>
                                                </div>
                                            </div>
                                            <div className="col-md mt16">
                                                <div className="course-find-cont">
                                                    <div className="row justify-content-between align-items-center">
                                                        <div className="col">
                                                            <div className="text-16 text-w-600">{course.name}</div>
                                                        </div>
                                                        <div className="col-auto sm-hide">
                                                            <div className="text-16 text-w-600">${course.price}</div>
                                                        </div>
                                                    </div>
                                                    <div className="row mt12">
                                                        <div className="col-md-11">
                                                            <div className="text-16 text-6E">{course.description}</div>
                                                        </div>
                                                    </div>
                                                    <div className="mt16 mb-mt-12 text-14 text-6E">13 total hours • 12 lessons • Expert</div>
                                                    <div className="mt16 text-16 sm-show text-w-600">$13.99</div>
                                                    <div className="mt32 mb-mt-16"><a className="btn btn--sm btn--white mb-wide" href="#"> <span className="btn__text">View Details</span></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>) : <p>Course not available!</p>}
                            </div>
                            <div className="filter-result-area__pagination mt40">
                                <div className="row justify-content-between align-items-center">
                                    <div className="col-md-3 col-auto"><a className="previos-page fll" href="#">
                                        <div className="row align-items-center px-7">
                                            <div className="col-auto">
                                                <div className="svg-image-page-prev"></div>
                                            </div>
                                            <div className="col-auto sm-hide">
                                                <div className="text-14 text-w-600 text-9A">Previous page</div>
                                            </div>
                                        </div></a>
                                    </div>
                                    <div className="col">
                                        <div className="row justify-content-center px-1 filter-pagination">
                                            <div className="col-auto"><span className="current-page">1</span></div>
                                            <div className="col-auto"><a href="#">2</a></div>
                                            <div className="col-auto"><a href="#">3</a></div>
                                            <div className="col-auto"><a href="#">4</a></div>
                                            <div className="col-auto"><a href="#">5</a></div>
                                            <div className="col-auto"><a href="#">6</a></div>
                                            <div className="col-auto sm-hide"><a href="#">…</a></div>
                                            <div className="col-auto sm-hide"><a href="#">24</a></div>
                                            <div className="col-auto sm-hide"><a href="#">25</a></div>
                                        </div>
                                    </div>
                                    <div className="col-md-3 col-auto"><a className="next-page flr" href="#">
                                        <div className="row align-items-center px-7">
                                            <div className="col-auto sm-hide">
                                                <div className="text-14 text-w-600 text-9A">Previous page</div>
                                            </div>
                                            <div className="col-auto">
                                                <div className="svg-image-page-next"></div>
                                            </div>
                                        </div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </>
    );
};

export default Courses;
