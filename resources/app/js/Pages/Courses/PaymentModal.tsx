import React from 'react';

const PaymentModal = () => {
    
    return (
        <>
            <div className="popup-inline-exa" id="course-buy-popup">
                <div className="svg-image-close" data-fancybox-close></div>
                <div className="tac">
                    <picture>
                        <source srcSet={`/assets/app/base/images/pay@1x.webp, /assets/app/base/images/pay@2x.webp 2x`} type="image/webp" />
                        <img className="responsive-img cover-img" src={`/assets/app/base/images/pay@1x.jpg`}  srcSet={`/assets/app/base/images/pay@1x.jpg, /assets/app/base/images/pay@2x.jpg`} alt="" />
                    </picture>
                </div>
                <button className="mt40 btn btn--md btn--wide btn--blue" data-fancybox data-src="#course-thanks-popup" data-modal="true">For developers 'Thanks' </button>
            </div>
        </>
    );
};

export default PaymentModal;
