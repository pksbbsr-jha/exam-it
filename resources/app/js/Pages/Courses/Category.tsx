import React from 'react';

type CategoryProps = {
    category: {
        name: string
    }
}
const Category = (props:CategoryProps) => {
    
    return (
        <>
            <div className="wpcf7-list-item">
                <label>
                    <input type="checkbox" /><span className="wpcf7-list-item-label">{props.category.name}</span>
                </label>
            </div>
        </>
    );
};

export default Category;
