import React from 'react';
import Course from '../../Components/Common/Course';
import MyAchievents from '../../Components/Common/MyAchievents';

const CourseList = () =>{

    const courses = [
        {
            id: 1,
            name: 'AP Physics A: Mechanics',
            img: {
                src: 'sm-head-decor@1x.png',
                sourceSet: ['course-demo@1x.webp', 'course-demo@2x.webp 2x'],
                srcSet: ['course-demo@1x.jpg', 'course-demo@2x.jpg 2x']
            }
        },
        {
            id: 2,
            name: 'AP Physics B: Mechanics',
            img: {
                src: 'cd2@1x.jpg',
                sourceSet: ['cd2@1x.webp', 'cd2@2x.webp 2x'],
                srcSet: ['course-demo@1x.jpg', 'course-demo@2x.jpg 2x']
            }
        },
        {
            id: 3,
            name: 'AP Physics C: Mechanics',
            img: {
                src: 'cd3@1x.jpg',
                sourceSet: ['cd3@1x.webp', 'cd3@2x.webp 2x'], srcSet: ['cd3@1x.jpg', 'cd3@2x.jpg 2x']
            }
        }
    ];
    const achievments = [
        {
            name: 'Award for Demo title',
            class: 'svg-image-silver-ach',
            received_on: '20 January 2021',
        },
        {
            name: 'Award for Demo title',
            class: 'svg-image-bronze-ach',
            received_on: '20 January 2021',
        },
        {
            name: 'Award for Demo title',
            class: 'svg-image-silver-ach',
            received_on: '20 January 2021',
        }
    ]
    return (
        <>
            <main className="main">
                <div className="small-decor-head">
                    <picture><img className="responsive-img cover-img" src="/assets/app/base/images/sm-head-decor@1x.png" srcSet="/assets/app/base/images/sm-head-decor@1x.png, /assets/app/base/images/sm-head-decor@2x.png 2x" alt="" /></picture>
                </div>
                <div className="container mt-96 z10">
                    <div className='user-all-course'>
                        <div className="user-all-course__head">
                            <div className="row align-items-center px-8 justify-content-between mt-12">
                                <div className="col-md-auto mt12">
                                    <div className="row align-items-center px-8 mt-12 justify-content-between">
                                        <div className="col-auto mt12">
                                            <div className="text-18 text-w-600">My Courses</div>
                                        </div>
                                        <div className="col-auto mt12 sm-show"><a className="btn-view" href="#"> <span className="btn__text">View More Courses</span></a></div>
                                        <div className="col-md-auto mt12">
                                            <ul className="grey-menu text-14 text-w-600">
                                                <li className="current-menu-item"><a href="#">All</a></li>
                                                <li><a href="#">Current</a></li>
                                                <li><a href="#">Completed</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="user-all-course__body">
                            <div className="row px-12 mt-24">
                                {courses.length > 0 ? courses.map((course, index) => <Course key={index} course={course} />) : <p className='text-right'>Courses not available.</p>}
                            </div>
                        </div>
                    </div>
                    <MyAchievents achievments={achievments}/>
                </div>
                <div className="mt70"></div>
            </main>
        </>
    );
};
export default CourseList;