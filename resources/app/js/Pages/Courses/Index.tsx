import React from 'react';
import Courses from './Courses';
import CourseHeader from '../../Components/Common/CourseHeader';
import Course from '../../Components/Common/Course';

const Index = () => {
    const courses = [
        {
            id: 1,
            name: 'AP Physics A: Mechanics',
            img: {
                src: 'sm-head-decor@1x.png',
                sourceSet: ['course-demo@1x.webp', 'course-demo@2x.webp 2x'],
                srcSet: ['course-demo@1x.jpg', 'course-demo@2x.jpg 2x']
            }
        },
        {
            id: 2,
            name: 'AP Physics B: Mechanics',
            img: {
                src: 'cd2@1x.jpg',
                sourceSet: ['cd2@1x.webp', 'cd2@2x.webp 2x'],
                srcSet: ['course-demo@1x.jpg', 'course-demo@2x.jpg 2x']
            }
        },
        {
            id: 3,
            name: 'AP Physics C: Mechanics',
            img: {
                src: 'cd3@1x.jpg',
                sourceSet: ['cd3@1x.webp', 'cd3@2x.webp 2x'], srcSet: ['cd3@1x.jpg', 'cd3@2x.jpg 2x']
            }
        }
    ];
    return (
        <>
            <main className="main">
                <div className="small-decor-head">
                    <picture><img className="responsive-img cover-img" src="/assets/app/base/images/sm-head-decor@1x.png" srcSet="/assets/app/base/images/sm-head-decor@1x.png, /assets/app/base/images/sm-head-decor@2x.png 2x" alt="" /></picture>
                </div>
                <div className="container mt-96 z10">
                    <div className='user-all-course'>
                        <CourseHeader />
                        <div className="user-all-course__body">
                            <div className="row px-12 mt-24">
                                {courses.length > 0 ? courses.map((course, index) => <Course key={index} course={course} />) : <p className='text-right'>Courses not available.</p>}
                            </div>
                        </div>
                    </div>
                    <div className='user-all-course mt30'>
                        <Courses/>
                    </div>
                </div>
                <div className="mt70"></div>
            </main>
        </>
    );
};

export default Index;
