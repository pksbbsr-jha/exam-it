import React from 'react';

const UnlockModal = () => {
    
    return (
        <>
            <div className="popup-inline-exa" id="unlock-course">
                <div className="svg-image-close" data-fancybox-close></div>
                <div className="w475">
                    <div className="tac"><img src={`/assets/app/base/images/unlock-image.svg`} alt=""/></div>
                    <div className="mt32 tac">
                        <h3 className="text-24 text-nova">Would you like to unlock this course?</h3>
                        <div className="tac text-16 mt12 text-6E">
                            <p>Gain access to the entire AP Physics C: Mechanics course, complete with open access to all social connections and linked references.</p>
                        </div><a className="mt40 btn btn--md btn--wide btn--blue" href="javascript:;" data-fancybox data-src="#course-buy-popup" data-modal="true"> <span className="btn__text">Buy $13.99</span></a>
                    </div>
                </div>
            </div>
        </>
    );
};

export default UnlockModal;
