import React from 'react';

const ThanksModal = () => {
    
    return (
        <>
            <div className="popup-inline-exa" id="course-thanks-popup">
                <div className="svg-image-close" data-fancybox-close></div>
                <div className="w475">
                    <div className="tac"><img src={`/assets/app/base/images/thanks-ill.svg`} alt=""/></div>
                    <div className="mt32 tac">
                        <h3 className="text-24 text-nova">Thank you!</h3>
                        <div className="tac text-16 mt12 text-6E">
                            <p>Payment successful. You now have access to all course content</p>
                        </div>
                        <a className="mt40 btn btn--md btn--wide btn--blue" href="javascript:;" data-fancybox-close> <span className="btn__text">Go to Course</span></a>
                    </div>
                </div>
            </div>
        </>
    );
};

export default ThanksModal;
