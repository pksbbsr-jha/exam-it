import {Link} from '@inertiajs/inertia-react';
import React from 'react';
import {InertiaProps, ModelCourse, Paginated} from '@admin/types';
import type {ProColumns} from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import ProCard from '@ant-design/pro-card';
import {Inertia} from '@inertiajs/inertia';
import EmptyTable from '@admin/Components/Tables/EmptyTable';

type Props = InertiaProps & {
    courses: Paginated<ModelCourse>;
}

const columns: ProColumns<ModelCourse>[] = [
    {
        title: 'id',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Actions',
        key: 'actions',
        valueType: 'option',
        align: 'right',
        render: (text, row) => [
            <Link key={'show'} href={window.route('admin.courses.show', row.id)}>Manage </Link>,
            <Link key={'edit'} href={window.route('admin.courses.edit', row.id)}>Edit </Link>
        ],
    },
];

const CustomTable: React.FunctionComponent<Props> = (props) => {

    return (
        <>
            <ProCard>
                <ProTable<ModelCourse>
                    columns={columns}
                    dataSource={props.courses.data}
                    locale={{
                        emptyText: <EmptyTable/>
                    }}
                    toolbar={{
                        filter: false,
                    }}
                    type={'table'}
                    pagination={{
                        pageSize: props.courses.meta.per_page,
                        total: props.courses.meta.total,
                        current: props.courses.meta.current_page,
                        onChange: (page, pageSize) => {
                            Inertia.reload({
                                data: {
                                    page,
                                    pageSize
                                }
                            });
                        },
                    }}
                    rowKey={record => record.id}
                    dateFormatter="string"
                    options={false}
                    toolBarRender={false}
                    search={false}
                />
            </ProCard>
        </>
    );
};

export default CustomTable;
