import {Empty} from 'antd';
import React from 'react';

export default () => {
    return <Empty description={'No results found'} style={{padding: '50px 10px'}}/>
}
