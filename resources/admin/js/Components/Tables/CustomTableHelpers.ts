import {Inertia} from '@inertiajs/inertia';
import {Paginated} from '@admin/types';
import {ProTableProps} from '@ant-design/pro-table/lib/typing';
import EmptyTable from '@admin/Components/Tables/EmptyTable';
import moment from 'moment';
import {AntSearchHelper} from '@admin/Helpers/AntHelpers';


interface EloquentPaginationHandlerConfig {
    prefix?: string;
}

export const EloquentPaginationHandler = (config: EloquentPaginationHandlerConfig = {
    prefix: ''
}) => {

    return (page: number, _pageSize: number) => {
        Inertia.reload({
            data: {
                [config.prefix + 'page']: page,
                // [config.prefix + 'pageSize']: pageSize
            }
        });
    }
}

export const EloquentTableHandler: <T>(data: Paginated<T>, config: EloquentPaginationHandlerConfig) => ProTableProps<T, any> = (data, config) => {

    return {
        dataSource: data.data,
        locale: {
            emptyText: EmptyTable,
        },
        toolbar: {
            search: false,
        },
        type: 'table',
        pagination: {
            pageSize: data.meta.per_page,
            total: data.meta.total,
            current: data.meta.current_page,
            onChange: (page: number, _pageSize: number) => {
                Inertia.reload({
                    data: {
                        [config.prefix + 'page']: page,
                        // [config.prefix + 'pageSize']: pageSize
                    }
                });
            }
        },
        onChange: (pagination, filters, sorter, extra) => {
            console.log('pagination', pagination, filters, sorter, extra);
        },
        beforeSearchSubmit: (params => {
            // console.log('beforeSearchSubmit', params, _.pick(params, ['name', 'username', 'email']));
            Inertia.reload({
                data: AntSearchHelper(['name', 'username', 'email'], params),
            });
        }),
        dateFormatter: (value) => moment(value).format('MM/DD/YYYY'),
    }
}
