import React from 'react';
import {Button, Empty} from 'antd';
import _ from 'lodash';
import {PlusOutlined} from '@ant-design/icons';
import {ModelCourseSubItemType} from '@admin/types';

type Props = {
    type: string;
    onAdd: (type: string) => void,
}

const EmptyTableWithAdd: React.FunctionComponent<Props> = (props) => {
    const capitalType = _.startCase(props.type);

    return (
        <Empty
            description={
                <>
                    <p>No {props.type}s yet</p>
                    <Button type={'primary'} icon={<PlusOutlined/>} onClick={() => props.onAdd(props.type)}>Add a
                        new {capitalType}</Button>
                </>
            }
        />
    )
        ;
};

export default EmptyTableWithAdd;
