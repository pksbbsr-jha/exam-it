import {Modal} from 'antd';

export const onFinishFailed = (errorInfo: any) => {
    Modal.error({
        title: 'Validation errors',
        content: 'Please fix validation errors before proceeding',
    });
    console.log('Failed:', errorInfo);
}
