import React from 'react';
import {usePage} from '@inertiajs/inertia-react';
import {Alert} from 'antd';

type Props = {
    style?: React.CSSProperties;
}

const ValidationErrorsAlert: React.FunctionComponent<Props> = (props) => {
    const page = usePage();

    if (!Object.keys(page.props.errors).length) {
        return <></>
    }

    return (
        <Alert
            message="Validation Errors"
            description={(
                <ul>
                    {Object.keys(page.props.errors).map((key, index) => (
                        <li key={index}>{page.props.errors[key]}</li>
                    ))}
                </ul>
            )}
            type="error"
            showIcon
            style={props.style}
        />
    );
};

export default ValidationErrorsAlert;
