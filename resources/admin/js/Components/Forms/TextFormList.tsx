import {Button, Form, Input, Space} from 'antd';
import React from 'react';
import {MinusCircleOutlined, PlusOutlined} from '@ant-design/icons';

interface Props {
    itemPlaceholder: string;
    name: string;
    value?: string[];
    onChange?: (e: Array<string|undefined>) => void;
}

const TextFormList: React.FunctionComponent<Props> = (props) => {
    return (
        <Form.List name={props.name}>
            {(fields, {add, remove}, { errors }) => {
                return (
                    <>
                        {fields.map(({key, name, ...restField}) => (
                            <Space key={key} style={{display: 'flex', marginBottom: 0}} align="start">
                                <Form.Item
                                    {...restField}
                                    name={name}
                                    rules={[{required: true, whitespace: true}]}
                                    style={{width: '280px', marginBottom: 10}}
                                >
                                    <Input placeholder={props.itemPlaceholder}/>
                                </Form.Item>
                                <MinusCircleOutlined onClick={() => remove(name)}/>
                            </Space>
                        ))}
                        <Form.ErrorList errors={errors} />
                        <Form.Item style={{width: '280px', marginBottom: 0}}>
                            <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined/>}>
                                Add item
                            </Button>
                        </Form.Item>
                    </>
                )
            }}
        </Form.List>
    )
}


export default TextFormList;
