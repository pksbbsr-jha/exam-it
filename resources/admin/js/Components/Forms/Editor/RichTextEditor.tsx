import React, {createRef} from 'react';


import {Editor} from '@tinymce/tinymce-react';

// TinyMCE so the global var exists
// eslint-disable-next-line no-unused-vars
// Theme
import 'tinymce/themes/silver';
// Toolbar icons
import 'tinymce/icons/default';
// Editor styles
import 'tinymce/skins/ui/oxide/skin.min.css';
// import 'tinymce/skins/content/default/content.min.css';
// importing the plugin js.
// import 'tinymce/plugins/advlist';
// import 'tinymce/plugins/autolink';
import 'tinymce/plugins/code';
// import 'tinymce/plugins/link';
import 'tinymce/plugins/image';
// import 'tinymce/plugins/imagetools';
import 'tinymce/plugins/lists';
// import 'tinymce/plugins/charmap';
// import 'tinymce/plugins/hr';
// import 'tinymce/plugins/anchor';
// import 'tinymce/plugins/searchreplace';
// import 'tinymce/plugins/wordcount';
// import 'tinymce/plugins/fullscreen';
// import 'tinymce/plugins/insertdatetime';
// import 'tinymce/plugins/media';
// import 'tinymce/plugins/nonbreaking';
import 'tinymce/plugins/table';
// import 'tinymce/plugins/template';
import 'tinymce/plugins/quickbars';
// import 'tinymce/plugins/help';
// Content styles, including inline UI like fake cursors
/* eslint import/no-webpack-loader-syntax: off */
// @ts-ignore
import contentCss from '!!raw-loader!tinymce/skins/content/default/content.min.css';  // It breaks mathtype css
// @ts-ignore
import contentUiCss from '!!raw-loader!tinymce/skins/ui/oxide/content.min.css';

import {IProps} from '@tinymce/tinymce-react/lib/es2015/main/ts/components/Editor';


export type OnChangeHandler = IProps['onEditorChange'];

interface Props {
    value?: string;
    initialValue: string;
    onChange: OnChangeHandler;
    height: number | string;
    isAdmin?: boolean;
}

const max_size_value = 1024;

const RichTextEditor: React.FunctionComponent<Props> = (props) => {
    const initialValue = props.value ?? props.initialValue;

    const editorRef = createRef<Editor>();

    const isAdmin = typeof props.isAdmin === 'undefined' ? false : true;

    /**
     * Do not pass value
     */
    return (
        <>
            <Editor
                apiKey={'ykfiu8d42yq3dpm22a1coe0bv026gbqhekqdgdcilo5xw109'}
                ref={editorRef}
                tinymceScriptSrc="/assets/admin/tinymce/tinymce.min.js"
                onEditorChange={props.onChange}
                initialValue={initialValue}
                key={Date.now()}

                init={{
                    // init_instance_callback: function (editor) {
                    //     editor.on('BeforeExecCommand', function (e) {
                    //         if (e.command == "mcePreview") {
                    //             //store content prior to changing.
                    //             // preProssesInnerHtml = editor.getContent();
                    //             // editor.setContent("changed content");
                    //         }
                    //         console.log('e.command', e.command);
                    //     });
                    //     editor.on("ExecCommand", function (e) {
                    //         if (e.command == "mcePreview") {
                    //             //Restore initial content.
                    //             // editor.setContent(preProssesInnerHtml);
                    //         }
                    //         console.log('e.command', e.command);
                    //     });
                    // },
                    base_url: '/assets/admin/tinymce',
                    // content_css: false,
                    content_style: [contentCss, contentUiCss].join('\n') + ' body {font-family: \'Helvetica\', \'Arial\', sans-serif; };',
                    height: props.height,
                    external_plugins: {
                        /**
                         * https://docs.wiris.com/en/mathtype/integrations/html/tinymce
                         *
                         * This is causing error on second load.
                         * var _this = _super.call(this, props) || this;
                         *
                         */
                        'tiny_mce_wiris': `/assets/admin/tinymce/plugins_custom/wiris/plugin.min.js`,
                        /**
                         * Tinydrive does not work this way
                         * 'tinydrive': 'https://cdn.tiny.cloud/1/ykfiu8d42yq3dpm22a1coe0bv026gbqhekqdgdcilo5xw109/tinydrive/5-stable/tinydrive.min.js'
                         */
                    },
                    plugins: ['code', 'image', 'lists', 'paste', 'quickbars', 'table', 'preview'],
                    // plugins: [
                    //     'advlist autolink lists link image charmap print preview anchor',
                    //     'searchreplace visualblocks code fullscreen',
                    //     'insertdatetime media table paste imagetools wordcount'
                    // ],
                    toolbar: [
                        {name: 'undo', items: ['undo', 'redo']},
                        {name: 'style', items: ['styleselect']},
                        {name: 'bold', items: ['bold', 'italic']},
                        {name: 'align', items: ['alignleft', 'aligncenter', 'alignright', 'alignjustify']},
                        {name: 'lists', items: ['bullist', 'numlist']},
                        {name: 'indent', items: ['outdent', 'indent']},
                        {name: 'code', items: ['code']},
                        {name: 'table', items: ['table']},
                        {name: 'image', items: ['image']},
                        {name: 'preview', items: ['preview']},
                        {
                            name: 'functions',
                            items: ['tiny_mce_wiris_formulaEditor', 'tiny_mce_wiris_formulaEditorChemistry']
                        },
                    ],

                    table_class_list: [
                        {title: 'None', value: ''},
                        {title: 'Equation box', value: 'tinymce_equation_box'},
                    ],

                    // Disable automatic 100% width
                    table_default_styles: {
                        'border-collapse': 'collapse',
                        'min-width': '20px'
                    },

                    quickbars_insert_toolbar: 'bold italic | outdent indent | bullist numlist | table | quickimage | tiny_mce_wiris_formulaEditor | tiny_mce_wiris_formulaEditorChemistry',

                    paste_block_drop: true,
                    // Images
                    image_title: true,
                    paste_data_images: false,
                    object_resizing: 'img',
                    automatic_uploads: true,
                    images_replace_blob_uris: true,
                    relative_urls: false,
                    // images_upload_url: '/admin/media/upload',

                    // // This creates a separate section "upload". We do not need it, since we can handle using `file_picker_types`
                    images_upload_handler: (blobInfo, success, failure, progress) => {
                        console.log('upload');
                        let formData;


                        const image_size = Math.round(blobInfo.blob().size / 1000);  // image size in kbytes
                        const max_size = max_size_value;                // max size in kbytes
                        if (image_size > max_size) {
                            return failure('Image is too large (' + image_size + ' kB), maximum image size is:' + max_size + ' kB', {
                                remove: true
                            });
                        }

                        let xhr = new XMLHttpRequest();
                        xhr.withCredentials = false;
                        xhr.open('POST', '/admin/media/upload');

                        xhr.upload.onprogress = function (e) {
                            progress!(e.loaded / e.total * 100);
                        };

                        xhr.onload = function () {

                            if (xhr.status === 422) {
                                console.log(xhr.responseText, xhr);
                                let errors: { file: string[] } = JSON.parse(xhr.responseText);

                                return failure(errors.file.join(', '), {remove: true});
                            }

                            if (xhr.status === 403) {
                                return failure('HTTP Error: ' + xhr.status, {remove: true});
                            }

                            if (xhr.status < 200 || xhr.status >= 300) {
                                return failure('HTTP Error: ' + xhr.status);
                            }

                            let json: { location: string } | null = xhr.responseText ? JSON.parse(xhr.responseText) : null;

                            console.log('json', json);

                            // noinspection SuspiciousTypeOfGuard
                            if (!json || typeof json.location !== 'string') {
                                return failure('Invalid JSON: ' + xhr.responseText);
                            }

                            success(json.location);
                        };

                        xhr.onerror = function () {
                            return failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status, {remove: true});
                        };

                        formData = new FormData();
                        formData.append('file', blobInfo.blob(), blobInfo.filename());

                        xhr.send(formData);
                    },
                    //
                    // file_picker_types: 'image',
                    // // https://stackoverflow.com/questions/63138053/tinymce-5-file-picker-callback-to-upload-with-custom-handler
                    // /* and here's our custom image picker*/
                    // file_picker_callback: function (cb, value, meta) {
                    //     console.log('cb', cb, value, meta);
                    //     let input = document.createElement('input');
                    //     input.setAttribute('type', 'file');
                    //     input.setAttribute('accept', 'image/*');
                    //
                    //     /*
                    //       Note: In modern browsers input[type="file"] is functional without
                    //       even adding it to the DOM, but that might not be the case in some older
                    //       or quirky browsers like IE, so you might want to add it to the DOM
                    //       just in case, and visually hide it. And do not forget do remove it
                    //       once you do not need it anymore.
                    //     */
                    //
                    //     input.onchange = function () {
                    //         // @ts-ignore
                    //         const files = this.files as unknown as FileList;
                    //         const file = files[0];
                    //
                    //         console.log('file', file);
                    //
                    //         let reader = new FileReader();
                    //
                    //         reader.onload = function (e) {
                    //             /*
                    //               Note: Now we need to register the blob in TinyMCEs image blob
                    //               registry. In the next release this part hopefully won't be
                    //               necessary, as we are looking to handle it internally.
                    //             */
                    //             const id = 'blobid' + (new Date()).getTime();
                    //             const blobCache = editorRef.current!.editor!.editorUpload.blobCache;
                    //             const base64 = (reader.result! as string).split(',')[1];
                    //             const blobInfo = blobCache.create(id, file, base64);
                    //             blobCache.add(blobInfo);
                    //
                    //             //Initiate the JavaScript Image object.
                    //             let image = new Image();
                    //
                    //             //Set the Base64 string return from FileReader as source.
                    //             // @ts-ignore
                    //             image.src = e.target!.result;
                    //
                    //             //Validate the File Height and Width.
                    //             image.onload = function () {
                    //                 // @ts-ignore
                    //                 let height = this.height;
                    //                 // @ts-ignore
                    //                 let width = this.width;
                    //
                    //                 let maxWidth = 800;
                    //
                    //                 if (width > maxWidth) {
                    //                     height = height / width * maxWidth;
                    //                     width = maxWidth;
                    //                 }
                    //
                    //                 if (file.size < 2000000) {
                    //                     /* call the callback and populate the Title field with the file name */
                    //                     cb(blobInfo.blobUri(), {
                    //                         title: file.name,
                    //                         width: `${width}px`,
                    //                         height: `${height}px`
                    //                     });
                    //                 } else {
                    //                     notification.error({
                    //                         message: 'File is too big!'
                    //                     });
                    //                 }
                    //
                    //             };
                    //
                    //
                    //         };
                    //
                    //         reader.readAsDataURL(file);
                    //     };
                    //
                    //     input.click();
                    // },
                    // images_replace_blob_uris: true,
                    // file_picker_callback: function(cb, value, meta) {
                    //     let input = document.createElement('input');
                    //     input.setAttribute('type', 'file');
                    //     input.setAttribute('accept', 'image/*');
                    //     input.onchange = function() {
                    //         var file = this.files[0];
                    //
                    //         var reader = new FileReader();
                    //         reader.readAsDataURL(file);
                    //         reader.onload = function () {
                    //             var id = 'blobid' + (new Date()).getTime();
                    //             var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    //             var base64 = reader.result.split(',')[1];
                    //             var blobInfo = blobCache.create(id, file, base64);
                    //             blobCache.add(blobInfo);
                    //             cb(blobInfo.blobUri(), { title: file.name });
                    //         };
                    //     };
                    //     input.click();
                    // }
                }}
            />
        </>
    );
};

RichTextEditor.defaultProps = {
    height: 500
}

// It does not re-render on props change
export default React.memo(RichTextEditor, () => true);
