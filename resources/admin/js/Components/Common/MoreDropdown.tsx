import React from 'react';
import {Dropdown, Menu} from 'antd';
import {ItemType} from 'antd/es/menu/hooks/useItems';

import {faEllipsis} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

interface Props {
    items: ItemType[],
}


const MoreDropdown: React.FunctionComponent<Props> = (props) => {
    const {children} = props;

    const label = children ?? <a onClick={e => e.preventDefault()}><FontAwesomeIcon icon={faEllipsis}/></a>;

    return (
        <Dropdown
            overlay={<Menu items={props.items}/>}
            placement="bottomRight"
            trigger={['click']}
        >
            {label}
        </Dropdown>
    )
};

export default MoreDropdown;
