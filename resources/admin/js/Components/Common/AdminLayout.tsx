import React, {ReactNode, useEffect, useState} from 'react';

import {Breadcrumb, BreadcrumbItemProps, Menu, notification} from 'antd';
import {
    BookOutlined,
    DashboardOutlined,
    DollarOutlined,
    MenuFoldOutlined,
    MenuUnfoldOutlined, MessageOutlined, UsergroupAddOutlined,
    UserOutlined,
} from '@ant-design/icons';
import {Head, Link, usePage} from '@inertiajs/inertia-react';

import ProLayout, {PageContainer} from '@ant-design/pro-layout';
import {Route} from '@ant-design/pro-layout/lib/typings';
import {Inertia} from '@inertiajs/inertia';
import {LaravelBreadcrumb, ModelUser, ToastDto} from '@admin/types';
import SubMenu from 'antd/lib/menu/SubMenu';


interface Props {
    title: string,
    subTitle?: string,
    headerRight?: React.ReactNode
}

const route: Route = {
    path: '/admin',
    routes: [
        {
            path: '/admin/dashboard',
            name: 'Dashboard',
            icon: <DashboardOutlined/>,
        },
        {
            path: '/admin/courses',
            name: 'Courses',
            icon: <BookOutlined/>,
        },
        {
            path: '/admin/users',
            name: 'Users',
            icon: <UserOutlined/>,
        },
        {
            path: '/admin/groups',
            name: 'Groups',
            icon: <UsergroupAddOutlined/>,
        },
        {
            path: '/admin/discussions',
            name: 'Discussions',
            icon: <MessageOutlined/>,
        },
        {
            path: '/admin/payments',
            name: 'Payments',
            icon: <DollarOutlined/>,
        },
    ],
};

const AdminLayout: React.FunctionComponent<Props> = (props) => {
    const {children} = props;

    const page = usePage();

    console.log('page.props', page.props);

    const user = page.props.user as ModelUser;
    const firstName = user.name.split(' ').slice(0, -1).join(' ');

    if (!user) {
        throw new Error('Unauthorized!');
    }

    const breadcrumbs = page.props.breadcrumbs as LaravelBreadcrumb[];

    const breadcrumbsRender = (
        breadcrumbs.length > 0 &&
        <Breadcrumb>
            {breadcrumbs.map(breadcrumb => {
                // Passing undefined still renders anchor
                let breadcrumbItemProps: BreadcrumbItemProps = {};

                if (!breadcrumb.is_current_page) {
                    breadcrumbItemProps.href = breadcrumb.url;
                }

                return (
                    <Breadcrumb.Item {...breadcrumbItemProps} key={breadcrumb.url}>{breadcrumb.title}</Breadcrumb.Item>
                )
            })}
        </Breadcrumb>
    )

    useEffect(() => {
        const toast = page.props.toast as undefined | ToastDto;

        if (toast) {
            notification[toast.level]({
                message: toast.title,
                description: toast.message,
            });
        }

    }, [page.props.toast])

    // useEffect(() => {
    //     console.log('AdminLayout use effect');
    //
    //     const toastEventListener = Inertia.on('success', (event) => {
    //         const toast = event.detail.page.props.toast as undefined | ToastDto;
    //
    //         console.log(`Successfully made a visit to ${event.detail.page.url}`)
    //
    //         if (toast) {
    //             notification[toast.level]({
    //                 message: toast.title,
    //                 description: toast.message
    //             })
    //         }
    //     });
    //
    //     return () => {
    //         toastEventListener();
    //     }
    // }, []);

    console.log('route.routes', route.routes![0]);


    // const menuProps: MenuProps = {
    //     /**
    //      * Needs custom handling, otherwise it always selects default
    //      */
    //     selectedKeys: selectedKeys()
    // }
    const [collapsed, setCollapsed] = useState(false);

    return (

        <div
            style={{
                height: '100vh',
            }}
        >
            <Head>
                <title>{props.title}</title>
            </Head>
            <ProLayout
                route={route}
                location={{
                    pathname: window.location.pathname,
                }}
                menuHeaderRender={() => (<img src={'/assets/images/examit-logo-dark.png'} style={{padding: 4}} />)}
                onMenuHeaderClick={() => Inertia.visit('/admin')}
                menuItemRender={(item: any, dom: ReactNode) => (
                    <Link href={item.path!}>{dom}</Link>
                )}
                rightContentRender={((props1: any) => {
                    console.log('props1', props1);
                    return (
                        <Menu selectedKeys={[]} mode="horizontal">

                            <SubMenu key="SubMenu" icon={<UserOutlined/>} title={firstName}>
                                {/*<Menu.ItemGroup title="Item 1">*/}
                                {/*    <Menu.Item key="setting:1">Option 1</Menu.Item>*/}
                                {/*    <Menu.Item key="setting:2">Option 2</Menu.Item>*/}
                                {/*</Menu.ItemGroup>*/}
                                {/*<Menu.ItemGroup title="Item 2">*/}
                                {/*    <Menu.Item key="setting:3">Option 3</Menu.Item>*/}
                                {/*    <Menu.Item key="setting:4">Option 4</Menu.Item>*/}
                                {/*</Menu.ItemGroup>*/}
                                <Menu.Item key="logout">
                                    <a href={window.route('logout')}>Log
                                        Out</a>
                                </Menu.Item>
                            </SubMenu>
                        </Menu>
                    )
                })}
                fixSiderbar={true}
                fixedHeader={true}
                footerRender={false}
                collapsed={collapsed}
                collapsedButtonRender={false}
                onCollapse={setCollapsed}
                headerContentRender={() => {
                    return (
                        <div
                            onClick={() => setCollapsed(!collapsed)}
                            style={{
                                cursor: 'pointer',
                                fontSize: '16px',
                            }}
                        >
                            {collapsed ? <MenuUnfoldOutlined/> : <MenuFoldOutlined/>}
                        </div>
                    );
                }}
            >
                <PageContainer
                    header={{
                        title: props.title,
                        subTitle: props.subTitle,
                        extra: props.headerRight,
                        breadcrumb: breadcrumbsRender
                    }}
                >
                    <div
                        style={{
                            minHeight: '80vh',
                        }}
                    >
                        {children}
                    </div>
                </PageContainer>
            </ProLayout>
        </div>
    );
};

export default AdminLayout;
