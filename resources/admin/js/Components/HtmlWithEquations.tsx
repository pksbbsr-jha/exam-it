import React, {useEffect} from 'react';

export interface HtmlWithEquationsData {
    id?: number;
    name: string;
    description: string;
    price: number;
    meta_includes: string[];
    meta_lessons: string[];
    meta_requirements: string[];
}

type Props = {
    html: string;
}

const HtmlWithEquations: React.FunctionComponent<Props> = (props) => {
    const element = React.createRef<HTMLDivElement>();

    useEffect(() => {
        // It does not work using WirisPlugin. It does not convert math tags to img
        // @ts-ignore
        // console.log('WirisPlugin.Parser.initParse(this.html.get(true));', WirisPlugin.Parser.initParse(props.html));

        waitForWindowVariable();
    });

    const waitForWindowVariable = () => {
        if((window as any).com) {
            convertEquations();
        } else {
            setTimeout(waitForWindowVariable, 500);
        }
    }

    const convertEquations = () => {
        (window as any).com.wiris.js.JsPluginViewer.parseElement(element.current, true,
            () => {
            }
        )
    }

    return (
        <>
            <div
                className={'tinyMce-content'}
                ref={element}
                dangerouslySetInnerHTML={{
                    __html: props.html
                }}
            />
        </>
    );
};

export default HtmlWithEquations;
