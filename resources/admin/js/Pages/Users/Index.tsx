import {Head, Link} from '@inertiajs/inertia-react';
import React, {useState} from 'react';
import {InertiaProps, ModelUser, Paginated} from '@admin/types';
import type {ProColumns} from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import ProCard from '@ant-design/pro-card';
import {EloquentTableHandler} from '@admin/Components/Tables/CustomTableHelpers';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import {Badge, BadgeProps, Button, Form, Input} from 'antd';
import moment from 'moment';
import {PlusOutlined} from '@ant-design/icons';
import UserFormModal from '@admin/Pages/Users/components/UserFormModal';
import {Inertia} from '@inertiajs/inertia';
import _ from 'lodash';

type Props = InertiaProps & {
    users: Paginated<ModelUser>;
}

const userStatus: (user: ModelUser) => { text: string; status: BadgeProps['status'] } = (user: ModelUser) => {
    if (user.blocked_at !== null) {
        return {status: 'error', text: 'Blocked'}
    }

    if (user.email_verified_at === null) {
        return {status: 'warning', text: 'Pending'}
    }

    return {status: 'success', text: 'Active'}
}

const columns: ProColumns<ModelUser>[] = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        defaultSortOrder: 'ascend',
        sorter: true,
    },
    {
        title: 'Username',
        dataIndex: 'username',
        key: 'username',
    },
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
    },
    {
        title: 'Status',
        key: 'status',
        render: (text, row: ModelUser) => (
            <Badge {...userStatus(row)} />
        ),
        hideInSearch: true,
        filters: [
            {text: 'Active', value: 'active'},
            {text: 'Pending', value: 'pending'},
            {text: 'Blocked', value: 'blocked'},
        ]
    },
    {
        title: 'Registration Date',
        dataIndex: 'created_at',
        key: 'created_at',
        renderText: text => moment(text).format('MM/DD/YYYY'),
        hideInSearch: true
    },
    {
        title: 'Actions',
        key: 'actions',
        valueType: 'option',
        align: 'right',
        render: (text, row) => [
            row.role === 'student' && (row.blocked_at === null ?
                <Link key={'block'} href={window.route('admin.users.block.add', row.id)} method={'PUT'}>Block </Link> :
                <Link key={'unblock'} href={window.route('admin.users.block.remove', row.id)}
                      method={'PUT'}>Unblock </Link>),
            row.role === 'student' &&
            <Link key={'make_admin'} href={window.route('admin.users.admin.add', row.id)} method={'PUT'}>Make
                Admin </Link>,
            row.role === 'admin' &&
            <Link key={'make_admin'} href={window.route('admin.users.admin.remove', row.id)} method={'PUT'}>Remove as
                Admin </Link>
            // <Link key={'edit'} href={window.route('admin.users.edit', row.id)}>Edit </Link>
        ],
    },
];

interface SearchForm {
    username?: string;
    email?: string;
}

const Dashboard: React.FunctionComponent<Props> = (props) => {

    const [modalForm, setModalForm] = useState(false);

    const [form] = Form.useForm();

    form.setFieldsValue(window.route().params);

    const onSearch = (values: SearchForm) => {
        console.log('submit', values, _.pickBy(values));
        Inertia.reload({
            // @ts-ignore
            data: values,
        });
    }

    const onSearchFailed = (e: unknown) => {
        console.log('unknown', e);
    }

    return (
        <>
            <Head>
                <title>Users</title>
            </Head>
            <AdminLayout
                title={'Users'}
                headerRight={
                    <Button type="primary" onClick={() => setModalForm(true)}>
                        <PlusOutlined/>
                        Add User
                    </Button>
                }
            >
                <ProCard>
                    <ProTable<ModelUser>
                        {...EloquentTableHandler(props.users, {})}
                        columns={columns}
                        search={{
                            filterType: 'light',
                            searchText: 'Search',
                            form
                        }}
                        onColumnsStateChange={map => {
                            console.log('map', map);
                        }}
                    />
                </ProCard>

                <UserFormModal show={modalForm}
                               onClose={() => setModalForm(false)}
                               onAdded={() => setModalForm(false)}
                />
            </AdminLayout>
        </>
    );
};

export default Dashboard;
