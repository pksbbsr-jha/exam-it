import React, {useState} from 'react';
import {Form, Input, Radio, Space} from 'antd';
import ValidationErrorsAlert from '@admin/Components/Forms/ValidationErrorsAlert';
import {onFinishFailed} from '@admin/Components/Forms/FormHelpers';
import {ModelUser, ModelUserRole} from '@admin/types';
import {Inertia} from '@inertiajs/inertia';
import {useForm} from 'antd/lib/form/Form';
import Modal from 'antd/lib/modal/Modal';

export interface UserFormData {
    name: string;
    username: string;
    email: string;
    password: string;
    password_confirmation: string;
    role: ModelUserRole
}

type Props = {
    show: boolean;
    onClose: () => void;
    onAdded: () => void;
    // user: ModelUser;
    // onSubmit: (data: UserFormData) => void
}

const UserFormModal: React.FunctionComponent<Props> = (props) => {

    const [form] = useForm();

    const onSubmit = (values: UserFormData) => {
        const payload: Partial<UserFormData> = {
            name: values.name,
            username: values.username,
            email: values.email,
            password: values.password,
            password_confirmation: values.password_confirmation,
            role: values.role
        }

        Inertia.post(window.route('admin.users.store'), payload, {
            preserveScroll: true,
            onSuccess: () => {
                form.resetFields();
                props.onAdded();
            }
        });
    }

    return (
        <Modal
            visible={props.show}
            onCancel={() => props.onClose()}
            onOk={form.submit}
            title={'Add User'}
            okText={'Add'}
        >
            <Form
                onFinish={onSubmit}
                onFinishFailed={onFinishFailed}
                autoComplete="on"
                layout={'vertical'}
                form={form}
            >
                <ValidationErrorsAlert style={{marginBottom: 20}}/>

                <Form.Item
                    label="Full Name"
                    name="name"
                    rules={[{required: true}]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Username"
                    name="username"
                    rules={[{
                        required: true,
                        pattern: new RegExp(/^[a-z0-9-_]{3,100}$/g),
                        message: 'Username has to be between 3-100 character long. It can only container lowercase characters including numbers, underscore (_) and dash (-).'
                    }]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Email"
                    name="email"
                    rules={[{required: true, type: 'email'}]}
                >
                    <Input type={'email'}/>
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{required: true}]}
                >
                    <Input type={'password'}/>
                </Form.Item>

                <Form.Item
                    label="Password Confirmation"
                    name="password_confirmation"
                    rules={[{required: true}]}
                >
                    <Input type={'password'}/>
                </Form.Item>

                <Form.Item
                    label="Role"
                    name="role"
                    rules={[{required: true}]}
                    initialValue={'student'}
                >
                    <Radio.Group>
                        <Space direction="vertical">
                            <Radio value={'student'}>Student</Radio>
                            <Radio value={'admin'}>Admin</Radio>
                        </Space>
                    </Radio.Group>
                </Form.Item>

            </Form>
        </Modal>
    );
};

export default UserFormModal;
