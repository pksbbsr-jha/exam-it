import React from 'react';
import {Button, Form, Input} from 'antd';
import RichTextEditor from '@admin/Components/Forms/Editor/RichTextEditor';
import ValidationErrorsAlert from '@admin/Components/Forms/ValidationErrorsAlert';
import {onFinishFailed} from '@admin/Components/Forms/FormHelpers';
import {ModelCourseSubmodule} from '@admin/types';
import {Inertia} from '@inertiajs/inertia';

export interface SubmoduleFormData {
    name: string;
    content: string;
}

type Props = {
    submodule: ModelCourseSubmodule;
    // onSubmit: (data: SubmoduleFormData) => void
}

const SubmoduleForm: React.FunctionComponent<Props> = (props) => {

    const onSubmit = (values: SubmoduleFormData) => {
        const payload: Partial<ModelCourseSubmodule> = {
            name: values.name,
            content: values.content,
        }

        Inertia.put(window.route('admin.submodules.update-content', props.submodule.id), payload, {
            preserveScroll: true
        });
    }

    return (
        <Form
            onFinish={onSubmit}
            onFinishFailed={onFinishFailed}
            autoComplete="on"
            layout={'vertical'}
        >
            <ValidationErrorsAlert style={{marginBottom: 20}}/>

            <Form.Item
                label="Course name"
                name="name"
                initialValue={props.submodule.name}
                rules={[{required: true}]}
            >
                <Input/>
            </Form.Item>


            <Form.Item
                label="Content"
                name="content"
                initialValue={props.submodule.content}
                rules={[{required: true}]}
            >
                {/*@ts-ignore*/}
                <RichTextEditor/>
            </Form.Item>

            <Form.Item>
                <Button type="primary" htmlType="submit">Update Submodule</Button>
            </Form.Item>
        </Form>
    );
};

export default SubmoduleForm;
