import React from 'react';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import {InertiaProps} from '@admin/types';
import ProCard from '@ant-design/pro-card';
import QuestionForm from '@admin/Pages/Submodules/Questions/Components/QuestionForm';

type Props = InertiaProps & {
    submodule_id: number;
}

const Create: React.FunctionComponent<Props> = (props) => {
    return (
        <>
            <AdminLayout title={'Add new Question'}>
                <ProCard>
                    <QuestionForm submodule_id={props.submodule_id}/>
                </ProCard>
            </AdminLayout>
        </>
    );
};

export default Create;
