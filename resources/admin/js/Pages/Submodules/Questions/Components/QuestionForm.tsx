import React from 'react';
import {Button, Form, Input, Space} from 'antd';
import RichTextEditor from '@admin/Components/Forms/Editor/RichTextEditor';
import ValidationErrorsAlert from '@admin/Components/Forms/ValidationErrorsAlert';
import {onFinishFailed} from '@admin/Components/Forms/FormHelpers';
import {ModelCourseQuestion} from '@admin/types';
import {Inertia} from '@inertiajs/inertia';
import {Rule} from 'rc-field-form/lib/interface';
import TextFormList from '@admin/Components/Forms/TextFormList';
import {MinusCircleOutlined, PlusOutlined} from '@ant-design/icons';

export interface QuestionFormData {
    name: string;
    question: string;
    answers: string[];
    explanation: string;
    duration: number;
}

type Props = {
    question?: ModelCourseQuestion;
    submodule_id: number;
    // onSubmit: (data: QuestionFormData) => void
}

const QuestionForm: React.FunctionComponent<Props> = (props) => {

    const onSubmit = (values: QuestionFormData) => {
        if (props.question) {
            // @ts-ignore
            Inertia.put(window.route('admin.submodules.questions.update', [props.submodule_id, props.question.id]), values);
        } else {
            // @ts-ignore
            Inertia.post(window.route('admin.submodules.questions.store', props.submodule_id), values);
        }
    }

    const submitButtonText = props.question ? 'Update Question' : 'Create Question';


    const listRule: Rule = (_form) => {
        return {
            validator: async (_rule, value: undefined | string[]) => {
                let hasError = !value;
                if (value) {
                    for (let i = 0; i < value.length; i++) {
                        if (value[i] === '') {
                            hasError = true;
                        }
                    }
                }

                if (hasError) {
                    throw new Error('Value cannot be empty.');
                }
            }
        }
    }

    return (
        <Form
            onFinish={onSubmit}
            onFinishFailed={onFinishFailed}
            autoComplete="on"
            layout={'vertical'}
        >
            <ValidationErrorsAlert style={{marginBottom: 20}}/>

            <Form.Item
                label="Question name"
                name="name"
                initialValue={props.question?.name ?? ''}
                rules={[{required: true}]}
            >
                <Input/>
            </Form.Item>

            <Form.Item
                label="Question"
                name="question"
                initialValue={props.question?.question ?? ''}
                rules={[{required: true}]}
            >
                {/*@ts-ignore*/}
                <RichTextEditor/>
            </Form.Item>

            <Form.Item
                label="Answers"
                name="answers"
                initialValue={props.question?.answers ?? Array(5).fill('')}
                rules={[listRule]}
            >
                <Form.List name={'answers'}>
                    {(fields, {add, remove}, { errors }) => {
                        return (
                            <>
                                {fields.map(({key, name, ...restField}) => (
                                    <Space key={key} style={{display: 'flex', marginBottom: 0}} align="start">
                                        <Form.Item
                                            {...restField}
                                            name={name}
                                            label={`Answer ${key+1} ${key === 0 ? '(Correct Answer)' : ''}`}
                                            rules={[{required: true, whitespace: true}]}
                                            style={{marginBottom: 10}}
                                        >
                                            {/*@ts-ignore*/}
                                            <RichTextEditor height={200}/>
                                        </Form.Item>
                                        {/*<MinusCircleOutlined onClick={() => remove(name)}/>*/}
                                    </Space>
                                ))}
                                <Form.ErrorList errors={errors} />
                                {/*<Form.Item style={{width: '280px', marginBottom: 0}}>*/}
                                {/*    <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined/>}>*/}
                                {/*        Add Answer*/}
                                {/*    </Button>*/}
                                {/*</Form.Item>*/}
                            </>
                        )
                    }}
                </Form.List>
            </Form.Item>

            <Form.Item
                label="Explanation"
                name="explanation"
                initialValue={props.question?.explanation ?? ''}
                rules={[{required: true}]}
            >
                {/*@ts-ignore*/}
                <RichTextEditor/>
            </Form.Item>

            {/*<Form.Item*/}
            {/*    label="Duration"*/}
            {/*    name="duration"*/}
            {/*    initialValue={props.question?.duration ?? ''}*/}
            {/*    rules={[{required: true}]}*/}
            {/*>*/}
            {/*    <Input type={'number'}/>*/}
            {/*</Form.Item>*/}

            <Form.Item>
                <Button type="primary" htmlType="submit">
                    {submitButtonText}
                </Button>
            </Form.Item>
        </Form>
    );
};

export default QuestionForm;
