import React from 'react';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import {InertiaProps, ModelCourseQuestion} from '@admin/types';
import ProCard from '@ant-design/pro-card';
import QuestionForm from '@admin/Pages/Submodules/Questions/Components/QuestionForm';

type Props = InertiaProps & {
    question: ModelCourseQuestion;
}

const Edit: React.FunctionComponent<Props> = (props) => {
    return (
        <>
            <AdminLayout title={'Edit Question'}>
                <ProCard>
                    <QuestionForm question={props.question} submodule_id={props.question.submodule_id}/>
                </ProCard>
            </AdminLayout>
        </>
    );
};

export default Edit;
