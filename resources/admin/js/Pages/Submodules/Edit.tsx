import React from 'react';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import {InertiaProps, ModelCourseSubmodule} from '@admin/types';
import ProCard from '@ant-design/pro-card';
import SubmoduleForm from '@admin/Pages/Submodules/Components/SubmoduleForm';

type Props = InertiaProps & {
    submodule: ModelCourseSubmodule;
}

const Edit: React.FunctionComponent<Props> = (props) => {
    return (
        <>
            <AdminLayout title={'Edit Submodule'}>
                <ProCard>
                    <SubmoduleForm submodule={props.submodule}/>
                </ProCard>
            </AdminLayout>
        </>
    );
};

export default Edit;
