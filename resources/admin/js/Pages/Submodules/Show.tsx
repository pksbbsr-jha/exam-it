import {Head, Link} from '@inertiajs/inertia-react';
import React, {useState} from 'react';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import {
    InertiaProps,
    ModelCourseQuestion,
    ModelCourseSubItemType,
    ModelCourseSubmodule,
    OrderDirection
} from '@admin/types';
import {Button, Descriptions, Popconfirm, Row, Table, TableColumnType} from 'antd';
import ProCard from '@ant-design/pro-card';
import HtmlWithEquations from '@admin/Components/HtmlWithEquations';
import {CourseContentItem} from '@admin/Pages/Courses/Components/CourseSubItemsList';
import {Inertia} from '@inertiajs/inertia';
import CourseSubItemsFormModal from '@admin/Pages/Courses/Components/CourseSubItemsFormModal';
import EmptyTableWithAdd from '@admin/Components/Tables/EmptyTableWithAdd';


type Props = InertiaProps & {
    submodule: ModelCourseSubmodule;
    questions: ModelCourseQuestion[];
}

const Show: React.FunctionComponent<Props> = (props) => {

    const title = `Submodule: ${props.submodule.name}`;

    const sectionGeneralInfo = (
        <ProCard
            title="General Info"
            extra={
                <Link href={window.route('admin.submodules.edit', props.submodule.id)}>
                    <Button
                        type="primary">Edit</Button>
                </Link>
            }
        >
            <Descriptions
                bordered
                column={1}
            >
                <Descriptions.Item label="Name">{props.submodule.name}</Descriptions.Item>
                <Descriptions.Item label="Content">
                    <HtmlWithEquations html={props.submodule.content}/>
                </Descriptions.Item>
            </Descriptions>
        </ProCard>
    );

    /**
     * Editing
     */
    const [modalForm, setModalForm] = useState<{ show: boolean; type: ModelCourseSubItemType; item: CourseContentItem | undefined, parentId: number }>({
        show: false,
        type: 'chapter',
        item: undefined,
        parentId: 0
    });

    const openModalForm = (type: ModelCourseSubItemType, parentId: number, item?: CourseContentItem) => {
        setModalForm({
            show: true,
            type,
            item,
            parentId
        })
    }

    const closeModalForm = () => {
        setModalForm({show: false, type: 'chapter', item: undefined, parentId: 0});
    }

    /**
     * Reordering
     */
    const [reorderingState, setReorderingState] = useState<{ type: ModelCourseSubItemType | null, parentId: number; }>({
        type: null,
        parentId: 1
    });

    const isReordering = (type: ModelCourseSubItemType, parentId?: number) => {
        if (parentId && parentId !== reorderingState.parentId) return false;

        return reorderingState.type === type;
    }

    const onReordering = (type: ModelCourseSubItemType, parentId: number, id: number, direction: OrderDirection) => {
        setReorderingState({
            type,
            parentId
        });

        Inertia.put(window.route(`admin.${type}s.reorder`, id), {
            direction
        }, {
            preserveScroll: true,
            onFinish: () => {
                setReorderingState({
                    type: null,
                    parentId: 0
                });
            }
        });
    }

    const onAdd = () => {
        Inertia.get(window.route('admin.submodules.questions.create', props.submodule.id));
    }

    const onRemoveQuestion = (question: ModelCourseQuestion) => {
        Inertia.delete(window.route('admin.submodules.questions.delete', [props.submodule.id, question.id]), {
            preserveScroll: true
        })
    }

    const questionsTableColumns: TableColumnType<ModelCourseQuestion>[] = [
        {
            title: 'ID',
            dataIndex: 'id'
        },
        {
            title: 'Name',
            dataIndex: 'name',
        },
        // {
        //     title: 'Duration',
        //     dataIndex: 'duration',
        // },
        {
            title: 'Actions',
            key: 'actions',
            align: 'right',
            render: (value: ModelCourseQuestion) => [
                <Link key={'edit'} href={window.route('admin.submodules.questions.edit', [value.submodule_id, value.id])}>Edit </Link>,
                <Popconfirm
                    key={'Delete'}
                    title="Are you sure to delete this question?"
                    onConfirm={() => onRemoveQuestion(value)}
                    onCancel={() => {}}
                    okText="Yes"
                    cancelText="No"
                >
                <a key={'delete'} >Delete </a>
                </Popconfirm>
            ]
        }
    ]


    return (
        <>
            <Head>
                <title>{title}</title>
            </Head>
            <AdminLayout
                title={title}
            >
                <Row gutter={[0, 16]}>
                    {sectionGeneralInfo}


                    <ProCard title={'Questions'} id={'questions'} extra={
                        <Button type="primary" onClick={() => onAdd()}>Add a new
                            Question</Button>
                    }>
                        <CourseSubItemsFormModal {...modalForm} onClose={closeModalForm}/>
                        {props.questions.length
                            ? <Table columns={questionsTableColumns} dataSource={props.questions} />
                            : <EmptyTableWithAdd type={'question'} onAdd={() => onAdd()}/>
                        }
                    </ProCard>

                </Row>
            </AdminLayout>
        </>
    );
};

export default Show;
