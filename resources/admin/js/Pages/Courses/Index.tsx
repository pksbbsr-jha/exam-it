import {Head, Link} from '@inertiajs/inertia-react';
import React from 'react';
import {InertiaProps, ModelCourse, Paginated} from '@admin/types';
import {PlusOutlined} from '@ant-design/icons';
import {Button} from 'antd';
import type {ProColumns} from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import ProCard from '@ant-design/pro-card';
import EmptyTable from '@admin/Components/Tables/EmptyTable';
import {EloquentPaginationHandler} from '@admin/Components/Tables/CustomTableHelpers';
import AdminLayout from '@admin/Components/Common/AdminLayout';

type Props = InertiaProps & {
    courses: Paginated<ModelCourse>;
}

const columns: ProColumns<ModelCourse>[] = [
    {
        title: 'id',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Actions',
        key: 'actions',
        valueType: 'option',
        align: 'right',
        render: (text, row) => [
            <Link key={'show'} href={window.route('admin.courses.show', row.id)}>Manage </Link>,
            <Link key={'edit'} href={window.route('admin.courses.edit', row.id)}>Edit </Link>
        ],
    },
];

const Dashboard: React.FunctionComponent<Props> = (props) => {

    return (
        <>
            <Head>
                <title>Courses</title>
            </Head>
            <AdminLayout
                title={'Courses'}
                headerRight={
                    <Link href={window.route('admin.courses.create')}>
                        <Button type="primary">
                            <PlusOutlined/>
                            Add Course
                        </Button>
                    </Link>
                }
            >
                <ProCard>
                    <ProTable<ModelCourse>
                        columns={columns}
                        dataSource={props.courses.data}
                        locale={{
                            emptyText: <EmptyTable/>
                        }}
                        toolbar={{
                            filter: false,
                        }}
                        type={'table'}
                        pagination={{
                            pageSize: props.courses.meta.per_page,
                            total: props.courses.meta.total,
                            current: props.courses.meta.current_page,
                            onChange: EloquentPaginationHandler()
                        }}
                        rowKey={record => record.id}
                        dateFormatter="string"
                        options={false}
                        toolBarRender={false}
                        search={false}
                    />
                </ProCard>
            </AdminLayout>
        </>
    );
};

export default Dashboard;
