import React from 'react';
import {Row, Spin} from 'antd';


type Props = {
    isReordering: boolean;
}

const CourseSubItemsContainer: React.FunctionComponent<Props> = (props) => {
    return (
        <Spin spinning={props.isReordering} delay={500}>
            <Row gutter={[0, 10]}>
                {props.children}
            </Row>
        </Spin>
    );
};

export default CourseSubItemsContainer;
