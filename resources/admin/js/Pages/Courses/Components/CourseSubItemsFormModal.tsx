import React, {createRef, useEffect} from 'react';
import {Form, Input, InputRef, Modal} from 'antd';
import ValidationErrorsAlert from '@admin/Components/Forms/ValidationErrorsAlert';
import {onFinishFailed} from '@admin/Components/Forms/FormHelpers';
import {ModelCourseSubItemType} from '@admin/types';
import {Inertia} from '@inertiajs/inertia';
import {useForm} from 'antd/lib/form/Form';
import _ from 'lodash';
import {VisitOptions} from '@inertiajs/inertia/types/types';

export interface SubItemData {
    id?: number;
    name: string;
}

type Props = {
    type: ModelCourseSubItemType;
    parentId: number; // Can be course_id, chapter_id, module_id
    item?: SubItemData;
    show: boolean;
    onClose: () => void,
}

const CourseSubItemsFormModal: React.FunctionComponent<Props> = (props) => {
    console.log('CourseSubItemsFormModal', props);
    const [form] = useForm();

    const submitButtonText = `${props.item ? 'Edit' : 'Create'} ${_.startCase(props.type)}`;

    const itemPlural = `${props.type}s`;

    const onSubmit = (formData: SubItemData) => {
        const options: VisitOptions = {
            onSuccess: () => {
                props.onClose();
                form.resetFields();
            },
            preserveScroll: true
        }

        const data = {
            name: formData.name,
            parent_id: props.parentId
        };

        if (props.item) {
            Inertia.put(window.route(`admin.${itemPlural}.update`, props.item.id), data, options);
        } else {
            Inertia.post(window.route(`admin.${itemPlural}.create`), data, options);
        }
    }

    useEffect(() => {
        if (props.show) {
            form.setFieldsValue({
                name: props.item?.name ?? '',
            })
        }
    }, [props.item]);

    useEffect(() => {
        setTimeout(() => {
            if (inputRef && inputRef.current) {
                inputRef.current.focus();
            }
        }, 250)
    }, [props.show])

    const inputRef = createRef<InputRef>();

    return (
        <Modal
            title={submitButtonText}
            visible={props.show}
            onCancel={() => props.onClose()}
            onOk={() => form.submit()}
            getContainer={false}
        >
            <Form
                onFinish={onSubmit}
                onFinishFailed={onFinishFailed}
                autoComplete="on"
                layout={'vertical'}
                form={form}
            >
                <ValidationErrorsAlert style={{marginBottom: 20}}/>

                <Form.Item
                    label={`${_.startCase(props.type)} Name`}
                    name="name"
                    initialValue={props.item?.name ?? ''}
                    rules={[{required: true}]}
                >
                    <Input autoFocus={true} ref={inputRef}/>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default CourseSubItemsFormModal;
