import React from 'react';
import {Button, Form, Input, InputNumber} from 'antd';
import RichTextEditor from '@admin/Components/Forms/Editor/RichTextEditor';
import {Rule} from 'rc-field-form/lib/interface';
import TextFormList from '@admin/Components/Forms/TextFormList';
import ValidationErrorsAlert from '@admin/Components/Forms/ValidationErrorsAlert';
import {onFinishFailed} from '@admin/Components/Forms/FormHelpers';
import {ModelCourse} from '@admin/types';
import {Inertia} from '@inertiajs/inertia';

export interface CourseFormData {
    name: string;
    description: string;
    price: number;
    meta_includes: string[];
    meta_lessons: string[];
    meta_requirements: string[];
}

type Props = {
    course?: ModelCourse;
    // onSubmit: (data: CourseFormData) => void
}

const CourseForm: React.FunctionComponent<Props> = (props) => {

    const onSubmit = (values: CourseFormData) => {
        const payload: Partial<ModelCourse> = {
            name: values.name,
            description: values.description,
            price: values.price,
            // @ts-ignore
            meta: {
                includes: values.meta_includes,
                lessons: values.meta_lessons,
                requirements: values.meta_requirements,
            },
        }

        if (props.course) {
            // @ts-ignore
            Inertia.put(window.route('admin.courses.update', props.course.id), payload);
        } else {
            // @ts-ignore
            Inertia.post(window.route('admin.courses.store'), payload);
        }
    }

    const submitButtonText = props.course ? 'Update Course' : 'Create Course';

    const listRule: Rule = (_form) => {
        return {
            validator: async (_rule, value: undefined | string[]) => {
                let hasError = !value;
                if (value) {
                    for (let i = 0; i < value.length; i++) {
                        if (value[i] === '') {
                            hasError = true;
                        }
                    }
                }

                if (hasError) {
                    throw new Error('Value cannot be empty.');
                }
            }
        }
    }

    return (
        <Form
            onFinish={onSubmit}
            onFinishFailed={onFinishFailed}
            autoComplete="on"
            layout={'vertical'}
        >
            <ValidationErrorsAlert style={{marginBottom: 20}}/>

            <Form.Item
                label="Course name"
                name="name"
                initialValue={props.course?.name ?? ''}
                rules={[{required: true}]}
            >
                <Input/>
            </Form.Item>

            <Form.Item
                label="Price"
                name="price"
                initialValue={props.course?.price ?? ''}
                rules={[{required: true}]}
            >
                <InputNumber
                    addonBefore={'$'}
                />
            </Form.Item>

            <Form.Item
                label="Description"
                name="description"
                initialValue={props.course?.description ?? ''}
                rules={[{required: true}]}
            >
                {/*@ts-ignore*/}
                <RichTextEditor/>
            </Form.Item>


            <Form.Item
                label="This course includes"
                name="meta_includes"
                initialValue={props.course?.meta.includes ?? []}
                rules={[listRule]}
            >
                <TextFormList
                    name={'meta_includes'}
                    itemPlaceholder={'This course includes...'}
                />
            </Form.Item>


            <Form.Item
                label="What student will learn"
                name="meta_lessons"
                initialValue={props.course?.meta.lessons ?? []}
                rules={[listRule]}
            >
                <TextFormList
                    name={'meta_lessons'}
                    itemPlaceholder={'What student will learn...'}
                />
            </Form.Item>

            <Form.Item
                label="Requirements"
                name="meta_requirements"
                initialValue={props.course?.meta.requirements ?? []}
                rules={[listRule]}
            >
                <TextFormList
                    name={'meta_requirements'}
                    itemPlaceholder={'Requirement...'}
                />
            </Form.Item>

            <Form.Item>
                <Button type="primary" htmlType="submit">
                    {submitButtonText}
                </Button>
            </Form.Item>
        </Form>
    );
};

export default CourseForm;
