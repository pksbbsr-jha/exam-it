import React from 'react';
import {Button, Empty} from 'antd';
import _ from 'lodash';
import {PlusOutlined} from '@ant-design/icons';
import {ModelCourseSubItemType} from '@admin/types';

export interface CourseContentItem {
    id: number;
    name: string;
    order: number;
}

type Props = {
    type: ModelCourseSubItemType;
    onAdd: (type: ModelCourseSubItemType) => void,
}

const CourseSubItemsEmpty: React.FunctionComponent<Props> = (props) => {
    const capitalType = _.startCase(props.type);

    return (
        <Empty
            description={
                <>
                    <p>No {props.type}s yet</p>
                    <Button type={'primary'} icon={<PlusOutlined/>} onClick={() => props.onAdd(props.type)}>Add a
                        new {capitalType}</Button>
                </>
            }
        />
    )
        ;
};

export default CourseSubItemsEmpty;
