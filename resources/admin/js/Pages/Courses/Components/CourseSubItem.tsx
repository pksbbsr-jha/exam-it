import React, {ReactNode, useState} from 'react';
import {Card, Col, Popconfirm, Space, Tooltip} from 'antd';
import {
    ArrowDownOutlined,
    ArrowUpOutlined,
    DeleteOutlined,
    DownOutlined,
    EditOutlined,
    PlusOutlined,
    RightOutlined
} from '@ant-design/icons';
import {blue, cyan, green} from '@ant-design/colors';
import {OrderDirection} from '@admin/types';
import _ from 'lodash';

type Props = {
    index: number;
    item: { id: number, name: string };
    onEdit: () => void;
    onAdd?: () => void;
    onDelete: () => void;
    canReorder: {
        up: boolean,
        down: boolean;
    }
    onReorder: (direction: OrderDirection) => void;
    type: 'chapter' | 'module' | 'submodule';
    additionalButtons?: ReactNode;
}

const CourseSubItem: React.FunctionComponent<Props> = (props) => {
    const capitalType = _.startCase(props.type);

    const childItem = props.type === 'chapter'
        ? 'module'
        : 'submodule'

    const background = props.type === 'chapter'
        ? green[0]
        : props.type === 'module' ? cyan[0] : blue[0];

    const [expanded, setExpanded] = useState(false);

    return (
        <Col span={24} key={props.index} id={`${props.type}-${props.item.id}`}>
            <Card title={<span onClick={() => setExpanded(!expanded)} style={{cursor: 'pointer'}}>
                {`${capitalType} ${props.index + 1}: ${props.item.name}`}
            </span>}
                  headStyle={{background: background}}
                  size={'small'}
                  extra={(
                      <Space>
                          {props.additionalButtons}
                          {props.canReorder.up &&
                              <Tooltip key={'move-up'} title="Move Up"><ArrowUpOutlined
                                  onClick={() => props.onReorder('up')}/></Tooltip>}
                          {props.canReorder.down &&
                              <Tooltip key={'move-down'} title="Move Down"><ArrowDownOutlined
                                  onClick={() => props.onReorder('down')}/></Tooltip>}
                          <Tooltip key={'edit-name'} title="Edit name"><EditOutlined onClick={props.onEdit}/></Tooltip>
                          <Tooltip key={'delete'} title="Delete"><Popconfirm
                              title={`Are you sure to delete this ${props.type}?`}
                              onConfirm={props.onDelete}><DeleteOutlined/></Popconfirm></Tooltip>
                          {props.onAdd && <Tooltip key={'create'} title={`Create a new ${childItem}`}><PlusOutlined
                              onClick={props.onAdd}/></Tooltip>}
                          {props.type !== 'submodule' &&
                              <Tooltip key={'collapse'} title={expanded ? 'Collapse' : 'Expand'}>
                                  {expanded
                                      ? <DownOutlined onClick={() => setExpanded(!expanded)}/>
                                      : <RightOutlined onClick={() => setExpanded(!expanded)}/>}
                              </Tooltip>
                          }
                      </Space>
                  )}
                  children={expanded ? props.children : null}
                  bodyStyle={{display: props.children && expanded ? 'block' : 'none'}}
            />
        </Col>
    );
};

export default CourseSubItem;
