import React, {useState} from 'react';
import {Card, Col, Empty, Row, Space, Spin} from 'antd';
import {ArrowDownOutlined, ArrowUpOutlined, EditOutlined} from '@ant-design/icons';
import {Inertia} from '@inertiajs/inertia';
import {blue} from '@ant-design/colors';
import _ from 'lodash';
import CourseSubItemsFormModal from '@admin/Pages/Courses/Components/CourseSubItemsFormModal';

export interface CourseContentItem {
    id: number;
    name: string;
    order: number;
}

type Props = {
    parentId: number;
    type: 'chapter' | 'module' | 'submodule';
    items: CourseContentItem[];
    children: (item: CourseContentItem) => React.ReactNode
}

const CourseSubItemsList: React.FunctionComponent<Props> = (props) => {

    const capitalType = _.startCase(props.type);

    const [modalForm, setModalForm] = useState<{ show: boolean; item: CourseContentItem | undefined }>({
        show: false,
        item: undefined
    });

    const openModalForm = (item: CourseContentItem) => {
        setModalForm({
            show: true,
            item
        })
    }

    const closeModalForm = () => {
        setModalForm({show: false, item: undefined});
    }

    const [reordering, setReordering] = useState(false);

    const reorder = (item: CourseContentItem, direction: 'up' | 'down') => {
        setReordering(true);
        Inertia.put(window.route(`admin.${props.type}s.reorder`, item.id), {
            direction
        }, {
            preserveScroll: true,
            onFinish: () => {
                setReordering(false);
            }
        });
    }


    return (
        <>
            <CourseSubItemsFormModal type={props.type} parentId={props.parentId} show={modalForm.show}
                                     onClose={() => closeModalForm()}
                                     item={modalForm.item}
            />
            {(props.items.length
                    ?
                    <Spin spinning={reordering} delay={500}>
                        <Row gutter={[0, 10]}>
                            {
                                props.items.map((item, index) => {
                                        return (
                                            <Col span={24} key={item.id}>
                                                <Card title={`${capitalType} ${index + 1}: ${item.name}`}
                                                      headStyle={{background: blue[0]}}
                                                      size={'small'}
                                                      extra={(
                                                          <Space>
                                                              {index > 0 &&
                                                                  <ArrowUpOutlined onClick={() => reorder(item, 'up')}/>}
                                                              {index < props.items.length - 1 &&
                                                                  <ArrowDownOutlined
                                                                      onClick={() => reorder(item, 'down')}/>}
                                                              <EditOutlined
                                                                  onClick={event => {
                                                                      event.stopPropagation();
                                                                      openModalForm(item);
                                                                  }}/>
                                                          </Space>
                                                      )}>
                                                    {props.children(item)}
                                                </Card>
                                            </Col>
                                        )
                                    }
                                )
                            }
                        </Row>
                    </Spin>
                    : <Empty description={`Start by adding a new ${capitalType}`}/>
            )}
        </>
    );
};

export default CourseSubItemsList;
