import React from 'react';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import {InertiaProps, ModelCourse} from '@admin/types';
import ProCard from '@ant-design/pro-card';
import CourseForm from '@admin/Pages/Courses/Components/CourseForm';

type Props = InertiaProps & {
    course: ModelCourse;
}

const Edit: React.FunctionComponent<Props> = (props) => {
    return (
        <>
            <AdminLayout title={'Edit Course'}>
                <ProCard>
                    <CourseForm course={props.course}/>
                </ProCard>
            </AdminLayout>
        </>
    );
};

export default Edit;
