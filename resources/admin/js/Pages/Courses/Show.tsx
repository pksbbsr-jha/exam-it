import {Head, Link} from '@inertiajs/inertia-react';
import React, {useState} from 'react';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import {
    InertiaProps,
    ModelCourse,
    ModelCourseSubItemType,
    ModuleCourseChapterWithModulesAndSubmodules,
    OrderDirection
} from '@admin/types';
import {Button, Descriptions, Row, Tooltip} from 'antd';
import ProCard from '@ant-design/pro-card';
import moment from 'moment';
import DateHelpers from '@admin/Helpers/DateHelpers';
import HtmlWithEquations from '@admin/Components/HtmlWithEquations';
import {CourseContentItem} from '@admin/Pages/Courses/Components/CourseSubItemsList';
import CourseSubItem from '@admin/Pages/Courses/Components/CourseSubItem';
import CourseSubItemsContainer from '@admin/Pages/Courses/Components/CourseSubItemsContainer';
import CourseSubItemsEmpty from '@admin/Pages/Courses/Components/CourseSubItemsEmpty';
import {Inertia} from '@inertiajs/inertia';
import CourseSubItemsFormModal from '@admin/Pages/Courses/Components/CourseSubItemsFormModal';
import {MessageOutlined, QuestionCircleOutlined} from '@ant-design/icons';


type Props = InertiaProps & {
    course: ModelCourse;
    chapters: ModuleCourseChapterWithModulesAndSubmodules[];
}

const Show: React.FunctionComponent<Props> = (props) => {

    const title = `Course: ${props.course.name}`;

    const displayList = (items: string[]) => {
        const listItems = items.map((item, index) => {
            return <li key={index}>{item}</li>;
        })

        return (
            <ul>{listItems}</ul>
        );
    }

    const sectionGeneralInfo = (
        <ProCard
            title="General Info"
            extra={
                <Link href={window.route('admin.courses.edit', props.course.id)}>
                    <Button
                        type="primary">Edit</Button>
                </Link>
            }
        >
            <Descriptions
                bordered
                column={1}

            >
                <Descriptions.Item label="Name">{props.course.name}</Descriptions.Item>
                <Descriptions.Item label="Price">${props.course.price}</Descriptions.Item>
                <Descriptions.Item label="Description">
                    <HtmlWithEquations html={props.course.description}/>
                </Descriptions.Item>
                <Descriptions.Item
                    label="Course includes">{displayList(props.course.meta.includes)}</Descriptions.Item>
                <Descriptions.Item
                    label="What you will learn">{displayList(props.course.meta.lessons)}</Descriptions.Item>
                <Descriptions.Item
                    label="Requirements">{displayList(props.course.meta.requirements)}</Descriptions.Item>
                <Descriptions.Item
                    label="Last Updated">{moment.unix(props.course.updated_at).format(DateHelpers.formats.default)}</Descriptions.Item>
            </Descriptions>
        </ProCard>
    );

    /**
     * Editing
     */
    const [modalForm, setModalForm] = useState<{ show: boolean; type: ModelCourseSubItemType; item: CourseContentItem | undefined, parentId: number }>({
        show: false,
        type: 'chapter',
        item: undefined,
        parentId: 0
    });

    const openModalForm = (type: ModelCourseSubItemType, parentId: number, item?: CourseContentItem) => {
        setModalForm({
            show: true,
            type,
            item,
            parentId
        })
    }

    const closeModalForm = () => {
        setModalForm({show: false, type: 'chapter', item: undefined, parentId: 0});
    }

    const onAdd = (type: ModelCourseSubItemType, parentId: number) => {
        setModalForm({
            show: true,
            type,
            parentId,
            item: undefined
        })
    }

    const onDelete = (type: ModelCourseSubItemType, id: number) => {
        console.log('deleting', type, id);
        Inertia.delete(window.route(`admin.${type}s.update`, id), {
            preserveScroll: true,
        });
    }


    /**
     * Reordering
     */
    const [reorderingState, setReorderingState] = useState<{ type: ModelCourseSubItemType | null, parentId: number; }>({
        type: null,
        parentId: 1
    });

    const isReordering = (type: ModelCourseSubItemType, parentId?: number) => {
        if (parentId && parentId !== reorderingState.parentId) return false;

        return reorderingState.type === type;
    }

    const onReordering = (type: ModelCourseSubItemType, parentId: number, id: number, direction: OrderDirection) => {
        setReorderingState({
            type,
            parentId
        });

        Inertia.put(window.route(`admin.${type}s.reorder`, id), {
            direction
        }, {
            preserveScroll: true,
            onFinish: () => {
                setReorderingState({
                    type: null,
                    parentId: 0
                });
            }
        });
    }

    const toggleModulePaidOrFree = (moduleId: number, isFree: boolean) => {
        Inertia.put(window.route(`admin.modules.togglePaidOrFree`, moduleId), {
            is_free: isFree
        }, {
            preserveScroll: true,
            onFinish: params => {
                console.log('params', params);
            }
        });
    }


    return (
        <>
            <Head>
                <title>{title}</title>
            </Head>
            <AdminLayout
                title={title}
            >
                <Row gutter={[0, 16]}>
                    {sectionGeneralInfo}

                    <ProCard id={'outline'} title={'Outline'} extra={
                        <Button type="primary" onClick={() => openModalForm('chapter', props.course.id)}>Add a new
                            Chapter</Button>
                    }>
                        <CourseSubItemsFormModal {...modalForm} onClose={closeModalForm}/>
                        {props.chapters.length
                            ? <CourseSubItemsContainer isReordering={isReordering('chapter')}>
                                {props.chapters.map((chapter, index) => {
                                    return (
                                        <CourseSubItem index={index}
                                                       key={`chapter-${chapter.id}`}
                                                       item={chapter}
                                                       onEdit={() => openModalForm('chapter', props.course.id, chapter)}
                                                       onAdd={() => openModalForm('module', chapter.id)}
                                                       onDelete={() => onDelete('chapter', chapter.id)}
                                                       canReorder={{
                                                           up: index > 0,
                                                           down: index < props.chapters.length - 1
                                                       }}
                                                       onReorder={(direction => onReordering('chapter', chapter.id, chapter.id, direction))}
                                                       type={'chapter'}>
                                            {chapter.modules.length
                                                ? <CourseSubItemsContainer
                                                    isReordering={isReordering('module', chapter.id)}>
                                                    {chapter.modules.map((module, index) => {
                                                        return (
                                                            <CourseSubItem index={index}
                                                                           item={module}
                                                                           key={`module-${module.id}`}
                                                                           onEdit={() => openModalForm('module', chapter.id, module)}
                                                                           onAdd={() => openModalForm('submodule', module.id)}
                                                                           onDelete={() => onDelete('module', module.id)}
                                                                           canReorder={{
                                                                               up: index > 0,
                                                                               down: index < chapter.modules.length - 1
                                                                           }}
                                                                           onReorder={(direction => onReordering('module', chapter.id, module.id, direction))}
                                                                           type={'module'}
                                                                           additionalButtons={[
                                                                               <Tooltip
                                                                                   key={'toggle'}
                                                                                   title="Toggle module as paid/free"><a
                                                                                   onClick={(() => toggleModulePaidOrFree(module.id, !module.is_free))}>{module.is_free ? '(Free)' : '(Paid)'} </a></Tooltip>,
                                                                           ]}
                                                            >
                                                                {module.submodules.length
                                                                    ? <CourseSubItemsContainer
                                                                        isReordering={isReordering('submodule', module.id)}>
                                                                        {module.submodules.map((submodule, index) => {
                                                                            return (
                                                                                <CourseSubItem index={index}
                                                                                               key={`submodule-${submodule.id}`}
                                                                                               item={submodule}
                                                                                               onEdit={() => openModalForm('submodule', module.id, submodule)}
                                                                                               onDelete={() => onDelete('submodule', submodule.id)}
                                                                                               canReorder={{
                                                                                                   up: index > 0,
                                                                                                   down: index < module.submodules.length - 1
                                                                                               }}
                                                                                               onReorder={(direction => onReordering('submodule', module.id, submodule.id, direction))}
                                                                                               type={'submodule'}
                                                                                               additionalButtons={[
                                                                                                   <Tooltip
                                                                                                       key={'show'}
                                                                                                       title="Manage Submodule"><Link
                                                                                                       href={window.route('admin.submodules.show', submodule.id)}>Manage </Link></Tooltip>,
                                                                                                   <Tooltip
                                                                                                       key={'edit'}
                                                                                                       title="Edit Submodule"><Link
                                                                                                       href={window.route('admin.submodules.edit', submodule.id)}>Edit </Link></Tooltip>,
                                                                                                   <Tooltip
                                                                                                       key={'questions'}
                                                                                                       title="Manage Questions"><Link
                                                                                                       href={window.route('admin.submodules.show', submodule.id) + '#questions'}>{submodule.questions_count}
                                                                                                       <QuestionCircleOutlined/></Link></Tooltip>,
                                                                                                   <Tooltip
                                                                                                       key={'discussions'}
                                                                                                       title="Manage Discussions"><Link
                                                                                                       href={window.route('admin.submodules.show', submodule.id) + '#discussions'}>0 <MessageOutlined/></Link></Tooltip>,
                                                                                               ]}
                                                                                >
                                                                                    {/*<Space>*/}
                                                                                    {/*    <Link href={window.route('admin.submodules.edit', submodule.id)}><Button>Edit Submodule</Button></Link>*/}
                                                                                    {/*    <Link href={window.route('admin.submodules.show', submodule.id)}><Button>Manage Submodule</Button></Link>*/}
                                                                                    {/*    <Link href={window.route('admin.submodules.show', submodule.id) + '#questions'}><Button>Manage Questions ({submodule.questions_count})</Button></Link>*/}
                                                                                    {/*    <Button disabled={true}>Manage*/}
                                                                                    {/*        Discussions</Button>*/}
                                                                                    {/*</Space>*/}
                                                                                </CourseSubItem>
                                                                            )
                                                                        })}
                                                                    </CourseSubItemsContainer>
                                                                    : <CourseSubItemsEmpty type={'submodule'}
                                                                                           onAdd={() => onAdd('submodule', module.id)}
                                                                    />
                                                                }
                                                            </CourseSubItem>
                                                        )
                                                    })}
                                                </CourseSubItemsContainer>
                                                : <CourseSubItemsEmpty type={'module'}
                                                                       onAdd={() => onAdd('module', chapter.id)}/>
                                            }
                                        </CourseSubItem>
                                    )
                                })}
                            </CourseSubItemsContainer>
                            : <CourseSubItemsEmpty type={'chapter'} onAdd={() => onAdd('chapter', props.course.id)}/>
                        }
                    </ProCard>


                    {/*<ProCard*/}
                    {/*    title={'Outline'}>*/}
                    {/*    <CourseSubItemsList parentId={props.course.id} type={'chapter'} items={props.chapters}>*/}
                    {/*        {(chapter: CourseContentItem) => (*/}
                    {/*            // @ts-ignore*/}
                    {/*            <CourseSubItemsList parentId={chapter.id} type={'module'} items={chapter.modules}>*/}
                    {/*                {(module: CourseContentItem) => (*/}
                    {/*                    // @ts-ignore*/}
                    {/*                    <CourseSubItemsList parentId={chapter.id} type={'submodule'}*/}
                    {/*                        // @ts-ignore*/}
                    {/*                                        items={module.submodules}>*/}
                    {/*                        {(submodule: CourseContentItem) => (*/}
                    {/*                            <h4>{submodule.name}</h4>*/}
                    {/*                        )}*/}
                    {/*                    </CourseSubItemsList>*/}
                    {/*                )}*/}
                    {/*            </CourseSubItemsList>*/}
                    {/*        )}*/}
                    {/*    </CourseSubItemsList>*/}
                    {/*</ProCard>*/}
                </Row>
            </AdminLayout>
        </>
    );
};

export default Show;
