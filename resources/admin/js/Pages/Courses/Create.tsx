import React from 'react';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import {InertiaProps} from '@admin/types';
import ProCard from '@ant-design/pro-card';
import {Inertia} from '@inertiajs/inertia';
import CourseForm, {CourseFormData} from '@admin/Pages/Courses/Components/CourseForm';

type Props = InertiaProps & {}

const Create: React.FunctionComponent<Props> = (props) => {
    return (
        <>
            <AdminLayout title={'Add Course'}>
                <ProCard>
                    <CourseForm />
                </ProCard>
            </AdminLayout>
        </>
    );
};

export default Create;
