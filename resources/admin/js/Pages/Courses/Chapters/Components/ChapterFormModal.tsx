import React, {useEffect} from 'react';
import {Form, Input, Modal} from 'antd';
import ValidationErrorsAlert from '@admin/Components/Forms/ValidationErrorsAlert';
import {onFinishFailed} from '@admin/Components/Forms/FormHelpers';
import {ModelCourseChapter} from '@admin/types';
import {Inertia} from '@inertiajs/inertia';
import {useForm} from 'antd/lib/form/Form';

export interface ChapterFormData {
    id?: number;
    name: string;
}

type Props = {
    courseId: number;
    show: boolean;
    onClose: () => void,
    chapter?: ModelCourseChapter;
    onCreate?: (data: ChapterFormData) => void;
    onUpdate?: (data: ChapterFormData) => void;
}

const ChapterFormModal: React.FunctionComponent<Props> = (props) => {
    console.log('ChapterFormModal', props);

    const [form] = useForm();

    const submitButtonText = props.chapter ? 'Edit Chapter' : 'Create Chapter';

    const onSubmit = (formData: ChapterFormData) => {
        if (props.chapter) {
            // @ts-ignore
            Inertia.put(window.route('admin.courses.chapters.update', [props.courseId, props.chapter.id]), formData, {
                onSuccess: params => {
                    props.onClose();
                    form.resetFields();
                },
                preserveScroll: true
            });
            if (props.onUpdate) props.onUpdate(formData);
        } else {
            // @ts-ignore
            Inertia.post(window.route('admin.courses.chapters.create', [props.courseId]), formData, {
                onSuccess: params => {
                    props.onClose();
                    form.resetFields();
                }
            });
            if (props.onCreate) props.onCreate(formData);
        }
    }

    useEffect(() => {
        form.setFieldsValue({
            name: props.chapter?.name ?? '',
        })
    }, [props.chapter]);

    return (
        <Modal
            title={submitButtonText}
            visible={props.show}
            onCancel={() => props.onClose()}
            onOk={() => form.submit()}
        >
            <Form
                onFinish={onSubmit}
                onFinishFailed={onFinishFailed}
                autoComplete="on"
                layout={'vertical'}
                form={form}
            >
                <ValidationErrorsAlert style={{marginBottom: 20}}/>

                <Form.Item
                    label="Chapter name"
                    name="name"
                    initialValue={props.chapter?.name ?? ''}
                    rules={[{required: true}]}
                >
                    <Input autoFocus={true} />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ChapterFormModal;
