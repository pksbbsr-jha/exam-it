import React, {useState} from 'react';
import {ModelCourseChapter, ModuleCourseChapterWithModulesAndSubmodules} from '@admin/types';
import {Button, Card, Col, Empty, Row, Space, Spin} from 'antd';
import ChapterFormModal from '@admin/Pages/Courses/Chapters/Components/ChapterFormModal';
import {ArrowDownOutlined, ArrowUpOutlined, EditOutlined, PlusOutlined} from '@ant-design/icons';
import ProCard from '@ant-design/pro-card';
import {Inertia} from '@inertiajs/inertia';
import {blue} from '@ant-design/colors';
import ModulesList from '@admin/Pages/Courses/Chapters/Modules/components/ModulesList';

type Props = {
    courseId: number;
    chapters: ModuleCourseChapterWithModulesAndSubmodules[];
}

const ChaptersList: React.FunctionComponent<Props> = (props) => {

    const [modalChapter, setModalChapter] = useState<{ show: boolean; chapter: ModelCourseChapter | undefined }>({
        show: false,
        chapter: undefined
    });

    const openModalChapter = (chapter?: ModelCourseChapter) => {
        setModalChapter({
            show: true,
            chapter
        })
    }

    const closeModalChapter = () => {
        setModalChapter({show: false, chapter: undefined});
    }

    const [reordering, setReordering] = useState(false);

    const reorder = (chapter: ModelCourseChapter, direction: 'up' | 'down') => {
        setReordering(true);
        Inertia.put(window.route('admin.courses.chapters.reorder', [props.courseId, chapter.id]), {
            direction
        }, {
            preserveScroll: true,
            onFinish: () => {
                setReordering(false);
            }
        });
    }


    return (
        <ProCard
            id={'outline'}
            title={'Outline'}
            extra={
                <Button type="primary" onClick={() => openModalChapter()} icon={<PlusOutlined/>}>Add a new
                    Chapter</Button>
            }
        >
            <ChapterFormModal courseId={props.courseId} chapter={modalChapter.chapter} show={modalChapter.show}
                              onClose={() => closeModalChapter()}/>
            {(props.chapters.length
                    ?
                    <Spin spinning={reordering} delay={500}>
                        <Row gutter={[0, 10]}>
                            {
                                props.chapters.map((chapter, index) => {
                                        return (
                                            <Col span={24} key={chapter.id}>
                                                <Card title={`Chapter ${index + 1}: ${chapter.name}`}
                                                      headStyle={{background: blue[0]}}
                                                      size={'small'}
                                                      extra={(
                                                          <Space>
                                                              {index > 0 &&
                                                                  <ArrowUpOutlined onClick={() => reorder(chapter, 'up')}/>}
                                                              {index < props.chapters.length - 1 &&
                                                                  <ArrowDownOutlined
                                                                      onClick={() => reorder(chapter, 'down')}/>}
                                                              <EditOutlined
                                                                  onClick={event => {
                                                                      event.stopPropagation();
                                                                      openModalChapter(chapter);
                                                                  }}/>
                                                          </Space>
                                                      )}>
                                                    <ModulesList courseId={props.courseId} chapter={chapter}/>
                                                </Card>
                                            </Col>
                                        )
                                    }
                                )
                            }
                        </Row>
                    </Spin>
                    : <Empty description={'Start by adding a new Chapter'}/>
            )}
        </ProCard>
    );
};

export default ChaptersList;
