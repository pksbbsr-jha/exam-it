import React, {useState} from 'react';
import {ModelCourseModuleMini, ModuleCourseChapterWithModulesAndSubmodules} from '@admin/types';
import {Button, Card, Col, Empty, Row, Space, Spin} from 'antd';
import {ArrowDownOutlined, ArrowUpOutlined, EditOutlined, PlusOutlined} from '@ant-design/icons';
import ProCard from '@ant-design/pro-card';
import {Inertia} from '@inertiajs/inertia';
import {blue} from '@ant-design/colors';
import {Link} from '@inertiajs/inertia-react';
import ModuleFormModal from '@admin/Pages/Courses/Chapters/Modules/components/ModuleFormModal';

type Props = {
    courseId: number;
    chapter: ModuleCourseChapterWithModulesAndSubmodules;
}

const ModulesList: React.FunctionComponent<Props> = (props) => {

    const [modalModule, setModalModule] = useState<{ show: boolean; module: ModelCourseModuleMini | undefined }>({
        show: false,
        module: undefined
    });

    const openModalModule = (module?: ModelCourseModuleMini) => {
        setModalModule({
            show: true,
            module
        })
    }

    const closeModalModule = () => {
        setModalModule({show: false, module: undefined});
    }

    const [reordering, setReordering] = useState(false);

    const reorder = (module: ModelCourseModuleMini, direction: 'up' | 'down') => {
        setReordering(true);
        Inertia.put(window.route('admin.courses.chapters.reorder', [props.courseId, module.id]), {
            direction
        }, {
            preserveScroll: true,
            onFinish: () => {
                setReordering(false);
            }
        });
    }


    return (
        <div>
            <ModuleFormModal courseId={props.courseId} chapterId={props.chapter.id} module={modalModule.module}
                             show={modalModule.show}
                             onClose={() => closeModalModule()}/>
            {(props.chapter.modules.length
                    ?
                    <Spin spinning={reordering} delay={500}>
                        <Row gutter={[0, 10]}>
                            {
                                props.chapter.modules.map((module, index) => {
                                        return (
                                            <Col span={24} key={module.id}>
                                                <Card title={`Module ${index + 1}: ${module.name}`}
                                                      headStyle={{background: blue[2]}}
                                                      size={'small'}
                                                      extra={(
                                                          <Space>
                                                              {index > 0 &&
                                                                  <ArrowUpOutlined onClick={() => reorder(module, 'up')}/>}
                                                              {index < props.chapter.modules.length - 1 &&
                                                                  <ArrowDownOutlined
                                                                      onClick={() => reorder(module, 'down')}/>}
                                                              <EditOutlined
                                                                  onClick={event => {
                                                                      event.stopPropagation();
                                                                      openModalModule(module);
                                                                  }}/>
                                                          </Space>
                                                      )}>
                                                    <>
                                                        {module.submodules.length === 0
                                                            ? <>
                                                                <Empty
                                                                    description={
                                                                        <>
                                                                            <p>Chapter does not have any modules yet</p>
                                                                            <Link
                                                                                href={window.route('admin.courses.chapters.modules.create', [props.courseId, props.chapter.id])}>
                                                                                <Button type={'primary'}
                                                                                        icon={<PlusOutlined/>}>Add a new
                                                                                    Module</Button>
                                                                            </Link>
                                                                        </>
                                                                    }
                                                                />
                                                            </>
                                                            : <h2>Todo</h2>
                                                        }
                                                    </>
                                                </Card>
                                            </Col>
                                        )
                                    }
                                )
                            }
                        </Row>
                    </Spin>
                    : <Empty
                        description={
                            <>
                                <p>Chapter does not have any modules yet</p>
                                <Button type={'primary'} icon={<PlusOutlined/>} onClick={() => openModalModule()}>Add a
                                    new
                                    Module</Button>
                            </>
                        }
                    />
            )}
        </div>
    );
};

export default ModulesList;


