import React, {useEffect} from 'react';
import {Form, Input, Modal} from 'antd';
import ValidationErrorsAlert from '@admin/Components/Forms/ValidationErrorsAlert';
import {onFinishFailed} from '@admin/Components/Forms/FormHelpers';
import {ModelCourseModuleMini} from '@admin/types';
import {Inertia} from '@inertiajs/inertia';
import {useForm} from 'antd/lib/form/Form';

export interface ModuleFormData {
    id?: number;
    name: string;
}

type Props = {
    courseId: number;
    chapterId: number;
    show: boolean;
    onClose: () => void,
    module?: ModelCourseModuleMini;
    onCreate?: (data: ModuleFormData) => void;
    onUpdate?: (data: ModuleFormData) => void;
}

const ModuleFormModal: React.FunctionComponent<Props> = (props) => {
    console.log('ModuleFormModal', props);

    const [form] = useForm();

    const submitButtonText = props.module ? 'Edit Module' : 'Create Module';

    const onSubmit = (formData: ModuleFormData) => {
        if (props.module) {
            // @ts-ignore
            Inertia.put(window.route('admin.courses.chapters.update', [props.courseId, props.module.id]), formData, {
                onSuccess: () => {
                    props.onClose();
                    form.resetFields();
                },
                preserveScroll: true
            });
            if (props.onUpdate) props.onUpdate(formData);
        } else {
            // @ts-ignore
            Inertia.post(window.route('admin.courses.chapters.create', [props.courseId]), formData, {
                onSuccess: () => {
                    props.onClose();
                    form.resetFields();
                }
            });
            if (props.onCreate) props.onCreate(formData);
        }
    }

    useEffect(() => {
        form.setFieldsValue({
            name: props.module?.name ?? '',
        })
    }, [props.module]);

    return (
        <Modal
            title={submitButtonText}
            visible={props.show}
            onCancel={() => props.onClose()}
            onOk={() => form.submit()}
        >
            <Form
                onFinish={onSubmit}
                onFinishFailed={onFinishFailed}
                autoComplete="on"
                layout={'vertical'}
                form={form}
            >
                <ValidationErrorsAlert style={{marginBottom: 20}}/>

                <Form.Item
                    label="Module name"
                    name="name"
                    initialValue={props.module?.name ?? ''}
                    rules={[{required: true}]}
                >
                    <Input/>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ModuleFormModal;
