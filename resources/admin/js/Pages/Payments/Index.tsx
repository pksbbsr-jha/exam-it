import {Head, Link} from '@inertiajs/inertia-react';
import React, {useState} from 'react';
import {InertiaProps, ModelPayment, Paginated} from '@admin/types';
import type {ProColumns} from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import ProCard from '@ant-design/pro-card';
import EmptyTable from '@admin/Components/Tables/EmptyTable';
import {EloquentPaginationHandler} from '@admin/Components/Tables/CustomTableHelpers';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import {Badge, BadgeProps, message, Popconfirm} from 'antd';
import moment from 'moment';
import UserFormModal from '@admin/Pages/Users/components/UserFormModal';
import {Inertia} from '@inertiajs/inertia';

type Props = InertiaProps & {
    payments: Paginated<ModelPayment>;
}

const paymentStatus: (payment: ModelPayment) => { text: string; status: BadgeProps['status'] } = (payment: ModelPayment) => {
    if (payment.refunded_at !== null) {
        return {status: 'warning', text: 'Refunded'}
    }

    return {status: 'success', text: 'Succeeded'}
}

const currencyFormatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',

    // These options are needed to round to whole numbers if that's what you want.
    //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
    //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
});



const PaymentsIndex: React.FunctionComponent<Props> = (props) => {

    const [modalForm, setModalForm] = useState(false);

    const refund = (paymentId: number) => {

        return () => {
            Inertia.put(window.route('admin.payments.refund', paymentId));
        }
    }

    const columns: ProColumns<ModelPayment>[] = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Amount',
            dataIndex: 'amount',
            key: 'amount',
            render: text => currencyFormatter.format(text as number / 100)
        },
        {
            title: 'User',
            dataIndex: ['user', 'email'],
        },
        {
            title: 'Status',
            key: 'status',
            render: (text, row: ModelPayment) => [
                <Badge {...paymentStatus(row)} />
            ]
        },
        {
            title: 'Payment Date',
            dataIndex: 'created_at',
            key: 'created_at',
            renderText: text => moment(text * 1000).format('MM/DD/YYYY')
        },
        {
            title: 'Refund Date',
            dataIndex: 'refunded_at',
            renderText: text => text ? moment(text * 1000).format('MM/DD/YYYY') : '-'
        },
        {
            title: 'Actions',
            key: 'actions',
            valueType: 'option',
            align: 'right',
            render: (text, row) => [
                row.refunded_at === null &&
                <Popconfirm
                    title="Are you sure you want to refund this payment?"
                    onConfirm={refund(row.id)}
                    okText="Yes"
                    cancelText="No"
                    placement={'left'}
                ><a href="#">Refund </a></Popconfirm>
            ],
        },
    ];

    return (
        <>
            <Head>
                <title>Payments</title>
            </Head>
            <AdminLayout
                title={'Payments'}
            >
                <ProCard>
                    <ProTable<ModelPayment>
                        columns={columns}
                        dataSource={props.payments.data}
                        locale={{
                            emptyText: <EmptyTable/>
                        }}
                        toolbar={{
                            filter: false,
                        }}
                        type={'table'}
                        pagination={{
                            pageSize: props.payments.meta.per_page,
                            total: props.payments.meta.total,
                            current: props.payments.meta.current_page,
                            onChange: EloquentPaginationHandler()
                        }}
                        rowKey={record => record.id}
                        dateFormatter={value => moment(value).format('MM/DD/YYYY')}
                        options={false}
                        toolBarRender={false}
                        search={false}
                    />
                </ProCard>

                <UserFormModal show={modalForm}
                               onClose={() => setModalForm(false)}
                               onAdded={() => setModalForm(false)}
                />
            </AdminLayout>
        </>
    );
};

export default PaymentsIndex;
