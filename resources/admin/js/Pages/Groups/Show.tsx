import {Head} from '@inertiajs/inertia-react';
import React from 'react';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import {InertiaProps, ModelGroup, ModelGroupInvitation, ModelUser,} from '@admin/types';
import {Badge, Descriptions, Row} from 'antd';
import ProCard from '@ant-design/pro-card';
import ProTable, {ProColumns} from '@ant-design/pro-table';
import EmptyTable from '@admin/Components/Tables/EmptyTable';
import moment from 'moment';
import MoreDropdown from '@admin/Components/Common/MoreDropdown';
import {ItemType} from 'antd/es/menu/hooks/useItems';
import {DeleteOutlined, MessageOutlined, RedoOutlined} from '@ant-design/icons';
import {Inertia} from '@inertiajs/inertia';

type Props = InertiaProps & {
    group: ModelGroup;
    members: ModelUser[];
    invitations: ModelGroupInvitation[];
}

type GroupMemberStatus = 'Active' | 'Invite sent';

interface GroupMemberOrInvitation {
    id: number;
    key: string;
    name: string;
    usernameOrEmail: string;
    status: GroupMemberStatus;
}

const Show: React.FunctionComponent<Props> = (props) => {

    const title = `Group: ${props.group.name}`;

    const sectionGeneralInfo = (
        <ProCard
            title="General Info"
            // extra={
            // <Link href={window.route('admin.discussions.edit', props.discussion.id)}>
            //     <Button
            //         type="primary">Edit</Button>
            // </Link>
            // }
        >
            <Descriptions
                bordered
                column={1}

            >
                <Descriptions.Item label="Name">{props.group.name}</Descriptions.Item>
                <Descriptions.Item label="Owner">{props.group.owner?.name}</Descriptions.Item>
            </Descriptions>
        </ProCard>
    );


    const memberOptions: (user: GroupMemberOrInvitation) => ItemType[] = (user) => {

        const options: ItemType[] = [];

        if (user.status === 'Invite sent') {
            options.push({
                key: 'resendInvite',
                label: 'Re-send Invite',
                icon: <RedoOutlined/>,
                onClick: () => {
                    Inertia.post(window.route('admin.groups.invitations.resendInvite', [props.group.id, user.id]), {}, {
                        preserveScroll: true
                    });
                }
            });
        }


        options.push({
            key: 'delete',
            label: 'Delete',
            icon: <DeleteOutlined/>,
            onClick: () => {
                const route = `admin.groups.${user.status === 'Active' ? 'members' : 'invitations'}.remove`;
                Inertia.delete(window.route(route, [props.group.id, user.id]))
            }
        });

        return options;
    };

    const membersAndInvitations: GroupMemberOrInvitation[] = [
        ...props.members.map(member => ({
            id: member.id,
            key: 'member_' + member.id,
            name: member.name,
            usernameOrEmail: member.username,
            status: 'Active' as GroupMemberStatus
        })),
        ...props.invitations.map(member => ({
            id: member.id,
            key: 'invitation_' + member.id,
            name: '',
            usernameOrEmail: member.email,
            status: 'Invite sent' as GroupMemberStatus
        })),
    ];

    const membersColumns: ProColumns<GroupMemberOrInvitation>[] = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Username / Email',
            dataIndex: 'usernameOrEmail',
            key: 'username',
        },
        {
            title: 'Status',
            key: 'status',
            render: (text, row) => {
                return <span><Badge status={row.status === 'Active' ? 'success' : 'warning'}/> {row.status}</span>
            }
        },
        {
            title: 'Actions',
            key: 'actions',
            valueType: 'option',
            align: 'right',
            render: (text, row) => [
                <MoreDropdown key={'options'} items={memberOptions(row)}/>
            ],
        },
    ]

    const sectionMembers = (
        <ProCard title={'Members'}>
            <ProTable<GroupMemberOrInvitation>
                columns={membersColumns}
                dataSource={membersAndInvitations}
                locale={{
                    emptyText: <EmptyTable/>
                }}
                toolbar={{
                    filter: false,
                }}
                type={'table'}
                rowKey={record => record.key}
                dateFormatter={value => moment(value).format('MM/DD/YYYY')}
                options={false}
                toolBarRender={false}
                search={false}
            />
        </ProCard>
    );

    return (
        <>
            <Head>
                <title>{title}</title>
            </Head>
            <AdminLayout
                title={title}
            >
                <Row gutter={[0, 16]}>
                    {sectionGeneralInfo}
                    {sectionMembers}
                </Row>
            </AdminLayout>
        </>
    );
};

export default Show;
