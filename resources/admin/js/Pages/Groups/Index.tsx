import {Head, Link} from '@inertiajs/inertia-react';
import React, {useState} from 'react';
import {InertiaProps, ModelGroup, Paginated} from '@admin/types';
import type {ProColumns} from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import ProCard from '@ant-design/pro-card';
import EmptyTable from '@admin/Components/Tables/EmptyTable';
import {EloquentPaginationHandler} from '@admin/Components/Tables/CustomTableHelpers';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import moment from 'moment';
import UserFormModal from '@admin/Pages/Users/components/UserFormModal';
import {Popconfirm} from 'antd';
import {Inertia} from '@inertiajs/inertia';

type Props = InertiaProps & {
    groups: Paginated<ModelGroup>;
}

const GroupsIndex: React.FunctionComponent<Props> = (props) => {

    const [modalForm, setModalForm] = useState(false);

    const columns: ProColumns<ModelGroup>[] = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        // {
        //     title: 'User',
        //     dataIndex: ['user', 'email'],
        // },
        {
            title: 'Owner',
            key: 'owner',
            render: (text, row: ModelGroup) => [
                <span key={'test2'}>{row.owner!.name}</span>
            ]
        },
        {
            title: 'Members Count',
            dataIndex: 'members_count',
        },
        {
            title: 'Created At',
            dataIndex: 'created_at',
            key: 'created_at',
            renderText: text => moment(text * 1000).format('MM/DD/YYYY')
        },
        {
            title: 'Actions',
            key: 'actions',
            valueType: 'option',
            align: 'right',
            render: (text, row) => [
                <Link key={'show'} href={window.route('admin.groups.show', row.id)}>Manage </Link>,
                <Popconfirm key={'delete'} title={'Are you sure you want to delete this group?'}
                onConfirm={() => Inertia.delete(window.route('admin.groups.delete', [row.id]))}>
                    <a
                onClick={e => e.preventDefault()}>Delete</a>
                </Popconfirm>,
            ],
        },
    ];

    return (
        <>
            <Head>
                <title>Groups</title>
            </Head>
            <AdminLayout
                title={'Groups'}
            >
                <ProCard>
                    <ProTable<ModelGroup>
                        columns={columns}
                        dataSource={props.groups.data}
                        locale={{
                            emptyText: <EmptyTable/>
                        }}
                        toolbar={{
                            filter: false,
                        }}
                        type={'table'}
                        pagination={{
                            pageSize: props.groups.meta.per_page,
                            total: props.groups.meta.total,
                            current: props.groups.meta.current_page,
                            onChange: EloquentPaginationHandler()
                        }}
                        rowKey={record => record.id}
                        dateFormatter={value => moment(value).format('MM/DD/YYYY')}
                        options={false}
                        toolBarRender={false}
                        search={false}
                    />
                </ProCard>

                <UserFormModal show={modalForm}
                               onClose={() => setModalForm(false)}
                               onAdded={() => setModalForm(false)}
                />
            </AdminLayout>
        </>
    );
};

export default GroupsIndex;
