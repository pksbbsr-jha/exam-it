import React from 'react';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import ProCard from '@ant-design/pro-card';

export interface IUser {
    name: string;
    age: number;
}


const Dashboard = () => {

    return (
        <>
            <AdminLayout title={'Dashboard'}>
                <div>
                    <ProCard title={'Hello World'}>

                    </ProCard>
                </div>
            </AdminLayout>
        </>
    );
};

export default Dashboard;
