import React from 'react';
import {Editor} from '@tinymce/tinymce-react';

// TinyMCE so the global var exists
// eslint-disable-next-line no-unused-vars

// Theme
import 'tinymce/themes/silver';
// Toolbar icons
import 'tinymce/icons/default';
// Editor styles
import 'tinymce/skins/ui/oxide/skin.min.css';

// importing the plugin js.
import 'tinymce/plugins/advlist';
import 'tinymce/plugins/autolink';
import 'tinymce/plugins/link';
import 'tinymce/plugins/image';
import 'tinymce/plugins/lists';
import 'tinymce/plugins/charmap';
import 'tinymce/plugins/hr';
import 'tinymce/plugins/anchor';
import 'tinymce/plugins/searchreplace';
import 'tinymce/plugins/wordcount';
import 'tinymce/plugins/code';
import 'tinymce/plugins/fullscreen';
import 'tinymce/plugins/insertdatetime';
import 'tinymce/plugins/media';
import 'tinymce/plugins/nonbreaking';
import 'tinymce/plugins/table';
import 'tinymce/plugins/template';
import 'tinymce/plugins/help';
import ProCard from '@ant-design/pro-card';


const EditorPage = () => {
    return (
        <>
            <ProCard title={'Editor Playground'}>
                <p>Image uploading feature will be implemented in this sprint.</p>
                <p>Equations can be inserted by using last 2 icons in the toolbar.</p>
                <Editor
                    tinymceScriptSrc="/assets/admin/tinymce/tinymce.min.js"
                    init={{
                        skin: false,
                        content_css: false,
                        height: 500,
                        external_plugins: {
                            'tiny_mce_wiris': `/assets/admin/tinymce/plugins_custom/wiris/plugin.min.js`,
                        },
                        plugins: ['image'],
                        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | image code | tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry',
                        base_url: '/js/tinymce',
                    }}
                />
            </ProCard>
        </>
    );
};

export default EditorPage;
