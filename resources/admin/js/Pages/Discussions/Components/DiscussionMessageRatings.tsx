import React from 'react';
import {ModelDiscussionMessageRating, ModelUser} from '@admin/types';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faThumbsDown as fasThumbsDown, faThumbsUp as fasThumbsUp} from '@fortawesome/free-solid-svg-icons';
import {faThumbsDown, faThumbsUp} from '@fortawesome/free-regular-svg-icons';
import {Space, Tooltip} from 'antd';
import {usePage} from '@inertiajs/inertia-react';
import {ErrorBag, Errors, PageProps} from '@inertiajs/inertia/types/types';
import {Inertia} from '@inertiajs/inertia';

type Props = {
    messageId: number;
    ratings: ModelDiscussionMessageRating[];
}

const DiscussionMessageRatings: React.FunctionComponent<Props> = (props) => {
    const {user} = usePage().props as PageProps & { errors: Errors & ErrorBag, user: ModelUser };

    const counts = props.ratings.reduce((previousValue, currentValue) => {
        if (currentValue.positive) {
            previousValue.up += 1;
        } else {
            previousValue.down += 1;
        }
        return previousValue;
    }, {up: 0, down: 0});

    let currentUsersRating: null | boolean = null;

    for (let i = 0; i < props.ratings.length; i++) {
        if (props.ratings[i].user_id === user.id) {
            currentUsersRating = props.ratings[i].positive;
            break;
        }
    }

    const toggleRating = async (value: boolean) => {
        const newValue = currentUsersRating === value ? null : value;

        Inertia.post(window.route('admin.discussionsMessages.rate', [props.messageId]), {
            positive: newValue
        }, {
            preserveScroll: true
        });
    }

    return (
        <Space>
            <a onClick={() => toggleRating(true)}>
                <Space>
                    <Tooltip title={'I like this'}><FontAwesomeIcon
                        icon={currentUsersRating === true ? fasThumbsUp : faThumbsUp}/>
                    </Tooltip>{counts.up}
                </Space>
            </a>
            <a onClick={() => toggleRating(false)}>
                <Space>
                    <Tooltip title={'I dislike this'}>
                        <FontAwesomeIcon icon={currentUsersRating === false ? fasThumbsDown : faThumbsDown}
                                         flip={'horizontal'}/>
                    </Tooltip>{counts.down}
                </Space>
            </a>
        </Space>
    );

};

export default DiscussionMessageRatings;
