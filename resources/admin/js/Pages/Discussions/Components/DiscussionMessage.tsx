import React, {useState} from 'react';
import {ModelDiscussionMessage} from '@admin/types';
import {Avatar, Button, Comment, Popconfirm, Space, Tooltip} from 'antd';
import RichTextEditor from '@admin/Components/Forms/Editor/RichTextEditor';
import DiscussionMessageRatings from '@admin/Pages/Discussions/Components/DiscussionMessageRatings';
import moment from 'moment';
import {Inertia} from '@inertiajs/inertia';

type Props = {
    message: ModelDiscussionMessage;
    // onSubmit: (data: DiscussionMessageData) => void
}

const DiscussionMessage: React.FunctionComponent<Props> = (props) => {

    const [isReplyingTo, setIsReplyingTo] = useState(false);

    const toggleReplyTo = (state: boolean | null = null) => {
        setIsReplyingTo(state === null ? !isReplyingTo : state);
    }

    const [replyToContent, setReplyToContent] = useState('');

    const date = moment(props.message.created_at * 1000);

    const onRemoveMessage = () => {
        Inertia.delete(window.route('admin.discussionsMessages.delete', [props.message.id]), {
            preserveScroll: true
        });
    }
    /**
     * TODO: enable mode just for commenting
     */
    return (
        <Comment
            author={<a>{props.message.user!.name}</a>}
            datetime={(
                <Tooltip title={date.format('YYYY-MM-DD HH:mm:ss')}>
                    <span>{date.fromNow()}</span>
                </Tooltip>
            )}
            avatar={<Avatar src="https://joeschmoe.io/api/v1/random" alt="Han Solo"/>}
            content={
                <div>
                    <p>
                        We supply a series of design principles, practical patterns and high quality design
                        resources (Sketch and Axure).
                    </p>

                    <Space>
                        <a key="comment-nested-reply-to" onClick={() => toggleReplyTo(null)}>Reply</a>
                        <DiscussionMessageRatings messageId={props.message.id} ratings={props.message.ratings!}/>
                        <Popconfirm
                            key={'Delete'}
                            title="Are you sure to delete this message?"
                            onConfirm={() => onRemoveMessage()}
                            onCancel={() => {
                            }}
                            okText="Yes"
                            cancelText="No"
                        >
                            <a key={'delete'}>Delete </a>
                        </Popconfirm>
                    </Space>
                    {isReplyingTo &&
                        <div>
                            <Space style={{marginTop: 20}} direction={'vertical'}>
                                <RichTextEditor height={350} initialValue={replyToContent}
                                                onChange={a => setReplyToContent(a)}/>
                                <Space>
                                    <Button type={'primary'}>Send</Button>
                                    <Button type={'default'} onClick={() => toggleReplyTo(false)}>Cancel</Button>
                                </Space>
                            </Space>
                        </div>}

                </div>
            }
        >
            {props.message.children_messages_recursive!.length! > 0
                && props.message.children_messages_recursive!.map(message => <DiscussionMessage key={message.id}
                                                                                                message={message}/>)
            }
        </Comment>
    )

};

export default DiscussionMessage;
