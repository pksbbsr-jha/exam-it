import {Head} from '@inertiajs/inertia-react';
import React from 'react';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import {InertiaProps, ModelDiscussion, ModelDiscussionMessage, Paginated,} from '@admin/types';
import {Avatar, Comment, Descriptions, Row, Space} from 'antd';
import ProCard from '@ant-design/pro-card';
import DiscussionMessage from '@admin/Pages/Discussions/Components/DiscussionMessage';

type Props = InertiaProps & {
    discussion: ModelDiscussion;
    messages: Paginated<ModelDiscussionMessage>;
}

const Show: React.FunctionComponent<Props> = (props) => {

    const title = `Discussion: ${props.discussion.subject}`;

    const sectionGeneralInfo = (
        <ProCard
            title="General Info"
            // extra={
            // <Link href={window.route('admin.discussions.edit', props.discussion.id)}>
            //     <Button
            //         type="primary">Edit</Button>
            // </Link>
            // }
        >
            <Descriptions
                bordered
                column={1}

            >
                <Descriptions.Item label="Subject">{props.discussion.subject}</Descriptions.Item>
                <Descriptions.Item label="Author">{props.discussion.user?.name}</Descriptions.Item>
                {props.discussion.group &&
                    <Descriptions.Item label="Group">{props.discussion.group.name}</Descriptions.Item>}
                {props.discussion.submodule &&
                    <Descriptions.Item label="Submodule">{props.discussion.submodule.name}</Descriptions.Item>}
            </Descriptions>
        </ProCard>
    );


    const ExampleComment: React.FC<{ children?: React.ReactNode }> = ({children}) => (
        <Comment
            actions={[<span key="comment-nested-reply-to">Reply</span>]}
            author={<Space>
                <a>Han Solo</a>
                <a>Han Solo</a>
            </Space>}
            avatar={<Avatar src="https://joeschmoe.io/api/v1/random" alt="Han Solo"/>}
            content={
                <p>
                    We supply a series of design principles, practical patterns and high quality design
                    resources (Sketch and Axure).
                </p>
            }
        >
            {children}
        </Comment>
    );


    const sectionMessages = (
        <ProCard title={'Messages'}>
            {props.messages.data.map(message => {
                return (<DiscussionMessage key={message.id} message={message}/>)
            })}

            {/*<pre>{JSON.stringify(props.messages, null, 4)}</pre>*/}
            {/*<ExampleComment>*/}
            {/*    <ExampleComment>*/}
            {/*        <ExampleComment/>*/}
            {/*        <ExampleComment/>*/}
            {/*    </ExampleComment>*/}
            {/*</ExampleComment>*/}
        </ProCard>
    );

    return (
        <>
            <Head>
                <title>{title}</title>
            </Head>
            <AdminLayout
                title={title}
            >
                <Row gutter={[0, 16]}>
                    {sectionGeneralInfo}
                    {sectionMessages}
                </Row>
            </AdminLayout>
        </>
    );
};

export default Show;
