import {Head, Link} from '@inertiajs/inertia-react';
import React, {useState} from 'react';
import {InertiaProps, ModelDiscussion, Paginated} from '@admin/types';
import type {ProColumns} from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';
import ProCard from '@ant-design/pro-card';
import EmptyTable from '@admin/Components/Tables/EmptyTable';
import {EloquentPaginationHandler} from '@admin/Components/Tables/CustomTableHelpers';
import AdminLayout from '@admin/Components/Common/AdminLayout';
import moment from 'moment';
import UserFormModal from '@admin/Pages/Users/components/UserFormModal';

type Props = InertiaProps & {
    discussions: Paginated<ModelDiscussion>;
}

const DiscussionIndex: React.FunctionComponent<Props> = (props) => {

    const [modalForm, setModalForm] = useState(false);

    const columns: ProColumns<ModelDiscussion>[] = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Subject',
            dataIndex: 'subject',
            key: 'subject',
        },
        {
            title: 'User',
            key: 'user',
            render: (text, row: ModelDiscussion) => <span>{row.user!.name}</span>
        },
        {
            title: 'Group',
            key: 'group',
            render: (text, row: ModelDiscussion) => <span>{row.group ? row.group?.name : '-'}</span>
        },
        {
            title: 'Submodule',
            key: 'submodule',
            render: (text, row: ModelDiscussion) => <span>{row.submodule ? row.submodule?.name : '-'}</span>
        },
        {
            title: 'Messages Count',
            dataIndex: 'messages_count',
        },
        {
            title: 'Created At',
            dataIndex: 'created_at',
            key: 'created_at',
            renderText: text => moment(text * 1000).format('MM/DD/YYYY')
        },
        {
            title: 'Actions',
            key: 'actions',
            valueType: 'option',
            align: 'right',
            render: (text, row) => [
                <Link key={'show'} href={window.route('admin.discussions.show', row.id)}>Manage </Link>,
            ],
        },
    ];

    return (
        <>
            <Head>
                <title>Discussion</title>
            </Head>
            <AdminLayout
                title={'Discussion'}
            >
                <ProCard>
                    <ProTable<ModelDiscussion>
                        columns={columns}
                        dataSource={props.discussions.data}
                        locale={{
                            emptyText: <EmptyTable/>
                        }}
                        toolbar={{
                            filter: false,
                        }}
                        type={'table'}
                        pagination={{
                            pageSize: props.discussions.meta.per_page,
                            total: props.discussions.meta.total,
                            current: props.discussions.meta.current_page,
                            onChange: EloquentPaginationHandler()
                        }}
                        rowKey={record => record.id}
                        dateFormatter={value => moment(value).format('MM/DD/YYYY')}
                        options={false}
                        toolBarRender={false}
                        search={false}
                    />
                </ProCard>

                <UserFormModal show={modalForm}
                               onClose={() => setModalForm(false)}
                               onAdded={() => setModalForm(false)}
                />
            </AdminLayout>
        </>
    );
};

export default DiscussionIndex;
