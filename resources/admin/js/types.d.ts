import {ErrorBag, Errors, PageProps} from '@inertiajs/inertia/types/types';
import {User} from '@sentry/react';

export type InertiaProps<SharedProps = PageProps> = PageProps & SharedProps & {
    errors: Errors & ErrorBag;
};

export interface ModelUser {
    name: string;
    email: string;
}

export type OrderDirection = 'up' | 'down';

export interface LaravelBreadcrumb {
    title: string;
    is_current_page: boolean;
    url: string;
}

export interface ModelCourse {
    id: number;
    name: string;
    description: string;
    price: number;
    meta: {
        includes: string[],
        lessons: string[],
        requirements: string[],
    };
    created_at: number;
    updated_at: number;
}

export type ModelCourseSubItemType = 'chapter' | 'module' | 'submodule';


export interface ModelCourseChapter {
    id: number;
    name: string;
    order: number;
    created_at: number;
    updated_at: number;
}

export type ModuleCourseChapterWithModulesAndSubmodules = (ModelCourseChapter & {
    modules: (ModelCourseModule & {
        submodules: ModelCourseSubmoduleMini[]
    })[]
});

export interface ModelCourseModule {
    id: number;
    name: string;
    order: number;
    duration: number;
    created_at: number;
    updated_at: number;
    is_free: boolean;
}

export interface ModelCourseSubmodule {
    id: number;
    name: string;
    order: number;
    content: string;
    questions_count?: number;
    created_at: number;
    updated_at: number;
}

export type ModelCourseModuleMini = Pick<ModelCourseModule, 'id' | 'name' | 'order'>;
export type ModelCourseSubmoduleMini = Pick<ModelCourseSubmodule, 'id' | 'name' | 'order' | 'questions_count'>;

export interface ModelCourseQuestion {
    id: number;
    submodule_id: number;
    name: string
    question: string;
    answers: string[];
    explanation: string;
    duration: number;
    created_at: number;
    updated_at: number;
}

type ModelUserRole = 'super_admin' | 'admin' | 'student';

export interface ModelUser {
    id: number;
    name: string;
    email: number;
    email_verified_at: string;
    created_at: number;
    updated_at: number;
    blocked_at: number | null;
    role: ModelUserRole;
    username: string;
}

export interface ModelPayment {
    id: number;
    amount: number;
    user_id: number;
    user?: ModelUser;
    created_at: number;
    updated_at: number;
    refunded_at: number;
}

export interface ModelGroup {
    id: number;
    name: string;
    created_at: number;
    updated_at: number;
    deleted_at: number;

    owner?: ModelUser;
    users?: User,
    members_count?: number,
}

export interface ModelGroupInvitation {
    id: number;
    group_id: number;
    email: string;
    invitation_sent_at: number;
    created_at: number;
    updated_at: number;
    deleted_at: number;

    group?: ModelGroup;
}

export interface ModelDiscussion {
    id: number;

    subject: string;

    created_at: number;
    updated_at: number;
    deleted_at: number;

    user?: ModelUser,
    messages_count?: number,

    group?: ModelGroup | null;
    submodule?: ModelCourseSubmodule | null;
}

export interface ModelDiscussionMessage {
    id: number;

    user_id: number;

    created_at: number;
    updated_at: number;
    deleted_at: number;

    user?: ModelUser;
    ratings?: ModelDiscussionMessageRating[];

    children_messages_recursive?: ModelDiscussionMessage[];
    children_messages_count?: number;
}

export interface ModelDiscussionMessageRating {
    id: number;

    user_id: number;

    positive: boolean;
    created_at: number;
    deleted_at: number;

    user?: ModelUser;
    discussionMessage?: ModelDiscussionMessage;
}

export type RequiredKeys<T, K extends keyof T> = Exclude<T, K> & Required<Pick<T, K>>;

export enum ToastLevel {
    success = 'success',
    error = 'error',
    info = 'info',
    warning = 'warning',
}

export interface ToastDto {
    level: ToastLevel;
    title?: string;
    message?: string;
}

export interface Paginated<T> {
    data: T[];
    links: {
        first: string;
        last: string;
        prev: string | null;
        next: string | null;
    };
    meta: {
        current_page: number;
        from: number;
        last_page: number;
        path: string;
        per_page: number;
        to: number;
        total: number;
    }
}


