import _ from 'lodash';

export function AntSearchHelper (keys: string[], newParams: Record<string, unknown>) {

    // const currentParams = window.route().params;

    return _.reduce(keys, (current, key) => {
        current[key] = typeof newParams[key] === 'undefined' ? undefined : newParams[key];
        return current;
    }, {} as any)
}
