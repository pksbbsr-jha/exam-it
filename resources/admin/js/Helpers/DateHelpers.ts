const DateHelpers = {
    formats: {
        default: 'YYYY-MM-DD HH:MM',
    }
}

export default DateHelpers;
