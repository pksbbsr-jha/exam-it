import {InertiaApp} from '@inertiajs/inertia-react'
import React from 'react'
import {render} from 'react-dom'

import {InertiaProgress} from '@inertiajs/progress'
import 'antd/dist/antd.css';
import {ConfigProvider} from 'antd';
import {ConfigProvider as ConfigProviderPro, createIntl} from '@ant-design/pro-provider';
import enUS from 'antd/lib/locale/en_US'
import moment from 'moment';
import * as Sentry from '@sentry/react';
import {BrowserTracing} from '@sentry/tracing';

const app = document.getElementById('app')

moment.locale('en_US');


Sentry.init({
    dsn: "https://6db8534671cb4ab78c982f666df98647@o1164505.ingest.sentry.io/6255109",
    integrations: [new BrowserTracing()],

    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    tracesSampleRate: 1.0,
});

const enLocale = {
    tableForm: {
        search: 'Query',
        reset: 'Reset',
        submit: 'Submit',
        collapsed: 'Expand',
        expand: 'Collapse',
        inputPlaceholder: ' ', // default: Please enter
        selectPlaceholder: 'Please select',
    },
    alert: {
        clear: 'Clear',
    },
    tableToolBar: {
        leftPin: 'Pin to left',
        rightPin: 'Pin to right',
        noPin: 'Unpinned',
        leftFixedTitle: 'Fixed the left',
        rightFixedTitle: 'Fixed the right',
        noFixedTitle: 'Not Fixed',
        reset: 'Reset',
        columnDisplay: 'Column Display',
        columnSetting: 'Settings',
        fullScreen: 'Full Screen',
        exitFullScreen: 'Exit Full Screen',
        reload: 'Refresh',
        density: 'Density',
        densityDefault: 'Default',
        densityLarger: 'Larger',
        densityMiddle: 'Middle',
        densitySmall: 'Compact',
    },
};

// const enUSIntl = createIntl('en_US', enLocale);
const enUSIntl = createIntl('en_US', enUS);

// noinspection JSCheckFunctionSignatures,RequiredAttributes
render(
    <ConfigProviderPro value={{
        // intl: enUSIntl
    }}>
        <ConfigProvider locale={enUS} prefixCls={'ant'}>
            <InertiaApp
                initialPage={JSON.parse(app.dataset.page)}
                resolveComponent={name => import(`./Pages/${name}`).then(module => module.default)}
            />
        </ConfigProvider>
    </ConfigProviderPro>,
    app
)

InertiaProgress.init();
