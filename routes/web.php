<?php

use App\Http\Controllers\CourseChapterController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\CourseModuleController;
use App\Http\Controllers\CourseSubmoduleController;
use App\Http\Controllers\CourseSubmoduleQuestionController;
use App\Http\Controllers\DiscussionController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\GroupMemberController;
use App\Http\Controllers\MediaUploadController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\UserController;
use App\Models\Course;
use App\Models\CourseChapter;
use App\Models\CourseModule;
use App\Models\CourseSubmodule;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__ . '/auth.php';


Route::get('/', [\App\Http\Controllers\FrontendController::class, 'index'])->name('app.index');


Route::group([
    //'middleware' => 'isActiveUser'
], function () {
    Route::get('/dashboard', [\App\Http\Controllers\FrontendController::class, 'dashboard'])->name('app.dashboard');
    Route::get('/courses', [\App\Http\Controllers\FrontendController::class, 'courses'])->name('app.courses');
    Route::get('/course/detail/{id}', [\App\Http\Controllers\FrontendController::class, 'courseDetail'])->name('app.course-detail');
    Route::get('/dashboard-discussion', [\App\Http\Controllers\FrontendController::class, 'discussion'])->name('app.dashboard-discussion');
    Route::get('/dashboard-forum', [\App\Http\Controllers\FrontendController::class, 'forum'])->name('app.dashboard-forum');
    Route::get('/dashboard-chat', [\App\Http\Controllers\FrontendController::class, 'chat'])->name('app.dashboard-chat');
    Route::get('/course-list', [\App\Http\Controllers\FrontendController::class, 'courseList'])->name('app.course-list');
    Route::get('/public-profile',[\App\Http\Controllers\FrontendController::class, 'publicProfile'])->name('app.public-profile');
    Route::get('/account-settings',[\App\Http\Controllers\FrontendController::class, 'accountSettings'])->name('app.account-settings');
    Route::get('/discussion-list',[\App\Http\Controllers\FrontendController::class, 'discussionList'])->name('app.discussion-list');
    Route::get('/explanation',[\App\Http\Controllers\FrontendController::class, 'explanation'])->name('app.explanation');
    Route::get('/course-page-detail',[\App\Http\Controllers\FrontendController::class, 'coursePage'])->name('app.course-page-detail');
    Route::get('/create-discussion-group',[\App\Http\Controllers\FrontendController::class, 'discussionGroup'])->name('app.create-discussion-group');
    Route::get('/quiz',[\App\Http\Controllers\FrontendController::class, 'quiz'])->name('app.quiz');
    Route::get('/discussion',[\App\Http\Controllers\FrontendController::class, 'discussionDetails'])->name('app.discussion');
    //Route::get('/course/course-page-purchased/{id}',[\App\Http\Controllers\FrontendController::class, 'coursePurchased'])->name('app.course-page-purchased');
    Route::get('/course-page-purchased',[\App\Http\Controllers\FrontendController::class, 'coursePurchased'])->name('app.course-page-purchased');


//    Route::get('/dashboard', function () {
//        return view('dashboard');
//    })->middleware(['auth', 'verified'])->name('dashboard');
});


Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth', 'isAdmin', 'isActiveUser']
], function () {

    Route::redirect('/', '/admin/dashboard')->name('admin')->breadcrumb('Admin');

    Route::post('media/upload', [MediaUploadController::class, 'upload'])->name('media.upload');

    Route::get('/dashboard', function () {
        return inertia('Dashboard', [
            'data' => 'something'
        ]);
    })->name('admin.dashboard')
        ->breadcrumb('Dashboard', 'admin');

    Route::group([
        'prefix' => 'courses'
    ], function () {
        Route::get('/', [CourseController::class, 'index'])->name('admin.courses')->breadcrumb('Courses', 'admin');
        Route::get('/create', [CourseController::class, 'create'])->name('admin.courses.create')->breadcrumb('Create', 'admin.courses');
        Route::post('/create', [CourseController::class, 'store'])->name('admin.courses.store');
        Route::get('/{course}', [CourseController::class, 'show'])->name('admin.courses.show')->breadcrumb(fn(Course $course) => "Course: {$course->name}", 'admin.courses');
        Route::get('/{course}/edit', [CourseController::class, 'edit'])->name('admin.courses.edit')->breadcrumb('Edit', 'admin.courses.show');
        Route::put('/{course}', [CourseController::class, 'update'])->name('admin.courses.update')->breadcrumb('Courses', 'admin.courses.show');
        // Route::delete('/{course}', [CourseController::class, 'destroy'])->name('admin.courses.delete')->breadcrumb('Courses',   'admin.courses');
    });
    Route::group([ 
        'prefix' => 'message'
    ], function(){
        Route::get('/', [MessageController::class, 'index'])->name('admin.message')->breadcrumb('message', 'admin');
    });
    /**
     * Chapters
     */
    Route::group([
        'prefix' => 'chapters'
    ], function () {
        Route::get('/', [CourseChapterController::class, 'redirectToCourses'])->name('admin.chapters');
        Route::get('/create', [CourseChapterController::class, 'redirectToCourses'])->name('admin.chapters.create');
        Route::post('/create', [CourseChapterController::class, 'store'])->name('admin.chapters.store');
        Route::get('/{chapter}', [CourseChapterController::class, 'redirectToCourse'])->name('admin.chapters.show')->breadcrumb(fn(CourseChapter $chapter) => sprintf("Chapter %s: %s", $chapter->order + 1, $chapter->name), 'admin.courses.show', fn(CourseChapter $chapter) => $chapter->course);
        Route::get('/{chapter}/edit', [CourseChapterController::class, 'redirectToCourse'])->name('admin.chapters.edit');
        Route::put('/{chapter}/reorder', [CourseChapterController::class, 'reorder'])->name('admin.chapters.reorder');
        Route::put('/{chapter}', [CourseChapterController::class, 'update'])->name('admin.chapters.update');
        Route::delete('/{chapter}', [CourseChapterController::class, 'destroy'])->name('admin.chapters.delete');
    });

    /**
     * Modules
     */
    Route::group([
        'prefix' => 'modules'
    ], function () {
        Route::get('/', [CourseModuleController::class, 'redirectToCourses'])->name('admin.modules');
        Route::get('/create', [CourseModuleController::class, 'redirectToCourses'])->name('admin.modules.create');
        Route::post('/create', [CourseModuleController::class, 'store'])->name('admin.modules.store');
        Route::get('/{module}', [CourseModuleController::class, 'redirectToChapter'])->name('admin.modules.show')->breadcrumb(fn(CourseModule $module) => sprintf("Module %d: %s", $module->order + 1, $module->name), 'admin.chapters.show', fn(CourseModule $module) => $module->chapter);
        Route::get('/{module}/edit', [CourseModuleController::class, 'redirectToChapter'])->name('admin.modules.edit');
        Route::put('/{module}/reorder', [CourseModuleController::class, 'reorder'])->name('admin.modules.reorder');
        Route::put('/{module}', [CourseModuleController::class, 'update'])->name('admin.modules.update');
        Route::put('/{module}', [CourseModuleController::class, 'update'])->name('admin.modules.update');
        Route::put('/{module}/toggle-paid-or-free', [CourseModuleController::class, 'togglePaidOrFree'])->name('admin.modules.togglePaidOrFree');
        Route::delete('/{module}', [CourseModuleController::class, 'destroy'])->name('admin.modules.delete');
    });

    /**
     * Submodules
     */
    Route::group([
        'prefix' => 'submodules'
    ], function () {
        Route::get('/', [CourseSubmoduleController::class, 'redirectToCourses'])->name('admin.submodules');
        Route::get('/create', [CourseSubmoduleController::class, 'redirectToCourses'])->name('admin.submodules.create');
        Route::post('/create', [CourseSubmoduleController::class, 'store'])->name('admin.submodules.store');
        Route::get('/{submodule}', [CourseSubmoduleController::class, 'show'])->name('admin.submodules.show')->breadcrumb(fn(CourseSubmodule $submodule) => sprintf("Submodule %d: %s", $submodule->order + 1, $submodule->name), 'admin.modules.show', fn(CourseSubmodule $submodule) => $submodule->module);
        Route::get('/{submodule}/edit', [CourseSubmoduleController::class, 'edit'])->name('admin.submodules.edit')->breadcrumb('Edit', 'admin.submodules.show');
        Route::put('/{submodule}/reorder', [CourseSubmoduleController::class, 'reorder'])->name('admin.submodules.reorder');

        Route::get('/{submodule}/questions/create', [CourseSubmoduleQuestionController::class, 'create'])->name('admin.submodules.questions.create')->breadcrumb('Add Question', 'admin.submodules.show');
        Route::post('/{submodule}/questions/create', [CourseSubmoduleQuestionController::class, 'store'])->name('admin.submodules.questions.store');

        Route::get('/{submodule}/questions/{question}/edit', [CourseSubmoduleQuestionController::class, 'edit'])->name('admin.submodules.questions.edit');
        Route::put('/{submodule}/questions/{question}', [CourseSubmoduleQuestionController::class, 'update'])->name('admin.submodules.questions.update');

        Route::delete('/{submodule}/questions/{question}', [CourseSubmoduleQuestionController::class, 'delete'])->name('admin.submodules.questions.delete');

        Route::put('/{submodule}', [CourseSubmoduleController::class, 'update'])->name('admin.submodules.update');
        Route::put('/{submodule}/update-content', [CourseSubmoduleController::class, 'updateContent'])->name('admin.submodules.update-content');
        Route::delete('/{submodule}', [CourseSubmoduleController::class, 'destroy'])->name('admin.submodules.delete');
    });

    Route::group([
        'prefix' => 'users'
    ], function () {
        Route::get('/', [UserController::class, 'index'])->name('admin.users')->breadcrumb('Users', 'admin');
//        Route::get('/create', [UserController::class, 'create'])->name('admin.users.create')->breadcrumb('Create', 'admin.users');
        Route::post('/create', [UserController::class, 'store'])->name('admin.users.store');
//        Route::get('/{user}', [UserController::class, 'show'])->name('admin.users.show')->breadcrumb(fn(User $user) => $user->name, 'admin.users');

//        Route::get('/{user}/edit', [UserController::class, 'edit'])->name('admin.users.edit')->breadcrumb('Edit', 'admin.users.show');
//        Route::put('/{user}', [UserController::class, 'update'])->name('admin.users.update')->breadcrumb('Users', 'admin.users.show');
        // Route::delete('/{user}', [UserController::class, 'delete'])->name('admin.users.delete')->breadcrumb('Users', 'admin.users');a

        Route::group([
            'middleware' => 'isSuperAdmin'
        ], function () {
            Route::put('/{user}/block-add', [UserController::class, 'blockAdd'])->name('admin.users.block.add');
            Route::put('/{user}/block-remove', [UserController::class, 'blockRemove'])->name('admin.users.block.remove');
            Route::put('/{user}/admin-add', [UserController::class, 'adminAdd'])->name('admin.users.admin.add');
            Route::put('/{user}/admin-remove', [UserController::class, 'adminRemove'])->name('admin.users.admin.remove');
        });
    });

    Route::group([
        'prefix' => 'groups'
    ], function () {
        Route::get('/', [\App\Http\Controllers\GroupController::class, 'index'])->name('admin.groups')->breadcrumb('Payments', 'admin');
//        Route::get('/create', [GroupController::class, 'create'])->name('admin.groups.create')->breadcrumb('Create', 'admin.groups');
//        Route::post('/create', [GroupController::class, 'store'])->name('admin.groups.store');
        Route::get('/{group}', [GroupController::class, 'show'])->name('admin.groups.show')->breadcrumb(fn(\App\Models\Group $group) => $group->name, 'admin.groups');

//        Route::get('/{group}/edit', [GroupController::class, 'edit'])->name('admin.groups.edit')->breadcrumb('Edit', 'admin.groups.show');
//        Route::put('/{group}', [GroupController::class, 'update'])->name('admin.groups.update')->breadcrumb('Users', 'admin.groups.show');
        Route::delete('/{group}', [GroupController::class, 'destroy'])->name('admin.groups.delete');


        Route::post('/{group}/members', [GroupMemberController::class, 'add'])->name('admin.groups.members.add');
        Route::delete('/{group}/members/{user}', [GroupMemberController::class, 'remove'])->name('admin.groups.members.remove');

        Route::post('/{group}/invitations/{groupInvitation}/resend', [\App\Http\Controllers\GroupInvitationController::class, 'resendInvite'])->name('admin.groups.invitations.resendInvite');
        Route::delete('/{group}/invitations/{groupInvitation}', [\App\Http\Controllers\GroupInvitationController::class, 'remove'])->name('admin.groups.invitations.remove');

    });

    Route::group([
        'prefix' => 'discussions'
    ], function () {
        Route::get('/', [DiscussionController::class, 'index'])->name('admin.discussions')->breadcrumb('Payments', 'admin');
//        Route::get('/create', [DiscussionController::class, 'create'])->name('admin.discussions.create')->breadcrumb('Create', 'admin.discussions');
//        Route::post('/create', [DiscussionController::class, 'store'])->name('admin.discussions.store');
        Route::get('/{discussion}', [DiscussionController::class, 'show'])->name('admin.discussions.show')->breadcrumb(fn(\App\Models\Discussion $discussion) => $discussion->subject, 'admin.discussions');

//        Route::get('/{discussion}/edit', [DiscussionController::class, 'edit'])->name('admin.discussions.edit')->breadcrumb('Edit', 'admin.discussions.show');
//        Route::put('/{discussion}', [DiscussionController::class, 'update'])->name('admin.discussions.update')->breadcrumb('Users', 'admin.discussions.show');
        // Route::delete('/{discussion}', [DiscussionController::class, 'delete'])->name('admin.discussions.delete')->breadcrumb('Users', 'admin.discussions');
    });

    Route::group([
        'prefix' => 'discussions-messages'
    ], function () {
//        Route::get('/', [DiscussionController::class, 'index'])->name('admin.discussions')->breadcrumb('Payments', 'admin');
//        Route::get('/create', [DiscussionController::class, 'create'])->name('admin.discussions.create')->breadcrumb('Create', 'admin.discussions');
//        Route::post('/create', [DiscussionController::class, 'store'])->name('admin.discussions.store');
//        Route::get('/{discussion}', [DiscussionController::class, 'show'])->name('admin.discussions.show')->breadcrumb(fn(\App\Models\Discussion $discussion) => $discussion->subject, 'admin.discussions');

        Route::post('/{discussionMessage}/rate', [\App\Http\Controllers\DiscussionMessageController::class, 'rate'])->name('admin.discussionsMessages.rate');
//        Route::put('/{discussion}', [DiscussionController::class, 'update'])->name('admin.discussions.update')->breadcrumb('Users', 'admin.discussions.show');
        Route::delete('/{discussionMessage}', [\App\Http\Controllers\DiscussionMessageController::class, 'delete'])->name('admin.discussionsMessages.delete');
    });

    Route::group([
        'prefix' => 'payments'
    ], function () {
        Route::get('/', [PaymentController::class, 'index'])->name('admin.payments')->breadcrumb('Payments', 'admin');
//        Route::get('/create', [PaymentController::class, 'create'])->name('admin.payments.create')->breadcrumb('Create', 'admin.payments');
//        Route::post('/create', [PaymentController::class, 'store'])->name('admin.payments.store');
//        Route::get('/{payment}', [PaymentController::class, 'show'])->name('admin.payments.show')->breadcrumb(fn(User $payment) => $payment->name, 'admin.payments');

//        Route::get('/{payment}/edit', [PaymentController::class, 'edit'])->name('admin.payments.edit')->breadcrumb('Edit', 'admin.payments.show');
//        Route::put('/{payment}', [PaymentController::class, 'update'])->name('admin.payments.update')->breadcrumb('Users', 'admin.payments.show');
        // Route::delete('/{payment}', [PaymentController::class, 'delete'])->name('admin.payments.delete')->breadcrumb('Users', 'admin.payments');

        Route::group([
            'middleware' => 'isAdmin'
        ], function () {
            Route::put('/{payment}/refund', [PaymentController::class, 'refund'])->name('admin.payments.refund');
        });
    });


    // @TODO: Remove
    Route::get('editor', function () {
        return inertia('Editor', [
            'data' => 'something'
        ]);
    })->name('admin.editor');

});


Route::get('editor', function () {
    return inertia('Editor', [
        'data' => 'something'
    ]);
})->name('editor');
