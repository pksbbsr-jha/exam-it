<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses_chapters', function (Blueprint $table) {
            $table->id();

            $table->foreignId('course_id')->constrained('courses', 'id');
            $table->unsignedInteger('order');

            $table->string('name');

            $table->timestamps();
            $table->softDeletes();

//            $table->unique(['course_id', 'order']); // keeping it simple for now
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses_chapters');
    }
};
