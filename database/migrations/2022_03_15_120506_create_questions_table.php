<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses_questions', function (Blueprint $table) {
            $table->id();

            $table->foreignId('submodule_id')->constrained('courses_submodules', 'id');

            $table->string('name');

            $table->text('question');
            $table->jsonb('answers');

            $table->text('explanation');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses_questions');
    }
};
