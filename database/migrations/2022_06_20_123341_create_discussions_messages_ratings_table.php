<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussions_messages_ratings', function (Blueprint $table) {
            $table->id();

            $table->foreignId('discussions_message_id')->constrained('discussions_messages', 'id');
            $table->foreignId('user_id')->constrained('users', 'id');

            $table->boolean('positive');

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->unique(['discussions_message_id', 'user_id', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discussions_messages_ratings');
    }
};
