<?php

namespace Database\Seeders;

use App\Models\CourseSubmodule;
use App\Models\Discussion;
use App\Models\DiscussionMessage;
use App\Models\DiscussionMessageRating;
use App\Models\Group;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class DiscussionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Group::count()) {
            $this->call(GroupSeeder::class);
        }

        if (!CourseSubmodule::count()) {
            $this->call(CourseSeeder::class);
        }

        $users = User::limit(10)->get();
        $groups = Group::limit(3)->get();
        $submodules = CourseSubmodule::limit(3)->get();

        $forumUsers = $users->random(3);

        // Forum
        $this->insertDiscussions(
            discussionAuthors: $forumUsers,
            users: $users
        );

        // Submodules
        $this->insertDiscussions(
            discussionAuthors: $forumUsers,
            users: $users,
            submodules: $submodules
        );

        // Groups
        $this->insertDiscussions(
            discussionAuthors: $forumUsers,
            users: $users,
            groups: $groups
        );

        // Submodules and Groups
        $this->insertDiscussions(
            discussionAuthors: $forumUsers,
            users: $users,
            submodules: $submodules,
            groups: $groups
        );

    }


    public function insertDiscussions(
        Collection      $discussionAuthors,
        Collection      $users,
        Collection|null $submodules = null,
        Collection|null $groups = null,
    )
    {
        $submodules = is_a($submodules, Collection::class) ? $submodules : collect([null]);
        $groups = is_a($groups, Collection::class) ? $groups : collect([null]);

        foreach ($submodules as $submodule) {
            foreach ($groups as $group) {
                foreach ($discussionAuthors as $discussionAuthor) {
                    $discussion = Discussion::factory()
                        ->for($discussionAuthor, 'user');

                    if ($group) {
                        $discussion = $discussion->for($group, 'group');
                    }

                    if ($submodule) {
                        $discussion = $discussion->for($submodule, 'submodule');
                    }

                    $discussion = $discussion->create();


                    $messagesAuthors = $users->random($this->randomNumber(2, 5))
                        ->prepend($discussionAuthor);

                    foreach ($messagesAuthors as $messagesAuthor) {
                        $message = DiscussionMessage::factory()
                            ->for($discussion, 'discussion')
                            ->for($messagesAuthor, 'user')
                            ->create();

                        $this->addRatings(
                            message: $message,
                            users: $users,
                            positive: $this->randomNumber(1, 5),
                            negative: $this->randomNumber(0, 4)
                        );

                        // Child
                        $message = DiscussionMessage::factory()
                            ->for($discussion, 'discussion')
                            ->for($users->random(), 'user')
                            ->for($message, 'parentMessage')
                            ->create();

                        $this->addRatings(
                            message: $message,
                            users: $users,
                            positive: $this->randomNumber(1, 5),
                            negative: $this->randomNumber(0, 4)
                        );
                    }
                }
            }
        }

    }

    private function addRatings(DiscussionMessage $message, Collection $users, int $positive, int $negative)
    {
        $isPositiveArray = [true, false];
        $users = clone $users;

        foreach($isPositiveArray as $isPositive) {
            $count = $isPositive ? $positive : $negative;

            for ($i = 0; $i < $count; $i++) {
                $user = $users->shift();

                DiscussionMessageRating::factory()
                    ->for($message, 'discussionMessage')
                    ->for($user, 'user')
                    ->create([
                        'positive' => $isPositive
                    ]);
            }

        }
    }

    private function randomNumber(int $min, int $max): int
    {
        try {
            return random_int($min, $max);
        } catch (\Exception $e) {
            return $min;
        }
    }
}
