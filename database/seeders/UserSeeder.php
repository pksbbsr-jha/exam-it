<?php

namespace Database\Seeders;

use App\Models\Enums\UserRole;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory()->create([
            'name' => 'Michael Scheller',
            'email' => 'michael_schneller@examit.org',
            'email_verified_at' => now(),
            // TODO: Make sure to change it on launch
            'password' => bcrypt('MichaelExamit2022##'),
            'role' => UserRole::SUPER_ADMIN,
        ]);

        \App\Models\User::factory()->create([
            'name' => 'Jan Demsar',
            'email' => 'jan@designindc.com',
            'email_verified_at' => now(),
            // TODO: Make sure to change it on launch
            'password' => bcrypt('JanExamit2022##'),
            'role' => UserRole::SUPER_ADMIN,
        ]);
    }
}
