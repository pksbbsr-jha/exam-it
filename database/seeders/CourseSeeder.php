<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\CourseChapter;
use App\Models\CourseModule;
use App\Models\CourseSubmodule;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = '<p>Hello World!</p>
<p>Equation Example 1:</p>
<p><math xmlns="http://www.w3.org/1998/Math/MathML"><mi>x</mi><mo>=</mo><mfrac><mrow><mo>-</mo><mi>b</mi><mo>&#177;</mo><msqrt><msup><mi>b</mi><mn>2</mn></msup><mo>-</mo><mn>4</mn><mi>a</mi><mi>c</mi></msqrt></mrow><mrow><mn>2</mn><mi>a</mi></mrow></mfrac></math></p>
<p>Equation Example 2:</p>
<p><math xmlns="http://www.w3.org/1998/Math/MathML"><msqrt><mn>2</mn></msqrt></math></p>
';

        /** @var Course $course */
        $course = Course::factory()
            ->has(
                CourseChapter::factory()
                    ->count(5)
                    ->has(CourseModule::factory()
                        ->count(4)
                        ->has(
                            CourseSubmodule::factory()->count(3)->state(['content' => $content]), 'submodules'
                        ), 'modules'),
                'chapters')
            ->create([
                'description' => $content
            ]);
    }
}
