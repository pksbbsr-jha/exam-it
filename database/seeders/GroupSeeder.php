<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $owners = User::limit(2)->get();

        /** @var Collection<User> $users */
        $users = User::whereNotIn('id', $owners->pluck('id')->toArray())->get();

        /** @var Collection<Group> $groups */
        $groups = collect([]);

        foreach ($owners as $owner) {
            $groups->push(...Group::factory()
                ->count(4)
                ->for($owner, 'owner')
                ->create());
        }

        foreach ($groups as $group) {
            $owner = $group->owner;

            $groupUsers = $users->random(3);
            $groupUsers->push($owner);

            $group->members()->attach($groupUsers);
        }
    }
}
