<?php

namespace Database\Seeders;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Database\Seeder;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::limit(10)->get();

        Payment::factory()->count(10)->create([
            'amount' => 9900,
            'user_id' => $users->random(1)->first()->id
        ]);
    }
}
