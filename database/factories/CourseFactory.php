<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class CourseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->words(3, true),
            'description' => $this->faker->paragraphs(5, true),
            'price' => $this->faker->numberBetween(20, 200),
            'meta' => [
                'includes' => $this->faker->words(5, false),
                'lessons' => $this->faker->words(5, false),
                'requirements' => $this->faker->words(5, false),
            ]
        ];
    }
}
