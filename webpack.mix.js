const mix = require('laravel-mix');
const path = require("path");
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

// TODO: Remove when we get frontend
// mix.js('resources/js/app.js', 'public/js')
//     .postCss('resources/css/app.css', 'public/css',
//         [
//             require('postcss-import'),
//             require('tailwindcss'),
//             require('autoprefixer'),
//         ]
//     );

// mix.webpackConfig({
//     resolve: {
//         alias: {
//             ziggy: path.resolve('vendor/tightenco/ziggy/dist'),
//         },
//     },
// });

mix.webpackConfig({
    module: {
        rules: [
            {
                test: /\.less$/,
                loader: 'less-loader',
                options: {
                    lessOptions: {
                        javascriptEnabled: true
                    }
                }
            },
        ]
    }
})

mix.alias({
    '@admin': path.join(__dirname, 'resources/admin/js'),
});

mix.sourceMaps(false, 'cheap-module-source-map');

mix.ts('resources/admin/js/app.js', 'public/assets/admin/js').react();
mix.ts('resources/app/js/app.js', 'public/assets/app/js').react();

mix.copy('resources/app/base', 'public/assets/app/base');

mix.less('resources/admin/css/app.less', 'public/assets/admin/css');
// .postCss('resources/admin/css/app.less', 'public/assets/admin/css')

