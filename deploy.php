<?php

namespace Deployer;

require 'recipe/laravel.php';
require 'contrib/php-fpm.php';
require 'contrib/npm.php';
require 'contrib/cachetool.php';

set('application', 'EXAMiT');
set('repository', 'git@gitlab.com:design-in-dc/examit-lms.git');
set('php_fpm_version', '8.0');

host('staging')
    ->setHostname('5.161.86.213')
    ->setDeployPath('/home/forge/examit-staging.demo.devindc.com')
    ->setRemoteUser('forge')
    ->setPort(22)
    ->setConfigFile('~/.ssh/config')
    ->setIdentityFile('~/.ssh/id_dev')
    ->setForwardAgent(true)
    ->setSshMultiplexing(true)
    ->setSshArguments([
        '-o UserKnownHostsFile=/dev/null',
        '-o StrictHostKeyChecking=no',
    ]);
host('production')
    ->setHostname('5.161.86.213')
    ->setDeployPath('/home/forge/examit.demo.devindc.com')
    ->setRemoteUser('forge')
    ->setPort(22)
    ->setConfigFile('~/.ssh/config')
    ->setIdentityFile('~/.ssh/id_dev')
    ->setForwardAgent(true)
    ->setSshMultiplexing(true)
    ->setSshArguments([
        '-o UserKnownHostsFile=/dev/null',
        '-o StrictHostKeyChecking=no',
    ]);


// Tasks

task('build', function () {
    cd('{{release_path}}');
    run('npm run build');
});

task('custom:upload-assets', function () {

    upload(__DIR__ . '/public/assets/', '{{release_path}}/public/assets', [
        // -azP = default
        // -I = Ignore Times (to overwrite)
        'flags' => '-azPI'
    ]);
    upload(__DIR__ . '/public/mix-manifest.json', '{{release_path}}/public/mix-manifest.json', [
        // -azP = default
        // -I = Ignore Times (to overwrite)
        'flags' => '-azPI'
    ]);
});


task('deploy', [
    'deploy:info',
    // Preparing host for deploy
    'deploy:setup',
    // Lock deploy
    'deploy:lock',
    // Prepare release. Clean up unfinished releases and prepare next release
    'deploy:release',
    //  Update code
    'deploy:update_code',
    // Creating symlinks for shared files and dirs
    'deploy:shared',
    // Make writable dirs
    'deploy:writable',
    // Installing vendors
    'deploy:vendors',
    'custom:upload-assets',
    'artisan:migrate',
    'artisan:storage:link',
    'artisan:event:cache',
    'artisan:route:cache',
    'artisan:view:cache',
    'artisan:config:cache',

    // deploy:publish contains deploy:symlink, deploy:unlock, deploy:cleanup, deploy:success
    // Creating symlink to release
    'deploy:symlink',
    // Terminate the master supervisor so it can be restarted
    // TODO: Enable
//    'artisan:horizon:terminate',
    // Unlock deploy
    'deploy:unlock',
    // Cleaning up old releases
    'deploy:cleanup',
    'deploy:success',

]);

// For Reference
//task('deploy:prepare', [
//    'deploy:info',
//    'deploy:setup',
//    'deploy:lock',
//    'deploy:release',
//    'deploy:update_code',
//    'deploy:shared',
//    'deploy:writable',
//]);


after('deploy:failed', 'deploy:unlock');
after('deploy:symlink', 'cachetool:clear:opcache');
